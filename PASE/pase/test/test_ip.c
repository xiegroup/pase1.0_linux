#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

#include "pase.h"
#include "app_slepc.h"
#include "petscmat.h"

static char help[] = "Test with Laplace Mat.\n";

void AugmentedMatrix(Mat A, Mat B, Mat *auA, Mat *auB);
BOUNDARYTYPE BoundCond(INT bdid);
BOUNDARYTYPE MassBoundCond(INT bdid);
void BoundFun(double X[3], int dim, double *values);
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum, MPI_Comm comm);
void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum);

int main(int argc, char *argv[])
{
    PetscErrorCode ierr;
    SlepcInitialize(&argc, &argv, (char *)0, help);

    int nev = 250, num_levels = 3;
    Mat *A_array, *B_array, *P_array;
    MatrixRead2(&A_array, &B_array, &P_array, 3, 1, 3); // ref1,ref2,num_level

    Mat AA, BB;
    AugmentedMatrix(A_array[1], B_array[1], &AA, &BB);

    EPS eps;
    EPSCreate(MPI_COMM_WORLD, &eps);
    EPSSetOperators(eps, AA, BB);
    EPSSetProblemType(eps, EPS_GHEP);                         // generalized Hermitian problem
    EPSSetDimensions(eps, nev, PETSC_DEFAULT, PETSC_DEFAULT); // 只指定了求解个数, ncv(subspace最大维数)和mpd(projected problem最大维数)默认
    EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE);         // 距离target最小in magnitude
    EPSSetTarget(eps, 1000);                                     // target = 0
    EPSSetTolerances(eps, 1e-8, PETSC_DEFAULT);               // maxitr默认

    // 下面这些似乎都没啥用
    ST st;
    EPSGetST(eps, &st);       // spectral transformation
    STSetType(st, STSINVERT); // shift and invert

    KSP ksp;
    PC pc;
    STGetKSP(st, &ksp);
    KSPSetType(ksp, KSPPREONLY);
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCCHOLESKY); // cholesky

    EPSKrylovSchurSetDetectZeros(eps, PETSC_TRUE);
    PCFactorSetMatSolverType(pc, MATSOLVERMUMPS);
    PetscOptionsInsertString(NULL, "-st_mat_mumps_icntl_13 1 -st_mat_mumps_icntl_24 1 -st_mat_mumps_cntl_3 1e-12");

    EPSSetFromOptions(eps);

    double start = MPI_Wtime();
    EPSSetUp(eps);
    double mid = MPI_Wtime();
    EPSSolve(eps);
    double end = MPI_Wtime();
    printf("setup time : %g, solve time : %g\n", mid - start, end - start);
    int i, rank;
    double eigr, eigi;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    for (i = 0; i < nev; i++)
    {
        EPSGetEigenvalue(eps, i, &eigr, &eigi);
        if (rank == 0)
            printf("[ %d ] %2.14f + %2.14f i\n", i, eigr, eigi);
    }
    EPSDestroy(&eps);

    ierr = SlepcFinalize();
    return 0;
}

void AugmentedMatrix(Mat A, Mat B, Mat *auA, Mat *auB)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    Mat AA, BB;
    MatCreate(PETSC_COMM_WORLD, &AA);
    MatCreate(PETSC_COMM_WORLD, &BB);
    int local_n, global_n;
    MatGetSize(A, &global_n, NULL);
    MatGetLocalSize(A, &local_n, NULL);
    int nev = 200;
    global_n += nev;
    if (rank == size - 1)
        local_n += nev;
    MatSetSizes(AA, local_n, local_n, global_n, global_n);
    MatSetSizes(BB, local_n, local_n, global_n, global_n);
    MatSetFromOptions(AA);
    MatSetFromOptions(BB);

    int *A_dnnz = (int *)calloc(local_n, sizeof(int));
    int *A_onnz = (int *)calloc(local_n, sizeof(int));
    int *B_dnnz = (int *)calloc(local_n, sizeof(int));
    int *B_onnz = (int *)calloc(local_n, sizeof(int));
    int i, j, A_ncols, B_ncols;
    const int *A_cols, *B_cols;
    const double *A_vals, *B_vals;
    int row_s, row_e;
    MatGetOwnershipRange(A, &row_s, &row_e);
    if (rank != size - 1)
        for (i = 0; i < local_n; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            for (j = 0; j < A_ncols; j++)
            {
                if (A_cols[j] >= row_s && A_cols[j] < row_e)
                    A_dnnz[i]++;
                else
                    A_onnz[i]++;
            }
            for (j = 0; j < B_ncols; j++)
            {
                if (B_cols[j] >= row_s && B_cols[j] < row_e)
                    B_dnnz[i]++;
                else
                    B_onnz[i]++;
            }
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            A_onnz[i] += nev;
            B_onnz[i] += nev;
        }
    else
    {
        for (i = 0; i < local_n - nev; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            for (j = 0; j < A_ncols; j++)
            {
                if (A_cols[j] >= row_s && A_cols[j] < row_e)
                    A_dnnz[i]++;
                else
                    A_onnz[i]++;
            }
            for (j = 0; j < B_ncols; j++)
            {
                if (B_cols[j] >= row_s && B_cols[j] < row_e)
                    B_dnnz[i]++;
                else
                    B_onnz[i]++;
            }
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            A_dnnz[i] += nev;
            B_dnnz[i] += nev;
        }
        for (i = local_n - nev; i < local_n; i++)
        {
            A_dnnz[i] += nev;
            B_dnnz[i] += nev;
        }
    }
    MatMPIAIJSetPreallocation(AA, 0, A_dnnz, 0, A_onnz);
    MatSeqAIJSetPreallocation(AA, 0, A_dnnz);
    MatMPIAIJSetPreallocation(BB, 0, B_dnnz, 0, B_onnz);
    MatSeqAIJSetPreallocation(BB, 0, B_dnnz);
    free(A_dnnz);
    free(A_onnz);
    free(B_dnnz);
    free(B_onnz);

    if (rank != size - 1)
        for (i = 0; i < local_n; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < A_ncols; j++)
                MatSetValue(AA, i + row_s, A_cols[j], A_vals[j], INSERT_VALUES);
            for (j = 0; j < B_ncols; j++)
                MatSetValue(BB, i + row_s, B_cols[j], B_vals[j], INSERT_VALUES);
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                a = 0.0, b = 0.0;
                MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
            }
        }
    else
    {
        for (i = 0; i < local_n - nev; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < A_ncols; j++)
                MatSetValue(AA, i + row_s, A_cols[j], A_vals[j], INSERT_VALUES);
            for (j = 0; j < B_ncols; j++)
                MatSetValue(BB, i + row_s, B_cols[j], B_vals[j], INSERT_VALUES);
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                a = 0.0, b = 0.0;
                MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
            }
        }
        for (i = local_n - nev; i < local_n; i++)
        {
            MatSetValue(BB, i + row_s, i + row_s, 1.0, INSERT_VALUES);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                if (i + row_s <= global_n - nev + j)
                {
                    if (i + row_s == global_n - nev + j)
                        a = 1.0, b = 1.0;
                    else
                        a = 0.0, b = 0.0;
                    MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                    MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
                }
            }
        }
    }

    MatAssemblyBegin(AA, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(AA, MAT_FINAL_ASSEMBLY);
    MatSetOption(AA, MAT_SYMMETRIC, PETSC_TRUE);
    MatAssemblyBegin(BB, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BB, MAT_FINAL_ASSEMBLY);
    MatSetOption(BB, MAT_SYMMETRIC, PETSC_TRUE);

    MatConvert(AA, MATSBAIJ, MAT_INITIAL_MATRIX, auA);
    MatConvert(BB, MATSBAIJ, MAT_INITIAL_MATRIX, auB);
    MatDestroy(&AA);
    MatDestroy(&BB);
}

void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum)
{
    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, MPI_COMM_WORLD);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum, MPI_Comm comm)
{
    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, comm);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P3_3D, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, C_T_P3_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P3_3D, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, C_T_P3_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}
