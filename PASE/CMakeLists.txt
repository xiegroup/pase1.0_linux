cmake_minimum_required(VERSION 2.8)
project (PASE)
set(PASE_VERSION_MAJOR 2)
set(PASE_VERSION_MINOR 1)
if(NOT CMAKE_BUILD_TYPE)
  message("Setting build type to 'RelWithDebInfo' as none was specified.")
  set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build." FORCE)
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/config/)

find_package(MPI REQUIRED)
SET(CMAKE_C_COMPILER "mpicc")
SET(CMAKE_CXX_COMPILER "mpicpc")

aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/app GCGE_APP)
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/src GCGE_SRC)
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/pase/src PASE_SRC)

include_directories($ENV{PETSC_DIR}/include)
include_directories($ENV{PETSC_DIR}/include/petsc/private)
include_directories($ENV{PETSC_SOURCE_DIR}/src/mat/impls/aij/mpi)
link_directories($ENV{PETSC_DIR}/lib)

include_directories($ENV{SLEPC_DIR}/include)
include_directories($ENV{SLEPC_DIR}/include/slepc/private)
link_directories($ENV{SLEPC_DIR}/lib)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/app)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/pase/src)

add_library(pase SHARED
    ${GCGE_APP}
    ${GCGE_SRC}
    ${PASE_SRC}
)

target_link_libraries(pase
    PUBLIC
        petsc
        slepc
        mpi
        m
        fblas
        flapack
        gfortran
)

set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/shared/lib)
set(INCLUDE_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/shared/include)
file(GLOB_RECURSE HEADER_ALL
    ${CMAKE_CURRENT_SOURCE_DIR}/app/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/pase/src/*.h
)
file(COPY ${HEADER_ALL} DESTINATION ${INCLUDE_OUTPUT_PATH})

include_directories($ENV{OPENPFEM_DIR}/include)
link_directories($ENV{OPENPFEM_DIR}/lib)

add_executable(test1 ${CMAKE_CURRENT_SOURCE_DIR}/pase/test/test_gmg1.c)
add_executable(test2 ${CMAKE_CURRENT_SOURCE_DIR}/pase/test/test_gmg2.c)
add_executable(test3 ${CMAKE_CURRENT_SOURCE_DIR}/pase/test/test_gmg3.c)
add_executable(test4 ${CMAKE_CURRENT_SOURCE_DIR}/pase/test/test_gmg4.c)
target_link_libraries(test1 
    PRIVATE 
        pase
        openpfem
)
target_link_libraries(test2 
    PRIVATE 
        pase
        openpfem
)
target_link_libraries(test3 
    PRIVATE 
        pase
        openpfem
)
target_link_libraries(test4 
    PRIVATE 
        pase
        openpfem
)