#include "mesh.h"
#include "meshcomm.h"
#include "constants.h"
#include "femspace.h"

void MeshRepartition(MESH *mesh, BRIDGE **bridge);

void SharedGeoReRank(MESH *mesh, PTW *Geo2Ranks, INT dim);
void RebuildSharedGeo(MESH *mesh, BRIDGE *bridge, INT *GIndex, INT *Index, INT dim);
void ParMetisPartition(MESH *mesh, PTW *Vert4Elems, INT NumElems, INT *ElemRanks);
void MeshReplace(MESH *oldmesh, MESH *newmesh);

void VertGlobalIndexGenerate(MESH *mesh);
void FaceGlobalIndexGenerate(MESH *mesh);
void VoluGlobalIndexGenerate(MESH *mesh);

INT Vert_PackageSize(MESH *mesh, PTW *Vert4Rank, PTW *Vert2Rank, INT **vertcount, INT **vertdispls);
void Vert_Package(MESH *mesh, BRIDGE *bridge, PTW *Vert4Rank, PTW *Vert2Rank, char **VertPackage, INT totalsize);
void VertBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Vert4Rank, char *VertBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize);
//建立网格的节点的信息, 消除重复的节点
void RebuildVerts(MESH *mesh);
//更新点的共享信息（全局编号变为局部，确认SharedIndex和Owner）

INT Line_PackageSize(MESH *mesh, PTW *Line4Rank, PTW *Line2Rank, INT **linecount, INT **linedispls);
void Line_Package(MESH *mesh, BRIDGE *bridge, PTW *Line4Rank, PTW *Line2Rank, char **LinePackage, INT totalsize);
void LineBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Line4Rank, char *LineBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex);
//建立线的信息，消除重复的线，同时建立线上的点的局部编号
void RebuildLines(MESH *mesh, INT *GVertIndex, INT *VertIndex);

INT Face_PackageSize(MESH *mesh, PTW *Face4Rank, PTW *Face2Rank, INT **facecount, INT **facedispls);
void Face_Package(MESH *mesh, BRIDGE *bridge, PTW *Face4Rank, PTW *Face2Rank, char **FacePackage, INT totalsize);
void FaceBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Face4Rank, char *FaceBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex);
//建立面的信息, 处理重复面的对象，同时建立点、线的局部信息
void RebuildFaces(MESH *mesh, INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex);

INT Volu_PackageSize(MESH *mesh, PTW *Volu4Rank, PTW *Volu2Rank, INT **volucount, INT **voludispls);
void Volu_Package(MESH *mesh, PTW *Volu4Rank, PTW *Volu2Rank, char **VoluPackage, INT totalsize);
void VoluBox2Mesh(MESH *newmesh, MESH *oldmesh, PTW *Volu4Rank, char *VoluBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex, INT *GFaceIndex, INT *FaceIndex);
//重建四面体的信息, 由于没有重复的四面体, 这里的主要任务是建立相应的点、线、面的局部信息
void RebuildVolus(MESH *mesh, INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex, INT *GFaceIndex, INT *FaceIndex);