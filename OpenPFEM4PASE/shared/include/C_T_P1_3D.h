#ifndef __CTP13D__
#define __CTP13D__
#include <stdbool.h>
#include "mesh.h"
#include "enumerations.h"
#include "constants.h"
// ***********************************************************************
// P1 element, conforming, 3D
// ***********************************************************************

/*lhc于2022.1.10号更改至新的编号方式*/

static INT C_T_P1_3D_dof[4] = {1,0,0,0};
//static DOUBLE C_T_P1_3D_nodal_points[12] = {0.0,0.0,0.0, 1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0}; 
static INT C_T_P1_3D_Num_Bas = 4;
static INT C_T_P1_3D_Value_Dim =1;
static INT C_T_P1_3D_Inter_Dim =1;
static INT C_T_P1_3D_Polydeg =1;
static bool C_T_P1_3D_IsOnlyDependOnRefCoord = 1;
static INT C_T_P1_3D_Accuracy = 1;
static MAPTYPE C_T_P1_3D_Maptype = Affine;

static void C_T_P1_3D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{
  values[0] = 1.0-RefCoord[0]-RefCoord[1]-RefCoord[2];
  values[1] = RefCoord[0];
  values[2] = RefCoord[1];    
  values[3] = RefCoord[2];
}


// base function values
static void C_T_P1_3D_D000(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0] = 1.0-RefCoord[0]-RefCoord[1]-RefCoord[2];
  values[1] = RefCoord[0];
  values[2] = RefCoord[1];    
  values[3] = RefCoord[2];
  //printf("v[0]=%f,  v[1]=%f,  v[2]=%f, v[3]=%f, sum=%f\n", values[0], values[1], values[2],values[3],values[0]+values[1]+values[2]+values[3]);
}

// values of the derivatives in RefCoord[0] direction
static void C_T_P1_3D_D100(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=-1;
  values[1]= 1;
  values[2]= 0;  
  values[3]= 0;
}

// values of the derivatives in RefCoord[1] direction
static void C_T_P1_3D_D010(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  //printf("RefCoord: [%f, %f, %f]\n", RefCoord[0], RefCoord[1], RefCoord[2]);
  values[0]=-1.0;
  values[1]= 0.0;
  values[2]= 1.0;
  values[3]= 0.0;  
  //printf("values: [%f, %f, %f, %f]\n", values[0], values[1], values[2],values[3]);
}
// values of the derivatives in RefCoord[2] direction
static void C_T_P1_3D_D001(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=-1.0;
  values[1]= 0.0;
  values[2]= 0.0;
  values[3]= 1.0;  
}

// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D200(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D020(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D002(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D110(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D101(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void C_T_P1_3D_D011(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
  values[3]=0;
}
static void C_T_P1_3D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  //INT dim = 1;  
  DOUBLE coord[3];
  INT i;
  for(i=0;i<4;i++)
  {
      coord[0] = elem->Vert_X[i]; 
      coord[1] = elem->Vert_Y[i];
      coord[2] = elem->Vert_Z[i];
      fun(coord, dim, values+i*dim);
  }
}
#endif
