/*
 * =====================================================================================
 *
 *       Filename:  D_T_RT0_2D.h
 *
 *    Description:  define the base functions in 2D case
 *
 *        Version:  1.0
 *        Created:  2012/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   (), @lsec.cc.ac.cn
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __DRTT02D__
#define __DRTT02D__

#include <stdbool.h>
#include "mesh.h"
#include "enumerations.h"
#include "constants.h"
// ***********************************************************************
// P1 element, conforming, 2D
// ***********************************************************************

static INT D_T_RT0_2D_dof[3] = {0,1,0};
static DOUBLE D_T_RT0_2D_nodal_points[6] = {0.0, 0.0,1.0,0.0,0.0,1.0}; 
static INT D_T_RT0_2D_Num_Bas = 3;
static INT D_T_RT0_2D_Value_Dim =2;
static INT D_T_RT0_2D_Inter_Dim =1;
static INT D_T_RT0_2D_Polydeg =1;
static bool D_T_RT0_2D_IsOnlyDependOnRefCoord = 1;
static INT D_T_RT0_2D_Accuracy = 1;
static MAPTYPE D_T_RT0_2D_Maptype = Affine;
// base function values
static void D_T_RT0_2D_D00(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=1-RefCoord[0]-RefCoord[1];
  values[1]=RefCoord[0];
  values[2]=RefCoord[1];
}

// values of the derivatives in RefCoord[0] direction
static void D_T_RT0_2D_D10(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=-1;
  values[1]= 1;
  values[2]= 0;  
}

// values of the derivatives in RefCoord[1] direction
static void D_T_RT0_2D_D01(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=-1;
  values[1]= 0;
  values[2]= 1;   
}
// values of the derivatives in RefCoord[0]-RefCoord[0]  direction
static void D_T_RT0_2D_D20(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
}
// values of the derivatives in RefCoord[0]-RefCoord[1] direction
static void D_T_RT0_2D_D11(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
}
// values of the derivatives in RefCoord[1]-RefCoord[1] direction
static void D_T_RT0_2D_D02(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
  values[0]=0;
  values[1]=0;
  values[2]=0;
}

//values of the derivatives in RefCoord[1]-RefCoord[1] direction
static void D_T_RT0_2D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, DOUBLE* values)
{
    INT dim = 1;  
    DOUBLE coord[2];
     INT i;
     for(i=0;i<3;i++)
     {
         coord[0] = elem->Vert_X[i]; 
         coord[1] = elem->Vert_Y[i];
          fun(coord, dim, values+i);
     }
}
#endif
