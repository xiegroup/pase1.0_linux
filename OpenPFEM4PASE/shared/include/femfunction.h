#ifndef __FEMFUNCTION__
#define __FEMFUNCTION__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "constants.h"
#include "enumerations.h"
#include "femspace.h"


typedef struct FEMFUNCTION_
{
    INT num;
    INT start, end;
    //自由度的个数
    INT LocalNumDOF;
    INT GlobalNumDOF;
    //有限元函数在每个自由度的值
    DOUBLE *Values;
    //相对应的有限元空间
    FEMSPACE *FEMSpace;
} FEMFUNCTION;


//相应的函数
//根据有限元空间来建立相应的有限元函数, 同时为有限元函数设置存储空间
FEMFUNCTION *FEMFunctionBuild(FEMSPACE *FEMSpace, INT num);
//根据有限元空间和相应的数组建立有限元函数
FEMFUNCTION *FEMFunctionBuildWithValues(FEMSPACE *FEMSpace, DOUBLE *Values, INT num);
//有限元插值
void FEMInterpolation(FEMFUNCTION *femfunction, FUNCTIONVEC *fun, INT num);
//输出有限元函数
void FEMFunctionPrint(FEMFUNCTION *femfunction);
//按MATLAB格式输出网格信息
void FEMFunction2DMatlabWrite(FEMFUNCTION *fefunction, char *file);
//释放有限元函数的内存空间
void FEMFunctionDestroy(FEMFUNCTION **femfunction);

void FEMFunctionSetRange(FEMFUNCTION *femfun, INT start, INT end);
void FEMFunctionCopy(FEMFUNCTION *femfunX, FEMFUNCTION *femfunY);//X->Y
void FEMFunctionAxpby(DOUBLE alpha, FEMFUNCTION *femfunX, DOUBLE beta, FEMFUNCTION *femfunY);

// FEMFUNCTIONS *FEMFunctionsBuild(FEMSPACE *FEMSpace, INT num);
// void FEMFuntionsDestroy(FEMFUNCTIONS **femfunctions);

void ReorderElemValuesForCurlElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *ElemValues);
void ReorderElemValuesForDivElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *ElemValues);

#endif