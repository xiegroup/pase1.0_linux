#ifndef __QUADRATURE__
#define __QUADRATURE__
#include "mesh.h"
#include "enumerations.h"
#include "constants.h"

typedef struct QUADRATURE_
{
    QUADRATURETYPE QuadratureType;
    INT QuadDim;    //表示积分区域的维数
    INT Order;
    INT NumPoints;
    DOUBLE *QuadX;
    DOUBLE *QuadY;
    DOUBLE *QuadZ;
    DOUBLE *QuadW;
} QUADRATURE;
//相应的操作函数
QUADRATURE *QuadratureBuild(QUADRATURETYPE QuadratureType);
void QuadraturePrint(QUADRATURE *Quadrature);
void QuadratureDestroy(QUADRATURE **Quadrature);
#endif