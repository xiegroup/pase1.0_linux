#include <stdio.h>
#include <malloc.h>
#include"constants.h"

//对向量a的 a[left:right]进行排练, 数组长度 right-left+1
void QuickSort_Int(int a[], int left, int right)
{
    if (left > right)
        return;
    int i, j, temp;
    i = left;
    j = right;
    temp = a[left];
    while (i != j)
    {
        while (a[j] >= temp && j > i)
            j--;
        // now: a[j]<a[left]
        if (j > i)
            a[i++] = a[j];
        while (a[i] <= temp && j > i)
            i++;
        if (j > i)
            a[j--] = a[i];
    }
    a[i] = temp;
    QuickSort_Int(a, left, i - 1);
    QuickSort_Int(a, i + 1, right);
}


// 带着一个int b一起排序，int b顺序要和int a保持一样，即int a换位置，对应int b换位置
void QuickSort_Int_WithInt(int a[], int b[], int left, int right)
{
    if (left > right)
    {
        return;
    }
    int i, j, temp;
    i = left;
    j = right;
    temp = a[left];
    int tempd = b[left];
    while (i != j)
    {
        while (a[j] >= temp && j > i)
        {
            j--;
        }
        // now: a[j]<a[left]
        if (j > i)
        {
            b[i] = b[j];
            a[i++] = a[j];
        }
        while (a[i] <= temp && j > i)
        {
            i++;
        }
        if (j > i)
        {
            b[j] = b[i];
            a[j--] = a[i];
        }
    }
    a[i] = temp;
    b[i] = tempd;
    QuickSort_Int_WithInt(a, b, left, i - 1);
    QuickSort_Int_WithInt(a, b, i + 1, right);
}


//带着一个double一起排序，double顺序要和int保持一样，即int换位置，对应double换位置
void QuickSort_Int_WithDouble(int a[], double b[], int left, int right)
{
    if (left > right)
    {
        return;
    }
    int i, j, temp;
    i = left;
    j = right;
    temp = a[left];
    double tempd = b[left];
    while (i != j)
    {
        while (a[j] >= temp && j > i)
        {
            j--;
        }
        // now: a[j]<a[left]
        if (j > i)
        {
            b[i] = b[j];
            a[i++] = a[j];
        }
        while (a[i] <= temp && j > i)
        {
            i++;
        }
        if (j > i)
        {
            b[j] = b[i];
            a[j--] = a[i];
        }
    }
    a[i] = temp;
    b[i] = tempd;
    QuickSort_Int_WithDouble(a, b, left, i - 1);
    QuickSort_Int_WithDouble(a, b, i + 1, right);
}

//快速排序
//对整型数组向量a的 a[left:right]进行排列, 同时用数组b来记录位置， 数组长度 right-left+1
void QuickSortIntValueVector(INT Data[],INT b[], INT low, INT high)
{
	if(low < high){
		INT  i = low;     //i表示左边开始位置
		INT  j = high;    //j表示右边结束为止
		INT  temp_pos;    //记录基准值所在的b值
		INT temp;      //记录目前用来分类的数值大小
		temp = Data[low]; //选最左端的值来进行比较
    temp_pos = b[low];
    /*控制在当组内寻找一遍*/
		while(i < j){
			//先从右边开始寻找
			while(i<j && Data[j]>=temp){ 
				j--;	/*向前寻找*/
			}		
			//现在碰到了 a[j] < temp, 直接把a[j]的值赋给a[i], 右端比较结束
			if(j>i){
				Data[i] = Data[j];
        b[i]    = b[j];
        //i++;
      }
			//开始从左端开始进行比较， 应该也可以从i+1的位置开始
			while (i < j && Data[i]<=temp){
				i++;
			}
			//现在碰到了a[i] > temp, 直接把a[i]的值赋给a[j], 左端比较结束
			if(i<j)
      {
				Data[j] = Data[i];
        b[j] = b[i];
      }
      //如果i还未到达j处，就继续进行比较
    }//end while(i < j)
		Data[i] = temp; 
    b[i]    = temp_pos;
		if(low<i-1)
		{ 
      /*最后用同样的方式对分出来的左边的小组进行同上的做法*/
			QuickSortIntValueVector(Data, b, low, i - 1);  
		}
		if(i+1<high)
    {
      /*用同样的方式对分出来的右边的小组进行同上的做法*/
			QuickSortIntValueVector(Data, b, i + 1, high);
    } 
		/*当然最后可能会出现很多分左右，直到每一组的i = j 为止*/
    return;
	}
	else
	{
		return;
	}
}//end for quicksort





//对向量a的 a[left:right]进行排列, 同时用数组b来记录位置， 数组长度 right-left+1
void QuickSortRealValueVector(DOUBLE Data[],INT b[], INT low, INT high)
{
	if(low < high){
		INT  i = low;     //i表示左边开始位置
		INT  j = high;    //j表示右边结束为止
		INT  temp_pos;    //记录基准值所在的b值
		DOUBLE temp;      //记录目前用来分类的数值大小
		temp = Data[low]; //选最左端的值来进行比较
    temp_pos = b[low];
    /*控制在当组内寻找一遍*/
		while(i < j){
			//先从右边开始寻找
			while(i<j && Data[j]>=temp){ 
				j--;	/*向前寻找*/
			}		
			//现在碰到了 a[j] < temp, 直接把a[j]的值赋给a[i], 右端比较结束
			if(j>i){
				Data[i] = Data[j];
        b[i]    = b[j];
        //i++;
      }
			//开始从左端开始进行比较， 应该也可以从i+1的位置开始
			while (i < j && Data[i]<=temp){
				i++;
			}
			//现在碰到了a[i] > temp, 直接把a[i]的值赋给a[j], 左端比较结束
			if(i<j)
      {
				Data[j] = Data[i];
        b[j] = b[i];
      }
      //如果i还未到达j处，就继续进行比较
    }//end while(i < j)
		Data[i] = temp; 
    b[i]    = temp_pos;
		if(low<i-1)
		{ 
      /*最后用同样的方式对分出来的左边的小组进行同上的做法*/
			QuickSortRealValueVector(Data, b, low, i - 1);  
		}
		if(i+1<high)
    {
      /*用同样的方式对分出来的右边的小组进行同上的做法*/
			QuickSortRealValueVector(Data, b, i + 1, high);
    } 
		/*当然最后可能会出现很多分左右，直到每一组的i = j 为止*/
    return;
	}
	else
	{
		return;
	}
}//end for quicksort

//多向量a进行排序，同时向量b进行相应的移动,  目前还不是快速排序
void SortTwoVec_Int(int a[],int b[], int left,int right)
{
  int i, j, pos, tmp;
  for(i=left;i<=right;i++)
  {
    tmp = a[i];
    pos = i;
    for(j=i+1;j<=right;j++)
    {
      if(a[j]<tmp)
      {
        tmp = a[j];
        pos = j;
      }
    }
    if(pos>i){
      a[pos] = a[i];
      a[i] = tmp;
      tmp = b[i];
      b[i] = b[pos];
      b[pos] = tmp;
    }
  }
}


//对向量a进行排序，同时向量b进行相应的移动，应该改成快速排序
void SortRealValueVector(DOUBLE a[],INT b[], INT left, INT right)
{
  int i, j, pos, tmppos; 
  double tmp;
  for(i=left;i<=right;i++)
  {
    tmp = a[i];  //从i开始往后进行比较
    pos = i;
    for(j=i+1;j<=right;j++)
    {
      if(a[j]>tmp)
      { //a[j]比a[i]要大,就更新pos的值
        tmp = a[j];  //tmp记录目前最大的值
        pos = j;
      }
    }//找到了最大值和它所在的位置
    if(pos>i){
      //就进行相应的值交换和位置交换
      a[pos] = a[i];
      a[i] = tmp;
      tmppos = b[i];
      b[i] = b[pos];
      b[pos] = tmppos;
    }
  }
}


//归并排序
void MergeSortRecursion(int A[], int B[], int left, int right)    // 递归实现的归并排序(自顶向下)
{
    if (left == right)    // 当待排序的序列长度为1时，递归开始回溯，进行merge操作
        return;
    int mid = (left + right) / 2;
    MergeSortRecursion(A, B, left, mid);
    MergeSortRecursion(A, B, mid + 1, right);
    Merge(A, B, left, mid, right);
}

void MergeSortIteration(int A[], int B[], int len)    // 非递归(迭代)实现的归并排序(自底向上)
{
    int left, mid, right;// 子数组索引,前一个为A[left...mid]，后一个子数组为A[mid+1...right]
    int i;
    for (i = 1; i < len; i *= 2)        // 子数组的大小i初始为1，每轮翻倍
    {
        left = 0;
        while (left + i < len)              // 后一个子数组存在(需要归并)
        {
            mid = left + i - 1;
            right = mid + i < len ? mid + i : len - 1;// 后一个子数组大小可能不够
            Merge(A, B,  left, mid, right);
            left = right + 1;               // 前一个子数组索引向后移动
        }
    }
}
// 合并两个已排好序的数组A[left...mid]和A[mid+1...right]
void Merge(int A[], int B[], int left, int mid, int right)
{
    int len = right - left + 1;
    int *temp = malloc(len*sizeof(int));       // 辅助空间O(n)
    int *Btemp = malloc(len*sizeof(int));
    int index = 0;
    int i = left;                   // 前一数组的起始元素
    int j = mid + 1;                // 后一数组的起始元素
    while (i <= mid && j <= right)
    {
        //temp[index++] = A[i] <= A[j] ? A[i++] : A[j++];  // 带等号保证归并排序的稳定性
        if(A[i] <= A[j])
        {
            Btemp[index]  =  B[i];
            temp[index++] =  A[i++];

        }
        else
        {
            Btemp[index]  =  B[j];
            temp[index++] =  A[j++];

        }
    }
    while (i <= mid)
    {
        Btemp[index]  = B[i];
        temp[index++] = A[i++];
    }
    while (j <= right)
    {
        Btemp[index]  = B[j];
        temp[index++] = A[j++];
    }
    int k;
    for (k = 0; k < len; k++)
    {
        B[left]   = Btemp[k];
        A[left++] = temp[k];
    }
    free(temp);
    free(Btemp);
}



//归并排序
void MergeSortRealRecursion(double A[], int B[], int left, int right)    // 递归实现的归并排序(自顶向下)
{
    if (left == right)    // 当待排序的序列长度为1时，递归开始回溯，进行merge操作
        return;
    int mid = (left + right) / 2;
    MergeSortRealRecursion(A, B, left, mid);
    MergeSortRealRecursion(A, B, mid + 1, right);
    MergeReal(A, B, left, mid, right);
}

void MergeSortRealIteration(double A[], int B[], int left, int right)    // 非递归(迭代)实现的归并排序(自底向上)
{
    int len, mid;// 子数组索引,前一个为A[left...mid]，后一个子数组为A[mid+1...right]
    len = right -left +1;
    int i;
    for (i = 1; i < len; i *= 2)        // 子数组的大小i初始为1，每轮翻倍
    {
        left = 0;
        while (left + i < len)              // 后一个子数组存在(需要归并)
        {
            mid = left + i - 1;
            right = mid + i < len ? mid + i : len - 1;// 后一个子数组大小可能不够
            MergeReal(A, B,  left, mid, right);
            left = right + 1;               // 前一个子数组索引向后移动
        }
    }
}
// 合并两个已排好序的数组A[left...mid]和A[mid+1...right]
void MergeReal(double A[], int B[], int left, int mid, int right)
{
    int len = right - left + 1;
    DOUBLE *temp = malloc(len*sizeof(DOUBLE));       // 辅助空间O(n)
    int *Btemp = malloc(len*sizeof(int));
    int index = 0;
    int i = left;                   // 前一数组的起始元素
    int j = mid + 1;                // 后一数组的起始元素
    while (i <= mid && j <= right)
    {
        //temp[index++] = A[i] <= A[j] ? A[i++] : A[j++];  // 带等号保证归并排序的稳定性
        if(A[i] <= A[j])
        {
            Btemp[index]  =  B[i];
            temp[index++] =  A[i++];

        }
        else
        {
            Btemp[index]  =  B[j];
            temp[index++] =  A[j++];

        }
    }
    while (i <= mid)
    {
        Btemp[index]  = B[i];
        temp[index++] = A[i++];
    }
    while (j <= right)
    {
        Btemp[index]  = B[j];
        temp[index++] = A[j++];
    }
    int k;
    for (k = 0; k < len; k++)
    {
        B[left]   = Btemp[k];
        A[left++] = temp[k];
    }
    free(temp);
    free(Btemp);
}
//在两个数值中找到最小的值
DOUBLE min(DOUBLE x, DOUBLE y)
{
  return x<y?x:y;
}
//在两个数值中找到最大的值
DOUBLE max(DOUBLE x, DOUBLE y)
{
  return x>y?x:y;
}


//计算3*3矩阵的逆矩阵
void
GetInverseMatrix(double **A,double **IA)
{
  double a11=A[0][0];
  double a12=A[0][1];
  double a13=A[0][2];
  double a21=A[1][0];
  double a22=A[1][1];
  double a23=A[1][2];
  double a31=A[2][0];
  double a32=A[2][1];
  double a33=A[2][2];
  double B=a11*a22*a33 - a11*a23*a32 - a12*a21*a33 + a12*a23*a31 + a13*a21*a32 - a13*a22*a31;
  IA[0][0]=  (a22*a33-a23*a32)/B;
  IA[0][1]= -(a12*a33-a13*a32)/B;
  IA[0][2]=  (a12*a23-a13*a22)/B;
  IA[1][0]= -(a21*a33-a23*a31)/B;
  IA[1][1]=  (a11*a33-a13*a31)/B;
  IA[1][2]= -(a11*a23-a13*a21)/B;
  IA[2][0]=  (a21*a32-a22*a31)/B;
  IA[2][1]= -(a11*a32-a12*a31)/B;
  IA[2][2]=  (a11*a22-a12*a21)/B;
}


void __OpenPFEM_Move(void *a, void *b, unsigned long n)
{
    if (a < b)
    {
        if (a <= b - n)
            memcpy(a, b, n);
        else
        {
            memcpy(a, b, (int)(b - a));
            __OpenPFEM_Move(b, b + (int)(b - a), n - (int)(b - a));
        }
    }
    else
    {
        if (b <= a - n)
            memcpy(a, b, n);
        else
        {
            memcpy(b + n, b + (n - (int)(a - b)), (int)(a - b));
            __OpenPFEM_Move(a, b, n - (int)(a - b));
        }
    }
}