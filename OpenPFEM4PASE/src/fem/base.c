#include <stdio.h>
#include <stdlib.h>
#include "base.h"

BASE *Base1DBuild(INT num_bas, INT value_dim, INT inter_dim, INT polydeg,
                  BOOL IsOnlyDependOnRefCoord, INT accuracy, MAPTYPE MapType, INT *dof,
                  BASEFUNCTION *baseD0, BASEFUNCTION *baseD1,
                  BASEFUNCTION *baseD2, NODALFUNCTION *NodalFun, FUNCTIONVEC *InterFun)
{
    BASE *Base;
    Base = malloc(sizeof(BASE));
    Base->worlddim = 1;
    Base->NumBase = num_bas;
    Base->ValueDim = value_dim;
    Base->InterpolationValueDim = inter_dim;
    Base->PolynomialDegree = polydeg;
    Base->IsOnlyDependOnRefCoord = IsOnlyDependOnRefCoord;
    Base->Accuracy = accuracy;
    Base->MapType = MapType;
    INT i;
    for (i = 0; i <= 1; i++)
    {
        Base->DOF[i] = dof[i];
    }
    Base->NMultiIndices = 3;
    // 建立3个相应的基函数集合
    Base->BaseFun = malloc(Base->NMultiIndices * sizeof(BASEFUNCTION *));
    Base->BaseFun[0] = baseD0;
    Base->BaseFun[1] = baseD1;
    Base->BaseFun[2] = baseD2;
    Base->NodalFun = NodalFun;
    Base->InterFun = InterFun;
    Base->Values_X = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_Y = NULL;
    Base->Values_XY = NULL;
    Base->Values_Z = NULL;
    Base->Values_XZ = NULL;
    Base->Values_YZ = NULL;
    return Base;
}

// 建立二维的有限元基函数对象
BASE *BaseBuild2D(int num_bas, int value_dim, INT inter_dim, int polydeg,
                  BOOL IsOnlyDependOnRefCoord, int accuracy, MAPTYPE MapType, int *dof,
                  BASEFUNCTION *baseD00, BASEFUNCTION *baseD10,
                  BASEFUNCTION *baseD01, BASEFUNCTION *baseD20,
                  BASEFUNCTION *baseD11, BASEFUNCTION *baseD02,
                  NODALFUNCTION *NodalFun, FUNCTIONVEC *InterFun)
{
    BASE *Base;
    Base = malloc(sizeof(BASE));
    Base->worlddim = 2;
    Base->NumBase = num_bas;
    Base->ValueDim = value_dim;
    Base->InterpolationValueDim = inter_dim;
    Base->PolynomialDegree = polydeg;
    Base->IsOnlyDependOnRefCoord = IsOnlyDependOnRefCoord;
    Base->Accuracy = accuracy;
    Base->MapType = MapType;
    int i;
    for (i = 0; i <= 2; i++)
    {
        Base->DOF[i] = dof[i];
    }
    Base->NMultiIndices = 6;
    // 建立6个相应的基函数集合
    Base->BaseFun = malloc(Base->NMultiIndices * sizeof(BASEFUNCTION *));
    Base->BaseFun[0] = baseD00;
    Base->BaseFun[1] = baseD10;
    Base->BaseFun[2] = baseD01;
    Base->BaseFun[3] = baseD20;
    Base->BaseFun[4] = baseD11;
    Base->BaseFun[5] = baseD02;
    Base->NodalFun = NodalFun;
    Base->InterFun = InterFun;
    // some seriers for save temp values
    Base->Values_X = malloc(num_bas * value_dim * sizeof(double));
    Base->Values_Y = malloc(num_bas * value_dim * sizeof(double));
    Base->Values_XY = malloc(num_bas * value_dim * sizeof(double));
    Base->Values_Z = NULL;
    Base->Values_XZ = NULL;
    Base->Values_YZ = NULL;
    return Base;
}
// 释放有限元基函数结构体的内存
void BaseDestroy(BASE **Base)
{
    OpenPFEM_Free((*Base)->Values_X);
    OpenPFEM_Free((*Base)->Values_Y);
    OpenPFEM_Free((*Base)->Values_Z);
    OpenPFEM_Free((*Base)->Values_XY);
    OpenPFEM_Free((*Base)->Values_XZ);
    OpenPFEM_Free((*Base)->Values_YZ);
    // 释放掉存储基函数的指针数组
    OpenPFEM_Free((*Base)->BaseFun);
    OpenPFEM_Free((*Base));
}

// 建立三维的有限元基函数
BASE *BaseBuild3D(INT num_bas, INT value_dim, INT inter_dim, INT polydeg,
                  BOOL IsOnlyDependOnRefCoord, INT accuracy, MAPTYPE MapType, INT *dof,
                  BASEFUNCTION *baseD000, BASEFUNCTION *baseD100,
                  BASEFUNCTION *baseD010, BASEFUNCTION *baseD001,
                  BASEFUNCTION *baseD200, BASEFUNCTION *baseD020,
                  BASEFUNCTION *baseD002, BASEFUNCTION *baseD110,
                  BASEFUNCTION *baseD101, BASEFUNCTION *baseD011,
                  NODALFUNCTION *NodalFun, FUNCTIONVEC *InterFun)
{
    BASE *Base;
    Base = malloc(sizeof(BASE));
    Base->worlddim = 3;
    Base->NumBase = num_bas;
    Base->ValueDim = value_dim;
    Base->InterpolationValueDim = inter_dim;
    Base->PolynomialDegree = polydeg;
    Base->IsOnlyDependOnRefCoord = IsOnlyDependOnRefCoord;
    Base->Accuracy = accuracy;
    Base->MapType = MapType;
    INT i;
    for (i = 0; i <= 3; i++)
    {
        Base->DOF[i] = dof[i];
    }
    Base->NMultiIndices = 10;
    // 建立10个相应的基函数集合
    Base->BaseFun = malloc(Base->NMultiIndices * sizeof(BASEFUNCTION *));
    Base->BaseFun[0] = baseD000;
    Base->BaseFun[1] = baseD100;
    Base->BaseFun[2] = baseD010;
    Base->BaseFun[3] = baseD001;
    Base->BaseFun[4] = baseD200;
    Base->BaseFun[5] = baseD020;
    Base->BaseFun[6] = baseD002;
    Base->BaseFun[7] = baseD110;
    Base->BaseFun[8] = baseD101;
    Base->BaseFun[9] = baseD011;
    Base->NodalFun = NodalFun;
    Base->InterFun = InterFun;
    // some seriers for save temp values
    Base->Values_X = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_Y = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_XY = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_Z = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_XZ = malloc(num_bas * value_dim * sizeof(DOUBLE));
    Base->Values_YZ = malloc(num_bas * value_dim * sizeof(DOUBLE));
    return Base;
}

void GetBaseValues(BASE *Base, MULTIINDEX MultiIndex,
                   ELEMENT *Elem, DOUBLE Coord[],
                   DOUBLE RefCoord[], DOUBLE *Values)
{
    INT i, j;
    INT worlddim = Base->worlddim;
    switch (Base->MapType)
    {
    case Affine:
        switch (worlddim)
        {
        case 1: // switch (worlddim)
            if (MultiIndex == D0)
            {
                Base->BaseFun[0](Elem, Coord, RefCoord, Values);
            } // end if(MultiIndex==D0)
            else if (MultiIndex == D1)
            {
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X);
                for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                {
                    Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0];
                }
            } // end if(MultiIndex==D1)
            else if (MultiIndex == D2)
            {
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_X);
                for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                {
                    Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0];
                }
            } // end if(MultiIndex==D2)
            else
            {
                printf("the MultiIndex of Base is wrong.\n");
            }
            break; // switch (worlddim)

        case 2: // switch (worlddim)
            if (MultiIndex == D00)
            {
                // printf("It should be here: Coord[0]=%2.8f, Coord[1]=%2.8f, RefCoord[0]=%2.8f, RefCoord[1]=%2.8f\n",
                //             Coord[0],Coord[1], RefCoord[0], RefCoord[1]);
                Base->BaseFun[0](Elem, Coord, RefCoord, Values);
                // printf("Values[0]=%2.8f\n", Values[0]);
                // printf("Is it ok?\n");
            }
            else if ((MultiIndex == D10) || (MultiIndex == D01))
            {
                // printf("compute the gradient!\n");
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X);
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_Y);
                switch (MultiIndex)
                {
                case D10:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D01:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D20) || (MultiIndex == D11) || (MultiIndex == D02))
            {
                Base->BaseFun[3](Elem, Coord, RefCoord, Base->Values_X);  // D20
                Base->BaseFun[4](Elem, Coord, RefCoord, Base->Values_XY); // D11
                Base->BaseFun[5](Elem, Coord, RefCoord, Base->Values_Y);  // D02
                switch (MultiIndex)
                {
                case D20:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0];
                        // printf("D20 value: %f\n",Values[i]);
                    }
                    break;
                case D11:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D02:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                } // end switch (MultiIndex)
            }
            else
            {
                printf("the MultiIndex of Base is wrong.\n");
            }
            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            if (MultiIndex == D000)
            {
                // 直接取值就可以，不需要进行变换的计算
                Base->BaseFun[0](Elem, Coord, RefCoord, Values);
            }
            else if ((MultiIndex == D100) || (MultiIndex == D010) || (MultiIndex == D001))
            {
                // 需要进行梯度的计算，并且同时需要进行变换计算
                // 首先取出基函数对x, y, z的导数
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X); // D100
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_Y); // D010
                Base->BaseFun[3](Elem, Coord, RefCoord, Base->Values_Z); // D001
                switch (MultiIndex)
                {
                    // 求对x的导数
                case D100:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0];
                    }
                    break;
                case D010:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1];
                        // printf("Values[%d]=%f, ", i, Values[i]);
                    }
                    // printf("\n");
                    break;
                case D001:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D200) || (MultiIndex == D020) || (MultiIndex == D002) ||
                     (MultiIndex == D110) || (MultiIndex == D101) || (MultiIndex == D011))
            {
                // printf("Come to the D200!\n");
                Base->BaseFun[4](Elem, Coord, RefCoord, Base->Values_X);  // D200
                Base->BaseFun[5](Elem, Coord, RefCoord, Base->Values_Y);  // D020
                Base->BaseFun[6](Elem, Coord, RefCoord, Base->Values_Z);  // D002
                Base->BaseFun[7](Elem, Coord, RefCoord, Base->Values_XY); // D110
                Base->BaseFun[8](Elem, Coord, RefCoord, Base->Values_XZ); // D101
                Base->BaseFun[9](Elem, Coord, RefCoord, Base->Values_YZ); // D011
                switch (MultiIndex)
                {
                case D200:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][0] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][0] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D020:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D002:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D110:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D101:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D011:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][2];
                    }
                    break;
                }
            }
            else
            {
                printf("the MultiIndex of Base is wrong.1111\n");
            }
            break; // switch (worlddim)
        }          // end switch (worlddim)
        break;

    case CR:
        // printf("CR element!  noinfo\n");
        switch (worlddim)
        {
        case 1: // switch (worlddim)
            printf("1D 无 CR 元.\n");
            break; // switch (worlddim)
        case 2:    // switch (worlddim)
            if (MultiIndex == D00)
            {
                Base->BaseFun[0](Elem, Coord, RefCoord, Values);
            }
            else if ((MultiIndex == D10) || (MultiIndex == D01))
            {
                // printf("compute the gradient!\n");
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X);
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_Y);
                switch (MultiIndex)
                {
                case D10:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D01:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D20) || (MultiIndex == D11) || (MultiIndex == D02))
            {
                Base->BaseFun[3](Elem, Coord, RefCoord, Base->Values_X);  // D20
                Base->BaseFun[4](Elem, Coord, RefCoord, Base->Values_XY); // D11
                Base->BaseFun[5](Elem, Coord, RefCoord, Base->Values_Y);  // D02
                switch (MultiIndex)
                {
                case D20:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D11:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D02:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else
            {
                printf("the MultiIndex of Base is wrong.\n");
            }
            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            printf("3 维数 CR元未补充\n");
            break; // switch (worlddim)
        }          // end switch (worlddim)
        break;

    case Piola:
        switch (worlddim)
        {
        case 1: // switch (worlddim)

            printf("1D 无 Piola 元.\n");

            break; // switch (worlddim)

        case 2: // switch (worlddim)

            printf("2D 无 Piola 元.\n");

            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            if (MultiIndex == D000)
            {
                // hat{u}
                Base->BaseFun[0](Elem, Coord, RefCoord, Values); // D000
            }
            else if ((MultiIndex == D100) || (MultiIndex == D010) || (MultiIndex == D001))
            {
                // 需要进行梯度的计算，并且同时需要进行变换计算
                // 首先取出基函数对x, y, z的导数
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X); // D100
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_Y); // D010
                Base->BaseFun[3](Elem, Coord, RefCoord, Base->Values_Z); // D001
                switch (MultiIndex)
                {
                    // 求对x的导数
                case D100:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0];
                    }
                    break;
                case D010:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1];
                        // printf("Values[%d]=%f, ", i, Values[i]);
                    }
                    // printf("\n");
                    break;
                case D001:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D200) || (MultiIndex == D020) || (MultiIndex == D002) ||
                     (MultiIndex == D110) || (MultiIndex == D101) || (MultiIndex == D011))
            {
                // printf("Come to the D200!\n");
                Base->BaseFun[4](Elem, Coord, RefCoord, Base->Values_X);  // D200
                Base->BaseFun[5](Elem, Coord, RefCoord, Base->Values_Y);  // D020
                Base->BaseFun[6](Elem, Coord, RefCoord, Base->Values_Z);  // D002
                Base->BaseFun[7](Elem, Coord, RefCoord, Base->Values_XY); // D110
                Base->BaseFun[8](Elem, Coord, RefCoord, Base->Values_XZ); // D101
                Base->BaseFun[9](Elem, Coord, RefCoord, Base->Values_YZ); // D011
                switch (MultiIndex)
                {
                case D200:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][0] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][0] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D020:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D002:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D110:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D101:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D011:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][2];
                    }
                    break;
                }
            }
            else
            {
                printf("the MultiIndex of Base is wrong.1111\n");
            }
            // $F_i = (DF_K^{-1})_{mi} F_m$
            DOUBLE value_temp[3];
            INT ValueDim = Base->ValueDim;
            for (i = 0; i < Base->NumBase; i++)
            {
                value_temp[0] = Elem->InvJacobian[0][0] * Values[i * ValueDim] + Elem->InvJacobian[1][0] * Values[i * ValueDim + 1] + Elem->InvJacobian[2][0] * Values[i * ValueDim + 2];
                value_temp[1] = Elem->InvJacobian[0][1] * Values[i * ValueDim] + Elem->InvJacobian[1][1] * Values[i * ValueDim + 1] + Elem->InvJacobian[2][1] * Values[i * ValueDim + 2];
                value_temp[2] = Elem->InvJacobian[0][2] * Values[i * ValueDim] + Elem->InvJacobian[1][2] * Values[i * ValueDim + 1] + Elem->InvJacobian[2][2] * Values[i * ValueDim + 2];
                Values[i * ValueDim] = value_temp[0];
                Values[i * ValueDim + 1] = value_temp[1];
                Values[i * ValueDim + 2] = value_temp[2];
            }
#if NEDELECSCALE == 1
            INT vert0[6] = {0, 0, 0, 1, 1, 2};
            INT vert1[6] = {1, 2, 3, 2, 3, 3};
            INT *DOF = Base->DOF;
            DOUBLE vec_line[3];
            DOUBLE length[6];
            // 线对应的自由度做scale
            for (i = 0; i < 6; i++)
            {
                vec_line[0] = Elem->Vert_X[vert1[i]] - Elem->Vert_X[vert0[i]];
                vec_line[1] = Elem->Vert_Y[vert1[i]] - Elem->Vert_Y[vert0[i]];
                vec_line[2] = Elem->Vert_Z[vert1[i]] - Elem->Vert_Z[vert0[i]];
                length[i] = sqrt(vec_line[0] * vec_line[0] + vec_line[1] * vec_line[1] + vec_line[2] * vec_line[2]);
#if length_change == 1
                length[i] = sqrt(length[i]);
#endif
                for (j = 0; j < DOF[1]; j++)
                {
                    Values[(i * DOF[1] + j) * ValueDim] *= length[i];
                    Values[(i * DOF[1] + j) * ValueDim + 1] *= length[i];
                    Values[(i * DOF[1] + j) * ValueDim + 2] *= length[i];
                }
            }
            INT line4face1[4] = {3, 1, 0, 0};
            INT line4face2[4] = {4, 2, 2, 1};
            // 面对应的自由度做scale
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < DOF[2] / 2; j++)
                {
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim + 1] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim + 2] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim] *= length[line4face2[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim + 1] *= length[line4face2[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim + 2] *= length[line4face2[i]];
                }
            }
            // 体对应的自由度做scale
            for (j = 0; j < DOF[3]; j++)
            {
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim] *= length[j];
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim + 1] *= length[j];
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim + 2] *= length[j];
            }
#endif
            break; // switch (worlddim)
        }          // end switch (worlddim)

        break;

    case Div:
        switch (worlddim)
        {
        case 1: // switch (worlddim)

            printf("1D 无 Piola 元.\n");

            break; // switch (worlddim)

        case 2: // switch (worlddim)

            printf("2D 无 Piola 元.\n");

            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            if (MultiIndex == D000)
            {
                // hat{u}
                Base->BaseFun[0](Elem, Coord, RefCoord, Values); // D000
            }
            else if ((MultiIndex == D100) || (MultiIndex == D010) || (MultiIndex == D001))
            {
                // 需要进行梯度的计算，并且同时需要进行变换计算
                // 首先取出基函数对x, y, z的导数
                Base->BaseFun[1](Elem, Coord, RefCoord, Base->Values_X); // D100
                Base->BaseFun[2](Elem, Coord, RefCoord, Base->Values_Y); // D010
                Base->BaseFun[3](Elem, Coord, RefCoord, Base->Values_Z); // D001
                switch (MultiIndex)
                {
                    // 求对x的导数
                case D100:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0];
                    }
                    break;
                case D010:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1];
                        // printf("Values[%d]=%f, ", i, Values[i]);
                    }
                    // printf("\n");
                    break;
                case D001:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D200) || (MultiIndex == D020) || (MultiIndex == D002) ||
                     (MultiIndex == D110) || (MultiIndex == D101) || (MultiIndex == D011))
            {
                // printf("Come to the D200!\n");
                Base->BaseFun[4](Elem, Coord, RefCoord, Base->Values_X);  // D200
                Base->BaseFun[5](Elem, Coord, RefCoord, Base->Values_Y);  // D020
                Base->BaseFun[6](Elem, Coord, RefCoord, Base->Values_Z);  // D002
                Base->BaseFun[7](Elem, Coord, RefCoord, Base->Values_XY); // D110
                Base->BaseFun[8](Elem, Coord, RefCoord, Base->Values_XZ); // D101
                Base->BaseFun[9](Elem, Coord, RefCoord, Base->Values_YZ); // D011
                switch (MultiIndex)
                {
                case D200:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][0] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][0] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][0] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D020:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D002:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D110:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][1] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][1] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][1] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D101:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D011:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = Base->Values_X[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][2] + Base->Values_Y[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][2] + Base->Values_Z[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][2] + Base->Values_XZ[i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][2] + Base->Values_YZ[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][2] + Base->Values_XY[i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][2] + Base->Values_XZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][2] + Base->Values_YZ[i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][2];
                    }
                    break;
                }
            }
            else
            {
                printf("the MultiIndex of Base is wrong.1111\n");
            }
            // $F_i = (DF_K)_{im} F_m / det(DF_K)$
            DOUBLE value_temp[3];   
            INT ValueDim = Base->ValueDim;
            for (i = 0; i < Base->NumBase; i++)
            {
                value_temp[0] = Elem->Jacobian[0][0] * Values[i * ValueDim] + Elem->Jacobian[0][1] * Values[i * ValueDim + 1] + Elem->Jacobian[0][2] * Values[i * ValueDim + 2];
                value_temp[1] = Elem->Jacobian[1][0] * Values[i * ValueDim] + Elem->Jacobian[1][1] * Values[i * ValueDim + 1] + Elem->Jacobian[1][2] * Values[i * ValueDim + 2];
                value_temp[2] = Elem->Jacobian[2][0] * Values[i * ValueDim] + Elem->Jacobian[2][1] * Values[i * ValueDim + 1] + Elem->Jacobian[2][2] * Values[i * ValueDim + 2];
                Values[i * ValueDim] = value_temp[0];
                Values[i * ValueDim + 1] = value_temp[1];
                Values[i * ValueDim + 2] = value_temp[2];
                if(Elem->Volumn < 0)
                    printf("have not compute invjacobian & volumn!\n");
                Values[i * ValueDim] /= (Elem->Volumn * 6.0);
                Values[i * ValueDim + 1] /= (Elem->Volumn * 6.0);
                Values[i * ValueDim + 2] /= (Elem->Volumn * 6.0);
            }
#if NEDELECSCALE == 1
            INT vert0[6] = {0, 0, 0, 1, 1, 2};
            INT vert1[6] = {1, 2, 3, 2, 3, 3};
            INT *DOF = Base->DOF;
            DOUBLE vec_line[3];
            DOUBLE length[6];
            // 线对应的自由度做scale
            for (i = 0; i < 6; i++)
            {
                vec_line[0] = Elem->Vert_X[vert1[i]] - Elem->Vert_X[vert0[i]];
                vec_line[1] = Elem->Vert_Y[vert1[i]] - Elem->Vert_Y[vert0[i]];
                vec_line[2] = Elem->Vert_Z[vert1[i]] - Elem->Vert_Z[vert0[i]];
                length[i] = sqrt(vec_line[0] * vec_line[0] + vec_line[1] * vec_line[1] + vec_line[2] * vec_line[2]);
#if length_change == 1
                length[i] = sqrt(length[i]);
#endif
                for (j = 0; j < DOF[1]; j++)
                {
                    Values[(i * DOF[1] + j) * ValueDim] *= length[i]/(Elem->Volumn * 6.0);
                    Values[(i * DOF[1] + j) * ValueDim + 1] *= length[i]/(Elem->Volumn * 6.0);
                    Values[(i * DOF[1] + j) * ValueDim + 2] *= length[i]/(Elem->Volumn * 6.0);
                }
            }
            INT line4face1[4] = {3, 1, 0, 0};
            INT line4face2[4] = {4, 2, 2, 1};
            // 面对应的自由度做scale
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < DOF[2] / 2; j++)
                {
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim + 1] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + j) * ValueDim + 2] *= length[line4face1[i]];
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim] *= length[line4face2[i]]/(Elem->Volumn * 6.0);
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim + 1] *= length[line4face2[i]]/(Elem->Volumn * 6.0);
                    Values[(6 * DOF[1] + i * DOF[2] + DOF[2] / 2 + j) * ValueDim + 2] *= length[line4face2[i]]/(Elem->Volumn * 6.0);
                }
            }
            // 体对应的自由度做scale
            for (j = 0; j < DOF[3]; j++)
            {
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim] *= length[j]/(Elem->Volumn * 6.0);
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim + 1] *= length[j]/(Elem->Volumn * 6.0);
                Values[(6 * DOF[1] + 4 * DOF[2] + j) * ValueDim + 2] *= length[j]/(Elem->Volumn * 6.0);
            }
#endif
            break; // switch (worlddim)
        }          // end switch (worlddim)

        break;

    default:
        printf("标准单元到实际单元的变换方式输入有误！\n");
        break;
    }

} // end fun
