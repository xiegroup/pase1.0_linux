#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "quadrature.h"

// 一维的积分区间为[0,1]
// 二维的积分区域为[0,0], [1,0], [0,1]
// 三维的积分区域为[0,0,0], [1,0,0], [0,1,0], [0,0,1]
QUADRATURE *QuadratureBuild(QUADRATURETYPE QuadratureType)
{
    QUADRATURE *Quadrature = malloc(sizeof(QUADRATURE));
    INT i;
    switch (QuadratureType)
    {
    case QuadLine1:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine1;
        Quadrature->Order = 1;
        Quadrature->NumPoints = 1;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX11[1] = {0.5};
        DOUBLE QuadW11[1] = {1.0};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX11[i];
            Quadrature->QuadW[i] = QuadW11[i];
        }
        break;
    case QuadLine2:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine2;
        Quadrature->Order = 3;
        Quadrature->NumPoints = 2;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX12[2] = {0.21132486540518711774, 0.78867513459481288226};
        DOUBLE QuadW12[2] = {0.5, 0.5};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX12[i];
            Quadrature->QuadW[i] = QuadW12[i];
        }
        break;
    case QuadLine3:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine3;
        Quadrature->Order = 5;
        Quadrature->NumPoints = 3;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX13[3] = {0.11270166537925831149, 0.5, 0.88729833462074168851};
        DOUBLE QuadW13[3] = {0.2777777777777778, 0.4444444444444444, 0.2777777777777778};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX13[i];
            Quadrature->QuadW[i] = QuadW13[i];
        }
        break;
    case QuadLine4:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine4;
        Quadrature->Order = 7;
        Quadrature->NumPoints = 4;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX14[4] = {0.069431844202973713731097404888715,
                             0.33000947820757187134432797392947,
                             0.66999052179242812865567202607053,
                             0.93056815579702628626890259511129};
        DOUBLE QuadW14[4] = {0.17392742256872692485636378,
                             0.3260725774312730473880606,
                             0.3260725774312730473880606,
                             0.17392742256872692485636378};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX14[i];
            Quadrature->QuadW[i] = QuadW14[i];
        }
        break;
    case QuadLine5:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine5;
        Quadrature->Order = 9;
        Quadrature->NumPoints = 5;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX15_1D[5] = {   0.04691007703066801815,    0.23076534494715850165,    0.50000000000000000000,    0.76923465505284149835,    0.95308992296933192634 };
        DOUBLE QuadW15_1D[5] = {   0.11846344252809454245,    0.23931433524968323545,    0.28444444444444444420,    0.23931433524968323545,    0.11846344252809454245};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX15_1D[i];
            Quadrature->QuadW[i] = QuadW15_1D[i];
        }
        break;
    case QuadLine6:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine6;
        Quadrature->Order = 11;
        Quadrature->NumPoints = 6;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX16_1D[6] = {   0.03376524289842397497,    0.16939530676686798127,    0.38069040695840150601,    0.61930959304159849399,    0.83060469323313201873,    0.96623475710157602503 };
        DOUBLE QuadW16_1D[6] = {   0.08566224618958499792,    0.18038078652406949742,    0.23395696728634549078,    0.23395696728634549078,    0.18038078652406949742,    0.08566224618958499792};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX16_1D[i];
            Quadrature->QuadW[i] = QuadW16_1D[i];
        }
        break;
    case QuadLine8:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine8;
        Quadrature->Order = 15;
        Quadrature->NumPoints = 8;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX18_1D[8] = {   0.01985507175123202295,    0.10166676129318652499,    0.23723379504183550459,    0.40828267875217499894,    0.59171732124782505657,    0.76276620495816449541,    0.89833323870681347501,    0.98014492824876797705 };
        DOUBLE QuadW18_1D[8] = {   0.05061426814518849709,    0.11119051722668700510,    0.15685332293894349576,    0.18134189168918099511,    0.18134189168918099511,    0.15685332293894349576,    0.11119051722668700510,    0.05061426814518849709};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX18_1D[i];
            Quadrature->QuadW[i] = QuadW18_1D[i];
        }
        break;
    case QuadLine12:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine12;
        Quadrature->Order = 23;
        Quadrature->NumPoints = 12;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX112_1D[12] = {   0.00921968287664048924,    0.04794137181476249010,    0.11504866290284748720,    0.20634102285669148058,    0.31608425050090999120,    0.43738329574426548785,    0.56261670425573451215,    0.68391574949908995329,    0.79365897714330846391,    0.88495133709715245729,    0.95205862818523745439,    0.99078031712335956627 };
        DOUBLE QuadW112_1D[12] = {   0.02358766819325590000,    0.05346966299765899822,    0.08003916427167249992,    0.10158371336153300402,    0.11674626826917799960,    0.12457352290670149764,    0.12457352290670149764,    0.11674626826917799960,    0.10158371336153300402,    0.08003916427167249992,    0.05346966299765899822,    0.02358766819325590000};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX112_1D[i];
            Quadrature->QuadW[i] = QuadW112_1D[i];
        }
        break;
    case QuadLine16:
        Quadrature->QuadDim = 1;
        Quadrature->QuadratureType = QuadLine16;
        Quadrature->Order = 31;
        Quadrature->NumPoints = 16;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX116_1D[16] = {   0.00529953250417497523,    0.02771248846338347782,    0.06718439880608401138,    0.12229779582249850067,    0.19106187779867800369,    0.27099161117138648169,    0.35919822461037048678,    0.45249374508118128668,    0.54750625491881865781,    0.64080177538962956874,    0.72900838882861351831,    0.80893812220132199631,    0.87770220417750155484,    0.93281560119391593311,    0.97228751153661652218,    0.99470046749582508028 };
        DOUBLE QuadW116_1D[16] = {   0.01357622970587699963,    0.03112676196932405090,    0.04757925584124569895,    0.06231448562776750050,    0.07479799440828900636,    0.08457825969750100426,    0.09130170752246150045,    0.09472530522753450088,    0.09472530522753450088,    0.09130170752246150045,    0.08457825969750100426,    0.07479799440828900636,    0.06231448562776750050,    0.04757925584124569895,    0.03112676196932405090,    0.01357622970587699963};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX116_1D[i];
            Quadrature->QuadW[i] = QuadW116_1D[i];
        }
        break;
    case QuadTriangle1:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle1;
        Quadrature->Order = 1;
        Quadrature->NumPoints = 1;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX21[1] = {0.33333333333333333333};
        DOUBLE QuadY21[1] = {0.33333333333333333333};
        DOUBLE QuadW21[1] = {1.0};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX21[i];
            Quadrature->QuadY[i] = QuadY21[i];
            Quadrature->QuadW[i] = QuadW21[i];
        }
        break;
    case QuadTriangle3:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle3;
        Quadrature->Order = 2;
        Quadrature->NumPoints = 3;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX23[3] = {0.5, 0.0, 0.5};
        DOUBLE QuadY23[3] = {0.5, 0.5, 0.0};
        DOUBLE QuadW23[3] = {1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX23[i];
            Quadrature->QuadY[i] = QuadY23[i];
            Quadrature->QuadW[i] = QuadW23[i];
        }
        break;
    case QuadTriangle4:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle4;
        Quadrature->Order = 4;
        Quadrature->NumPoints = 4;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX24[4] = {0.07503111022261, 0.17855872826362, 0.28001991549907, 0.66639024601470};
        DOUBLE QuadY24[4] = {0.28001991549907, 0.66639024601470, 0.07503111022261, 0.17855872826362};
        DOUBLE QuadW24[4] = {0.18195861825602, 0.31804138174398, 0.18195861825602, 0.31804138174398};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX24[i];
            Quadrature->QuadY[i] = QuadY24[i];
            Quadrature->QuadW[i] = QuadW24[i];
        }
        break;
    case QuadTriangle7:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle7;
        Quadrature->Order = 7;
        Quadrature->NumPoints = 7;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX27[7] = {0.333333330000000, 0.059615870000000, 0.470142060000000,
                             0.470142060000000, 0.797426990000000, 0.101286510000000,
                             0.101286510000000};
        DOUBLE QuadY27[7] = {0.333333330000000, 0.470142060000000, 0.059615870000000,
                             0.470142060000000, 0.101286510000000, 0.797426990000000,
                             0.101286510000000};
        DOUBLE QuadW27[7] = {0.225000000000000, 0.132394150000000, 0.132394150000000,
                             0.132394150000000, 0.125939180000000, 0.125939180000000,
                             0.125939180000000};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX27[i];
            Quadrature->QuadY[i] = QuadY27[i];
            Quadrature->QuadW[i] = QuadW27[i];
        }
        break;
    case QuadTriangle13:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle13;
        Quadrature->Order = 13;
        Quadrature->NumPoints = 13;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX213[13] = {0.333333333333000,
                               0.260345966079000,
                               0.260345966079000,
                               0.479308067842000,
                               0.065130102902000,
                               0.065130102902000,
                               0.869739794196000,
                               0.312865496005000,
                               0.638444188570000,
                               0.048690315425000,
                               0.638444188570000,
                               0.312865496005000,
                               0.048690315425000};
        DOUBLE QuadY213[13] = {0.333333333333000,
                               0.260345966079000,
                               0.479308067842000,
                               0.260345966079000,
                               0.065130102902000,
                               0.869739794196000,
                               0.065130102902000,
                               0.638444188570000,
                               0.048690315425000,
                               0.312865496005000,
                               0.312865496005000,
                               0.048690315425000,
                               0.638444188570000};
        DOUBLE QuadW213[13] = {-0.149570044468000,
                               0.175615257433000,
                               0.175615257433000,
                               0.175615257433000,
                               0.053347235609000,
                               0.053347235609000,
                               0.053347235609000,
                               0.077113760890000,
                               0.077113760890000,
                               0.077113760890000,
                               0.077113760890000,
                               0.077113760890000,
                               0.077113760890000};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX213[i];
            Quadrature->QuadY[i] = QuadY213[i];
            Quadrature->QuadW[i] = QuadW213[i];
        }
        break;
    case QuadTriangle27:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle27;
        Quadrature->Order = 27;
        Quadrature->NumPoints = 27;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX227[27] = {0.032364948111276,
                               0.032364948111276,
                               0.935270103777448,
                               0.119350912282581,
                               0.119350912282581,
                               0.761298175434837,
                               0.534611048270758,
                               0.534611048270758,
                               -0.069222096541517,
                               0.203309900431282,
                               0.203309900431282,
                               0.593380199137435,
                               0.398969302965855,
                               0.398969302965855,
                               0.202061394068290,
                               0.593201213428213,
                               0.050178138310495,
                               0.356620648261293,
                               0.593201213428213,
                               0.050178138310495,
                               0.356620648261293,
                               0.807489003159792,
                               0.021022016536166,
                               0.171488980304042,
                               0.807489003159792,
                               0.021022016536166,
                               0.171488980304042};
        DOUBLE QuadY227[27] = {0.032364948111276,
                               0.935270103777448,
                               0.032364948111276,
                               0.119350912282581,
                               0.761298175434837,
                               0.119350912282581,
                               0.534611048270758,
                               -0.069222096541517,
                               0.534611048270758,
                               0.203309900431282,
                               0.593380199137435,
                               0.203309900431282,
                               0.398969302965855,
                               0.202061394068290,
                               0.398969302965855,
                               0.050178138310495,
                               0.356620648261293,
                               0.593201213428213,
                               0.356620648261293,
                               0.593201213428213,
                               0.050178138310495,
                               0.021022016536166,
                               0.171488980304042,
                               0.807489003159792,
                               0.171488980304042,
                               0.807489003159792,
                               0.021022016536166};
        DOUBLE QuadW227[27] = {0.013659731002678,
                               0.013659731002678,
                               0.013659731002678,
                               0.036184540503418,
                               0.036184540503418,
                               0.036184540503418,
                               0.000927006328961,
                               0.000927006328961,
                               0.000927006328961,
                               0.059322977380774,
                               0.059322977380774,
                               0.059322977380774,
                               0.077149534914813,
                               0.077149534914813,
                               0.077149534914813,
                               0.052337111962204,
                               0.052337111962204,
                               0.052337111962204,
                               0.052337111962204,
                               0.052337111962204,
                               0.052337111962204,
                               0.020707659639141,
                               0.020707659639141,
                               0.020707659639141,
                               0.020707659639141,
                               0.020707659639141,
                               0.020707659639141};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX227[i];
            Quadrature->QuadY[i] = QuadY227[i];
            Quadrature->QuadW[i] = QuadW227[i];
        }
        break;
    case QuadTriangle36:
        Quadrature->QuadDim = 2;
        Quadrature->QuadratureType = QuadTriangle36;
        Quadrature->Order = 36;
        Quadrature->NumPoints = 36;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));

        DOUBLE QuadX236[36] = {0.0024666971526702431011246,
                               0.0123750604174400428286740,
                               0.0278110821153605899946903,
                               0.0452432465648983234141056,
                               0.0606792682628188653759516,
                               0.0705876315275886651035009,
                               0.0077918747012864289502865,
                               0.0390907007328242447896649,
                               0.0878504549759971942179959,
                               0.1429156829939483008828915,
                               0.1916754372371212433723286,
                               0.2229742632686590730894949,
                               0.0149015633666711548588335,
                               0.0747589734626491059232833,
                               0.1680095191211918581597473,
                               0.2733189621072580344218750,
                               0.3665695077658007727805511,
                               0.4264269178617787203755540,
                               0.0223868729780306273402513,
                               0.1123116817809537010264265,
                               0.2524035680765180367224332,
                               0.4106117416423277211023901,
                               0.5507036279378919596538822,
                               0.6406284367408150437483982,
                               0.0287653330125591175092659,
                               0.1443114869504166508651366,
                               0.3243183045887760296288604,
                               0.5276030957427396694825461,
                               0.7076099133810990204906943,
                               0.8231560673189565191520956,
                               0.0327753666144598859721881,
                               0.1644292415948274965753484,
                               0.3695299243723767501634825,
                               0.6011536484678384750779401,
                               0.8062543312453877009104986,
                               0.9379082062257553253914466};
        DOUBLE QuadY236[36] = {0.0705876315275886651035009,
                               0.0606792682628188653759516,
                               0.0452432465648983164752117,
                               0.0278110821153605830557964,
                               0.0123750604174400410939505,
                               0.0024666971526702413664012,
                               0.2229742632686590730894949,
                               0.1916754372371212433723286,
                               0.1429156829939483008828915,
                               0.0878504549759971942179959,
                               0.0390907007328242517285588,
                               0.0077918747012864220113926,
                               0.4264269178617787203755540,
                               0.3665695077658007727805511,
                               0.2733189621072580344218750,
                               0.1680095191211918304041717,
                               0.0747589734626490920454955,
                               0.0149015633666711444504926,
                               0.6406284367408150437483982,
                               0.5507036279378919596538822,
                               0.4106117416423276655912389,
                               0.2524035680765179812112819,
                               0.1123116817809537426597899,
                               0.0223868729780306585652738,
                               0.8231560673189565191520956,
                               0.7076099133810990204906943,
                               0.5276030957427395584602436,
                               0.3243183045887759741177092,
                               0.1443114869504166231095610,
                               0.0287653330125591244481598,
                               0.9379082062257553253914466,
                               0.8062543312453877009104986,
                               0.6011536484678384750779401,
                               0.3695299243723767501634825,
                               0.1644292415948275243309240,
                               0.0327753666144598998499760};

        DOUBLE QuadW236[36] = {0.0014970851224726396642983,
                               0.0031524435080471811462810,
                               0.0040887731830897217424892,
                               0.0040887731830897217424892,
                               0.0031524435080471811462810,
                               0.0014970851224726396642983,
                               0.0075305964253833514512881,
                               0.0158573346675929711946385,
                               0.0205672344575326543347771,
                               0.0205672344575326543347771,
                               0.0158573346675929711946385,
                               0.0075305964253833514512881,
                               0.0169030715938862825808986,
                               0.0355931519940526144840653,
                               0.0461649273027165268912242,
                               0.0461649273027165268912242,
                               0.0355931519940526144840653,
                               0.0169030715938862825808986,
                               0.0241212128085302492108699,
                               0.0507925431780953395843881,
                               0.0658787978015734215775367,
                               0.0658787978015734215775367,
                               0.0507925431780953395843881,
                               0.0241212128085302492108699,
                               0.0232217495339950702470944,
                               0.0488985245161156767412791,
                               0.0634222231814079995260158,
                               0.0634222231814079995260158,
                               0.0488985245161156767412791,
                               0.0232217495339950702470944,
                               0.0123885307053177209229977,
                               0.0260867886601656891187861,
                               0.0338350113600253410428564,
                               0.0338350113600253410428564,
                               0.0260867886601656891187861,
                               0.0123885307053177209229977};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX236[i];
            Quadrature->QuadY[i] = QuadY236[i];
            Quadrature->QuadW[i] = QuadW236[i];
        }
        break;

        // case QuadTetrahedral1:
        //     Quadrature->QuadratureType = QuadTetrahedral1;
        //     Quadrature->Order = 1;
        //     Quadrature->NumPoints = 1;
        //     Quadrature->QuadX = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
        //     Quadrature->QuadY = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
        //     Quadrature->QuadW = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
        //     DOUBLE Quad3X1[1] = {0.25};
        //     DOUBLE Quad3Y1[1] = {0.25};
        //     DOUBLE Quad3Z1[1] = {0.25};
        //     DOUBLE Quad3W1[1] = {1.0};
        //     for(i=0;i<Quadrature->NumPoints;i++)
        //     {
        //         Quadrature->QuadX[i] = Quad3X1[i];
        //         Quadrature->QuadY[i] = Quad3Y1[i];
        //         Quadrature->QuadZ[i] = Quad3Z1[i];
        //         Quadrature->QuadW[i] = Quad3W1[i];
        //     }
        break;
    case QuadTetrahedral1:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral1;
        Quadrature->Order = 1;
        Quadrature->NumPoints = 1;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX1[1] = {0.25};
        DOUBLE QuadY1[1] = {0.25};
        DOUBLE QuadZ1[1] = {0.25};
        DOUBLE QuadW1[1] = {1.0};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX1[i];
            Quadrature->QuadY[i] = QuadY1[i];
            Quadrature->QuadZ[i] = QuadZ1[i];
            Quadrature->QuadW[i] = QuadW1[i];
        }
        break;
    // case QuadTetrahedral4:
    //     Quadrature->QuadratureType = QuadTetrahedral4;
    //     Quadrature->Order = 4;
    //     Quadrature->NumPoints = 4;
    //     Quadrature->QuadX = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
    //     Quadrature->QuadY = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
    //     Quadrature->QuadZ = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
    //     Quadrature->QuadW = malloc(Quadrature->NumPoints*sizeof(DOUBLE));
    //     DOUBLE QuadX4[4] = {0.138196601125010515179541316563,
    //                      0.138196601125010515179541316563,
    //                      0.138196601125010515179541316563,
    //                      0.585410196624968454461376050311};
    //     DOUBLE QuadY4[4] = {0.138196601125010515179541316563,
    //                      0.138196601125010515179541316563,
    //                      0.585410196624968454461376050311,
    //                      0.138196601125010515179541316563};
    //     DOUBLE QuadZ4[4] = {0.138196601125010515179541316563,
    //                      0.585410196624968454461376050311,
    //                      0.138196601125010515179541316563,
    //                      0.138196601125010515179541316563};
    //     DOUBLE QuadW4[4] = {0.25, 0.25, 0.25, 0.25};
    //     for(i=0;i<Quadrature->NumPoints;i++)
    //     {
    //         Quadrature->QuadX[i] = QuadX4[i];
    //         Quadrature->QuadY[i] = QuadY4[i];
    //         Quadrature->QuadZ[i] = QuadZ4[i];
    //         Quadrature->QuadW[i] = QuadW4[i];
    //     }
    //     break;
    /*lhc 1.12更新*/
    case QuadTetrahedral4:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral4;
        Quadrature->Order = 2;
        Quadrature->NumPoints = 4;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX4[4] = {.13819660112501051517954131656343619,
                            1.0 - 3.0 * .13819660112501051517954131656343619,
                            .13819660112501051517954131656343619,
                            .13819660112501051517954131656343619};
        DOUBLE QuadY4[4] = {.13819660112501051517954131656343619,
                            .13819660112501051517954131656343619,
                            1.0 - 3.0 * .13819660112501051517954131656343619,
                            .13819660112501051517954131656343619};
        DOUBLE QuadZ4[4] = {.13819660112501051517954131656343619,
                            .13819660112501051517954131656343619,
                            .13819660112501051517954131656343619,
                            1.0 - 3.0 * .13819660112501051517954131656343619};
        DOUBLE QuadW4[4] = {0.25, 0.25, 0.25, 0.25};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX4[i];
            Quadrature->QuadY[i] = QuadY4[i];
            Quadrature->QuadZ[i] = QuadZ4[i];
            Quadrature->QuadW[i] = QuadW4[i];
        }
        break;
    case QuadTetrahedral11:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral11;
        Quadrature->Order = 11;
        Quadrature->NumPoints = 11;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX311[11] = {.25,
                               .0714285714285714285714285714286,
                               .785714285714285714285714285714,
                               .0714285714285714285714285714286,
                               .0714285714285714285714285714286,
                               .399403576166799204996102147462,
                               .399403576166799204996102147462,
                               .100596423833200795003897852538,
                               .100596423833200795003897852538,
                               .100596423833200795003897852538,
                               .399403576166799204996102147462};
        DOUBLE QuadY311[11] = {.25,
                               .0714285714285714285714285714286,
                               .0714285714285714285714285714286,
                               .785714285714285714285714285714,
                               .0714285714285714285714285714286,
                               .399403576166799204996102147462,
                               .100596423833200795003897852538,
                               .399403576166799204996102147462,
                               .100596423833200795003897852538,
                               .399403576166799204996102147462,
                               .100596423833200795003897852538};
        DOUBLE QuadZ311[11] = {.25,
                               .0714285714285714285714285714286,
                               .0714285714285714285714285714286,
                               .0714285714285714285714285714286,
                               .785714285714285714285714285714,
                               .100596423833200795003897852538,
                               .399403576166799204996102147462,
                               .399403576166799204996102147462,
                               .399403576166799204996102147462,
                               .100596423833200795003897852538,
                               .100596423833200795003897852538};
        DOUBLE QuadW311[11] = {-0.07893333333333333333333,
                               0.045733333333333333333333333333,
                               0.045733333333333333333333333333,
                               0.045733333333333333333333333333,
                               0.045733333333333333333333333333,
                               0.149333333333333333333333333333,
                               0.149333333333333333333333333333,
                               0.149333333333333333333333333333,
                               0.149333333333333333333333333333,
                               0.149333333333333333333333333333,
                               0.149333333333333333333333333333};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX311[i];
            Quadrature->QuadY[i] = QuadY311[i];
            Quadrature->QuadZ[i] = QuadZ311[i];
            Quadrature->QuadW[i] = QuadW311[i];
        }
        break;
    case QuadTetrahedral15:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral15;
        Quadrature->Order = 15;
        Quadrature->NumPoints = 15;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX15[15] = {0.333333333333333333333333333333333,
                              0.333333333333333333333333333333333,
                              0,
                              0.333333333333333333333333333333333,
                              0.25,
                              0.909090909090909090909090909090909e-1,
                              0.909090909090909090909090909090909e-1,
                              0.727272727272727272727272727272727,
                              0.909090909090909090909090909090909e-1,
                              0.665501535736642982398804642263025e-1,
                              0.433449846426335701760119535773697,
                              0.433449846426335701760119535773697,
                              0.665501535736642982398804642263025e-1,
                              0.665501535736642982398804642263025e-1,
                              0.433449846426335701760119535773697};
        DOUBLE QuadY15[15] = {0.333333333333333333333333333333333,
                              0,
                              0.333333333333333333333333333333333,
                              0.333333333333333333333333333333333,
                              0.25,
                              0.909090909090909090909090909090909e-1,
                              0.727272727272727272727272727272727,
                              0.909090909090909090909090909090909e-1,
                              0.909090909090909090909090909090909e-1,
                              0.433449846426335701760119535773697,
                              0.665501535736642982398804642263025e-1,
                              0.433449846426335701760119535773697,
                              0.665501535736642982398804642263025e-1,
                              0.433449846426335701760119535773697,
                              0.665501535736642982398804642263025e-1};
        DOUBLE QuadZ15[15] = {0,
                              0.333333333333333333333333333333333,
                              0.333333333333333333333333333333333,
                              0.333333333333333333333333333333333,
                              0.25,
                              0.727272727272727272727272727272727,
                              0.90909090909090909090909090909091e-1,
                              0.909090909090909090909090909090911e-1,
                              0.909090909090909090909090909090911e-1,
                              0.433449846426335701760119535773699,
                              0.433449846426335701760119535773699,
                              0.66550153573664298239880464226304e-1,
                              0.433449846426335701760119535773699,
                              0.66550153573664298239880464226304e-1,
                              0.665501535736642982398804642263035e-1};
        DOUBLE QuadW15[15] = {0.602678571428571428571428571428571e-2 * 6,
                              0.602678571428571428571428571428571e-2 * 6,
                              0.602678571428571428571428571428571e-2 * 6,
                              0.602678571428571428571428571428571e-2 * 6,
                              0.302836780970891758063769725577305e-1 * 6,
                              0.116452490860289694108936091443380e-1 * 6,
                              0.116452490860289694108936091443380e-1 * 6,
                              0.116452490860289694108936091443380e-1 * 6,
                              0.116452490860289694108936091443380e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6,
                              0.109491415613864593456430191124068e-1 * 6};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX15[i];
            Quadrature->QuadY[i] = QuadY15[i];
            Quadrature->QuadZ[i] = QuadZ15[i];
            Quadrature->QuadW[i] = QuadW15[i];
        }
        break;

    case QuadTetrahedral35:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral35;
        Quadrature->Order = 35;
        Quadrature->NumPoints = 35;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX35[35] = {0.0267367755543735,
                              0.9197896733368800,
                              0.0267367755543735,
                              0.0267367755543735,
                              0.7477598884818090,
                              0.1740356302468940,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.1740356302468940,
                              0.7477598884818090,
                              0.1740356302468940,
                              0.7477598884818090,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.4547545999844830,
                              0.0452454000155172,
                              0.0452454000155172,
                              0.4547545999844830,
                              0.4547545999844830,
                              0.0452454000155172,
                              0.2232010379623150,
                              0.5031186450145980,
                              0.2232010379623150,
                              0.2232010379623150,
                              0.5031186450145980,
                              0.2232010379623150,
                              0.0504792790607720,
                              0.0504792790607720,
                              0.0504792790607720,
                              0.5031186450145980,
                              0.2232010379623150,
                              0.2232010379623150,
                              0.2500000000000000};
        DOUBLE QuadY35[35] = {0.0267367755543735,
                              0.0267367755543735,
                              0.9197896733368800,
                              0.0267367755543735,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.7477598884818090,
                              0.1740356302468940,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.7477598884818090,
                              0.1740356302468940,
                              0.0391022406356488,
                              0.0391022406356488,
                              0.1740356302468940,
                              0.7477598884818090,
                              0.0452454000155172,
                              0.4547545999844830,
                              0.0452454000155172,
                              0.4547545999844830,
                              0.0452454000155172,
                              0.4547545999844830,
                              0.2232010379623150,
                              0.2232010379623150,
                              0.5031186450145980,
                              0.0504792790607720,
                              0.0504792790607720,
                              0.0504792790607720,
                              0.2232010379623150,
                              0.5031186450145980,
                              0.2232010379623150,
                              0.2232010379623150,
                              0.5031186450145980,
                              0.2232010379623150,
                              0.2500000000000000};
        DOUBLE QuadZ35[35] = {
            0.0267367755543735,
            0.0267367755543735,
            0.0267367755543735,
            0.9197896733368800,
            0.0391022406356488,
            0.0391022406356488,
            0.0391022406356488,
            0.0391022406356488,
            0.7477598884818090,
            0.1740356302468940,
            0.0391022406356488,
            0.0391022406356488,
            0.7477598884818090,
            0.1740356302468940,
            0.7477598884818090,
            0.1740356302468940,
            0.0452454000155172,
            0.0452454000155172,
            0.4547545999844830,
            0.0452454000155172,
            0.4547545999844830,
            0.4547545999844830,
            0.0504792790607720,
            0.0504792790607720,
            0.0504792790607720,
            0.2232010379623150,
            0.2232010379623150,
            0.5031186450145980,
            0.2232010379623150,
            0.2232010379623150,
            0.5031186450145980,
            0.2232010379623150,
            0.2232010379623150,
            0.5031186450145980,
            0.2500000000000000};
        DOUBLE QuadW35[35] = {0.0021900463965388,
                              0.0021900463965388,
                              0.0021900463965388,
                              0.0021900463965388,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0143395670177665,
                              0.0250305395686746,
                              0.0250305395686746,
                              0.0250305395686746,
                              0.0250305395686746,
                              0.0250305395686746,
                              0.0250305395686746,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0479839333057554,
                              0.0931745731195340};

        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX35[i];
            Quadrature->QuadY[i] = QuadY35[i];
            Quadrature->QuadZ[i] = QuadZ35[i];
            Quadrature->QuadW[i] = QuadW35[i];
        }
        break;
    case QuadTetrahedral56:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral56;
        Quadrature->Order = 56;
        Quadrature->NumPoints = 56;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX56[56] = {0.0149520651530592,
                              0.9551438045408220,
                              0.0149520651530592,
                              0.0149520651530592,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.7799760084415400,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.1518319491659370,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.3549340560639790,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.5526556431060170,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1344783347929940,
                              0.5965649956210169,
                              0.1344783347929940,
                              0.1344783347929940};
        DOUBLE QuadY56[56] = {0.0149520651530592,
                              0.0149520651530592,
                              0.9551438045408220,
                              0.0149520651530592,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.7799760084415400,
                              0.1518319491659370,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.3549340560639790,
                              0.5526556431060170,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.1344783347929940,
                              0.1344783347929940,
                              0.5965649956210169,
                              0.1344783347929940};
        DOUBLE QuadZ56[56] = {0.0149520651530592,
                              0.0149520651530592,
                              0.0149520651530592,
                              0.9551438045408220,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.0340960211962615,
                              0.0340960211962615,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.1518319491659370,
                              0.7799760084415400,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.0462051504150017,
                              0.0462051504150017,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.5526556431060170,
                              0.3549340560639790,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.0055147549744775,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.2281904610687610,
                              0.2281904610687610,
                              0.5381043228880020,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.0992057202494530,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.3523052600879940,
                              0.3523052600879940,
                              0.1961837595745600,
                              0.1344783347929940,
                              0.1344783347929940,
                              0.1344783347929940,
                              0.5965649956210169};
        DOUBLE QuadW56[56] = {0.0010373112336140,
                              0.0010373112336140,
                              0.0010373112336140,
                              0.0010373112336140,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0096016645399480,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0164493976798232,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0153747766513310,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0293520118375230,
                              0.0366291366405108,
                              0.0366291366405108,
                              0.0366291366405108,
                              0.0366291366405108};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX56[i];
            Quadrature->QuadY[i] = QuadY56[i];
            Quadrature->QuadZ[i] = QuadZ56[i];
            Quadrature->QuadW[i] = QuadW56[i];
        }
        break;
    case QuadTetrahedral127:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral127;
        Quadrature->Order = 12;
        Quadrature->NumPoints = 127;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX127[127] = {.25000000000000000000000000000000000,
                                .19318721110347230000000000000000000,
                                1. - 3. * .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                .01811701371436566878506928822499717,
                                1. - 3. * .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                .10700751831426066518406159227423033,
                                1. - 3. * .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                .29936173715970702940603127680004538,
                                1. - 3. * .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                .33333033333333333042835213613025030,
                                1. - 3. * .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                .16575369007421640000000000000000000,
                                1. - 3. * .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .59982639757597731668263005976738196,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .47400425629911050000000000000000000,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .83056291375422969598432041821082569,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .02265922072588833582931396831630072,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .47765370899783134571567376444973682,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .81083799468092699988474915243749073,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .22646235632397177636617160407210034,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .65250697573013212016385330106711095,
                                .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034,
                                .22646235632397177636617160407210034,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605};
        DOUBLE QuadY127[127] = {.25000000000000000000000000000000000,
                                .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                1. - 3. * .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                1. - 3. * .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                1. - 3. * .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                1. - 3. * .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                1. - 3. * .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                1. - 3. * .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                .5 - .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .59982639757597731668263005976738196,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .47400425629911050000000000000000000,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .83056291375422969598432041821082569,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .02265922072588833582931396831630072,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .47765370899783134571567376444973682,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .81083799468092699988474915243749073,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034};
        DOUBLE QuadZ127[127] = {.25000000000000000000000000000000000,
                                .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                .19318721110347230000000000000000000,
                                1. - 3. * .19318721110347230000000000000000000,
                                .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                .01811701371436566878506928822499717,
                                1. - 3. * .01811701371436566878506928822499717,
                                .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                .10700751831426066518406159227423033,
                                1. - 3. * .10700751831426066518406159227423033,
                                .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                .29936173715970702940603127680004538,
                                1. - 3. * .29936173715970702940603127680004538,
                                .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                .33333033333333333042835213613025030,
                                1. - 3. * .33333033333333333042835213613025030,
                                .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                .16575369007421640000000000000000000,
                                1. - 3. * .16575369007421640000000000000000000,
                                .5 - .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                .5 - .04009986052352575650366980228640728,
                                .04009986052352575650366980228640728,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .59982639757597731668263005976738196,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                1. - 2. * .01951844463761131301132122485607343 - .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                .59982639757597731668263005976738196,
                                .01951844463761131301132122485607343,
                                .01951844463761131301132122485607343,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .47400425629911050000000000000000000,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                1. - 2. * .24970741896308715787490891769354198 - .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                .47400425629911050000000000000000000,
                                .24970741896308715787490891769354198,
                                .24970741896308715787490891769354198,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .83056291375422969598432041821082569,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                1. - 2. * .07674205857869954726322831328843659 - .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                .83056291375422969598432041821082569,
                                .07674205857869954726322831328843659,
                                .07674205857869954726322831328843659,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .02265922072588833582931396831630072,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                1. - 2. * .43011409627915217536723647418133112 - .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                .02265922072588833582931396831630072,
                                .43011409627915217536723647418133112,
                                .43011409627915217536723647418133112,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .47765370899783134571567376444973682,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                1. - 2. * .12197854304894211937147375564906792 - .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                .47765370899783134571567376444973682,
                                .12197854304894211937147375564906792,
                                .12197854304894211937147375564906792,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .81083799468092699988474915243749073,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                1. - 2. * .01480482319031682427540691439704854 - .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                .81083799468092699988474915243749073,
                                .01480482319031682427540691439704854,
                                .01480482319031682427540691439704854,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .02251830769546778956654013747639605,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                1. - .65250697573013212016385330106711095 - .22646235632397177636617160407210034 - .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034,
                                .65250697573013212016385330106711095,
                                .02251830769546778956654013747639605,
                                .22646235632397177636617160407210034,
                                .02251830769546778956654013747639605,
                                .65250697573013212016385330106711095,
                                .22646235632397177636617160407210034,
                                .65250697573013212016385330106711095};
        DOUBLE QuadW127[127] = {.02340581914868067999082580773836836,
                                .00484469946470415656870798306091558,
                                .00484469946470415656870798306091558,
                                .00484469946470415656870798306091558,
                                .00484469946470415656870798306091558,
                                .00079865303812732982185563521014343,
                                .00079865303812732982185563521014343,
                                .00079865303812732982185563521014343,
                                .00079865303812732982185563521014343,
                                .01311872008808756207964488505025527,
                                .01311872008808756207964488505025527,
                                .01311872008808756207964488505025527,
                                .01311872008808756207964488505025527,
                                .02352182961292765917274505054313770,
                                .02352182961292765917274505054313770,
                                .02352182961292765917274505054313770,
                                .02352182961292765917274505054313770,
                                .00210860882494149803857437048649497,
                                .00210860882494149803857437048649497,
                                .00210860882494149803857437048649497,
                                .00210860882494149803857437048649497,
                                .00047839298963616600187228601742259,
                                .00047839298963616600187228601742259,
                                .00047839298963616600187228601742259,
                                .00047839298963616600187228601742259,
                                .00204546234216855322941711800170502,
                                .00204546234216855322941711800170502,
                                .00204546234216855322941711800170502,
                                .00204546234216855322941711800170502,
                                .00204546234216855322941711800170502,
                                .00204546234216855322941711800170502,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .00334576331671817115245418532677178,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .01181044822479275264785338274950585,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00290156990282342152841364375092118,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .00949250645501753676094846901252898,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .02094018358085748583183796760479700,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00171435866337409051521874943702732,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409,
                                .00759915954173370886076474450830409};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX127[i];
            Quadrature->QuadY[i] = QuadY127[i];
            Quadrature->QuadZ[i] = QuadZ127[i];
            Quadrature->QuadW[i] = QuadW127[i];
        }
        break;
    case QuadTetrahedral304:
        Quadrature->QuadDim = 3;
        Quadrature->QuadratureType = QuadTetrahedral304;
        Quadrature->Order = 16;
        Quadrature->NumPoints = 304;
        Quadrature->QuadX = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadY = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadZ = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        Quadrature->QuadW = malloc(Quadrature->NumPoints * sizeof(DOUBLE));
        DOUBLE QuadX304[304] = {.32967147384406063619375162258031196,
                                1. - 3. * .32967147384406063619375162258031196,
                                .32967147384406063619375162258031196,
                                .32967147384406063619375162258031196,
                                .11204210441737877557977355269085785,
                                1. - 3. * .11204210441737877557977355269085785,
                                .11204210441737877557977355269085785,
                                .11204210441737877557977355269085785,
                                .28044602591109291917326968759374922,
                                1. - 3. * .28044602591109291917326968759374922,
                                .28044602591109291917326968759374922,
                                .28044602591109291917326968759374922,
                                .03942164444076165530186041315194336,
                                1. - 3. * .03942164444076165530186041315194336,
                                .03942164444076165530186041315194336,
                                .03942164444076165530186041315194336,
                                .07491741856476755331936887653337858,
                                .5 - .07491741856476755331936887653337858,
                                .5 - .07491741856476755331936887653337858,
                                .07491741856476755331936887653337858,
                                .07491741856476755331936887653337858,
                                .5 - .07491741856476755331936887653337858,
                                .33569310295563458148449388353009909,
                                .5 - .33569310295563458148449388353009909,
                                .5 - .33569310295563458148449388353009909,
                                .33569310295563458148449388353009909,
                                .33569310295563458148449388353009909,
                                .5 - .33569310295563458148449388353009909,
                                .04904898759556674851708836681755199,
                                .04904898759556674851708836681755199,
                                .76468706758018040128352585522097679,
                                .76468706758018040128352585522097679,
                                1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                                1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                                .04904898759556674851708836681755199,
                                .04904898759556674851708836681755199,
                                1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                                .04904898759556674851708836681755199,
                                .04904898759556674851708836681755199,
                                .76468706758018040128352585522097679,
                                .01412609568309253397023383730394044,
                                .01412609568309253397023383730394044,
                                .23282680458942511289537501209852151,
                                .23282680458942511289537501209852151,
                                1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                                1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                                .01412609568309253397023383730394044,
                                .01412609568309253397023383730394044,
                                1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                                .01412609568309253397023383730394044,
                                .01412609568309253397023383730394044,
                                .23282680458942511289537501209852151,
                                .06239652058154325208658365747436279,
                                .06239652058154325208658365747436279,
                                .28324176830779468643565585504428986,
                                .28324176830779468643565585504428986,
                                1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                                1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                                .06239652058154325208658365747436279,
                                .06239652058154325208658365747436279,
                                1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                                .06239652058154325208658365747436279,
                                .06239652058154325208658365747436279,
                                .28324176830779468643565585504428986,
                                .18909592756965597717224790600688877,
                                .18909592756965597717224790600688877,
                                .01283187405611823970074593204830391,
                                .01283187405611823970074593204830391,
                                1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                                1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                                .18909592756965597717224790600688877,
                                .18909592756965597717224790600688877,
                                1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                                .18909592756965597717224790600688877,
                                .18909592756965597717224790600688877,
                                .01283187405611823970074593204830391,
                                .27501760012954439487094433006628105,
                                .27501760012954439487094433006628105,
                                .38727096031949030862955433360477075,
                                .38727096031949030862955433360477075,
                                1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                                1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                                .27501760012954439487094433006628105,
                                .27501760012954439487094433006628105,
                                1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                                .27501760012954439487094433006628105,
                                .27501760012954439487094433006628105,
                                .38727096031949030862955433360477075,
                                .00594489825256994551853672034278369,
                                .00594489825256994551853672034278369,
                                .03723805935523541954759090988386045,
                                .03723805935523541954759090988386045,
                                1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                                1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                                .00594489825256994551853672034278369,
                                .00594489825256994551853672034278369,
                                1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                                .00594489825256994551853672034278369,
                                .00594489825256994551853672034278369,
                                .03723805935523541954759090988386045,
                                .11830580710999444802339227394482751,
                                .11830580710999444802339227394482751,
                                .74829410783088587547640692441989412,
                                .74829410783088587547640692441989412,
                                1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                                1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                                .11830580710999444802339227394482751,
                                .11830580710999444802339227394482751,
                                1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                                .11830580710999444802339227394482751,
                                .11830580710999444802339227394482751,
                                .74829410783088587547640692441989412,
                                .51463578878883950389557459439302904,
                                .51463578878883950389557459439302904,
                                .39080211141879205490649181473579692,
                                .39080211141879205490649181473579692,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                .08011846127872502380489538529429122,
                                .08011846127872502380489538529429122,
                                .39080211141879205490649181473579692,
                                .39080211141879205490649181473579692,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                .08011846127872502380489538529429122,
                                .08011846127872502380489538529429122,
                                .51463578878883950389557459439302904,
                                .51463578878883950389557459439302904,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                                .08011846127872502380489538529429122,
                                .08011846127872502380489538529429122,
                                .51463578878883950389557459439302904,
                                .51463578878883950389557459439302904,
                                .39080211141879205490649181473579692,
                                .39080211141879205490649181473579692,
                                .16457394683790985618365497081112626,
                                .16457394683790985618365497081112626,
                                .06995093322963368912711775566495230,
                                .06995093322963368912711775566495230,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                .31025854986272726854690199497600369,
                                .31025854986272726854690199497600369,
                                .06995093322963368912711775566495230,
                                .06995093322963368912711775566495230,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                .31025854986272726854690199497600369,
                                .31025854986272726854690199497600369,
                                .16457394683790985618365497081112626,
                                .16457394683790985618365497081112626,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                                .31025854986272726854690199497600369,
                                .31025854986272726854690199497600369,
                                .16457394683790985618365497081112626,
                                .16457394683790985618365497081112626,
                                .06995093322963368912711775566495230,
                                .06995093322963368912711775566495230,
                                .03435867950145695762069918674064249,
                                .03435867950145695762069918674064249,
                                .85571569922057519215400444464572650,
                                .85571569922057519215400444464572650,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                .10852408019289846676496859793631046,
                                .10852408019289846676496859793631046,
                                .85571569922057519215400444464572650,
                                .85571569922057519215400444464572650,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                .10852408019289846676496859793631046,
                                .10852408019289846676496859793631046,
                                .03435867950145695762069918674064249,
                                .03435867950145695762069918674064249,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                                .10852408019289846676496859793631046,
                                .10852408019289846676496859793631046,
                                .03435867950145695762069918674064249,
                                .03435867950145695762069918674064249,
                                .85571569922057519215400444464572650,
                                .85571569922057519215400444464572650,
                                .66253175448505093003700617661466167,
                                .66253175448505093003700617661466167,
                                .01098323448764900444073955046178437,
                                .01098323448764900444073955046178437,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                .24838249878149546329329939696635675,
                                .24838249878149546329329939696635675,
                                .01098323448764900444073955046178437,
                                .01098323448764900444073955046178437,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                .24838249878149546329329939696635675,
                                .24838249878149546329329939696635675,
                                .66253175448505093003700617661466167,
                                .66253175448505093003700617661466167,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                                .24838249878149546329329939696635675,
                                .24838249878149546329329939696635675,
                                .66253175448505093003700617661466167,
                                .66253175448505093003700617661466167,
                                .01098323448764900444073955046178437,
                                .01098323448764900444073955046178437,
                                .01226898678006518846085017274454891,
                                .01226898678006518846085017274454891,
                                .01878187449597509734542135006798163,
                                .01878187449597509734542135006798163,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                                .39600912110670350629865233312005293,
                                .39600912110670350629865233312005293,
                                .01878187449597509734542135006798163,
                                .01878187449597509734542135006798163,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,

                                .39600912110670350629865233312005293,
                                .39600912110670350629865233312005293,
                                .01226898678006518846085017274454891,
                                .01226898678006518846085017274454891,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                                1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                                .39600912110670350629865233312005293,
                                .39600912110670350629865233312005293,
                                .01226898678006518846085017274454891,
                                .01226898678006518846085017274454891,
                                .01878187449597509734542135006798163,
                                .01878187449597509734542135006798163,
                                .20546049913241051283959332798606714,
                                .20546049913241051283959332798606714,
                                .13624495088858952521524101996191479,
                                .13624495088858952521524101996191479,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                .06367516197137305787610880659547363,
                                .06367516197137305787610880659547363,
                                .13624495088858952521524101996191479,
                                .13624495088858952521524101996191479,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                .06367516197137305787610880659547363,
                                .06367516197137305787610880659547363,
                                .20546049913241051283959332798606714,
                                .20546049913241051283959332798606714,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                                .06367516197137305787610880659547363,
                                .06367516197137305787610880659547363,
                                .20546049913241051283959332798606714,
                                .20546049913241051283959332798606714,
                                .13624495088858952521524101996191479,
                                .13624495088858952521524101996191479,
                                .46106788607969942837687869968339046,
                                .46106788607969942837687869968339046,
                                .13875357096122530918456923931101452,
                                .13875357096122530918456923931101452,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                .17576504661391044061489056539266476,
                                .17576504661391044061489056539266476,
                                .13875357096122530918456923931101452,
                                .13875357096122530918456923931101452,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                .17576504661391044061489056539266476,
                                .17576504661391044061489056539266476,
                                .46106788607969942837687869968339046,
                                .46106788607969942837687869968339046,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                                .17576504661391044061489056539266476,
                                .17576504661391044061489056539266476,
                                .46106788607969942837687869968339046,
                                .46106788607969942837687869968339046,
                                .13875357096122530918456923931101452,
                                .13875357096122530918456923931101452,
                                .01344788610299629067374252417192062,
                                .01344788610299629067374252417192062,
                                .32213983065389956084144880408640740,
                                .32213983065389956084144880408640740,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                .47799425320067043636083307018835951,
                                .47799425320067043636083307018835951,
                                .32213983065389956084144880408640740,
                                .32213983065389956084144880408640740,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                .47799425320067043636083307018835951,
                                .47799425320067043636083307018835951,
                                .01344788610299629067374252417192062,
                                .01344788610299629067374252417192062,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                                .47799425320067043636083307018835951,
                                .47799425320067043636083307018835951,
                                .01344788610299629067374252417192062,
                                .01344788610299629067374252417192062,
                                .32213983065389956084144880408640740,
                                .32213983065389956084144880408640740};
        DOUBLE QuadY304[304] = {
            .32967147384406063619375162258031196,
            .32967147384406063619375162258031196,
            1. - 3. * .32967147384406063619375162258031196,
            .32967147384406063619375162258031196,
            .11204210441737877557977355269085785,
            .11204210441737877557977355269085785,
            1. - 3. * .11204210441737877557977355269085785,
            .11204210441737877557977355269085785,
            .28044602591109291917326968759374922,
            .28044602591109291917326968759374922,
            1. - 3. * .28044602591109291917326968759374922,
            .28044602591109291917326968759374922,
            .03942164444076165530186041315194336,
            .03942164444076165530186041315194336,
            1. - 3. * .03942164444076165530186041315194336,
            .03942164444076165530186041315194336,
            .5 - .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .5 - .07491741856476755331936887653337858,
            .5 - .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .5 - .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            .5 - .33569310295563458148449388353009909,
            .5 - .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            .76468706758018040128352585522097679,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .04904898759556674851708836681755199,
            .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .23282680458942511289537501209852151,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .01412609568309253397023383730394044,
            .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .28324176830779468643565585504428986,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .06239652058154325208658365747436279,
            .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .01283187405611823970074593204830391,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .18909592756965597717224790600688877,
            .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .38727096031949030862955433360477075,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .27501760012954439487094433006628105,
            .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .03723805935523541954759090988386045,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .00594489825256994551853672034278369,
            .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .74829410783088587547640692441989412,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .11830580710999444802339227394482751,
            .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .39080211141879205490649181473579692,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            .39080211141879205490649181473579692,
            .39080211141879205490649181473579692,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .51463578878883950389557459439302904,
            .51463578878883950389557459439302904,
            .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .51463578878883950389557459439302904,
            .06995093322963368912711775566495230,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            .06995093322963368912711775566495230,
            .06995093322963368912711775566495230,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .16457394683790985618365497081112626,
            .16457394683790985618365497081112626,
            .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .16457394683790985618365497081112626,
            .85571569922057519215400444464572650,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            .85571569922057519215400444464572650,
            .85571569922057519215400444464572650,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .03435867950145695762069918674064249,
            .03435867950145695762069918674064249,
            .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .03435867950145695762069918674064249,
            .01098323448764900444073955046178437,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            .01098323448764900444073955046178437,
            .01098323448764900444073955046178437,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .66253175448505093003700617661466167,
            .66253175448505093003700617661466167,
            .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .66253175448505093003700617661466167,
            .01878187449597509734542135006798163,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            .01878187449597509734542135006798163,
            .01878187449597509734542135006798163,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01226898678006518846085017274454891,
            .01226898678006518846085017274454891,
            .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01226898678006518846085017274454891,
            .13624495088858952521524101996191479,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            .13624495088858952521524101996191479,
            .13624495088858952521524101996191479,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .20546049913241051283959332798606714,
            .20546049913241051283959332798606714,
            .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .20546049913241051283959332798606714,
            .13875357096122530918456923931101452,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            .13875357096122530918456923931101452,
            .13875357096122530918456923931101452,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .46106788607969942837687869968339046,
            .46106788607969942837687869968339046,
            .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .46106788607969942837687869968339046,
            .32213983065389956084144880408640740,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            .32213983065389956084144880408640740,
            .32213983065389956084144880408640740,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .01344788610299629067374252417192062,
            .01344788610299629067374252417192062,
            .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .01344788610299629067374252417192062,
        };
        DOUBLE QuadZ304[304] = {
            .32967147384406063619375162258031196,
            .32967147384406063619375162258031196,
            .32967147384406063619375162258031196,
            1. - 3. * .32967147384406063619375162258031196,
            .11204210441737877557977355269085785,
            .11204210441737877557977355269085785,
            .11204210441737877557977355269085785,
            1. - 3. * .11204210441737877557977355269085785,
            .28044602591109291917326968759374922,
            .28044602591109291917326968759374922,
            .28044602591109291917326968759374922,
            1. - 3. * .28044602591109291917326968759374922,
            .03942164444076165530186041315194336,
            .03942164444076165530186041315194336,
            .03942164444076165530186041315194336,
            1. - 3. * .03942164444076165530186041315194336,
            .5 - .07491741856476755331936887653337858,
            .5 - .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .5 - .07491741856476755331936887653337858,
            .07491741856476755331936887653337858,
            .5 - .33569310295563458148449388353009909,
            .5 - .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            .5 - .33569310295563458148449388353009909,
            .33569310295563458148449388353009909,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .76468706758018040128352585522097679,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .04904898759556674851708836681755199,
            .76468706758018040128352585522097679,
            .04904898759556674851708836681755199,
            .04904898759556674851708836681755199,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .23282680458942511289537501209852151,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .01412609568309253397023383730394044,
            .23282680458942511289537501209852151,
            .01412609568309253397023383730394044,
            .01412609568309253397023383730394044,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .28324176830779468643565585504428986,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .06239652058154325208658365747436279,
            .28324176830779468643565585504428986,
            .06239652058154325208658365747436279,
            .06239652058154325208658365747436279,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .01283187405611823970074593204830391,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .18909592756965597717224790600688877,
            .01283187405611823970074593204830391,
            .18909592756965597717224790600688877,
            .18909592756965597717224790600688877,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .38727096031949030862955433360477075,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .27501760012954439487094433006628105,
            .38727096031949030862955433360477075,
            .27501760012954439487094433006628105,
            .27501760012954439487094433006628105,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .03723805935523541954759090988386045,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .00594489825256994551853672034278369,
            .03723805935523541954759090988386045,
            .00594489825256994551853672034278369,
            .00594489825256994551853672034278369,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .74829410783088587547640692441989412,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .11830580710999444802339227394482751,
            .74829410783088587547640692441989412,
            .11830580710999444802339227394482751,
            .11830580710999444802339227394482751,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .39080211141879205490649181473579692,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .39080211141879205490649181473579692,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .51463578878883950389557459439302904,
            .08011846127872502380489538529429122,
            .39080211141879205490649181473579692,
            .51463578878883950389557459439302904,
            .39080211141879205490649181473579692,
            .08011846127872502380489538529429122,
            .51463578878883950389557459439302904,
            .08011846127872502380489538529429122,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .06995093322963368912711775566495230,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .06995093322963368912711775566495230,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .16457394683790985618365497081112626,
            .31025854986272726854690199497600369,
            .06995093322963368912711775566495230,
            .16457394683790985618365497081112626,
            .06995093322963368912711775566495230,
            .31025854986272726854690199497600369,
            .16457394683790985618365497081112626,
            .31025854986272726854690199497600369,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .85571569922057519215400444464572650,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .85571569922057519215400444464572650,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .03435867950145695762069918674064249,
            .10852408019289846676496859793631046,
            .85571569922057519215400444464572650,
            .03435867950145695762069918674064249,
            .85571569922057519215400444464572650,
            .10852408019289846676496859793631046,
            .03435867950145695762069918674064249,
            .10852408019289846676496859793631046,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .01098323448764900444073955046178437,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .01098323448764900444073955046178437,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .66253175448505093003700617661466167,
            .24838249878149546329329939696635675,
            .01098323448764900444073955046178437,
            .66253175448505093003700617661466167,
            .01098323448764900444073955046178437,
            .24838249878149546329329939696635675,
            .66253175448505093003700617661466167,
            .24838249878149546329329939696635675,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01878187449597509734542135006798163,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01878187449597509734542135006798163,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01226898678006518846085017274454891,
            .39600912110670350629865233312005293,
            .01878187449597509734542135006798163,
            .01226898678006518846085017274454891,
            .01878187449597509734542135006798163,
            .39600912110670350629865233312005293,
            .01226898678006518846085017274454891,
            .39600912110670350629865233312005293,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .13624495088858952521524101996191479,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .13624495088858952521524101996191479,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .20546049913241051283959332798606714,
            .06367516197137305787610880659547363,
            .13624495088858952521524101996191479,
            .20546049913241051283959332798606714,
            .13624495088858952521524101996191479,
            .06367516197137305787610880659547363,
            .20546049913241051283959332798606714,
            .06367516197137305787610880659547363,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .13875357096122530918456923931101452,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .13875357096122530918456923931101452,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .46106788607969942837687869968339046,
            .17576504661391044061489056539266476,
            .13875357096122530918456923931101452,
            .46106788607969942837687869968339046,
            .13875357096122530918456923931101452,
            .17576504661391044061489056539266476,
            .46106788607969942837687869968339046,
            .17576504661391044061489056539266476,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .32213983065389956084144880408640740,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .32213983065389956084144880408640740,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .01344788610299629067374252417192062,
            .47799425320067043636083307018835951,
            .32213983065389956084144880408640740,
            .01344788610299629067374252417192062,
            .32213983065389956084144880408640740,
            .47799425320067043636083307018835951,
            .01344788610299629067374252417192062,
            .47799425320067043636083307018835951,
        };
        DOUBLE QuadW304[304] = {.00403487893723887049265039601220684,
                                .00403487893723887049265039601220684,
                                .00403487893723887049265039601220684,
                                .00403487893723887049265039601220684,
                                .00526342800804762849619159416605960,
                                .00526342800804762849619159416605960,
                                .00526342800804762849619159416605960,
                                .00526342800804762849619159416605960,
                                .01078639106857763822747522856875491,
                                .01078639106857763822747522856875491,
                                .01078639106857763822747522856875491,
                                .01078639106857763822747522856875491,
                                .00189234002903099684224605393918402,
                                .00189234002903099684224605393918402,
                                .00189234002903099684224605393918402,
                                .00189234002903099684224605393918402,
                                .00601451100300045870650067420901317,
                                .00601451100300045870650067420901317,
                                .00601451100300045870650067420901317,
                                .00601451100300045870650067420901317,
                                .00601451100300045870650067420901317,
                                .00601451100300045870650067420901317,
                                .00869219123287302312727334847940610,
                                .00869219123287302312727334847940610,
                                .00869219123287302312727334847940610,
                                .00869219123287302312727334847940610,
                                .00869219123287302312727334847940610,
                                .00869219123287302312727334847940610,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00350762328578499275659307009126951,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00118866227331919807192940951748929,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00434522531446666750864079816261451,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00266068657020316603764648612019192,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00555317430212401341478384714866174,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00015314314053958076996931089638380,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00223120413671567928866289542090939,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00247237715624996985149659692307816,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00659612289181792506876909773112724,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00060043490676974216087269159340661,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00224233099687652742449689510982133,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00082444223624718372748546097521409,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00438801583526791034381358666584691,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00345427842574096232228714634129452,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535,
                                .00392928947333557070846156486164535};
        for (i = 0; i < Quadrature->NumPoints; i++)
        {
            Quadrature->QuadX[i] = QuadX304[i];
            Quadrature->QuadY[i] = QuadY304[i];
            Quadrature->QuadZ[i] = QuadZ304[i];
            Quadrature->QuadW[i] = QuadW304[i];
        }
        break;

        // #endif
    }
    // printf("W[0] = %2.14f\n",Quadrature->QuadW[0]);
    return Quadrature;
}

// 输出积分信息
void QuadraturePrint(QUADRATURE *Quadrature)
{
    OpenPFEM_Print("Output the quadrature information!\n");
    INT i, NumPoints = Quadrature->NumPoints;
    OpenPFEM_Print("Order = %d,  NumPoints = %d\n", Quadrature->Order, NumPoints);
    DOUBLE *X = Quadrature->QuadX, *Y = Quadrature->QuadY, *Z = Quadrature->QuadZ;
    DOUBLE *W = Quadrature->QuadW;
    switch (Quadrature->QuadratureType)
    {
    case QuadLine1:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f\n", X[i], W[i]);
        }
        break;
    case QuadLine2:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f\n", X[i], W[i]);
        }
        break;
    case QuadLine3:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f\n", X[i], W[i]);
        }
        break;
    case QuadLine4:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f\n", X[i], W[i]);
        }
        break;
    case QuadTriangle1:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle3:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle4:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle7:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle13:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle27:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTriangle36:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.14f,  %2.14f,  %2.14f\n", X[i], Y[i], W[i]);
        }
        break;
    case QuadTetrahedral1:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral4:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral11:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral15:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral35:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral56:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral127:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    case QuadTetrahedral304:
        for (i = 0; i < NumPoints; i++)
        {
            OpenPFEM_Print("%2.10f,  %2.10f,  %2.10f,  %2.10f\n", X[i], Y[i], Z[i], W[i]);
        }
        break;
    }
    return;
}

// QuadDim: 表示积分区域的维数
void QuadratureDestroy(QUADRATURE **Quadrature)
{
    if ((*Quadrature)->QuadDim >= 1)
        OpenPFEM_Free((*Quadrature)->QuadX);
    if ((*Quadrature)->QuadDim > 1)
    {
        OpenPFEM_Free((*Quadrature)->QuadY);
    }
    if ((*Quadrature)->QuadDim > 2)
    {
        OpenPFEM_Free((*Quadrature)->QuadZ);
    }
    OpenPFEM_Free((*Quadrature)->QuadW);
    OpenPFEM_Free((*Quadrature));
}