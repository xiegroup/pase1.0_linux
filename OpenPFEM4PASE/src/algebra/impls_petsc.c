#include "opsimpls.h"

#if defined(PETSC_USE)
void MatrixCreate_PETSc(MATRIX *matrix)
{
    matrix->if_petsc = true;
    matrix->data_petsc = NULL;
}

void MatrixDestroy_PETSc(MATRIX *matrix)
{
    MatDestroy((Mat *)(&(matrix->data_petsc)));
    matrix->if_petsc = false;
}

void MatrixSturctureCompare_PETSc(MATRIX *A, MATRIX *B, NONZERO_STRUCT *nonzero_struct)
{
    RaiseError("MatrixSturctureCompare_PETSc", "Not implemented!");
}

void MatrixScale_PETSc(DOUBLE alpha, MATRIX *A)
{
    MatScale((Mat)(A->data_petsc), alpha);
}

void MatrixAxpby_PETSc(DOUBLE alpha, MATRIX *A, DOUBLE beta, MATRIX *B, NONZERO_STRUCT nonzero_struct)
{
    if (beta != 1.0)
        MatScale((Mat)(B->data_petsc), beta);
    if (nonzero_struct == NONZERO_SAME)
    {
        MatAXPY((Mat)(B->data_petsc), alpha, (Mat)(A->data_petsc), SAME_NONZERO_PATTERN);
    }
    else if (nonzero_struct == NONZERO_SUBSET)
    {
        MatAXPY((Mat)(B->data_petsc), alpha, (Mat)(A->data_petsc), SUBSET_NONZERO_PATTERN);
    }
    else if (nonzero_struct == NONZERO_DIFF)
    {
        MatAXPY((Mat)(B->data_petsc), alpha, (Mat)(A->data_petsc), DIFFERENT_NONZERO_PATTERN);
    }
}

void VectorCreateByMatrix_PETSc(VECTOR **vector, MATRIX *matrix)
{
    DATATYPE type = matrix->type;
    VectorCreate(vector, type);
    (*vector)->local_length = matrix->local_nrows;
    (*vector)->global_length = matrix->global_nrows;
    (*vector)->start = matrix->rows_start;
    (*vector)->end = matrix->rows_end;
    (*vector)->comm = matrix->comm;
    Vec vec;
    MatCreateVecs((Mat)(matrix->data_petsc), NULL, &vec);
    VecZeroEntries(vec);
    (*vector)->data_petsc = (void *)vec;
}

void VectorCreateByMatrixT_PETSc(VECTOR **vector, MATRIX *matrix)
{
    DATATYPE type = matrix->type;
    VectorCreate(vector, type);
    (*vector)->local_length = matrix->local_ncols;
    (*vector)->global_length = matrix->global_ncols;
    (*vector)->start = matrix->cols_start;
    (*vector)->end = matrix->cols_end;
    (*vector)->comm = matrix->comm;
    Vec vec;
    MatCreateVecs((Mat)(matrix->data_petsc), &vec, NULL);
    VecZeroEntries(vec);
    (*vector)->data_petsc = (void *)vec;
}

void MatrixVectorMult_PETSc(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MatMult((Mat)(A->data_petsc), (Vec)(x->data_petsc), (Vec)(y->data_petsc));
}

void MatrixTransposeVectorMult_PETSc(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MatMultTranspose((Mat)(A->data_petsc), (Vec)(x->data_petsc), (Vec)(y->data_petsc));
}

void MatrixVectorMultAdd_PETSc(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MatMultAdd((Mat)(A->data_petsc), (Vec)(x->data_petsc), (Vec)(y->data_petsc), (Vec)(y->data_petsc));
}

void MatrixVectorMultSub_PETSc(MATRIX *A, VECTOR *x, VECTOR *y)
{
    VectorScale_PETSc(-1.0, y);
    MatMultAdd((Mat)(A->data_petsc), (Vec)(x->data_petsc), (Vec)(y->data_petsc), (Vec)(y->data_petsc));
    VectorScale_PETSc(-1.0, y);
}

void VectorCreate_PETSc(VECTOR *vector)
{
    vector->if_petsc = true;
    vector->data_petsc = NULL;
}

void VectorDestroy_PETSc(VECTOR *vector)
{
    VecDestroy((Vec *)(&(vector->data_petsc)));
    vector->if_petsc = false;
}

void VectorGetArray_PETSc(VECTOR *vector, DOUBLE **data)
{
    VecGetArray((Vec)(vector->data_petsc), data);
}

void VectorScale_PETSc(DOUBLE alpha, VECTOR *x)
{
#if defined(BLAS_USE)
    DOUBLE *data_x;
    VecGetArray((Vec)(x->data_petsc), &data_x);
    if (alpha == 0.0)
    {
        memset(x->data, 0.0, x->local_length * sizeof(DOUBLE));
    }
    else if (alpha != 1.0)
    {
        INT one = 1;
        // dscal_(&(x->local_length), &alpha, x->data, &one);
    }
#else
    VecScale((Vec)(x->data_petsc), alpha);
#endif
}

void VectorAxpby_PETSc(DOUBLE alpha, VECTOR *x, DOUBLE beta, VECTOR *y)
{
#if defined(BLAS_USE)
    DOUBLE *data_x, *data_y;
    VecGetArray((Vec)(x->data_petsc), &data_x);
    VecGetArray((Vec)(y->data_petsc), &data_y);
    if (beta == 0.0)
    {
        memset(data_y, 0.0, y->local_length * sizeof(DOUBLE));
    }
    else if (beta != 1.0)
    {
        INT one = 1;
        // dscal_(&(y->local_length), &beta, data_y, &one);
    }
    if (alpha == 0.0)
    {
        return;
    }
    INT one = 1;
    // daxpy_(&(y->local_length), &alpha, data_x, &one, data_y, &one);
#else
    VecAXPBY((Vec)(y->data_petsc), alpha, beta, (Vec)(x->data_petsc));
#endif
}

void VectorNorm_PETSc(VECTOR *vector, DOUBLE *alpha)
{
    VecNorm((Vec)(vector->data_petsc), NORM_2, alpha);
}

void SetRhsAsInitial(VECTOR *Rhs, VECTOR *Solution){
    VecCopy((Vec)(Rhs->data_petsc), (Vec)(Solution->data_petsc));
}

#endif