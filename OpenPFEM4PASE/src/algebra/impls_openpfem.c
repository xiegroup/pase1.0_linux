#include "opsimpls.h"

#define CSRMATMULTLOOPSIZE 2
#if (CSRMATMULTLOOPSIZE == 1)
#define CSRMultRowDotPlus(sum, ent, kc, x, n) \
    do                                        \
    {                                         \
        int _ii;                              \
        for (_ii = 0; _ii < n; _ii++)         \
        {                                     \
            sum += ent[_ii] * x[kc[_ii]];     \
        }                                     \
    } while (0)
#elif (CSRMATMULTLOOPSIZE == 2)
#define CSRMultRowDotPlus(sum, ent, kc, x, n)                   \
    do                                                          \
    {                                                           \
        int _ii, _c1, _c2;                                      \
        for (_ii = 0; _ii < (n)-1; _ii += 2)                    \
        {                                                       \
            _c1 = kc[_ii];                                      \
            _c2 = kc[_ii + 1];                                  \
            sum += (ent[_ii] * x[_c1] + ent[_ii + 1] * x[_c2]); \
        }                                                       \
        if ((n)&0x1)                                            \
        {                                                       \
            sum += ent[_ii] * x[kc[_ii]];                       \
        }                                                       \
    } while (0)
#elif (CSRMATMULTLOOPSIZE == 4)
#define CSRMultRowDotPlus(sum, ent, kc, x, n)              \
    do                                                     \
    {                                                      \
        int _ii;                                           \
        for (_ii = 0; _ii < (n)-3; _ii += 4)               \
        {                                                  \
            sum += ent[0] * x[kc[0]] + ent[1] * x[kc[1]] + \
                   ent[2] * x[kc[2]] + ent[3] * x[kc[3]];  \
            kc += 4;                                       \
            ent += 4;                                      \
        }                                                  \
        switch ((n)&0x3)                                   \
        {                                                  \
        case 3:                                            \
            sum += *ent++ * x[*kc++];                      \
        case 2:                                            \
            sum += *ent++ * x[*kc++];                      \
        case 1:                                            \
            sum += *ent++ * x[*kc++];                      \
        }                                                  \
    } while (0)
#endif

#define SetDoubleNeigAlltoallBegin(x, sind, n, stmp, rtmp, snum, sdsps, rnum, rdsps, comm, req) \
    do                                                                                          \
    {                                                                                           \
        int _ii;                                                                                \
        for (_ii = 0; _ii < n; _ii++)                                                           \
        {                                                                                       \
            stmp[_ii] = x[sind[_ii]];                                                           \
        }                                                                                       \
        MPI_Ineighbor_alltoallv(stmp, snum, sdsps, MPI_DOUBLE,                                  \
                                rtmp, rnum, rdsps, MPI_DOUBLE, comm, &(req));                   \
    } while (0)

#define SetDoubleNeigAlltoallEnd(req)        \
    do                                       \
    {                                        \
        MPI_Wait(&(req), MPI_STATUS_IGNORE); \
        MPI_Request_free(&(req));            \
    } while (0)

#define AddDoubleNeigAlltoallBegin(stmp, rtmp, snum, sdsps, rnum, rdsps, comm, req) \
    do                                                                              \
    {                                                                               \
        MPI_Neighbor_alltoallv(stmp, snum, sdsps, MPI_DOUBLE,                       \
                               rtmp, rnum, rdsps, MPI_DOUBLE, comm);                \
    } while (0)

#define AddDoubleNeigAlltoallEnd(y, rtmp, rind, n) \
    do                                             \
    {                                              \
        int _ii;                                   \
        for (_ii = 0; _ii < n; _ii++)              \
        {                                          \
            y[rind[_ii]] += rtmp[_ii];             \
        }                                          \
    } while (0)

static void
openpfem_csrmatmult(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE sum;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        sum = 0.0;
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        CSRMultRowDotPlus(sum, entries_tmp, kcol_tmp, xx, num);
        yy[idx_row] = sum;
    }
}

static void
openpfem_csrmatmult_add(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE sum;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        sum = 0.0;
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        CSRMultRowDotPlus(sum, entries_tmp, kcol_tmp, xx, num);
        yy[idx_row] += sum;
    }
}

static void
openpfem_csrmatmult_sub(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE sum;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        sum = 0.0;
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        CSRMultRowDotPlus(sum, entries_tmp, kcol_tmp, xx, num);
        yy[idx_row] -= sum;
    }
}

static void
openpfem_csrmatmultaddinplace(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE sum;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        sum = 0.0;
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        CSRMultRowDotPlus(sum, entries_tmp, kcol_tmp, xx, num);
        yy[idx_row] += sum;
    }
}

static void
openpfem_csrmatmultaddinplace_sub(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE sum;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        sum = 0.0;
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        CSRMultRowDotPlus(sum, entries_tmp, kcol_tmp, xx, num);
        yy[idx_row] -= sum;
    }
}

static void
openpfem_csrmatmult_T(CSRMAT *csrmat, DOUBLE *xx, DOUBLE *yy)
{
    INT NumRows = csrmat->NumRows, idx_row, num, i;
    INT *kcol = csrmat->KCol, *kcol_tmp, *rowptr = csrmat->RowPtr;
    DOUBLE *entries = csrmat->Entries, *entries_tmp;
    DOUBLE alpha;
    memset(yy, 0.0, csrmat->NumColumns * sizeof(DOUBLE));
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        num = rowptr[idx_row + 1] - rowptr[idx_row];
        kcol_tmp = kcol + rowptr[idx_row];
        entries_tmp = entries + rowptr[idx_row];
        alpha = xx[idx_row];
        for (i = 0; i < num; i++)
        {
            yy[kcol_tmp[i]] += alpha * entries_tmp[i];
        }
    }
}

void MatrixCreate_OpenPFEM(MATRIX *matrix)
{
    matrix->if_matrix = true;
    matrix->diag = NULL;
    matrix->ndiag = NULL;
    matrix->ndiag_globalcols = NULL;
    matrix->mult_comm = MPI_COMM_NULL;
    matrix->inverse_mult_comm = MPI_COMM_NULL;
    matrix->mult_sendnum = NULL;
    matrix->mult_recvnum = NULL;
    matrix->mult_sendnum_displs = NULL;
    matrix->mult_recvnum_displs = NULL;
    matrix->mult_sendindex = NULL;
    matrix->mult_sendtmp = NULL;
    matrix->mult_recvtmp = NULL;
}

void MatrixDestroy_OpenPFEM(MATRIX *matrix)
{
    CSRMatDestory(matrix->diag);
    CSRMatDestory(matrix->ndiag);
    OpenPFEM_Free(matrix->ndiag_globalcols);
    MPI_Comm_free(&((matrix)->mult_comm));
    MPI_Comm_free(&((matrix)->inverse_mult_comm));
    OpenPFEM_Free(matrix->mult_sendnum);
    OpenPFEM_Free(matrix->mult_recvnum);
    OpenPFEM_Free(matrix->mult_sendnum_displs);
    OpenPFEM_Free(matrix->mult_recvnum_displs);
    OpenPFEM_Free(matrix->mult_sendindex);
    OpenPFEM_Free(matrix->mult_sendtmp);
    OpenPFEM_Free(matrix->mult_recvtmp);
    (matrix)->if_matrix = false;
}

// A 是 B 的 same / subset / diff
void MatrixSturctureCompare_OpenPFEM(MATRIX *A, MATRIX *B, NONZERO_STRUCT *nonzero_struct)
{
    NONZERO_STRUCT str = NONZERO_SAME;
    // 先比较对角部分
    CSRMAT *a = A->diag, *b = B->diag;
    INT *rowptr_a = a->RowPtr, *rowptr_b = b->RowPtr;
    INT *kcol_a = a->KCol, *kcol_b = b->KCol;
    if (A->local_nrows != B->local_nrows || A->local_ncols != B->local_ncols)
        RaiseError("MatrixSturctureCompare_OpenPFEM", "The size of A and B are different!");
    INT idx_row, num, position, position2;
    for (idx_row = 0; idx_row < A->local_nrows; idx_row++)
    {
        num = rowptr_b[idx_row + 1] - rowptr_b[idx_row];
        if ((rowptr_a[idx_row + 1] - rowptr_a[idx_row]) > num)
        {
            str = NONZERO_DIFF;
            goto struct_compare_end;
        }
        else if ((rowptr_a[idx_row + 1] - rowptr_a[idx_row]) == num)
        {
            for (position = rowptr_b[idx_row]; position < rowptr_b[idx_row + 1]; position++)
            {
                if (kcol_a[position] != kcol_b[position])
                {
                    str = NONZERO_DIFF;
                    goto struct_compare_end;
                }
            }
        }
        else
        {
            position2 = rowptr_b[idx_row];
            for (position = rowptr_a[idx_row]; position < rowptr_a[idx_row + 1]; position++)
            {
                while (position2 < rowptr_b[idx_row + 1])
                {
                    if (kcol_b[position2] != kcol_a[position])
                    {
                        position2++;
                    }
                    else
                        break;
                }
            }
            if (position2 < rowptr_b[idx_row + 1])
                str = NONZERO_SUBSET;
            else
            {
                str = NONZERO_DIFF;
                goto struct_compare_end;
            }
        }
    }
struct_compare_end:
    *nonzero_struct = str;
}

void MatrixScale_OpenPFEM(DOUBLE alpha, MATRIX *A)
{
    if (alpha == 0.0)
    {
        memset(A->diag->Entries, 0.0, A->diag->NumEntries * sizeof(DOUBLE));
        memset(A->ndiag->Entries, 0.0, A->ndiag->NumEntries * sizeof(DOUBLE));
    }
    else if (alpha != 1.0)
    {
        INT one = 1;
        // dscal(&(A->diag->NumEntries), &alpha, A->diag->Entries, &one);
        // dscal(&(A->ndiag->NumEntries), &alpha, A->ndiag->Entries, &one);
    }
}

void MatrixAxpby_OpenPFEM(DOUBLE alpha, MATRIX *A, DOUBLE beta, MATRIX *B, NONZERO_STRUCT nonzero_struct)
{
    if (nonzero_struct == NONZERO_SAME)
    {
        if (beta == 0.0)
        {
            memset(B->diag->Entries, 0.0, B->diag->NumEntries * sizeof(DOUBLE));
            memset(B->ndiag->Entries, 0.0, B->ndiag->NumEntries * sizeof(DOUBLE));
        }
        else if (beta != 1.0)
        {
            INT one = 1;
            // dscal_(&(B->diag->NumEntries), &beta, B->diag->Entries, &one);
            // dscal_(&(B->ndiag->NumEntries), &beta, B->ndiag->Entries, &one);
        }
        if (alpha == 0.0)
        {
            return;
        }
        INT one = 1;
        // daxpy_(&(A->diag->NumEntries), &alpha, A->diag->Entries, &one, B->diag->Entries, &one);
        // daxpy_(&(A->ndiag->NumEntries), &alpha, A->ndiag->Entries, &one, B->ndiag->Entries, &one);
    }
    else
    {
        RaiseError("MatrixAxpby_OpenPFEM", "This structure not implemented!");
    }
}

void VectorCreateByMatrix_OpenPFEM(VECTOR **vector, MATRIX *matrix)
{
    DATATYPE type = matrix->type;
    VectorCreate(vector, type);
    (*vector)->local_length = matrix->local_nrows;
    (*vector)->global_length = matrix->global_nrows;
    (*vector)->start = matrix->rows_start;
    (*vector)->end = matrix->rows_end;
    MPI_Comm_dup(matrix->comm, &((*vector)->comm));
    (*vector)->data = (DOUBLE *)calloc(matrix->local_nrows, sizeof(DOUBLE));
}

void VectorCreateByMatrixT_OpenPFEM(VECTOR **vector, MATRIX *matrix)
{
    DATATYPE type = matrix->type;
    VectorCreate(vector, type);
    (*vector)->local_length = matrix->local_ncols;
    (*vector)->global_length = matrix->global_ncols;
    (*vector)->start = matrix->cols_start;
    (*vector)->end = matrix->cols_end;
    MPI_Comm_dup(matrix->comm, &((*vector)->comm));
    (*vector)->data = (DOUBLE *)calloc(matrix->local_ncols, sizeof(DOUBLE));
}

void MatrixVectorMult_OpenPFEM(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MPI_Request multreq;
    MPI_Comm mult_comm = A->mult_comm;
    DOUBLE *sendtmp = A->mult_sendtmp, *recvtmp = A->mult_recvtmp;
    INT *sendindex = A->mult_sendindex;
    DOUBLE *xx = x->data;
    SetDoubleNeigAlltoallBegin(xx, sendindex, A->mult_sendtotalnum, sendtmp, recvtmp, A->mult_sendnum, A->mult_sendnum_displs, A->mult_recvnum, A->mult_recvnum_displs, mult_comm, multreq);
    openpfem_csrmatmult(A->diag, x->data, y->data);
    SetDoubleNeigAlltoallEnd(multreq);
    openpfem_csrmatmultaddinplace(A->ndiag, recvtmp, y->data);
}

void MatrixTransposeVectorMult_OpenPFEM(MATRIX *A, VECTOR *x, VECTOR *y)
{
    DOUBLE *recvtmp = A->mult_sendtmp, *sendtmp = A->mult_recvtmp;
    INT *recvindex = A->mult_sendindex;
    MPI_Comm multT_comm = A->inverse_mult_comm;
    DOUBLE *yy = y->data, *xx = x->data;
    openpfem_csrmatmult_T(A->diag, xx, yy);
    openpfem_csrmatmult_T(A->ndiag, xx, sendtmp);
    AddDoubleNeigAlltoallBegin(sendtmp, recvtmp, A->mult_recvnum, A->mult_recvnum_displs, A->mult_sendnum, A->mult_sendnum_displs, multT_comm, NULL);
    AddDoubleNeigAlltoallEnd(yy, recvtmp, recvindex, A->mult_sendtotalnum);
}

void MatrixVectorMultAdd_OpenPFEM(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MPI_Request multreq;
    MPI_Comm mult_comm = A->mult_comm;
    DOUBLE *sendtmp = A->mult_sendtmp, *recvtmp = A->mult_recvtmp;
    INT *sendindex = A->mult_sendindex;
    DOUBLE *xx = x->data;
    SetDoubleNeigAlltoallBegin(xx, sendindex, A->mult_sendtotalnum, sendtmp, recvtmp, A->mult_sendnum, A->mult_sendnum_displs, A->mult_recvnum, A->mult_recvnum_displs, mult_comm, multreq);
    openpfem_csrmatmult_add(A->diag, x->data, y->data);
    SetDoubleNeigAlltoallEnd(multreq);
    openpfem_csrmatmultaddinplace(A->ndiag, recvtmp, y->data);
}

void MatrixVectorMultSub_OpenPFEM(MATRIX *A, VECTOR *x, VECTOR *y)
{
    MPI_Request multreq;
    MPI_Comm mult_comm = A->mult_comm;
    DOUBLE *sendtmp = A->mult_sendtmp, *recvtmp = A->mult_recvtmp;
    INT *sendindex = A->mult_sendindex;
    DOUBLE *xx = x->data;
    SetDoubleNeigAlltoallBegin(xx, sendindex, A->mult_sendtotalnum, sendtmp, recvtmp, A->mult_sendnum, A->mult_sendnum_displs, A->mult_recvnum, A->mult_recvnum_displs, mult_comm, multreq);
    openpfem_csrmatmult_sub(A->diag, x->data, y->data);
    SetDoubleNeigAlltoallEnd(multreq);
    openpfem_csrmatmultaddinplace_sub(A->ndiag, recvtmp, y->data);
}

void VectorsCreateByMatrix_OpenPFEM(VECTORS **vectors, MATRIX *matrix, INT num)
{
    DATATYPE type = matrix->type;
    VectorsCreate(vectors, type);
    (*vectors)->nvecs = num;
    (*vectors)->local_length = matrix->local_nrows;
    (*vectors)->global_length = matrix->global_nrows;
    (*vectors)->start = matrix->rows_start;
    (*vectors)->end = matrix->rows_end;
    (*vectors)->start_cols = 0;
    (*vectors)->end_cols = num;
    MPI_Comm_dup(matrix->comm, &((*vectors)->comm));
    (*vectors)->data = (DOUBLE *)malloc(num * matrix->local_nrows * sizeof(DOUBLE));
}

void VectorsCreateByMatrixT_OpenPFEM(VECTORS **vectors, MATRIX *matrix, INT num)
{
    DATATYPE type = matrix->type;
    VectorsCreate(vectors, type);
    (*vectors)->nvecs = num;
    (*vectors)->local_length = matrix->local_ncols;
    (*vectors)->global_length = matrix->global_ncols;
    (*vectors)->start = matrix->cols_start;
    (*vectors)->end = matrix->cols_end;
    (*vectors)->start_cols = 0;
    (*vectors)->end_cols = num;
    MPI_Comm_dup(matrix->comm, &((*vectors)->comm));
    (*vectors)->data = (DOUBLE *)malloc(num * matrix->local_ncols * sizeof(DOUBLE));
}

void MatrixVectorsMult_OpenPFEM(MATRIX *A, VECTORS *x, VECTORS *y)
{
    RaiseError("MatrixVectorsMult_OpenPFEM", "not implemented yet!");
}

void MatrixTransposeVectorsMult_OpenPFEM(MATRIX *A, VECTORS *x, VECTORS *y)
{
    RaiseError("MatrixTransposeVectorsMult_OpenPFEM", "not implemented yet!");
}

void VectorCreate_OpenPFEM(VECTOR *vector)
{
    vector->if_vector = true;
    vector->data = NULL;
}
void VectorDestroy_OpenPFEM(VECTOR *vector)
{
    OpenPFEM_Free(vector->data);
    vector->if_vector = false;
}

void VectorGetArray_OpenPFEM(VECTOR *vector, DOUBLE **data)
{
    *data = vector->data;
}

void VectorScale_OpenPFEM(DOUBLE alpha, VECTOR *x)
{
    if (alpha == 0.0)
    {
        memset(x->data, 0.0, x->local_length * sizeof(DOUBLE));
    }
    else if (alpha != 1.0)
    {
        INT one = 1;
        // dscal(&(x->local_length), &alpha, x->data, &one);
    }
}

void VectorAxpby_OpenPFEM(DOUBLE alpha, VECTOR *x, DOUBLE beta, VECTOR *y)
{
    if (beta == 0.0)
    {
        memset(y->data, 0.0, y->local_length * sizeof(DOUBLE));
    }
    else if (beta != 1.0)
    {
        INT one = 1;
        // dscal(&(y->local_length), &beta, y->data, &one);
    }
    if (alpha == 0.0)
    {
        return;
    }
    INT one = 1;
    // daxpy_(&(y->local_length), &alpha, x->data, &one, y->data, &one);
}

void VectorNorm_OpenPFEM(VECTOR *vector, DOUBLE *alpha)
{
    DOUBLE *data = vector->data, sum = 0.0;
    INT i;
    for (i = 0; i < vector->local_length; i++)
    {
        sum += data[i] * data[i];
    }
    MPI_Allreduce(&sum, MPI_IN_PLACE, 1, MPI_DOUBLE, MPI_SUM, vector->comm);
    *alpha = sum;
}

void VectorsCreate_OpenPFEM(VECTORS *vectors)
{
    vectors->if_vectors = true;
    vectors->data = NULL;
}

void VectorsDestroy_OpenPFEM(VECTORS *vectors)
{
    OpenPFEM_Free(vectors->data);
    vectors->if_vectors = false;
}

void VectorsGetArray_OpenPFEM(VECTORS *vectors, DOUBLE **data)
{
    *data = vectors->data;
}

void VectorsRestoreArray_OpenPFEM(VECTORS *vectors, DOUBLE **data)
{
    return;
}