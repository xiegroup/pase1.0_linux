#include "matvec.h"

void VectorGetFEMFunction(VECTOR *vector, FEMFUNCTION *femvec, INT ind)
{
    FEMSPACE *femspace = femvec->FEMSpace;
    if (vector->global_length != femspace->GlobalNumDOF)
        RaiseError("VectorGetFEMFunction", "Inconsistent global size!");
    if (vector->local_length != femspace->MyNumDOF)
        RaiseError("VectorGetFEMFunction", "Inconsistent local size!");
    if (ind >= femvec->num)
        RaiseError("VectorGetFEMFunction", "index >= number of femfunctions!");
    DOUBLE *data = NULL;
    VectorGetArray(vector, &data);
    DOUBLE *values = femvec->Values + ind * femvec->LocalNumDOF;
    INT idx, solindex;
    INT start = femspace->GDOFstart;
    INT end = femspace->GDOFend;
    INT *Gindex = femspace->GlobalIndex;
    INT DofNum = femspace->NumDOF;
    for (idx = 0; idx < DofNum; idx++)
    {
        solindex = Gindex[idx];
        if (solindex >= start && solindex < end)
        {
            values[idx] = data[solindex - start];
        }
    }
    SHAREDDOF *shareddofs = femspace->SharedDOF, *shareddof;
    SHAREDINFO *sharedinfo = femspace->Mesh->SharedInfo;
    INT idx_neig, sharednum;
    INT neignum = sharedinfo->NumNeighborRanks, neigrank;
    INT *neigranks = sharedinfo->NeighborRanks;
    INT *sendsize = (INT *)calloc(neignum, sizeof(INT));
    INT *recvsize = (INT *)calloc(neignum, sizeof(INT));
    INT *owner;
    INT rank, size;
    MPI_Comm comm = sharedinfo->neighborcomm;
    MPI_Comm_rank(femspace->Mesh->comm, &rank);
    MPI_Comm_size(femspace->Mesh->comm, &size);
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        neigrank = neigranks[idx_neig];
        for (idx = 0; idx < sharednum; idx++)
        {
            if (rank == owner[idx])
            {
                sendsize[idx_neig]++;
            }
            else if (neigrank == owner[idx])
            {
                recvsize[idx_neig]++;
            }
        }
    }

    INT sendpackagesize = sendsize[0], recvpackagesize = recvsize[0];
    INT *senddispls = (INT *)malloc(idx_neig * sizeof(INT));
    senddispls[0] = 0;
    INT *recvdispls = (INT *)malloc(idx_neig * sizeof(INT));
    recvdispls[0] = 0;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        senddispls[idx_neig] = sendpackagesize;
        sendpackagesize += sendsize[idx_neig];
        recvdispls[idx_neig] = recvpackagesize;
        recvpackagesize += recvsize[idx_neig];
    }

    DOUBLE *sendpackage = (DOUBLE *)malloc(sendpackagesize * sizeof(DOUBLE));
    DOUBLE *recvpackage = (DOUBLE *)malloc(recvpackagesize * sizeof(DOUBLE));
    INT position = 0, dofindex, dofgindex, doflindex;
    INT *index;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        index = shareddof->Index;
        for (idx = 0; idx < sharednum; idx++)
        {
            dofindex = index[idx];
            if (rank == owner[idx])
            {
                sendpackage[position] = values[dofindex];
                position++;
            }
        }
    }

    MPI_Neighbor_alltoallv(sendpackage, sendsize, senddispls, MPI_DOUBLE, recvpackage, recvsize,
                           recvdispls, MPI_DOUBLE, comm);
    OpenPFEM_Free(sendsize);
    OpenPFEM_Free(senddispls);
    OpenPFEM_Free(sendpackage);
    OpenPFEM_Free(recvsize);
    OpenPFEM_Free(recvdispls);

    position = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        neigrank = neigranks[idx_neig];
        index = shareddof->Index;
        for (idx = 0; idx < sharednum; idx++)
        {
            dofindex = index[idx];
            if (neigrank == owner[idx])
            {
                values[dofindex] = recvpackage[position];
                position++;
            }
        }
    }
    OpenPFEM_Free(recvpackage);
}

void VectorsGetFEMFunction(VECTORS *vectors, FEMFUNCTION *femfunc)
{
    FEMSPACE *femspace = femfunc->FEMSpace;
    INT v_s = vectors->start_cols, v_e = vectors->end_cols;
    INT f_s = femfunc->start, f_e = femfunc->end;
    if (vectors->global_length != femspace->GlobalNumDOF)
        RaiseError("VectorsGetFEMFunction", "Inconsistent global size!");
    if (vectors->local_length != femspace->MyNumDOF)
        RaiseError("VectorsGetFEMFunction", "Inconsistent local size!");
    if (v_e - v_s != f_e - f_s)
        RaiseError("VectorsGetFEMFunction", "Inconsistent number of cols!");

    DOUBLE *data = NULL;
    VectorsGetArray(vectors, &data);
    data = data + v_s * vectors->local_length;
    DOUBLE *values = femfunc->Values + f_s * femfunc->LocalNumDOF;
    INT idx, solindex;
    INT start = femspace->GDOFstart;
    INT end = femspace->GDOFend;
    INT *Gindex = femspace->GlobalIndex;
    INT DofNum = femspace->NumDOF;
    INT colnum = f_e - f_s, idx_col;
    INT local_dofnum = femfunc->LocalNumDOF, local_vecnum = vectors->local_length;
    for (idx_col = 0; idx_col < colnum; idx_col++)
    {
        for (idx = 0; idx < DofNum; idx++)
        {
            solindex = Gindex[idx];
            if (solindex >= start && solindex < end)
            {
                values[local_dofnum * idx_col + idx] = data[local_vecnum * idx_col + solindex - start];
            }
        }
    }
    SHAREDDOF *shareddofs = femspace->SharedDOF, *shareddof;
    SHAREDINFO *sharedinfo = femspace->Mesh->SharedInfo;
    INT idx_neig, sharednum;
    INT neignum = sharedinfo->NumNeighborRanks, neigrank;
    INT *neigranks = sharedinfo->NeighborRanks;
    INT *sendsize = (INT *)calloc(neignum, sizeof(INT));
    INT *recvsize = (INT *)calloc(neignum, sizeof(INT));
    INT *owner;
    INT rank, size;
    MPI_Comm comm = sharedinfo->neighborcomm;
    MPI_Comm_rank(femspace->Mesh->comm, &rank);
    MPI_Comm_size(femspace->Mesh->comm, &size);
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        neigrank = neigranks[idx_neig];
        for (idx = 0; idx < sharednum; idx++)
        {
            if (rank == owner[idx])
            {
                sendsize[idx_neig] += colnum;
            }
            else if (neigrank == owner[idx])
            {
                recvsize[idx_neig] += colnum;
            }
        }
    }
    INT sendpackagesize = sendsize[0], recvpackagesize = recvsize[0];
    INT *senddispls = (INT *)malloc(idx_neig * sizeof(INT));
    senddispls[0] = 0;
    INT *recvdispls = (INT *)malloc(idx_neig * sizeof(INT));
    recvdispls[0] = 0;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        senddispls[idx_neig] = sendpackagesize;
        sendpackagesize += sendsize[idx_neig];
        recvdispls[idx_neig] = recvpackagesize;
        recvpackagesize += recvsize[idx_neig];
    }

    DOUBLE *sendpackage = (DOUBLE *)malloc(sendpackagesize * sizeof(DOUBLE));
    DOUBLE *recvpackage = (DOUBLE *)malloc(recvpackagesize * sizeof(DOUBLE));
    INT position = 0, dofindex, dofgindex, doflindex;
    INT *index;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        index = shareddof->Index;
        for (idx_col = 0; idx_col < colnum; idx_col++)
        {
            for (idx = 0; idx < sharednum; idx++)
            {
                dofindex = index[idx];
                if (rank == owner[idx])
                {
                    sendpackage[position] = values[idx_col * local_dofnum + dofindex];
                    position++;
                }
            }
        }
    }

    MPI_Neighbor_alltoallv(sendpackage, sendsize, senddispls, MPI_DOUBLE, recvpackage, recvsize,
                           recvdispls, MPI_DOUBLE, comm);
    OpenPFEM_Free(sendsize);
    OpenPFEM_Free(senddispls);
    OpenPFEM_Free(sendpackage);
    OpenPFEM_Free(recvsize);
    OpenPFEM_Free(recvdispls);

    position = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        shareddof = &(shareddofs[idx_neig]);
        sharednum = shareddof->SharedNum;
        owner = shareddof->Owner;
        neigrank = neigranks[idx_neig];
        index = shareddof->Index;
        for (idx_col = 0; idx_col < colnum; idx_col++)
        {
            for (idx = 0; idx < sharednum; idx++)
            {
                dofindex = index[idx];
                if (neigrank == owner[idx])
                {
                    values[idx_col * local_dofnum + dofindex] = recvpackage[position];
                    position++;
                }
            }
        }
    }
    OpenPFEM_Free(recvpackage);
}

void FEMFunctionGetVector(FEMFUNCTION *femvec, VECTOR *vector)
{
    FEMSPACE *femspace = femvec->FEMSpace;
    DOUBLE *data = NULL;
    VectorGetArray(vector, &data);
    DOUBLE *Values = femvec->Values;
    INT begin = femspace->GDOFstart;
    INT end = femspace->GDOFend;
    INT *gindex = femspace->GlobalIndex;
    INT i;
    for (i = 0; i < femspace->NumDOF; i++)
    {
        if (gindex[i] >= begin && gindex[i] < end)
        {
            data[gindex[i] - begin] = Values[i];
        }
    }
}