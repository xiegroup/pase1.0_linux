#include "opsimpls.h"
#include "matvec.h"
#include "assemble.h"

#define MatrixCleanOpenPFEM(matrix)                    \
    do                                                 \
    {                                                  \
        CSRMatDestory((matrix)->diag);                 \
        CSRMatDestory((matrix)->ndiag);                \
        OpenPFEM_Free((matrix)->ndiag_globalcols);     \
        MPI_Comm_free(&((matrix)->mult_comm));         \
        MPI_Comm_free(&((matrix)->inverse_mult_comm)); \
        OpenPFEM_Free((matrix)->mult_sendnum);         \
        OpenPFEM_Free((matrix)->mult_recvnum);         \
        OpenPFEM_Free((matrix)->mult_sendnum_displs);  \
        OpenPFEM_Free((matrix)->mult_recvnum_displs);  \
        OpenPFEM_Free((matrix)->mult_sendindex);       \
        OpenPFEM_Free((matrix)->mult_sendtmp);         \
        OpenPFEM_Free((matrix)->mult_recvtmp);         \
        (matrix)->if_matrix = false;                   \
    } while (0)

#define VectorCleanPetsc(vector)                      \
    do                                                \
    {                                                 \
        VecDestroy((Vec *)(&((vector)->data_petsc))); \
        (vector)->if_petsc = false;                   \
    } while (0)

#define VectorCleanOpenPFEM(vector)    \
    do                                 \
    {                                  \
        OpenPFEM_Free((vector)->data); \
        (vector)->if_vector = false;   \
    } while (0)

static void CSRMatCreate(CSRMAT **Matrix)
{
    CSRMAT *matrix = (CSRMAT *)malloc(sizeof(CSRMAT));
    matrix->NumRows = 0;
    matrix->NumColumns = 0;
    matrix->NumEntries = 0;
    matrix->RowPtr = NULL;
    matrix->KCol = NULL;
    matrix->Entries = NULL;
    *Matrix = matrix;
}

/////////////////////////////////////////////////////////////////////////////
/*                                 Matrix                                  */
/////////////////////////////////////////////////////////////////////////////

void MatrixCreate(MATRIX **Matrix, DATATYPE type)
{
    MATRIX *matrix = (MATRIX *)malloc(sizeof(MATRIX));
    matrix->type = type;
    matrix->global_nrows = 0;
    matrix->global_ncols = 0;
    matrix->local_nrows = 0;
    matrix->local_ncols = 0;
    matrix->rows_start = 0;
    matrix->rows_end = 0;
    matrix->cols_start = 0;
    matrix->cols_end = 0;
    matrix->symmetric = false;
    matrix->comm = MPI_COMM_NULL;
    // OpenPFEM
    matrix->if_matrix = false;
    matrix->diag = NULL;
    matrix->ndiag = NULL;
    matrix->ndiag_globalcols = NULL;
    matrix->mult_comm = MPI_COMM_NULL;
    matrix->inverse_mult_comm = MPI_COMM_NULL;
    matrix->mult_sendnum = NULL;
    matrix->mult_recvnum = NULL;
    matrix->mult_sendnum_displs = NULL;
    matrix->mult_recvnum_displs = NULL;
    matrix->mult_sendindex = NULL;
    matrix->mult_sendtmp = NULL;
    matrix->mult_recvtmp = NULL;
    // PETSC
    matrix->if_petsc = false;
    matrix->data_petsc = NULL;
    // assemble_info
    matrix->assemble_info = NULL;
    // dirichlet bd
    matrix->delete_dirichletbd = false;

    MatrixOpsCreate(matrix, type);
    ((MATRIX_OPS *)(matrix->ops))->create(matrix);
    *Matrix = matrix;
}

void MatrixDestroy(MATRIX **matrix)
{
    if ((*matrix)->if_matrix)
    {
        MatrixDestroy_OpenPFEM(*matrix);
    }
    if ((*matrix)->if_petsc)
    {
#if defined(PETSC_USE)
        MatrixDestroy_PETSc(*matrix);
#else
        RaisePetscError("MatrixDestory");
#endif
    }
    MPI_Comm_free(&((*matrix)->comm));
    if ((*matrix)->assemble_info != NULL)
    {
        RaiseError("MatrixDestroy", "assemble information not free!");
    }
    OpenPFEM_Free((*matrix)->ops);
    OpenPFEM_Free(*matrix);
}

// 删掉零元来压缩矩阵大小
void MatrixCompress(MATRIX *matrix)
{
    DATATYPE type = matrix->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        // 先处理diag部分
        CSRMAT *mat = matrix->diag;
        INT *rowptr = mat->RowPtr, *kcol = mat->KCol;
        DOUBLE *entries = mat->Entries;
        INT num_row = mat->NumRows, rowstart = matrix->rows_start, colstart = matrix->cols_start;
        INT idx_row, i, start = rowptr[0], end, num, position = 0;
        for (idx_row = 0; idx_row < num_row - 1; idx_row++)
        {
            num = 0;
            end = rowptr[idx_row + 1];
            for (i = start; i < end; i++)
            {
                if (entries[i] != 0.0)
                {
                    num++;
                }
                else if (idx_row + rowstart == kcol[i] + colstart)
                {
                    entries[i] = __DBL_MAX__;
                    num++;
                }
            }
            start = rowptr[idx_row + 1];
            rowptr[idx_row + 1] = rowptr[idx_row] + num;
        }
        num = 0;
        end = rowptr[num_row];
        for (i = start; i < end; i++)
        {
            if (entries[i] != 0.0)
            {
                num++;
            }
            else if (idx_row + rowstart == kcol[i] + colstart)
            {
                num++;
            }
        }
        rowptr[num_row] = rowptr[num_row - 1] + num;
        for (i = 0; i < mat->NumEntries; i++)
        {
            if (entries[i] != 0.0)
            {
                if (entries[i] == __DBL_MAX__)
                    entries[i] = 0.0;
                kcol[position] = kcol[i];
                entries[position] = entries[i];
                position++;
            }
        }
        mat->NumEntries = rowptr[num_row];
        mat->KCol = realloc(mat->KCol, mat->NumEntries * sizeof(INT));
        mat->Entries = realloc(mat->Entries, mat->NumEntries * sizeof(DOUBLE));
        // 再处理ndiag部分
        mat = matrix->ndiag;
        rowptr = mat->RowPtr, kcol = mat->KCol;
        entries = mat->Entries;
        num_row = mat->NumRows;
        idx_row, i, start = rowptr[0], end, num, position = 0;
        for (idx_row = 0; idx_row < num_row - 1; idx_row++)
        {
            num = 0;
            end = rowptr[idx_row + 1];
            for (i = start; i < end; i++)
            {
                if (entries[i] != 0.0)
                {
                    num++;
                }
            }
            start = rowptr[idx_row + 1];
            rowptr[idx_row + 1] = rowptr[idx_row] + num;
        }
        num = 0;
        end = rowptr[num_row];
        for (i = start; i < end; i++)
        {
            if (entries[i] != 0.0)
            {
                num++;
            }
        }
        rowptr[num_row] = rowptr[num_row - 1] + num;
        for (i = 0; i < mat->NumEntries; i++)
        {
            if (entries[i] != 0.0)
            {
                kcol[position] = kcol[i];
                entries[position] = entries[i];
                position++;
            }
        }
        mat->NumEntries = rowptr[num_row];
        mat->KCol = realloc(mat->KCol, mat->NumEntries * sizeof(INT));
        mat->Entries = realloc(mat->Entries, mat->NumEntries * sizeof(DOUBLE));
    }
    else if (DataTypePetscEqual(type))
    {
        RaiseError("MatrixCompression", "Only matrix of type[OpenPFEM] can be compressed.");
    }
}

void MatrixPrint(MATRIX *mat, INT printrank)
{
    MPI_Comm comm = mat->comm;
    INT myrank;
    MPI_Comm_rank(comm, &myrank);
    if (myrank != printrank)
    {
        return;
    }
    printf("================================================\n");
    printf("矩阵的全局尺寸：%d x %d\n ", mat->global_nrows, mat->global_ncols);
    printf("矩阵的局部尺寸：%d(%d-%d) x %d(%d-%d)\n ",
           mat->local_nrows, mat->rows_start, mat->rows_end,
           mat->local_ncols, mat->cols_start, mat->cols_end);
    printf("------------------------------------------------\n");
    INT i, j, stop;
    CSRMAT *diag = mat->diag, *ndiag = mat->ndiag;
    INT *ndiag_globalcols = mat->ndiag_globalcols;
    for (i = 0; i < mat->local_nrows; i++)
    {
        printf("-- 第 %d 行:\n", i);
        stop = ndiag->RowPtr[i + 1];
        for (j = ndiag->RowPtr[i]; j < ndiag->RowPtr[i + 1]; j++)
        {
            if (ndiag_globalcols[ndiag->KCol[j]] < mat->cols_start)
            {
                printf("[%d,%d]=%g\t", i + mat->rows_start, ndiag_globalcols[ndiag->KCol[j]], ndiag->Entries[j]);
            }
            else
            {
                stop = j;
                break;
            }
        }
        for (j = diag->RowPtr[i]; j < diag->RowPtr[i + 1]; j++)
        {
            printf("[%d,%d]=%g\t", i + mat->rows_start,
                   diag->KCol[j] + mat->cols_start, diag->Entries[j]);
        }
        for (j = stop; j < ndiag->RowPtr[i + 1]; j++)
        {
            printf("[%d,%d]=%g\t", i + mat->rows_start, ndiag_globalcols[ndiag->KCol[j]], ndiag->Entries[j]);
        }
        printf("\n");
    }
    printf("================================================\n");
}

void MatrixPrint2Matlab(MATRIX *mat, INT printrank)
{
    MPI_Comm comm = mat->comm;
    INT myrank;
    MPI_Comm_rank(comm, &myrank);
    if (myrank != printrank)
    {
        return;
    }
    // printf("================================================\n");
    // printf("矩阵的全局尺寸：%d x %d\n ", mat->global_nrows, mat->global_ncols);
    // printf("矩阵的局部尺寸：%d(%d-%d) x %d(%d-%d)\n ",
    //        mat->local_nrows, mat->rows_start, mat->rows_end,
    //        mat->local_ncols, mat->cols_start, mat->cols_end);
    // printf("------------------------------------------------\n");
    INT i, j, stop;
    CSRMAT *diag = mat->diag, *ndiag = mat->ndiag;
    INT *ndiag_globalcols = mat->ndiag_globalcols;
    for (i = 0; i < mat->local_nrows; i++)
    {
        // printf("-- 第 %d 行:\n", i);
        stop = ndiag->RowPtr[i + 1];
        for (j = ndiag->RowPtr[i]; j < ndiag->RowPtr[i + 1]; j++)
        {
            if (ndiag_globalcols[ndiag->KCol[j]] < mat->cols_start)
            {
                printf("A(%d,%d)=%g;\t", i + mat->rows_start + 1, ndiag_globalcols[ndiag->KCol[j]] + 1, ndiag->Entries[j]);
            }
            else
            {
                stop = j;
                break;
            }
        }
        for (j = diag->RowPtr[i]; j < diag->RowPtr[i + 1]; j++)
        {
            printf("A(%d,%d)=%g;\t", i + mat->rows_start + 1,
                   diag->KCol[j] + mat->cols_start + 1, diag->Entries[j]);
        }
        for (j = stop; j < ndiag->RowPtr[i + 1]; j++)
        {
            printf("A(%d,%d)=%g;\t", i + mat->rows_start + 1, ndiag_globalcols[ndiag->KCol[j]] + 1, ndiag->Entries[j]);
        }
        printf("\n");
    }
    // printf("================================================\n");
}

void MatrixOutput(MATRIX *matrix, const char *name)
{
    if (DataTypePetscEqual(matrix->type))
    {
#if defined(PETSC_USE)
        MPI_Comm comm;
        Mat mat = (Mat)(matrix->data_petsc);
        PetscObjectGetComm((PetscObject)mat, &comm);
        PetscViewer viewer;
        PetscViewerBinaryOpen(comm, name, FILE_MODE_WRITE, &viewer);
        MatView(mat, viewer);
        PetscViewerDestroy(&viewer);
#else
        RaisePetscError("MatrixOutput");
#endif
    }
    else
    {
        RaiseError("MatrixOutput", "Not implemented!");
    }
}

void MatrixRowsReOrder(MATRIX **matrix, INT *old_gindex, INT rownum, BRIDGE *bridge)
{
    if (bridge == NULL)
    {
        RaiseError("MatrixRowsReOrder", "not implemented yet!");
    }

    INT i, j, k, start, end, start2, end2;
    INT myrank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    DATATYPE datatype = (*matrix)->type;

    MPI_Comm old2new_comm, new2old_comm;
    MPI_Comm_dup(bridge->BridgeComm, &old2new_comm);
    INT old2new_ranknum = bridge->DestinationRankNum; // 旧网格发往了几个进程
    INT *old2new_rank = bridge->DestinationRank;
    INT new2old_ranknum = bridge->OriginRankNum; // 新网格来自几个进程
    INT *new2old_rank = bridge->OriginRank;
    BridgeReverse(bridge);
    MPI_Comm_dup(bridge->BridgeComm, &new2old_comm);

    CSRMAT *old_diag = (*matrix)->diag;
    CSRMAT *old_ndiag = (*matrix)->ndiag;
    INT *old_diag_kcol, *old_diag_rowptr, *old_ndiag_kcol, *old_ndiag_rowptr, *old_ndiag_globalcols;
    DOUBLE *old_diag_entries, *old_ndiag_entries;
    if (DataTypeOpenPFEMEqual(datatype))
    {
        old_diag_kcol = old_diag->KCol;
        old_diag_rowptr = old_diag->RowPtr;
        old_diag_entries = old_diag->Entries;
        old_ndiag_kcol = old_ndiag->KCol;
        old_ndiag_rowptr = old_ndiag->RowPtr;
        old_ndiag_entries = old_ndiag->Entries;
        old_ndiag_globalcols = (*matrix)->ndiag_globalcols;
    }

    // (1) 计算新的这些行都会从哪个进程过来
    // (1.1) 先获得 (每个进程获得的行会来自哪些进程) 这些进程的起始编号
    INT *old_rank_se = (INT *)malloc(new2old_ranknum * 2 * sizeof(INT));
    INT startend[2] = {(*matrix)->rows_start, (*matrix)->rows_end};
    MPI_Neighbor_allgather(startend, 2, MPI_INT, old_rank_se, 2, MPI_INT, old2new_comm);
    // (1.2) 计算新的行对应的旧进程, 和一共会有多少行来自每个进程
    INT *newrow_fromrank = (INT *)malloc(rownum * sizeof(INT)); // 来自每个邻居的行
    INT *newrow_fromrank_num = (INT *)calloc(new2old_ranknum, sizeof(INT));
    INT *newrow_oldrank = (INT *)malloc(rownum * sizeof(INT));      // 每一行对应原来的哪个进程
    INT *newrow_oldrankindex = (INT *)malloc(rownum * sizeof(INT)); // 每一行对应原来进程的第几个
    for (i = 0; i < new2old_ranknum; i++)
    {
        start = old_rank_se[2 * i];
        end = old_rank_se[2 * i + 1];
        for (j = 0; j < rownum; j++)
        {
            if (old_gindex[j] < end && old_gindex[j] >= start)
            {
                newrow_oldrank[j] = i;
                newrow_oldrankindex[j] = newrow_fromrank_num[i];
                newrow_fromrank_num[i]++;
            }
        }
    }
    INT *newrow_fromrank_num_displs = (INT *)malloc(new2old_ranknum * sizeof(INT));
    newrow_fromrank_num_displs[0] = 0;
    for (i = 0; i < new2old_ranknum - 1; i++)
    {
        newrow_fromrank_num_displs[i + 1] = newrow_fromrank_num_displs[i] + newrow_fromrank_num[i];
        newrow_fromrank_num[i] = 0;
    }
    newrow_fromrank_num[new2old_ranknum - 1] = 0;
    if (DataTypeOpenPFEMEqual(datatype))
    {
        for (i = 0; i < new2old_ranknum; i++)
        {
            start = old_rank_se[2 * i];
            end = old_rank_se[2 * i + 1];
            for (j = 0; j < rownum; j++)
            {
                if (old_gindex[j] < end && old_gindex[j] >= start)
                {
                    newrow_fromrank[newrow_fromrank_num_displs[i] + newrow_fromrank_num[i]] = old_gindex[j] - start;
                    newrow_fromrank_num[i]++;
                }
            }
        }
    }
    else if (DataTypePetscEqual(datatype))
    {
        for (i = 0; i < new2old_ranknum; i++)
        {
            start = old_rank_se[2 * i];
            end = old_rank_se[2 * i + 1];
            for (j = 0; j < rownum; j++)
            {
                if (old_gindex[j] < end && old_gindex[j] >= start)
                {
                    newrow_fromrank[newrow_fromrank_num_displs[i] + newrow_fromrank_num[i]] = old_gindex[j];
                    newrow_fromrank_num[i]++;
                }
            }
        }
    }
    else
    {
        RaiseError("MatrixRowsReOrder", "wrong datatype!");
    }
    OpenPFEM_Free(old_rank_se);

    // (2) 通信获得旧矩阵每行要发往哪里
    INT *oldrow_torank_num = (INT *)malloc(old2new_ranknum * sizeof(INT));
    INT *oldrow_torank = (INT *)malloc((*matrix)->local_nrows * sizeof(INT));
    MPI_Neighbor_alltoall(newrow_fromrank_num, 1, MPI_INT, oldrow_torank_num, 1, MPI_INT, new2old_comm);
    INT *oldrow_torank_num_displs = (INT *)malloc(old2new_ranknum * sizeof(INT));
    oldrow_torank_num_displs[0] = 0;
    for (i = 0; i < old2new_ranknum - 1; i++)
    {
        oldrow_torank_num_displs[i + 1] = oldrow_torank_num_displs[i] + oldrow_torank_num[i];
    }
    MPI_Neighbor_alltoallv(newrow_fromrank, newrow_fromrank_num, newrow_fromrank_num_displs, MPI_INT,
                           oldrow_torank, oldrow_torank_num, oldrow_torank_num_displs, MPI_INT, new2old_comm);

    // (3) 旧的矩阵打包需要的信息
    // (3.1) 对于每个要发送的进程都整理一个 csrmat, 计算其大小
    INT *packagesize = (INT *)malloc(old2new_ranknum * sizeof(INT));
    INT *packagedispls = (INT *)malloc(old2new_ranknum * sizeof(INT));
    CSRMAT *sendmat = (CSRMAT *)malloc(old2new_ranknum * sizeof(CSRMAT));
    INT entnum = 0, rowindex, colnum, sizenum;
    if (DataTypeOpenPFEMEqual(datatype))
    {
        for (i = 0; i < old2new_ranknum; i++)
        {
            if (myrank == old2new_rank[i])
            {
                // 自己给自己的就不重新整理了
                packagesize[i] = 0;
                sendmat[i].NumEntries = 0;
            }
            else
            {
                entnum = 0;
                sendmat[i].NumRows = oldrow_torank_num[i];
                sendmat[i].NumColumns = (*matrix)->global_nrows;
                INT *rowptr = (INT *)malloc((oldrow_torank_num[i] + 1) * sizeof(INT));
                start = oldrow_torank_num_displs[i];
                end = start + oldrow_torank_num[i];
                rowptr[0] = 0;
                for (j = start; j < end; j++)
                {
                    rowindex = oldrow_torank[j];
                    colnum = old_diag_rowptr[rowindex + 1] - old_diag_rowptr[rowindex] +
                             old_ndiag_rowptr[rowindex + 1] - old_ndiag_rowptr[rowindex];
                    rowptr[j - start + 1] = rowptr[j - start] + colnum;
                    entnum += colnum;
                }
                sendmat[i].NumEntries = entnum;
                sendmat[i].RowPtr = rowptr;
                // 计算 package 的 size
                sizenum = 3 * sizeof(INT) + (oldrow_torank_num[i] + 1) * sizeof(INT) +
                          entnum * sizeof(INT) + entnum * sizeof(DOUBLE);
                packagesize[i] = sizenum;
            }
        }
    }
    else if (DataTypePetscEqual(datatype))
    {
#if defined(PETSC_USE)
        Mat A = (Mat)((*matrix)->data_petsc);
        for (i = 0; i < old2new_ranknum; i++)
        {
            if (myrank == old2new_rank[i])
            {
                // 自己给自己的就不重新整理了
                packagesize[i] = 0;
                sendmat[i].NumEntries = 0;
            }
            else
            {
                entnum = 0;
                sendmat[i].NumRows = oldrow_torank_num[i];
                sendmat[i].NumColumns = (*matrix)->global_nrows;
                INT *rowptr = (INT *)malloc((oldrow_torank_num[i] + 1) * sizeof(INT));
                start = oldrow_torank_num_displs[i];
                end = start + oldrow_torank_num[i];
                rowptr[0] = 0;
                for (j = start; j < end; j++)
                {
                    rowindex = oldrow_torank[j];
                    MatGetRow(A, rowindex, &colnum, NULL, NULL);
                    rowptr[j - start + 1] = rowptr[j - start] + colnum;
                    entnum += colnum;
                    MatRestoreRow(A, rowindex, &colnum, NULL, NULL);
                }
                sendmat[i].NumEntries = entnum;
                sendmat[i].RowPtr = rowptr;
                // 计算 package 的 size
                sizenum = 3 * sizeof(INT) + (oldrow_torank_num[i] + 1) * sizeof(INT) +
                          entnum * sizeof(INT) + entnum * sizeof(DOUBLE);
                packagesize[i] = sizenum;
            }
        }
#else
        RaisePetscError("MatrixRowsReOrder");
#endif
    }
    else
    {
        RaiseError("MatrixRowsReOrder", "wrong datatype!");
    }
    packagedispls[0] = 0;
    for (i = 0; i < old2new_ranknum - 1; i++)
    {
        packagedispls[i + 1] = packagedispls[i] + packagesize[i];
    }
    INT packagesize_total = packagedispls[old2new_ranknum - 1] + packagesize[old2new_ranknum - 1];

    INT *recvsize = (INT *)malloc(new2old_ranknum * sizeof(INT));
    INT *recvdispls = (INT *)malloc(new2old_ranknum * sizeof(INT));
    MPI_Neighbor_alltoall(packagesize, 1, MPI_INT, recvsize, 1, MPI_INT, old2new_comm);
    recvdispls[0] = 0;
    for (i = 0; i < new2old_ranknum - 1; i++)
    {
        recvdispls[i + 1] = recvdispls[i] + recvsize[i];
    }
    INT recvsize_total = recvdispls[new2old_ranknum - 1] + recvsize[new2old_ranknum - 1];

    // (3.2) 具体整理 csrmat 的内容并打包
    char *package = (char *)malloc(packagesize_total * sizeof(char));
    char *recv = (char *)malloc(recvsize_total * sizeof(char));
    INT position = 0, current = 0, old_gstart = (*matrix)->cols_start;
    if (DataTypeOpenPFEMEqual(datatype))
    {
        for (i = 0; i < old2new_ranknum; i++)
        {
            if (myrank == old2new_rank[i])
            {
                // 自己给自己的就不重新整理了
            }
            else
            {
                MPI_Pack(&(sendmat[i].NumRows), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(&(sendmat[i].NumColumns), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(&(sendmat[i].NumEntries), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(sendmat[i].RowPtr, sendmat[i].NumRows + 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                INT *rowptr = sendmat[i].RowPtr;
                INT *kcol = (INT *)malloc(sendmat[i].NumEntries * sizeof(INT));
                DOUBLE *entries = (DOUBLE *)malloc(sendmat[i].NumEntries * sizeof(DOUBLE));
                start = oldrow_torank_num_displs[i];
                end = start + oldrow_torank_num[i];
                for (j = start; j < end; j++)
                {
                    rowindex = oldrow_torank[j];
                    current = rowptr[j - start];
                    // diag 部分
                    start2 = old_diag_rowptr[rowindex];
                    end2 = old_diag_rowptr[rowindex + 1];
                    for (k = start2; k < end2; k++)
                    {
                        kcol[current] = old_diag_kcol[k] + old_gstart;
                        entries[current] = old_diag_entries[k];
                        current++;
                    }
                    // ndiag 部分
                    start2 = old_ndiag_rowptr[rowindex];
                    end2 = old_ndiag_rowptr[rowindex + 1];
                    for (k = start2; k < end2; k++)
                    {
                        kcol[current] = old_ndiag_globalcols[old_ndiag_kcol[k]];
                        entries[current] = old_ndiag_entries[k];
                        current++;
                    }
                }
                MPI_Pack(kcol, sendmat[i].NumEntries, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(entries, sendmat[i].NumEntries, MPI_DOUBLE, package, packagesize_total, &position, old2new_comm);
                OpenPFEM_Free(kcol);
                OpenPFEM_Free(entries);
                OpenPFEM_Free(rowptr);
            }
        }
    }
    else if (DataTypePetscEqual(datatype))
    {
#if defined(PETSC_USE)
        Mat A = (Mat)((*matrix)->data_petsc);
        const INT *tmpkcol;
        const DOUBLE *tmpentries;
        for (i = 0; i < old2new_ranknum; i++)
        {
            if (myrank == old2new_rank[i])
            {
                // 自己给自己的就不重新整理了
            }
            else
            {
                MPI_Pack(&(sendmat[i].NumRows), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(&(sendmat[i].NumColumns), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(&(sendmat[i].NumEntries), 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(sendmat[i].RowPtr, sendmat[i].NumRows + 1, MPI_INT, package, packagesize_total, &position, old2new_comm);
                INT *rowptr = sendmat[i].RowPtr;
                INT *kcol = (INT *)malloc(sendmat[i].NumEntries * sizeof(INT));
                DOUBLE *entries = (DOUBLE *)malloc(sendmat[i].NumEntries * sizeof(DOUBLE));
                start = oldrow_torank_num_displs[i];
                end = start + oldrow_torank_num[i];
                for (j = start; j < end; j++)
                {
                    rowindex = oldrow_torank[j];
                    MatGetRow(A, rowindex, &colnum, &tmpkcol, &tmpentries);
                    current = rowptr[j - start];
                    memcpy(kcol + current, tmpkcol, colnum * sizeof(INT));
                    memcpy(entries + current, tmpentries, colnum * sizeof(DOUBLE));
                    MatRestoreRow(A, rowindex, &colnum, &tmpkcol, &tmpentries);
                }
                MPI_Pack(kcol, sendmat[i].NumEntries, MPI_INT, package, packagesize_total, &position, old2new_comm);
                MPI_Pack(entries, sendmat[i].NumEntries, MPI_DOUBLE, package, packagesize_total, &position, old2new_comm);
                OpenPFEM_Free(kcol);
                OpenPFEM_Free(entries);
                OpenPFEM_Free(rowptr);
            }
        }
#else
        RaisePetscError("MatrixRowsReOrder");
#endif
    }
    else
    {
        RaiseError("MatrixRowsReOrder", "wrong datatype!");
    }
    OpenPFEM_Free(sendmat);

    // (3.3) 通信
    MPI_Neighbor_alltoallv(package, packagesize, packagedispls, MPI_PACKED,
                           recv, recvsize, recvdispls, MPI_PACKED, old2new_comm);

    // (4) 形成新的矩阵
    // (4.1) 拆开收到的信息
    CSRMAT *recvmat = (CSRMAT *)malloc(new2old_ranknum * sizeof(CSRMAT));
    for (i = 0; i < new2old_ranknum; i++)
    {
        if (myrank != new2old_rank[i])
        {
            position = recvdispls[i];
            MPI_Unpack(recv, recvsize_total, &position, &(recvmat[i].NumRows), 1, MPI_INT, old2new_comm);
            MPI_Unpack(recv, recvsize_total, &position, &(recvmat[i].NumColumns), 1, MPI_INT, old2new_comm);
            MPI_Unpack(recv, recvsize_total, &position, &(recvmat[i].NumEntries), 1, MPI_INT, old2new_comm);
            INT *rowptr = (INT *)malloc((recvmat[i].NumRows + 1) * sizeof(INT));
            MPI_Unpack(recv, recvsize_total, &position, rowptr, recvmat[i].NumRows + 1, MPI_INT, old2new_comm);
            INT *kcol = (INT *)malloc(recvmat[i].NumEntries * sizeof(INT));
            DOUBLE *entries = (DOUBLE *)malloc(recvmat[i].NumEntries * sizeof(DOUBLE));
            MPI_Unpack(recv, recvsize_total, &position, kcol, recvmat[i].NumEntries, MPI_INT, old2new_comm);
            MPI_Unpack(recv, recvsize_total, &position, entries, recvmat[i].NumEntries, MPI_DOUBLE, old2new_comm);
            recvmat[i].RowPtr = rowptr;
            recvmat[i].KCol = kcol;
            recvmat[i].Entries = entries;
        }
    }

    // (4.3) 生成新矩阵结构
    MATRIX *newmatrix = NULL;
    MatrixCreate(&newmatrix, datatype);
    MPI_Comm_dup((*matrix)->comm, &(newmatrix->comm));
    newmatrix->global_nrows = (*matrix)->global_nrows;
    newmatrix->global_ncols = (*matrix)->global_ncols;
    newmatrix->local_nrows = rownum;
    newmatrix->local_ncols = (*matrix)->local_ncols;
    newmatrix->symmetric = false;
    newmatrix->delete_dirichletbd = (*matrix)->delete_dirichletbd;
    MPI_Scan(&rownum, &(newmatrix->rows_end), 1, MPI_INT, MPI_SUM, newmatrix->comm);
    newmatrix->rows_start = newmatrix->rows_end - rownum;
    newmatrix->cols_start = (*matrix)->cols_start;
    newmatrix->cols_end = (*matrix)->cols_end;
    if (DataTypePetscEqual(datatype))
    {
        Mat A;
        MatCreate(newmatrix->comm, &A);
        MatSetSizes(A, newmatrix->local_nrows, newmatrix->local_ncols, newmatrix->global_nrows, newmatrix->global_ncols);
        MatSetFromOptions(A);
        newmatrix->data_petsc = (void *)A;
    }

    // (4.2) 统计新矩阵每行 diag / ndiag 的个数
    INT rank, oldindex, *tmp, diagnum, ndiagnum;
    INT colstart = newmatrix->cols_start, colend = newmatrix->cols_end;
    if (DataTypeOpenPFEMEqual(datatype))
    {
        CSRMAT *diag = NULL, *ndiag = NULL;
        CSRMatCreate(&diag);
        CSRMatCreate(&ndiag);
        diag->NumRows = rownum;
        diag->NumColumns = newmatrix->local_ncols;
        ndiag->NumRows = rownum;
        INT *new_diag_rowptr = (INT *)malloc((rownum + 1) * sizeof(INT));
        INT *new_ndiag_rowptr = (INT *)malloc((rownum + 1) * sizeof(INT));
        new_diag_rowptr[0] = 0, new_ndiag_rowptr[0] = 0;
        INT old_rowstart = (*matrix)->rows_start;
        for (i = 0; i < rownum; i++)
        {
            rank = new2old_rank[newrow_oldrank[i]]; // 这行来自哪个进程
            if (rank == myrank)                     // 本进程的话就不依赖通信来的数据了
            {
                oldindex = old_gindex[i] - old_rowstart;
                new_diag_rowptr[i + 1] = new_diag_rowptr[i] + old_diag_rowptr[oldindex + 1] - old_diag_rowptr[oldindex];
                new_ndiag_rowptr[i + 1] = new_ndiag_rowptr[i] + old_ndiag_rowptr[oldindex + 1] - old_ndiag_rowptr[oldindex];
            }
            else
            {
                oldindex = newrow_oldrankindex[i];
                start = recvmat[newrow_oldrank[i]].RowPtr[oldindex];
                end = recvmat[newrow_oldrank[i]].RowPtr[oldindex + 1];
                tmp = recvmat[newrow_oldrank[i]].KCol;
                diagnum = 0;
                ndiagnum = 0;
                for (j = start; j < end; j++)
                {
                    if (tmp[j] >= colstart && tmp[j] < colend)
                        diagnum++;
                    else
                        ndiagnum++;
                }
                new_diag_rowptr[i + 1] = new_diag_rowptr[i] + diagnum;
                new_ndiag_rowptr[i + 1] = new_ndiag_rowptr[i] + ndiagnum;
            }
        }
        diag->NumEntries = new_diag_rowptr[rownum];
        ndiag->NumEntries = new_ndiag_rowptr[rownum];
        diag->RowPtr = new_diag_rowptr;
        ndiag->RowPtr = new_ndiag_rowptr;
        newmatrix->diag = diag;
        newmatrix->ndiag = ndiag;
    }
    else if (DataTypePetscEqual(datatype))
    {
#if defined(PETSC_USE)
        Mat A = (Mat)((*matrix)->data_petsc);
        const INT *tmpkcol;
        INT old_rowstart = (*matrix)->rows_start;
        INT *dnnz = (INT *)malloc(rownum * sizeof(INT));
        INT *onnz = (INT *)malloc(rownum * sizeof(INT));
        for (i = 0; i < rownum; i++)
        {
            rank = new2old_rank[newrow_oldrank[i]]; // 这行来自哪个进程
            if (rank == myrank)                     // 本进程的话就不依赖通信来的数据了
            {
                oldindex = old_gindex[i];
                MatGetRow(A, oldindex, &colnum, &tmpkcol, NULL);
                diagnum = 0;
                ndiagnum = 0;
                for (j = 0; j < colnum; j++)
                {
                    if (tmpkcol[j] >= colstart && tmpkcol[j] < colend)
                        diagnum++;
                    else
                        ndiagnum++;
                }
                dnnz[i] = diagnum;
                onnz[i] = ndiagnum;
                MatRestoreRow(A, oldindex, &colnum, &tmpkcol, NULL);
            }
            else
            {
                oldindex = newrow_oldrankindex[i];
                start = recvmat[newrow_oldrank[i]].RowPtr[oldindex];
                end = recvmat[newrow_oldrank[i]].RowPtr[oldindex + 1];
                tmp = recvmat[newrow_oldrank[i]].KCol;
                diagnum = 0;
                ndiagnum = 0;
                for (j = start; j < end; j++)
                {
                    if (tmp[j] >= colstart && tmp[j] < colend)
                        diagnum++;
                    else
                        ndiagnum++;
                }
                dnnz[i] = diagnum;
                onnz[i] = ndiagnum;
            }
        }
        MatMPIAIJSetPreallocation((Mat)(newmatrix->data_petsc), 0, dnnz, 0, onnz);
        MatSeqAIJSetPreallocation((Mat)(newmatrix->data_petsc), 0, dnnz);
        OpenPFEM_Free(dnnz);
        OpenPFEM_Free(onnz);
#else
        RaisePetscError("MatrixRowsReOrder");
#endif
    }
    else
    {
        RaiseError("MatrixRowsReOrder", "wrong datatype!");
    }

    // (4.3) 生成矩阵
    if (DataTypeOpenPFEMEqual(datatype))
    {
        CSRMAT *diag = newmatrix->diag, *ndiag = newmatrix->ndiag;
        INT *new_diag_rowptr = diag->RowPtr, *new_ndiag_rowptr = ndiag->RowPtr;
        INT *new_diag_kcol = (INT *)malloc(diag->NumEntries * sizeof(INT));
        INT *new_ndiag_kcol = (INT *)malloc(ndiag->NumEntries * sizeof(INT));
        DOUBLE *new_diag_entries = (DOUBLE *)malloc(diag->NumEntries * sizeof(DOUBLE));
        DOUBLE *new_ndiag_entries = (DOUBLE *)malloc(ndiag->NumEntries * sizeof(DOUBLE));
        DOUBLE *tmp2;
        INT old_rowstart = (*matrix)->rows_start;
        for (i = 0; i < rownum; i++)
        {
            rank = new2old_rank[newrow_oldrank[i]]; // 这行来自哪个进程
            if (rank == myrank)                     // 本进程的话就不依赖通信来的数据了
            {
                oldindex = old_gindex[i] - old_rowstart;
                start = old_diag_rowptr[oldindex];
                end = old_diag_rowptr[oldindex + 1];
                start2 = new_diag_rowptr[i];
                memcpy(new_diag_kcol + start2, old_diag_kcol + start, (end - start) * sizeof(INT));
                memcpy(new_diag_entries + start2, old_diag_entries + start, (end - start) * sizeof(DOUBLE));
                start = old_ndiag_rowptr[oldindex];
                end = old_ndiag_rowptr[oldindex + 1];
                start2 = new_ndiag_rowptr[i];
                for (j = 0; j < end - start; j++) // 非对角部分先都用全局编号
                {
                    new_ndiag_kcol[start2 + j] = old_ndiag_globalcols[old_ndiag_kcol[start + j]];
                }
                memcpy(new_ndiag_entries + start2, old_ndiag_entries + start, (end - start) * sizeof(DOUBLE));
            }
            else
            {
                oldindex = newrow_oldrankindex[i];
                start = recvmat[newrow_oldrank[i]].RowPtr[oldindex];
                end = recvmat[newrow_oldrank[i]].RowPtr[oldindex + 1];
                start2 = new_diag_rowptr[i];
                end2 = new_ndiag_rowptr[i];
                tmp = recvmat[newrow_oldrank[i]].KCol;
                tmp2 = recvmat[newrow_oldrank[i]].Entries;
                diagnum = 0;
                ndiagnum = 0;
                for (j = start; j < end; j++)
                {
                    if (tmp[j] >= colstart && tmp[j] < colend)
                    {
                        new_diag_kcol[start2 + diagnum] = tmp[j] - colstart;
                        new_diag_entries[start2 + diagnum] = tmp2[j];
                        diagnum++;
                    }
                    else
                    {
                        new_ndiag_kcol[end2 + ndiagnum] = tmp[j];
                        new_ndiag_entries[end2 + ndiagnum] = tmp2[j];
                        ndiagnum++;
                    }
                }
            }
        }
        diag->KCol = new_diag_kcol;
        ndiag->KCol = new_ndiag_kcol;
        diag->Entries = new_diag_entries;
        ndiag->Entries = new_ndiag_entries;
        MatrixComplete(newmatrix);
    }
    else if (DataTypePetscEqual(datatype))
    {
#if defined(PETSC_USE)
        Mat A = (Mat)((*matrix)->data_petsc);
        Mat newA = (Mat)(newmatrix->data_petsc);
        const INT *tmpkcol;
        const DOUBLE *tmpentries;
        INT old_rowstart = (*matrix)->rows_start;
        INT new_rowstart = newmatrix->rows_start;
        for (i = 0; i < rownum; i++)
        {
            rank = new2old_rank[newrow_oldrank[i]]; // 这行来自哪个进程
            if (rank == myrank)                     // 本进程的话就不依赖通信来的数据了
            {
                oldindex = old_gindex[i];
                MatGetRow(A, oldindex, &colnum, &tmpkcol, &tmpentries);
                for (j = 0; j < colnum; j++)
                {
                    MatSetValue(newA, i + new_rowstart, tmpkcol[j], tmpentries[j], INSERT_VALUES);
                }
                MatRestoreRow(A, oldindex, &colnum, &tmpkcol, &tmpentries);
            }
            else
            {
                oldindex = newrow_oldrankindex[i];
                start = recvmat[newrow_oldrank[i]].RowPtr[oldindex];
                end = recvmat[newrow_oldrank[i]].RowPtr[oldindex + 1];
                tmpkcol = recvmat[newrow_oldrank[i]].KCol;
                tmpentries = recvmat[newrow_oldrank[i]].Entries;
                for (j = start; j < end; j++)
                {
                    MatSetValue(newA, i + new_rowstart, tmpkcol[j], tmpentries[j], INSERT_VALUES);
                }
            }
        }
        MatAssemblyBegin((Mat)(newmatrix->data_petsc), MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd((Mat)(newmatrix->data_petsc), MAT_FINAL_ASSEMBLY);
#else
        RaisePetscError("MatrixRowsReOrder");
#endif
    }
    else
    {
        RaiseError("MatrixRowsReOrder", "wrong datatype!");
    }

    for (i = 0; i < new2old_ranknum; i++)
    {
        if (myrank != new2old_rank[i])
        {
            OpenPFEM_Free(recvmat[i].RowPtr);
            OpenPFEM_Free(recvmat[i].KCol);
            OpenPFEM_Free(recvmat[i].Entries);
        }
    }
    OpenPFEM_Free(recvmat);
    OpenPFEM_Free(oldrow_torank_num_displs);
    OpenPFEM_Free(newrow_fromrank_num_displs);
    OpenPFEM_Free(newrow_fromrank);
    OpenPFEM_Free(newrow_fromrank_num);
    OpenPFEM_Free(oldrow_torank);
    OpenPFEM_Free(oldrow_torank_num);
    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(recvsize);
    OpenPFEM_Free(recvdispls);
    OpenPFEM_Free(package);
    OpenPFEM_Free(recv);
    OpenPFEM_Free(newrow_oldrank);
    OpenPFEM_Free(newrow_oldrankindex);

    // MatView((Mat)(newmatrix->data_petsc), PETSC_VIEWER_STDOUT_WORLD);
    // MPI_Barrier(MPI_COMM_WORLD);
    //     MPI_Abort(MPI_COMM_WORLD, 1);
    // MatrixPrint(newmatrix, 0);

    MatrixDestroy(matrix);
    *matrix = newmatrix;
}

void MatrixConvert(MATRIX *matrix, DATATYPE target_type, bool remain_current)
{
    DATATYPE current_type = matrix->type;
    matrix->type = target_type;
    if (DataTypeOpenPFEMEqual(target_type))
    {
        if (matrix->if_matrix)
        {
            // OpenPFEM -> OpenPFEM
            return;
        }
        else if (DataTypePetscEqual(current_type))
        {
            // PETSc -> OpenPFEM
            RaiseError("MatrixConvert", "Convertion from PETSc to OpenPFEM not implemented.");
        }
        else
        {
            RaiseError("MatrixConvert", "Unrecognized target type.");
        }
    }
    else if (DataTypePetscEqual(target_type))
    {
        if (matrix->if_petsc)
        {
            // PETSc -> PETSc
            return;
        }
        else if (DataTypeOpenPFEMEqual(current_type))
        {
            // OpenPFEM -> PETSc
#if defined(PETSC_USE)
            matrix->if_petsc = true;
            Mat A;
            MPI_Comm comm = matrix->comm;
            MatCreate(comm, &A);
            INT local_nrows = matrix->local_nrows, local_ncols = matrix->local_ncols;
            INT global_nrows = matrix->global_nrows, global_ncols = matrix->global_ncols;
            MatSetSizes(A, local_nrows, local_ncols, global_nrows, global_ncols);
            MatSetFromOptions(A);
            INT *dnnz = (INT *)malloc(local_nrows * sizeof(INT));
            INT *onnz = (INT *)malloc(global_nrows * sizeof(INT));
            INT *diag_rowptr = matrix->diag->RowPtr, *ndiag_rowptr = matrix->ndiag->RowPtr;
            INT *diag_kcol = matrix->diag->KCol, *ndiag_kcol = matrix->ndiag->KCol;
            DOUBLE *diag_entries = matrix->diag->Entries, *ndiag_entries = matrix->ndiag->Entries;
            INT idx_row;
            for (idx_row = 0; idx_row < local_nrows; idx_row++)
            {
                dnnz[idx_row] = diag_rowptr[idx_row + 1] - diag_rowptr[idx_row];
                onnz[idx_row] = ndiag_rowptr[idx_row + 1] - ndiag_rowptr[idx_row];
            }
            MatMPIAIJSetPreallocation(A, 0, dnnz, 0, onnz);
            MatSeqAIJSetPreallocation(A, 0, dnnz);
            OpenPFEM_Free(dnnz);
            OpenPFEM_Free(onnz);
            INT rowindex_g, colindex_g, idx_col;
            INT row_start = matrix->rows_start, col_start = matrix->cols_start;
            INT *ndiag_globalcols = matrix->ndiag_globalcols;
            DOUBLE value;
            for (idx_row = 0; idx_row < local_nrows; idx_row++)
            {
                rowindex_g = row_start + idx_row;
                // diag
                for (idx_col = diag_rowptr[idx_row]; idx_col < diag_rowptr[idx_row + 1]; idx_col++)
                {
                    colindex_g = diag_kcol[idx_col] + col_start;
                    value = diag_entries[idx_col];
                    MatSetValue(A, rowindex_g, colindex_g, value, INSERT_VALUES);
                }
                // ndiag
                for (idx_col = ndiag_rowptr[idx_row]; idx_col < ndiag_rowptr[idx_row + 1]; idx_col++)
                {
                    colindex_g = ndiag_globalcols[ndiag_kcol[idx_col]];
                    value = ndiag_entries[idx_col];
                    MatSetValue(A, rowindex_g, colindex_g, value, INSERT_VALUES);
                }
            }
            MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
            matrix->data_petsc = (void *)A;
            if (!remain_current)
            {
                MatrixCleanOpenPFEM(matrix);
            }
#else
            RaisePetscError("MatrixConvert");
#endif
        }
        else
        {
            RaiseError("MatrixConvert", "Unrecognized target type.");
        }
    }
    OpenPFEM_Free(matrix->ops);
    MatrixOpsCreate(matrix, target_type);
}

void MatrixOpsCreate(MATRIX *matrix, DATATYPE type)
{
    MATRIX_OPS *ops = (MATRIX_OPS *)malloc(sizeof(MATRIX_OPS));
    if (DataTypeOpenPFEMEqual(type))
    {
        ops->create = MatrixCreate_OpenPFEM;
        ops->destroy = MatrixDestroy_OpenPFEM;
        ops->sturcturecompare = MatrixSturctureCompare_OpenPFEM;
        ops->scale = MatrixScale_OpenPFEM;
        ops->axpby = MatrixAxpby_OpenPFEM;

        ops->vectorcreate = VectorCreateByMatrix_OpenPFEM;
        ops->vectorcreate_T = VectorCreateByMatrixT_OpenPFEM;
        ops->matrixvectormult = MatrixVectorMult_OpenPFEM;
        ops->matrixvectormult_T = MatrixTransposeVectorMult_OpenPFEM;
        ops->matrixvectormult_add = MatrixVectorMultAdd_OpenPFEM;
        ops->matrixvectormult_sub = MatrixVectorMultSub_OpenPFEM;

        ops->vectorscreate = VectorsCreateByMatrix_OpenPFEM;
        ops->vectorscreate_T = VectorsCreateByMatrixT_OpenPFEM;
        ops->matrixvectorsmult = MatrixVectorsMult_OpenPFEM;
        ops->matrixvectorsmult_T = MatrixTransposeVectorsMult_OpenPFEM;
    }
    if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        ops->create = MatrixCreate_PETSc;
        ops->destroy = MatrixDestroy_PETSc;
        ops->sturcturecompare = MatrixSturctureCompare_PETSc;
        ops->scale = MatrixScale_PETSc;
        ops->axpby = MatrixAxpby_PETSc;

        ops->vectorcreate = VectorCreateByMatrix_PETSc;
        ops->vectorcreate_T = VectorCreateByMatrixT_PETSc;
        ops->matrixvectormult = MatrixVectorMult_PETSc;
        ops->matrixvectormult_T = MatrixTransposeVectorMult_PETSc;
        ops->matrixvectormult_add = MatrixVectorMultAdd_PETSc;
        ops->matrixvectormult_sub = MatrixVectorMultSub_PETSc;
#else
        RaisePetscError("MatrixOpsCreation");
#endif
#if defined(SLEPC_USE)
        ops->vectorscreate = VectorsCreateByMatrix_SLEPc;
        ops->vectorscreate_T = VectorsCreateByMatrixT_SLEPc;
        ops->matrixvectorsmult = MatrixVectorsMult_SLEPc;
        ops->matrixvectorsmult_T = MatrixTransposeVectorsMult_SLEPc;
#else
        ops->vectorscreate = NULL;
        ops->vectorscreate_T = NULL;
        ops->matrixvectorsmult = NULL;
        ops->matrixvectorsmult_T = NULL;
#endif
    }
    matrix->ops = (void *)ops;
}

void MatrixSturctureCompare(MATRIX *A, MATRIX *B, NONZERO_STRUCT *nonzero_struct)
{
    if (DataTypeEqual(A->type, B->type))
    {
        ((MATRIX_OPS *)(A->ops))->sturcturecompare(A, B, nonzero_struct);
    }
    else
    {
        RaiseError("MatrixSturctureCompare", "Inconsistent datatype!");
    }
}

void MatrixScale(DOUBLE alpha, MATRIX *A)
{
    ((MATRIX_OPS *)(A->ops))->scale(alpha, A);
}

void MatrixAxpby(DOUBLE alpha, MATRIX *A, DOUBLE beta, MATRIX *B, NONZERO_STRUCT nonzero_struct)
{
    if (DataTypeEqual(A->type, B->type))
    {
        ((MATRIX_OPS *)(A->ops))->axpby(alpha, A, beta, B, nonzero_struct);
    }
    else
    {
        RaiseError("MatrixAxpby", "Inconsistent datatype!");
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                                 Vector                                  */
/////////////////////////////////////////////////////////////////////////////

void VectorCreate(VECTOR **Vector, DATATYPE type)
{
    VECTOR *vector = (VECTOR *)malloc(sizeof(VECTOR));
    vector->type = type;
    vector->local_length = 0;
    vector->global_length = 0;
    vector->start = 0;
    vector->end = 0;
    vector->comm = MPI_COMM_NULL;
    vector->delete_dirichletbd = false;
    // OpenPFEM
    vector->if_vector = false;
    vector->data = NULL;
    // PETSc
    vector->if_petsc = false;
    vector->data_petsc = NULL;

    VectorOpsCreate(vector, type);
    ((VECTOR_OPS *)(vector->ops))->create(vector);
    *Vector = vector;
}

void VectorDestroy(VECTOR **vector)
{
    if ((*vector)->if_vector)
    {
        VectorDestroy_OpenPFEM(*vector);
    }
    if ((*vector)->if_petsc)
    {
#if defined(PETSC_USE)
        VectorDestroy_PETSc(*vector);
#else
        RaisePetscError("VectorDestroy");
#endif
    }
    MPI_Comm_free(&((*vector)->comm));
    OpenPFEM_Free(*vector);
}

void VectorPrint(VECTOR *vector, INT printrank)
{
    MPI_Comm comm = vector->comm;
    INT myrank;
    MPI_Comm_rank(comm, &myrank);
    if (myrank != printrank)
    {
        return;
    }
    printf("================================================\n");
    printf("向量的全局尺寸：%d\n ", vector->global_length);
    printf("向量的局部尺寸：%d(%d-%d)\n ",
           vector->local_length, vector->start, vector->end);
    printf("------------------------------------------------\n");
    INT i;
    for (i = 0; i < vector->local_length; i++)
    {
        printf("-- [%d]=%g\n", i, vector->data[i]);
    }
    printf("================================================\n");
}

void VectorOutput(VECTOR *vector, const char *name)
{
    if (DataTypePetscEqual(vector->type))
    {
#if defined(PETSC_USE)
        MPI_Comm comm;
        Vec vec = (Vec)(vector->data_petsc);
        PetscObjectGetComm((PetscObject)vec, &comm);
        PetscViewer viewer;
        // PetscViewerBinaryOpen(comm, name, FILE_MODE_WRITE, &viewer);
        VecView(vec, viewer);
        PetscViewerDestroy(&viewer);
#else
        RaisePetscError("VectorOutput");
#endif
    }
    else
    {
        RaiseError("VectorOutput", "Not implemented!");
    }
}

void VectorConvert(VECTOR *vector, DATATYPE target_type, bool remain_current)
{
    DATATYPE current_type = vector->type;
    vector->type = target_type;
    if (DataTypeOpenPFEMEqual(target_type))
    {
        if (vector->if_vector)
        {
            // OpenPFEM -> OpenPFEM
            return;
        }
        else if (DataTypePetscEqual(current_type))
        {
            // PETSc -> OpenPFEM
#if defined(PETSC_USE)
            DOUBLE *array = NULL;
            VecGetArray((vector->data_petsc), &array);
            INT local_length = vector->local_length;
            vector->data = (DOUBLE *)malloc(local_length * sizeof(DOUBLE));
            memcpy(vector->data, array, local_length * sizeof(INT));
            if (!remain_current)
            {
                VectorCleanPetsc(vector);
            }
#else
            RaisePetscError("VectorConvert");
#endif
        }
        else
        {
            RaiseError("VectorConvert", "Unrecognized target type.");
            return;
        }
    }
    else if (DataTypePetscEqual(target_type))
    {
        if (vector->if_petsc)
        {
            // PETSc -> PETSc
            return;
        }
        else if (DataTypeOpenPFEMEqual(current_type))
        {
            // OpenPFEM -> PETSc
#if defined(PETSC_USE)
            vector->if_petsc = true;
            Vec vec;
            MPI_Comm comm = vector->comm;
            VecCreate(comm, &vec);
            INT local_length = vector->local_length, global_length = vector->global_length;
            VecSetSizes(vec, local_length, global_length);
            VecSetFromOptions(vec);
            INT *Gindex = (INT *)malloc(local_length * sizeof(INT));
            INT i;
            for (i = 0; i < local_length; i++)
            {
                Gindex[i] = vector->start + i;
            }
            VecSetValues(vec, local_length, Gindex, vector->data, INSERT_VALUES);
            VecAssemblyBegin(vec);
            VecAssemblyEnd(vec);
            OpenPFEM_Free(Gindex);
            vector->data_petsc = (void *)vec;
            if (!remain_current)
            {
                VectorCleanOpenPFEM(vector);
            }
#else
            RaisePetscError("VectorConvert");
#endif
        }
        else
        {
            RaiseError("VectorConvert", "Unrecognized target type.");
            return;
        }
    }
    OpenPFEM_Free(vector->ops);
    VectorOpsCreate(vector, target_type);
}

void VectorOpsCreate(VECTOR *vector, DATATYPE type)
{
    VECTOR_OPS *ops = (VECTOR_OPS *)malloc(sizeof(VECTOR_OPS));
    if (DataTypeOpenPFEMEqual(type))
    {
        ops->create = VectorCreate_OpenPFEM;
        ops->destroy = VectorDestroy_OpenPFEM;
        ops->getarray = VectorGetArray_OpenPFEM;
        ops->scale = VectorScale_OpenPFEM;
        ops->axpby = VectorAxpby_OpenPFEM;
        ops->norm = VectorNorm_OpenPFEM;
    }
    if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        ops->create = VectorCreate_PETSc;
        ops->destroy = VectorDestroy_PETSc;
        ops->getarray = VectorGetArray_PETSc;
        ops->scale = VectorScale_PETSc;
        ops->axpby = VectorAxpby_PETSc;
        ops->norm = VectorNorm_PETSc;
#else
        RaisePetscError("VectorOpsCreation");
#endif
    }
    vector->ops = (void *)ops;
}

void VectorGetArray(VECTOR *vector, DOUBLE **data)
{
    ((VECTOR_OPS *)(vector->ops))->getarray(vector, data);
}

void VectorScale(DOUBLE alpha, VECTOR *x)
{
    ((VECTOR_OPS *)(x->ops))->scale(alpha, x);
}

void VectorAxpby(DOUBLE alpha, VECTOR *x, DOUBLE beta, VECTOR *y)
{
    if (DataTypeEqual(x->type, y->type))
    {
        ((VECTOR_OPS *)(x->ops))->axpby(alpha, x, beta, y);
    }
    else
    {
        RaiseError("VectorAxpby", "Inconsistent datatype!");
    }
}

void VectorNorm(VECTOR *vector, DOUBLE *alpha)
{
    ((VECTOR_OPS *)(vector->ops))->norm(vector, alpha);
}

/////////////////////////////////////////////////////////////////////////////
/*                                Vectors                                  */
/////////////////////////////////////////////////////////////////////////////

void VectorsCreate(VECTORS **Vectors, DATATYPE type)
{
    VECTORS *vectors = (VECTORS *)malloc(sizeof(VECTORS));
    vectors->type = type;
    vectors->nvecs = 0;
    vectors->local_length = 0;
    vectors->global_length = 0;
    vectors->start = 0;
    vectors->end = 0;
    vectors->start_cols = 0;
    vectors->end_cols = 0;
    vectors->comm = MPI_COMM_NULL;
    vectors->delete_dirichletbd = false;
    // OpenPFEM
    vectors->if_vectors = false;
    vectors->data = NULL;
    // SLEPc
    vectors->if_slepc = false;
    vectors->data_slepc = NULL;

    VectorsOpsCreate(vectors, type);
    ((VECTORS_OPS *)(vectors->ops))->create(vectors);
    *Vectors = vectors;
}

void VectorsSetRange(VECTORS *vectors, INT start, INT end)
{
    if (start < 0)
        RaiseError("VectorsSetRange", "start should be greater than 0!\n");
    if (end > vectors->nvecs)
        RaiseError("VectorsSetRange", "end couldn't be greater than or equal to the number of vector(s)!\n");
    if (start >= end)
        RaiseError("VectorsSetRange", "start should be less than end!\n");
    vectors->start_cols = start;
    vectors->end_cols = end;
}

void VectorsDestroy(VECTORS **vectors)
{
    if ((*vectors)->if_vectors)
    {
        VectorsDestroy_OpenPFEM(*vectors);
    }
    if ((*vectors)->if_slepc)
    {
#if defined(SLEPC_USE)
        VectorsDestroy_SLEPc(*vectors);
#else
        RaiseSlepcError("VectorsDestroy");
#endif
    }
    MPI_Comm_free(&((*vectors)->comm));
    OpenPFEM_Free(*vectors);
}

void VectorsOpsCreate(VECTORS *vectors, DATATYPE type)
{
    VECTORS_OPS *ops = (VECTORS_OPS *)malloc(sizeof(VECTORS_OPS));
    if (DataTypeOpenPFEMEqual(type))
    {
        ops->create = VectorsCreate_OpenPFEM;
        ops->destroy = VectorsDestroy_OpenPFEM;
        ops->getarray = VectorsGetArray_OpenPFEM;
        ops->restorearray = VectorsRestoreArray_OpenPFEM;
    }
    if (DataTypeSlepcEqual(type))
    {
#if defined(SLEPC_USE)
        ops->create = VectorsCreate_SLEPc;
        ops->destroy = VectorsDestroy_SLEPc;
        ops->getarray = VectorsGetArray_SLEPc;
        ops->restorearray = VectorsRestoreArray_SLEPc;
#else
        RaiseSlepcError("VectorsOpsCreation");
#endif
    }
    vectors->ops = (void *)ops;
}

void VectorsGetArray(VECTORS *vectors, DOUBLE **data)
{
    ((VECTORS_OPS *)(vectors->ops))->getarray(vectors, data);
}

void VectorsRestoreArray(VECTORS *vectors, DOUBLE **data)
{
    ((VECTORS_OPS *)(vectors->ops))->restorearray(vectors, data);
}

void VectorsAxpby(DOUBLE alpha, VECTORS *x, DOUBLE beta, VECTORS *y)
{
    if (x->local_length != y->local_length)
        RaiseError("VectorsAxpby", "Different local length!\n");
    INT x_s = x->start_cols, x_e = x->end_cols;
    INT y_s = y->start_cols, y_e = y->end_cols;
    INT col_num = x_e - x_s, length = x->local_length, num = col_num * length;
    if (x_e - x_s != y_e - y_s)
        RaiseError("VectorsAxpby", "Inconsistent number of column!\n");
    DOUBLE *data_x = NULL, *data_y = NULL;
    VectorsGetArray(x, &data_x);
    VectorsGetArray(y, &data_y);
#if defined(BLAS_USE)
    if (beta == 0.0)
    {
        memset(data_y + y_s * length, 0.0, num * sizeof(DOUBLE));
    }
    else if (beta != 1.0)
    {
        INT one = 1;
        // dscal_(&(num), &beta, data_y + y_s * length, &one);
    }
    if (alpha == 0.0)
    {
        return;
    }
    INT one = 1;
    // daxpy_(&(num), &alpha, data_x + x_s * length, &one, data_y + y_s * length, &one);
#else
    INT i;
    DOUBLE temp;
    for (i = 0; i < num; i++)
    {
        temp = alpha * data_x[i + x_s * length] + beta * data_y[i + y_s * length];
        data_y[i + y_s * length] = temp;
    }
#endif
    VectorsRestoreArray(x, &data_x);
    VectorsRestoreArray(y, &data_y);
}

/////////////////////////////////////////////////////////////////////////////
/*                            Matrix - Vector                              */
/////////////////////////////////////////////////////////////////////////////

void VectorCreateByMatrix(VECTOR **vector, MATRIX *matrix)
{
    MATRIX_OPS *ops = (MATRIX_OPS *)(matrix->ops);
    ops->vectorcreate(vector, matrix);
    (*vector)->delete_dirichletbd = matrix->delete_dirichletbd;
}

void VectorCreateByMatrixTranspose(VECTOR **vector, MATRIX *matrix)
{
    MATRIX_OPS *ops = (MATRIX_OPS *)(matrix->ops);
    ops->vectorcreate_T(vector, matrix);
    (*vector)->delete_dirichletbd = matrix->delete_dirichletbd;
}

void MatrixVectorMult(MATRIX *A, VECTOR *x, VECTOR *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        ((MATRIX_OPS *)(A->ops))->matrixvectormult(A, x, y);
    }
    else
    {
        RaiseError("MatrixVectorMult", "Inconsistent datatype!");
    }
}

void MatrixTransposeVectorMult(MATRIX *A, VECTOR *x, VECTOR *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        if (A->symmetric)
            ((MATRIX_OPS *)(A->ops))->matrixvectormult(A, x, y);
        else
            ((MATRIX_OPS *)(A->ops))->matrixvectormult_T(A, x, y);
    }
    else
    {
        RaiseError("MatrixTransposeVectorMult", "Inconsistent datatype!");
    }
}

void MatrixVectorMultAdd(MATRIX *A, VECTOR *x, VECTOR *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        ((MATRIX_OPS *)(A->ops))->matrixvectormult_add(A, x, y);
    }
    else
    {
        RaiseError("MatrixVectorMultAdd", "Inconsistent datatype!");
    }
}

void MatrixVectorMultSub(MATRIX *A, VECTOR *x, VECTOR *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        ((MATRIX_OPS *)(A->ops))->matrixvectormult_sub(A, x, y);
    }
    else
    {
        RaiseError("MatrixVectorMultSub", "Inconsistent datatype!");
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                           Matrix - Vectors                              */
/////////////////////////////////////////////////////////////////////////////

void VectorsCreateByMatrix(VECTORS **vectors, MATRIX *matrix, INT num)
{
    MATRIX_OPS *ops = (MATRIX_OPS *)(matrix->ops);
    ops->vectorscreate(vectors, matrix, num);
    (*vectors)->delete_dirichletbd = matrix->delete_dirichletbd;
}

void VectorsCreateByMatrixTranspose(VECTORS **vectors, MATRIX *matrix, INT num)
{
    MATRIX_OPS *ops = (MATRIX_OPS *)(matrix->ops);
    ops->vectorscreate_T(vectors, matrix, num);
    (*vectors)->delete_dirichletbd = matrix->delete_dirichletbd;
}

void MatrixVectorsMult(MATRIX *A, VECTORS *x, VECTORS *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        ((MATRIX_OPS *)(A->ops))->matrixvectorsmult(A, x, y);
    }
    else
    {
        RaiseError("MatrixVectorsMult", "Inconsistent datatype!");
    }
}

void MatrixTransposeVectorsMult(MATRIX *A, VECTORS *x, VECTORS *y)
{
    if (DataTypeEqual(A->type, x->type) && DataTypeEqual(A->type, y->type))
    {
        if (A->symmetric)
            ((MATRIX_OPS *)(A->ops))->matrixvectorsmult(A, x, y);
        else
            ((MATRIX_OPS *)(A->ops))->matrixvectorsmult_T(A, x, y);
    }
    else
    {
        RaiseError("MatrixTransposeVectorsMult", "Inconsistent datatype!");
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                           Vectors - Vector                              */
/////////////////////////////////////////////////////////////////////////////