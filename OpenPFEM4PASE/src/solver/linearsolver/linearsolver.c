#include "linearsolver.h"

void LinearSolverCreate(LINEARSOLVER **linearsolver, SOLVERTYPE type)
{
    *linearsolver = (LINEARSOLVER *)malloc(sizeof(LINEARSOLVER));
    (*linearsolver)->solvertype = type;
    // 设置
    (*linearsolver)->rtol = 1.0e-14;
    (*linearsolver)->atol = 1.0e-14;
    (*linearsolver)->maxitr = 10000;
    // 一些可能需要的结果
    (*linearsolver)->calltimes = 0;
    (*linearsolver)->residualnorm = -1.0;
    // 数据
    (*linearsolver)->matrix = NULL;
    (*linearsolver)->vector_rhs = NULL;
    (*linearsolver)->vector_sol = NULL;
    (*linearsolver)->multilevel = NULL;
    (*linearsolver)->alreadysetup = false;
    // 实现
    (*linearsolver)->ops = NULL;
    (*linearsolver)->data = NULL;
    LinearSolverSetType((*linearsolver), type);
}

void LinearSolverSetType(LINEARSOLVER *linearsolver, SOLVERTYPE type)
{
    if (SolverTypeEqual_PETSC_KSPCG(type))
    {
#if defined(PETSC_USE)
        OpenPFEM_Free(linearsolver->ops);
        LinearSolverCreate_PETSc_KSPCG(linearsolver);
#else
        RaisePetscError("LinearSolverSetType");
#endif
    }
    else if (SolverTypeEqual_PETSC_SUPERLU(type))
    {
#if defined(PETSC_USE)
        OpenPFEM_Free(linearsolver->ops);
        LinearSolverCreate_PETSc_SUPERLU(linearsolver);
#else
        RaisePetscError("LinearSolverSetType");
#endif
    }
    else if (SolverTypeEqual_MULTILEVEL_LINEAR(type))
    {
        OpenPFEM_Free(linearsolver->ops);
        LinearSolverCreate_MultiLevel(linearsolver);
    }
    else
    {
        RaiseError("LinearSolverSetType", "Wrong solvertype!");
    }
}

void LinearSolverSetUp(LINEARSOLVER *solver, MATRIX *matrix)
{
    if (solver->alreadysetup)
        RaiseError("LinearSolverSetUp", "Already set up!");
    solver->matrix = matrix;
    ((LINEARSOLVER_OPS *)(solver->ops))->setup(solver);
    solver->alreadysetup = true;
}

void LinearSolve(LINEARSOLVER *solver, MATRIX *matrix, VECTOR *rhs, VECTOR *solution)
{
    LINEARSOLVER_OPS *ops = (LINEARSOLVER_OPS *)(solver->ops);
    if (solver->alreadysetup && matrix != solver->matrix)
        RaiseError("LinearSolve", "Matrix is different from the set up matrix!");
    solver->matrix = matrix;
    solver->vector_rhs = rhs;
    solver->vector_sol = solution;
    if (!solver->alreadysetup)
    {
        ops->setup(solver);
    }
    ops->solve(solver);
    solver->calltimes++;
    if (!solver->alreadysetup)
    {
        ops->destory(solver);
        solver->matrix = NULL;
    }
    solver->vector_rhs = NULL;
    solver->vector_sol = NULL;
}

void LinearSolverReSet(LINEARSOLVER *solver, MATRIX *matrix)
{
    if (!solver->alreadysetup)
        RaiseError("LinearSolverReSet", "Never set up!");
    ((LINEARSOLVER_OPS *)(solver->ops))->destory(solver);
    solver->matrix = matrix;
    solver->multilevel = NULL;
    ((LINEARSOLVER_OPS *)(solver->ops))->setup(solver);
}

void LinearSolverDestroy(LINEARSOLVER **solver)
{
    (*solver)->matrix = NULL;
    (*solver)->vector_rhs = NULL;
    (*solver)->vector_sol = NULL;
    (*solver)->multilevel = NULL;
    if ((*solver)->ops != NULL)
    {
        if ((*solver)->alreadysetup)
            ((LINEARSOLVER_OPS *)((*solver)->ops))->destory(*solver);
        OpenPFEM_Free((*solver)->ops);
    }
    OpenPFEM_Free((*solver));
}

void LinearSolverMultiLevelSetUp(LINEARSOLVER *solver, MULTILEVEL *multilevel)
{
    if (solver->alreadysetup)
        RaiseError("LinearSolverMultiLevelSetUp", "Already set up!");
    solver->multilevel = multilevel;
    ((LINEARSOLVER_OPS *)(solver->ops))->setup(solver);
    solver->alreadysetup = true;
}

void LinearSolveMultiLevel(LINEARSOLVER *solver, MULTILEVEL *multilevel)
{
    LINEARSOLVER_OPS *ops = (LINEARSOLVER_OPS *)(solver->ops);
    if (solver->alreadysetup && multilevel != solver->multilevel)
        RaiseError("LinearSolveMultiLevel", "Multilevel is different from the set up one!");
    solver->multilevel = multilevel;
    if (!solver->alreadysetup)
    {
        ops->setup(solver);
    }
    ops->solve(solver);
    solver->calltimes++;
    if (!solver->alreadysetup)
    {
        ops->destory(solver);
        solver->multilevel = NULL;
    }
}

void LinearSolverMultiLevelReSet(LINEARSOLVER *solver, MULTILEVEL *multilevel)
{
    if (!solver->alreadysetup)
        RaiseError("LinearSolverMultiLevelReSet", "Never set up!");
    ((LINEARSOLVER_OPS *)(solver->ops))->destory(solver);
    solver->multilevel = multilevel;
    ((LINEARSOLVER_OPS *)(solver->ops))->setup(solver);
    solver->matrix = NULL;
    solver->vector_rhs = NULL;
    solver->vector_sol = NULL;
}