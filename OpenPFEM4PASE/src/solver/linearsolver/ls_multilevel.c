#include "linearsolver.h"

typedef const char *METHOD;
#define V_CYCLE "V-cycle"

typedef const char *SMOOTHMETHOD;
#define CG_SMOOTHING "cg"

// 用来存放本solver各类数据和参数的结构体，会保存在linearsolver->data中，随着destory而销毁
typedef struct LS_MULTILEVEL
{
    METHOD method;
    DATATYPE datatype;
    SMOOTHMETHOD smoothmethod;
    SOLVERTYPE coarse_solvertype;
    INT level_num;
    INT current_level;
    INT coarse_level;
    INT fine_level;
    INT initialsolution_level;
    INT cycle_times;
    INT presmooth_itrtimes;
    INT postsmooth_itrtimes;
    LINEARSOLVER *coarse_solver;
    VECTOR **vector_tmp;
} LS_MULTILEVEL;

/////////////////////////////////////////////////////////////////////////////
/*                         prolong / restrict                              */
/////////////////////////////////////////////////////////////////////////////

static void Prolong_formItoJ(LINEARSOLVER *solver, VECTOR *vector_i, VECTOR *vector_j, INT i, INT j)
{
    INT ii;
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    VECTOR *from, *to;

    MATRIX **prolongs = ml->prolongs;
    MATRIX **restricts = ml->restricts;
    for (ii = i; ii < j; ii++)
    {
        if (ii == i)
            from = vector_i;
        else
            from = data->vector_tmp[ii];
        if (ii == j - 1)
            to = vector_j;
        else
            to = data->vector_tmp[ii + 1];
        if (prolongs[ii] != NULL)
            MatrixVectorMult(prolongs[ii], from, to);
        else if (restricts[ii] != NULL)
            MatrixTransposeVectorMult(restricts[ii], from, to);
        else
            RaiseError("Prolong_formItoJ", "Prolong or Restrict matrices dont exist!");
    }
}

static void Restrict_formItoJ(LINEARSOLVER *solver, VECTOR *vector_i, VECTOR *vector_j, INT i, INT j)
{
    INT ii;
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    VECTOR *from, *to;
    MATRIX **restricts = ml->restricts;
    MATRIX **prolongs = ml->prolongs;
    for (ii = i; ii > j; ii--)
    {
        if (ii == i)
            from = vector_i;
        else
            from = data->vector_tmp[ii];
        if (ii == j + 1)
            to = vector_j;
        else
            to = data->vector_tmp[ii - 1];
        if (restricts[ii - 1] != NULL)
            MatrixVectorMult(restricts[ii - 1], from, to);
        else if (prolongs[ii - 1] != NULL)
            MatrixTransposeVectorMult(prolongs[ii - 1], from, to);
        else
            RaiseError("Restrict_formItoJ", "Prolong or Restrict matrices dont exist!");
    }
}

// 把 vector 的第 i 层 插值/限制 到第 j 层
static void LEVEL_formItoJ(LINEARSOLVER *solver, VECTOR *vector_i, VECTOR *vector_j, INT i, INT j)
{
    if (i < j)
    {
        Prolong_formItoJ(solver, vector_i, vector_j, i, j);
    }
    else if (i > j)
    {
        Restrict_formItoJ(solver, vector_i, vector_j, i, j);
    }
    else
    {
        if (vector_i != vector_j)
        {
            VectorAxpby(1.0, vector_i, 0.0, vector_j);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                              smoothing                                  */
/////////////////////////////////////////////////////////////////////////////

static void MLLS_Smoothing_CG(MATRIX *A, VECTOR *b, VECTOR *x, INT times)
{
    DATATYPE type = A->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        RaiseError("MLLS_Smoothing_CG", "Type OpenPFEM not implemented!");
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        Vec bb = (Vec)b->data_petsc;
        Mat AA = (Mat)A->data_petsc;
        Vec xx = (Vec)x->data_petsc;
        MPI_Comm comm;
        PetscObjectGetComm((PetscObject)AA, &comm);
        KSP ksp;
        PC pc;
        KSPCreate(comm, &ksp);
        KSPSetType(ksp, KSPCG);
        KSPSetOperators(ksp, AA, AA);
        KSPSetTolerances(ksp, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, times);
        KSPSetConvergenceTest(ksp, KSPConvergedSkip, NULL, NULL);
        KSPSetInitialGuessNonzero(ksp, PETSC_TRUE);
        KSPSetFromOptions(ksp);
        KSPSetUp(ksp);
        KSPSolve(ksp, bb, xx);
        KSPDestroy(&ksp);
#else
        RaisePetscError(MultiLevelLinearSolver_Smoothing_CG);
#endif
    }
}

static void MLLS_Smoothing(LINEARSOLVER *solver, INT level, INT times)
{
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    INT l_index = data->current_level;
    if (strcmp(data->smoothmethod, CG_SMOOTHING) == 0)
    {
        MLLS_Smoothing_CG(ml->stiff_matrices[level], ml->rhs[level], ml->solution[level], times);
    }
    else
    {
        RaiseError("MLLS_Smoothing", "Smoothing method not implemented!");
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                               V - cycle                                 */
/////////////////////////////////////////////////////////////////////////////

static void MLLS_V_Cycle(LINEARSOLVER *solver)
{
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    INT current_level = data->current_level;
    if (current_level > data->coarse_level)
    {
        // (1) 前光滑
        MLLS_Smoothing(solver, current_level, data->presmooth_itrtimes);
        // (2)残差计算
        MatrixVectorMult(ml->stiff_matrices[current_level], ml->solution[current_level], data->vector_tmp[current_level]);
        VectorAxpby(1.0, ml->rhs[current_level], -1.0, data->vector_tmp[current_level]);
        // (3) 把残差限制到粗一层网格上
        LEVEL_formItoJ(solver, data->vector_tmp[current_level], ml->rhs[current_level - 1], current_level, current_level - 1);
        data->current_level = current_level - 1;
        // (4) 递归调用粗网格上的校正函数
        MLLS_V_Cycle(solver);
        // (5) 把粗网格上的误差近似添加到细网格的解上
        data->current_level = current_level;
        if (ml->prolongs != NULL)
            MatrixVectorMultAdd(ml->prolongs[current_level - 1], ml->solution[current_level - 1], ml->solution[current_level]);
        // (6) 后光滑
        MLLS_Smoothing(solver, current_level, data->postsmooth_itrtimes);
    }
    else
    {
        if (data->coarse_solver == NULL)
        {
            LINEARSOLVER *coarse_ls = NULL;
            LinearSolverCreate(&coarse_ls, PETSC_SUPERLU);
            LinearSolverSetUp(coarse_ls, ml->stiff_matrices[current_level]);
            data->coarse_solver = coarse_ls;
        }
        else if (data->coarse_solver->matrix != ml->stiff_matrices[current_level])
        {
            LinearSolverReSet(data->coarse_solver, ml->stiff_matrices[current_level]);
        }
        LinearSolve(data->coarse_solver, ml->stiff_matrices[current_level],
                    ml->rhs[current_level], ml->solution[current_level]);
    }
}

/////////////////////////////////////////////////////////////////////////////
/*                                核心函数                                  */
/////////////////////////////////////////////////////////////////////////////

static void MLLS_V_Setup(LINEARSOLVER *solver)
{
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)malloc(sizeof(LS_MULTILEVEL));
    // 默认设置
    data->method = V_CYCLE;
    data->datatype = ml->datatype;
    data->smoothmethod = CG_SMOOTHING;
    data->coarse_solvertype = PETSC_SUPERLU;

    data->level_num = ml->num_levels;
    data->current_level = -1; // 这三个置为-1的参数都留到具体solve的时候赋值
    data->coarse_level = -1;
    data->fine_level = -1;
    data->initialsolution_level = 0;

    data->cycle_times = 1;
    data->presmooth_itrtimes = 3;
    data->postsmooth_itrtimes = 3;

    data->coarse_solver = NULL;
    data->vector_tmp = (VECTOR **)malloc(ml->num_levels * sizeof(VECTOR *));
    INT i;
    for (i = 0; i < ml->num_levels; i++)
    {
        VectorCreateByMatrix(&(data->vector_tmp[i]), ml->stiff_matrices[i]);
    }

    solver->data = (void *)data;
}

static void MLLS_V_Solve(LINEARSOLVER *solver)
{
    MULTILEVEL *ml = solver->multilevel;
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    if (data->current_level == -1)
        data->current_level = ml->num_levels - 1;
    if (data->coarse_level == -1)
        data->coarse_level = 0;
    if (data->fine_level == -1)
        data->fine_level = ml->num_levels - 1;

    if (data->level_num < ml->num_levels)
    {
        data->vector_tmp = realloc(data->vector_tmp, ml->num_levels * sizeof(VECTOR *));
        INT i;
        for (i = data->level_num; i < ml->num_levels; i++)
        {
            VectorCreateByMatrix(&(data->vector_tmp[i]), ml->stiff_matrices[i]);
        }
        data->level_num = ml->num_levels;
    }

    // (1) 把初始的解插值到最细层
    LEVEL_formItoJ(solver, ml->solution[data->initialsolution_level], ml->solution[data->current_level],
                   data->initialsolution_level, data->current_level);

    // (2) 进行迭代
    INT idx_iter;
    for (idx_iter = 0; idx_iter < data->cycle_times; idx_iter++)
    {
        MLLS_V_Cycle(solver);
    }

    data->initialsolution_level = data->fine_level; // 这次计算到这里，下一次就可以拿这里当初值
    data->current_level = -1;
    data->coarse_level = -1;
    data->fine_level = -1;
}

static void MLLS_V_Destory(LINEARSOLVER *solver)
{
    LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
    if (data->coarse_solver != NULL)
        LinearSolverDestroy(&(data->coarse_solver));
    if (data->vector_tmp != NULL)
    {
        INT i;
        for (i = 0; i < data->level_num; i++)
        {
            VectorDestroy(data->vector_tmp + i);
        }
        OpenPFEM_Free(data->vector_tmp);
    }
    OpenPFEM_Free(data);
    solver->data = NULL;
}

void LinearSolverCreate_MultiLevel(LINEARSOLVER *solver)
{
    LINEARSOLVER_OPS *ops = (LINEARSOLVER_OPS *)malloc(sizeof(LINEARSOLVER_OPS));
    ops->setup = MLLS_V_Setup;
    ops->solve = MLLS_V_Solve;
    ops->destory = MLLS_V_Destory;
    solver->ops = (void *)ops;
}

/////////////////////////////////////////////////////////////////////////////
/*                                外部接口                                  */
/////////////////////////////////////////////////////////////////////////////

void LinearSolverMultiLevelSetLevel(LINEARSOLVER *solver, INT initial_level, INT coarse_level, INT fine_level)
{
    if (solver->alreadysetup)
    {
        LS_MULTILEVEL *data = (LS_MULTILEVEL *)(solver->data);
        if (initial_level != OpenPFEM_DEFAULT)
            data->initialsolution_level = initial_level;
        if (coarse_level != OpenPFEM_DEFAULT)
            data->initialsolution_level = coarse_level;
        if (fine_level != OpenPFEM_DEFAULT)
            data->initialsolution_level = fine_level;
    }
    else
    {
        RaiseError("LinearSolverMultiLevelSetInitialLevel", "Not set up");
    }
}