#include "linearsolver.h"

#if defined(PETSC_USE)

#define PETSCSOLVER_TYPECHECK(name, solver)                                \
    do                                                                     \
    {                                                                      \
        if (!(DataTypePetscEqual(solver->matrix->type)))                   \
            RaiseError(name, "the type of matrix is not PETSc.");          \
        if (!(DataTypePetscEqual(solver->vector_rhs->type)))               \
            RaiseError(name, "the type of rhs vector is not PETSc.");      \
        if (!(DataTypePetscEqual(solver->vector_sol->type)))               \
            RaiseError(name, "the type of solution vector is not PETSc."); \
    } while (0)

#define PETSCSOLVER_COMMCHECK(name, solver)                                              \
    do                                                                                   \
    {                                                                                    \
        int result;                                                                      \
        MPI_Comm_compare(solver->matrix->comm, solver->vector_rhs->comm, &result);       \
        if (result != MPI_IDENT && result != MPI_CONGRUENT)                              \
            RaiseError(name, "the communicators of matrix and rhs are different!");      \
        MPI_Comm_compare(solver->matrix->comm, solver->vector_sol->comm, &result);       \
        if (result != MPI_IDENT && result != MPI_CONGRUENT)                              \
            RaiseError(name, "the communicators of matrix and solution are different!"); \
    } while (0)

//////////////////////////////////////////////////////////////////////////////////
/*                                 Petsc KSPCG                                  */
//////////////////////////////////////////////////////////////////////////////////

static void Setup_PETSc_KSPCG(LINEARSOLVER *solver)
{
    KSP ksp;
    MPI_Comm comm = solver->matrix->comm;
    KSPCreate(comm, &ksp);
    KSPSetType(ksp, KSPCG);
    Mat A = (Mat)(solver->matrix->data_petsc);
    KSPSetOperators(ksp, A, A);
    KSPSetTolerances(ksp, solver->rtol, solver->atol, PETSC_DEFAULT, solver->maxitr);
    KSPSetInitialGuessNonzero(ksp, PETSC_TRUE);
    KSPSetFromOptions(ksp);
    KSPSetUp(ksp);
    solver->data = (void *)ksp;
}

static void Solve_PETSc_KSPCG(LINEARSOLVER *solver)
{
    PETSCSOLVER_TYPECHECK("Solve_PETSc_KSPCG", solver);
    PETSCSOLVER_COMMCHECK("Solve_PETSc_KSPCG", solver);
    KSPSolve((KSP)(solver->data), (Vec)(solver->vector_rhs->data_petsc),
             (Vec)(solver->vector_sol->data_petsc));
    Vec y;
    VecDuplicate((Vec)(solver->vector_rhs->data_petsc), &y);
    MatMult((Mat)(solver->matrix->data_petsc), (Vec)(solver->vector_sol->data_petsc), y);
    VecAXPY(y, -1.0, (Vec)(solver->vector_rhs->data_petsc));
    PetscReal val;
    VecNorm(y, NORM_2, &val);
    OpenPFEM_Print("||Ax-b||_2 %2.16f\n", val);
}

static void Destory_PETSc_KSPCG(LINEARSOLVER *solver)
{
    KSPDestroy((KSP *)(&(solver->data)));
    solver->data = NULL;
}

void LinearSolverCreate_PETSc_KSPCG(LINEARSOLVER *solver)
{
    LINEARSOLVER_OPS *ops = (LINEARSOLVER_OPS *)malloc(sizeof(LINEARSOLVER_OPS));
    ops->setup = Setup_PETSc_KSPCG;
    ops->solve = Solve_PETSc_KSPCG;
    ops->destory = Destory_PETSc_KSPCG;
    solver->ops = (void *)ops;
}

//////////////////////////////////////////////////////////////////////////////////
/*                         Petsc PCLU SUPERLU_DIST                              */
//////////////////////////////////////////////////////////////////////////////////
static void Setup_PETSc_SUPERLU(LINEARSOLVER *solver)
{
    KSP ksp;
    PC pc;
    Mat A = (Mat)(solver->matrix->data_petsc);
    MPI_Comm comm;
    PetscObjectGetComm((PetscObject)A, &comm);
    KSPCreate(comm, &ksp);
    KSPSetOperators(ksp, A, A);
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCLU);
    KSPSetFromOptions(ksp);
    PCFactorSetMatSolverType(pc, MATSOLVERSUPERLU_DIST);
    KSPSetUp(ksp);
    solver->data = (void *)ksp;
}

static void Solve_PETSc_SUPERLU(LINEARSOLVER *solver)
{
    PETSCSOLVER_TYPECHECK("Solve_PETSc_SUPERLU", solver);
    // PETSCSOLVER_COMMCHECK("Solve_PETSc_SUPERLU", solver);
    KSPSolve((KSP)(solver->data), (Vec)(solver->vector_rhs->data_petsc),
             (Vec)(solver->vector_sol->data_petsc));
    Vec y;
    VecDuplicate((Vec)(solver->vector_rhs->data_petsc), &y);
    MatMult((Mat)(solver->matrix->data_petsc), (Vec)(solver->vector_sol->data_petsc), y);
    VecAXPY(y, -1.0, (Vec)(solver->vector_rhs->data_petsc));
    PetscReal val;
    VecNorm(y, NORM_2, &val);
    OpenPFEM_Print("||Ax-b||_2 %2.16f\n", val);
}

static void Destory_PETSc_SUPERLU(LINEARSOLVER *solver)
{
    KSPDestroy((KSP *)(&(solver->data)));
    solver->data = NULL;
}

void LinearSolverCreate_PETSc_SUPERLU(LINEARSOLVER *solver)
{
    LINEARSOLVER_OPS *ops = (LINEARSOLVER_OPS *)malloc(sizeof(LINEARSOLVER_OPS));
    ops->setup = Setup_PETSc_SUPERLU;
    ops->solve = Solve_PETSc_SUPERLU;
    ops->destory = Destory_PETSc_SUPERLU;
    solver->ops = (void *)ops;
}

#endif