#include "eigensolver.h"

#if defined(SLEPC_USE)

#define SLEPCSOLVER_TYPECHECK(name, solver)                             \
    do                                                                  \
    {                                                                   \
        if (!(DataTypeSlepcEqual(solver->A->type)))                     \
            RaiseError(name, "the type of matrix A is not SLEPc.");     \
        if (!(DataTypeSlepcEqual(solver->B->type)))                     \
            RaiseError(name, "the type of matrix A is not SLEPc.");     \
        if (!(DataTypeSlepcEqual(solver->evecs->type)))                 \
            RaiseError(name, "the type of eigenvectors is not SLEPc."); \
    } while (0)

//////////////////////////////////////////////////////////////////////////////////
/*                              Slpec KrylovSchur                               */
//////////////////////////////////////////////////////////////////////////////////

static void Setup_SLEPc_KrylovSchur(EIGENSOLVER *solver)
{
    EPS eps;
    Mat A = (Mat)(solver->A->data_petsc);
    Mat B = (Mat)(solver->B->data_petsc);
    MPI_Comm comm;
    PetscObjectGetComm((PetscObject)A, &comm);
    EPSCreate(comm, &eps);
    EPSSetType(eps, EPSKRYLOVSCHUR);
    EPSSetOperators(eps, A, B);
    EPSSetProblemType(eps, EPS_GHEP);
    EPSSetDimensions(eps, solver->nev, PETSC_DEFAULT, PETSC_DEFAULT);
    EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE);
    EPSSetTarget(eps, 0);
    EPSSetTolerances(eps, solver->tol, PETSC_DEFAULT);
    ST st;
    EPSGetST(eps, &st);
    STSetType(st, STSINVERT);

    EPSSetFromOptions(eps);
    EPSSetUp(eps);
    solver->data = (void *)eps;
}

static void Solve_SLEPc_KrylovSchur(EIGENSOLVER *solver)
{
    SLEPCSOLVER_TYPECHECK("Solve_SLEPc_KrylovSchur", solver);
    EPS eps = (EPS)(solver->data);
    EPSSolve(eps);
    BV eps_bv;
    EPSGetBV(eps, &eps_bv);
    DOUBLE *eps_bv_array, *solver_array;
    BVGetArray(eps_bv, &eps_bv_array);
    BVGetArray((BV)(solver->evecs->data_slepc), &solver_array);
    INT i, eigind, length = solver->evecs->local_length;
    for (i = 0; i < solver->nev; i++)
    {
        eigind = eps->perm[i];
        solver->evals[i] = eps->eigr[eigind];
        memcpy(solver_array + i * length, eps_bv_array + eigind * length, length * sizeof(DOUBLE));
    }
    BVRestoreArray(eps_bv, &eps_bv_array);
    BVRestoreArray((BV)(solver->evecs->data_slepc), &solver_array);


    BVMatMult((BV)(solver->evecs->data_slepc),(Mat)(solver->B->data_petsc),eps_bv);
    

}

static void Destory_SLEPc_KrylovSchur(EIGENSOLVER *solver)
{
    EPSDestroy((EPS *)(&(solver->data)));
    solver->data = NULL;
}

void EigenSolverCreate_SLEPc_KrylovSchur(EIGENSOLVER *solver)
{
    EIGENSOLVER_OPS *ops = (EIGENSOLVER_OPS *)malloc(sizeof(EIGENSOLVER_OPS));
    ops->setup = Setup_SLEPc_KrylovSchur;
    ops->solve = Solve_SLEPc_KrylovSchur;
    ops->destory = Destory_SLEPc_KrylovSchur;
    solver->ops = (void *)ops;
}

#endif