#include "eigensolver.h"

void EigenSolverCreate(EIGENSOLVER **solver, SOLVERTYPE type)
{
    *solver = (EIGENSOLVER *)malloc(sizeof(EIGENSOLVER));
    (*solver)->solvertype = type;
    // 设置
    (*solver)->nev = 0;
    (*solver)->tol = 1e-8;
    // 一些可能需要的结果
    (*solver)->calltimes = 0;
    // 数据
    (*solver)->A = NULL;
    (*solver)->B = NULL;
    (*solver)->evals = NULL;
    (*solver)->evecs = NULL;
    (*solver)->multilevel = NULL;
    (*solver)->alreadysetup = false;
    // 实现
    (*solver)->ops = NULL;
    (*solver)->data = NULL;
    EigenSolverSetType((*solver), type);
}

void EigenSolverSetType(EIGENSOLVER *solver, SOLVERTYPE type)
{
    if (SolverTypeEqual_SLEPC_KRYLOVSCHUR(type))
    {
#if defined(SLEPC_USE)
        OpenPFEM_Free(solver->ops);
        EigenSolverCreate_SLEPc_KrylovSchur(solver);
#else
        RaiseSlepcError("EigenSolverSetType");
#endif
    }
    else if (SolverTypeEqual_GCGE(type))
    {
#if defined(PASE_USE)
        OpenPFEM_Free(solver->ops);
        EigenSolverCreate_GCGE(solver);
#else
        RaiseSlepcError("EigenSolverSetType");
#endif
    }
    else if (SolverTypeEqual_PASE(type))
    {
#if defined(PASE_USE)
        OpenPFEM_Free(solver->ops);
        EigenSolverCreate_PASE(solver);
#else
        RaisePASEError("EigenSolverSetType");
#endif
    }
    else
    {
        RaiseError("EigenSolverSetType", "Wrong solvertype!");
    }
}

void EigenSolverSolve(EIGENSOLVER *solver, MATRIX *A, MATRIX *B, DOUBLE *eval, VECTORS *evec, INT num)
{
    EIGENSOLVER_OPS *ops = (EIGENSOLVER_OPS *)(solver->ops);
    if (solver->alreadysetup)
        if (A != solver->A || B != solver->B)
            RaiseError("EigenSolverSolve", "Matrix is different from the set up matrix!");

    solver->A = A;
    solver->B = B;
    solver->evals = eval;
    solver->evecs = evec;
    solver->nev = num;
    if (!solver->alreadysetup)
    {
        ops->setup(solver);
    }
    ops->solve(solver);
    solver->calltimes++;
    if (!solver->alreadysetup)
    {
        ops->destory(solver);
        solver->A = NULL;
        solver->B = NULL;
    }
    solver->evals = NULL;
    solver->evecs = NULL;
}

void EigenSolverDestroy(EIGENSOLVER **solver)
{
    (*solver)->A = NULL;
    (*solver)->B = NULL;
    (*solver)->evals = NULL;
    (*solver)->evecs = NULL;
    (*solver)->multilevel = NULL;
    if ((*solver)->ops != NULL)
    {
        if ((*solver)->alreadysetup)
            ((EIGENSOLVER_OPS *)((*solver)->ops))->destory(*solver);
        OpenPFEM_Free((*solver)->ops);
    }
    OpenPFEM_Free((*solver));
}

void EigenSolverMultiLevelSetUp(EIGENSOLVER *solver, MULTILEVEL *multilevel, INT nev)
{
    if (solver->alreadysetup)
        RaiseError("LinearSolverMultiLevelSetUp", "Already set up!");
    solver->multilevel = multilevel;
    solver->nev = nev;
    ((EIGENSOLVER_OPS *)(solver->ops))->setup(solver);
    solver->alreadysetup = true;
}

void EigenSolverMultiLevelSolve(EIGENSOLVER *solver, MULTILEVEL *multilevel, DOUBLE *eval, VECTORS *evec, INT num)
{
    EIGENSOLVER_OPS *ops = (EIGENSOLVER_OPS *)(solver->ops);

    solver->multilevel = multilevel;
    solver->evals = eval;
    solver->evecs = evec;
    solver->nev = num;
    if (!solver->alreadysetup)
    {
        ops->setup(solver);
    }
    ops->solve(solver);
    solver->calltimes++;
    if (!solver->alreadysetup)
    {
        ops->destory(solver);
        solver->multilevel = NULL;
    }
    solver->evals = NULL;
    solver->evecs = NULL;
}