#include "eigensolver.h"
#include <float.h>

#ifdef PASE_USE

//////////////////////////////////////////////////////////////////////////////////
/*                             GCGE                               */
//////////////////////////////////////////////////////////////////////////////////

static void Setup_GCGE(EIGENSOLVER *solver)
{
}

static void Solve_GCGE(EIGENSOLVER *solver)
{
    OPS *ops = NULL;
    OPS_Create(&ops);
    OPS_SLEPC_Set(ops);
    OPS_Setup(ops);
    Mat A = (Mat)(solver->A->data_petsc);
    Mat B = (Mat)(solver->B->data_petsc);
    int nevConv = solver->nev, multiMax = 1;
    double gapMin = 1e-5;
    int nevGiven = 0, block_size = nevConv / 4, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;

    if (nevConv > 300)
    {
        block_size = nevConv / 10;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }
    if (nevConv > 2000)
    {
        block_size = 200;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }

    nevInit = nevInit < nevMax ? nevInit : nevMax;
    int max_iter_gcg = 500;
    double tol_gcg[2] = {1e-5, 1e-10};
    tol_gcg[0] = solver->tol * 1e4;
    tol_gcg[1] = solver->tol;

    double *eval;
    void **evec;
    eval = malloc(nevMax * sizeof(double));
    memset(eval, 0, nevMax * sizeof(double));
    ops->MultiVecCreateByMat(&evec, nevMax, A, ops);
    ops->MultiVecSetRandomValue(evec, 0, nevMax, ops);
    void **gcg_mv_ws[4];
    double *dbl_ws;
    int *int_ws;
    ops->MultiVecCreateByMat(&gcg_mv_ws[0], nevMax + 2 * block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[0], 0, nevMax + 2 * block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[1], block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[1], 0, block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[2], block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[2], 0, block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[3], block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[3], 0, block_size, ops);
    int sizeV = nevInit + 2 * block_size;
    int length_dbl_ws = 2 * sizeV * sizeV + 10 * sizeV + (nevMax + 2 * block_size) + (nevMax)*block_size;
    int length_int_ws = 6 * sizeV + 2 * (block_size + 3);
    dbl_ws = malloc(length_dbl_ws * sizeof(double));
    memset(dbl_ws, 0, length_dbl_ws * sizeof(double));
    int_ws = malloc(length_int_ws * sizeof(int));
    memset(int_ws, 0, length_int_ws * sizeof(int));

    srand(0);
    int flag = 0;
    EigenSolverSetup_GCG(multiMax, gapMin, nevInit, nevMax, block_size,
                         tol_gcg, max_iter_gcg, flag, 0.0, gcg_mv_ws, dbl_ws, int_ws, ops);

    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 30;
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;
    char compW_method[8] = "bcg";

    EigenSolverSetParameters_GCG(
        check_conv_max_num,
        initX_orth_method, initX_orth_block_size,
        initX_orth_max_reorth, initX_orth_zero_tol,
        compP_orth_method, compP_orth_block_size,
        compP_orth_max_reorth, compP_orth_zero_tol,
        compW_orth_method, compW_orth_block_size,
        compW_orth_max_reorth, compW_orth_zero_tol,
        compW_bpcg_max_iter, compW_bpcg_rate,
        compW_bpcg_tol, compW_bpcg_tol_type, 0, // without shift
        compRR_min_num, compRR_min_gap,
        compRR_tol,compW_method,
        ops);

    MPI_Barrier(MPI_COMM_WORLD);
    OpenPFEM_Print("进入\n");
    ops->EigenSolver(A, B, NULL, eval, evec, nevGiven, &nevConv, 0.0, ops);
    MPI_Barrier(MPI_COMM_WORLD);
    OpenPFEM_Print("出来\n");

    ops->MultiVecDestroy(&gcg_mv_ws[0], nevMax + 2 * block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[1], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[2], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[3], block_size, ops);
    free(dbl_ws);
    free(int_ws);

    MPI_Barrier(MPI_COMM_WORLD);
    OpenPFEM_Print("1\n");
    double *origin = NULL, *aim = NULL;
    BVGetArray((BV)evec, &origin);
    VectorsGetArray(solver->evecs, &aim);
    MPI_Barrier(MPI_COMM_WORLD);
    OpenPFEM_Print("2\n");
    memcpy(solver->evals, eval, solver->nev * sizeof(DOUBLE));
    MPI_Barrier(MPI_COMM_WORLD);
    OpenPFEM_Print("3\n");
    memcpy(aim, origin, solver->nev * solver->evecs->local_length * sizeof(DOUBLE));
    BVRestoreArray((BV)evec, &origin);
    VectorsRestoreArray(solver->evecs, &aim);
    OpenPFEM_Free(eval);
    BV evec_tmp = (BV)evec;
    BVDestroy(&evec_tmp);
}

static void Destory_GCGE(EIGENSOLVER *solver)
{
}

void EigenSolverGCGESolve(EIGENSOLVER *solver, MATRIX *A, MATRIX *B, DOUBLE **eval, VECTORS **evec, INT *num)
{
    OPS *ops = NULL;
    OPS_Create(&ops);
    OPS_SLEPC_Set(ops);
    OPS_Setup(ops);
    Mat gcge_A = (Mat)(A->data_petsc);
    Mat gcge_B = (Mat)(B->data_petsc);
    int nevConv = *num, multiMax = 1;
    double gapMin = 1e-5;
    int nevGiven = 0, block_size = nevConv / 4, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;

    if (nevConv > 300)
    {
        block_size = nevConv / 10;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }
    if (nevConv > 2000)
    {
        block_size = 200;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }

    nevInit = nevInit < nevMax ? nevInit : nevMax;
    int max_iter_gcg = 500;
    double tol_gcg[2] = {1e-5, 1e-10};
    tol_gcg[0] = solver->tol * 1e4;
    tol_gcg[1] = solver->tol;

    double *gcge_eval;
    void **gcge_evec;
    gcge_eval = malloc(nevMax * sizeof(double));
    memset(gcge_eval, 0, nevMax * sizeof(double));
    ops->MultiVecCreateByMat(&gcge_evec, nevMax, gcge_A, ops);
    ops->MultiVecSetRandomValue(gcge_evec, 0, nevMax, ops);
    void **gcg_mv_ws[4];
    double *dbl_ws;
    int *int_ws;
    ops->MultiVecCreateByMat(&gcg_mv_ws[0], nevMax + 2 * block_size, gcge_A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[0], 0, nevMax + 2 * block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[1], block_size, gcge_A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[1], 0, block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[2], block_size, gcge_A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[2], 0, block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[3], block_size, gcge_A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[3], 0, block_size, ops);
    int sizeV = nevInit + 2 * block_size;
    int length_dbl_ws = 2 * sizeV * sizeV + 10 * sizeV + (nevMax + 2 * block_size) + (nevMax)*block_size;
    int length_int_ws = 6 * sizeV + 2 * (block_size + 3);
    dbl_ws = malloc(length_dbl_ws * sizeof(double));
    memset(dbl_ws, 0, length_dbl_ws * sizeof(double));
    int_ws = malloc(length_int_ws * sizeof(int));
    memset(int_ws, 0, length_int_ws * sizeof(int));

    srand(0);
    int flag = 0;
    EigenSolverSetup_GCG(multiMax, gapMin, nevInit, nevMax, block_size,
                         tol_gcg, max_iter_gcg, flag, 0.0, gcg_mv_ws, dbl_ws, int_ws, ops);

    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 30;
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;
    char compW_method[8] = "bcg";
    EigenSolverSetParameters_GCG(
        check_conv_max_num,
        initX_orth_method, initX_orth_block_size,
        initX_orth_max_reorth, initX_orth_zero_tol,
        compP_orth_method, compP_orth_block_size,
        compP_orth_max_reorth, compP_orth_zero_tol,
        compW_orth_method, compW_orth_block_size,
        compW_orth_max_reorth, compW_orth_zero_tol,
        compW_bpcg_max_iter, compW_bpcg_rate,
        compW_bpcg_tol, compW_bpcg_tol_type, 0, // without shift
        compRR_min_num, compRR_min_gap,
        compRR_tol,compW_method,
        ops);

    ops->EigenSolver(gcge_A, gcge_B, NULL, gcge_eval, gcge_evec, nevGiven, &nevConv, 0.0, ops);

    ops->MultiVecDestroy(&gcg_mv_ws[0], nevMax + 2 * block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[1], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[2], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[3], block_size, ops);
    free(dbl_ws);
    free(int_ws);

    double *origin = NULL, *aim = NULL;

    *num = nevConv;
    VectorsCreateByMatrix(evec, A, nevConv);
    VectorsSetRange(*evec, 0, nevConv);
    BVGetArray((BV)gcge_evec, &origin);
    VectorsGetArray(*evec, &aim);
    *eval = (double *)malloc(nevConv * sizeof(double));
    memcpy(*eval, gcge_eval, nevConv * sizeof(DOUBLE));
    memcpy(aim, origin, nevConv * (*evec)->local_length * sizeof(DOUBLE));
    BVRestoreArray((BV)gcge_evec, &origin);
    VectorsRestoreArray(*evec, &aim);
    OpenPFEM_Free(gcge_eval);
    BV evec_tmp = (BV)gcge_evec;
    BVDestroy(&evec_tmp);
}

void EigenSolverCreate_GCGE(EIGENSOLVER *solver)
{
    EIGENSOLVER_OPS *ops = (EIGENSOLVER_OPS *)malloc(sizeof(EIGENSOLVER_OPS));
    ops->setup = Setup_GCGE;
    ops->solve = Solve_GCGE;
    ops->destory = Destory_GCGE;
    solver->ops = (void *)ops;
}

#endif
