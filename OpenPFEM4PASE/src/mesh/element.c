#include "element.h"

void ElementRefCoord2Coord(ELEMENT *Elem, DOUBLE RefCoord[], DOUBLE Coord[])
{
    INT worlddim = Elem->worlddim;
    DOUBLE weight[worlddim + 1];
    switch (worlddim)
    {
    case 1:
        if (Elem->NumVerts == 2)
        { // 线段的情况
            weight[0] = 1 - RefCoord[0];
            weight[1] = RefCoord[0];
            Coord[0] = weight[0] * Elem->Vert_X[0] + weight[1] * Elem->Vert_X[1];
        }
        break;
    case 2:
        if (Elem->NumVerts == 3)
        { // 三角形的情况
            weight[0] = 1 - RefCoord[0] - RefCoord[1];
            weight[1] = RefCoord[0];
            weight[2] = RefCoord[1];
            Coord[0] = weight[0] * Elem->Vert_X[0] + weight[1] * Elem->Vert_X[1] + weight[2] * Elem->Vert_X[2];
            Coord[1] = weight[0] * Elem->Vert_Y[0] + weight[1] * Elem->Vert_Y[1] + weight[2] * Elem->Vert_Y[2];
        }
        break;
    case 3:
        if (Elem->NumVerts == 4)
        { // 四面体的情况
            weight[0] = 1 - RefCoord[0] - RefCoord[1] - RefCoord[2];
            weight[1] = RefCoord[0];
            weight[2] = RefCoord[1];
            weight[3] = RefCoord[2];
            Coord[0] = weight[0] * Elem->Vert_X[0] + weight[1] * Elem->Vert_X[1] + weight[2] * Elem->Vert_X[2] + weight[3] * Elem->Vert_X[3];
            Coord[1] = weight[0] * Elem->Vert_Y[0] + weight[1] * Elem->Vert_Y[1] + weight[2] * Elem->Vert_Y[2] + weight[3] * Elem->Vert_Y[3];
            Coord[2] = weight[0] * Elem->Vert_Z[0] + weight[1] * Elem->Vert_Z[1] + weight[2] * Elem->Vert_Z[2] + weight[3] * Elem->Vert_Z[3];
        }
        break;
    }
}

ELEMENT *ElementInitial(MESH *mesh)
{
    ELEMENT *Elem = malloc(sizeof(ELEMENT));
    INT i, Index = 0;
    INT NumVerts; // the number of vertices in this element
    LINE line;
    FACE face;
    VOLU volu;
    INT worlddim = mesh->worlddim;
    switch (worlddim)
    {
    case 1:
        line = mesh->Lines[Index];
        NumVerts = 2;
        Elem->Vert_X = malloc(NumVerts * sizeof(double));
        Elem->Vert_Y = NULL;
        Elem->Vert_Z = NULL;
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        Elem->worlddim = 1;
        Elem->is_curl = 0;
        Elem->is_div = 0;
        break;
    case 2:
        face = mesh->Faces[Index];
        NumVerts = face.NumVerts;
        Elem->Vert_X = malloc(NumVerts * sizeof(double));
        Elem->Vert_Y = malloc(NumVerts * sizeof(double));
        Elem->Vert_Z = NULL;
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        Elem->worlddim = 2;
        Elem->is_curl = 0;
        Elem->is_div = 0;
        break;
    case 3:
        volu = mesh->Volus[Index];
        NumVerts = volu.NumVerts;
        Elem->Vert_X = malloc(NumVerts * sizeof(DOUBLE));
        Elem->Vert_Y = malloc(NumVerts * sizeof(DOUBLE));
        Elem->Vert_Z = malloc(NumVerts * sizeof(DOUBLE));
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        Elem->worlddim = 3;
        Elem->is_curl = 0;
        Elem->is_div = 0;
        break;
    }
    return Elem;
}

/* 根据网格建立有限元单元对象, 即更新单元的坐标信息*/
void ElementBuild(MESH *mesh, INT Index, ELEMENT *Elem)
{
    // 0409add curl元转换局部编号的顺序
    if (Elem->is_curl == 1 || Elem->is_div == 1)
    {
        CurlElementBuild(mesh, Index, Elem);
        return;
    }
    INT i;
    INT NumVerts; // the number of vertices in this element
    VERT vert;
    LINE line;
    FACE face;
    VOLU volu;
    INT worlddim = mesh->worlddim;
    switch (worlddim)
    {
    case 1:
        line = mesh->Lines[Index];
        NumVerts = 2;
        // 定义单元节点的坐标
        INT *vert4line = line.Vert4Line;
        for (i = 0; i < NumVerts; i++)
        {
            vert = mesh->Verts[vert4line[i]];
            Elem->Vert_X[i] = vert.Coord[0];
        }
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        break;
    case 2:
        face = mesh->Faces[Index];
        NumVerts = face.NumVerts;
        // 定义单元节点坐标
        INT *vert4face = face.Vert4Face;
        for (i = 0; i < NumVerts; i++)
        {
            vert = mesh->Verts[vert4face[i]];
            Elem->Vert_X[i] = vert.Coord[0];
            Elem->Vert_Y[i] = vert.Coord[1];
        }
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        break;
    case 3:
        volu = mesh->Volus[Index];
        NumVerts = volu.NumVerts;
        INT *vert4volu = volu.Vert4Volu;
        for (i = 0; i < NumVerts; i++)
        {
            vert = mesh->Verts[vert4volu[i]];
            Elem->Vert_X[i] = vert.Coord[0];
            Elem->Vert_Y[i] = vert.Coord[1];
            Elem->Vert_Z[i] = vert.Coord[2];
        }
        Elem->NumVerts = NumVerts;
        Elem->Volumn = -1.0;
        break;
    }
    return;
}

void CurlElementBuild(MESH *mesh, INT Index, ELEMENT *Elem)
{
    INT i, j, temp;
    INT NumVerts; // the number of vertices in this element
    VERT vert;
    LINE line;
    FACE face;
    VOLU volu;
    INT worlddim = mesh->worlddim;
    switch (worlddim)
    {
    case 1:
        RaiseError("CURLElementBuild", "dim = 1");
        break;
    case 2:
        RaiseError("CURLElementBuild", "dim = 2");
        break;
    case 3:
        volu = mesh->Volus[Index];
        NumVerts = volu.NumVerts;
        // 下面的功能不一定都需要 可以拆分开 判断需要计算哪些信息！
        // 1.确定点在原RefElem中的对应编号
        INT *vert4volu = volu.Vert4Volu;              // 体中的点在本进程中的编号(按照体中的局部编号顺序排列)
        INT *VertIdInRefElem = Elem->VertIdInRefElem; // 体中点的局部编号
        for (i = 0; i < NumVerts; i++)
            VertIdInRefElem[i] = i;
        // 对于vert4volu中点的编号进行排序 且同步对体中点的局部编号进行变换 保证两者的对应
        // OpenPFEM_Print("vert4volu %d %d %d %d\n", vert4volu[0], vert4volu[1], vert4volu[2], vert4volu[3]);
        QuickSort_Int_WithInt(vert4volu, VertIdInRefElem, 0, NumVerts - 1);
        // OpenPFEM_Print("vert4volu %d %d %d %d\n", vert4volu[0], vert4volu[1], vert4volu[2], vert4volu[3]);
        // OpenPFEM_Print("VertIdInRefElem %d %d %d %d\n", VertIdInRefElem[0], VertIdInRefElem[1], VertIdInRefElem[2], VertIdInRefElem[3]);
        // 2.按照排序结果对点进行逐个赋值
        for (i = 0; i < NumVerts; i++)
        {
            vert = mesh->Verts[vert4volu[i]]; // 点在本进程中编号
            Elem->Vert_X[i] = vert.Coord[0];
            Elem->Vert_Y[i] = vert.Coord[1];
            Elem->Vert_Z[i] = vert.Coord[2];
        }
        // 3.找到Curl单元中新编号方式中的线 在原编号方式中的序号 以及线的方向是否和原来同向
        INT start[6] = {0, 0, 0, 1, 1, 2};
        INT end[6] = {1, 2, 3, 2, 3, 3};
        INT start_id, end_id;
        for (i = 0; i < 6; i++)
        {
            start_id = VertIdInRefElem[start[i]]; // 新的编号顺序中第i条边的起点 在原编号顺序中单元内的局部编号
            end_id = VertIdInRefElem[end[i]];     // 新的编号顺序中第i条边的终点 在原编号顺序中单元内的局部编号
            // 判断方向 如果start_id<end_id 同向 否则反向
            if (start_id > end_id)
            {
                temp = start_id;
                start_id = end_id;
                end_id = temp; // 反向交换两点顺序 方便下面做判断
            }                  // end if
            // if (start_id < end_id)
            // {
            //     Elem->if_line_positive[i] = 1;
            // }
            // else
            // {
            //     Elem->if_line_positive[i] = 0;
            //     temp = start_id;
            //     start_id = end_id;
            //     end_id = temp; // 反向交换两点顺序 方便下面做判断
            // }                  // end if
            // 找到相同顶点的线的原编号
            for (j = 0; j < 6; j++)
            {
                if (start[j] == start_id)
                {
                    if (end[j] == end_id)
                    {
                        Elem->LineIdInRefElem[i] = j;
                        j = 7;
                    }
                }
            } // end for j
        }     // end for i
        // OpenPFEM_Print("LineIdInRefElem %d %d %d %d %d %d\n", Elem->LineIdInRefElem[0], Elem->LineIdInRefElem[1], Elem->LineIdInRefElem[2], Elem->LineIdInRefElem[3], Elem->LineIdInRefElem[4], Elem->LineIdInRefElem[5]);
        // 4.判断面的外法向和原来是否同向
        INT facevert[4][3] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1}; // 用来记录一个四面体的四个面的局部节点编号
        INT newvertnum[3];
        INT *oldvertnum;
        for (i = 0; i < 4; i++)
        {
            // 新局部坐标下第i个面 在原来的局部坐标下 三点的编号为
            oldvertnum = facevert[VertIdInRefElem[i]];
            // OpenPFEM_Print("oldvertnum %d %d %d\n", oldvertnum[0], oldvertnum[1], oldvertnum[2]);
            // 新局部坐标下第i个面 在新的局部坐标下 三点的编号为
            for (j = 0; j < 3; j++)
                newvertnum[j] = VertIdInRefElem[(i+j+1) % 4];
            // OpenPFEM_Print("newvertnum %d %d %d\n", newvertnum[0], newvertnum[1], newvertnum[2]);
            // 判断两组编号是否是同向的
            // for (j = 0; j < 3; j++)
            // {
            //     // 首先确定同一个编号 判断下一个编号相同(外法向量同向)还是不同(外法向量反向)
            //     if (oldvertnum[0] == newvertnum[j])
            //     {
            //         if (oldvertnum[1] == newvertnum[(j + 1) % 3])
            //         {
            //             Elem->if_norm_positive[i] = 1;
            //         }
            //         else
            //         {
            //             Elem->if_norm_positive[i] = 0;
            //         }
            //         j = 4;
            //     }
            // }
        }
        // OpenPFEM_Print("if_norm_positive %d %d %d %d\n", Elem->if_norm_positive[0], Elem->if_norm_positive[1], Elem->if_norm_positive[2], Elem->if_norm_positive[3]);
        // RaiseError("CURLElementBuild", "dim = 3");
    }
    return;
}

// 计算单元变换的Jacobian矩阵, 目前实现了Affine类型的变换的Jacobian矩阵
void ComputeElementJacobian(ELEMENT *Elem)
{
    INT i, j;
    INT NumVerts = Elem->NumVerts;
    DOUBLE A11, A12, A13, volumn;
    INT worlddim = Elem->worlddim;
    switch (worlddim)
    {
    case 1:
        if (NumVerts == 2)
        {
            Elem->Jacobian[0][0] = Elem->Vert_X[1] - Elem->Vert_X[0];
            Elem->Volumn = Elem->Jacobian[0][0];
        }
        break;
    case 2:
        if (NumVerts == 3)
        {
            Elem->Jacobian[0][0] = Elem->Vert_X[1] - Elem->Vert_X[0];
            Elem->Jacobian[0][1] = Elem->Vert_X[2] - Elem->Vert_X[0];
            Elem->Jacobian[1][0] = Elem->Vert_Y[1] - Elem->Vert_Y[0];
            Elem->Jacobian[1][1] = Elem->Vert_Y[2] - Elem->Vert_Y[0];
            Elem->Volumn = 0.5 * (Elem->Jacobian[0][0] * Elem->Jacobian[1][1] - Elem->Jacobian[0][1] * Elem->Jacobian[1][0]);
        }
        break;
    case 3:
        if (NumVerts == 4)
        {
            // 三维情况下四面体单元的雅可比矩阵,六面体单元并非线性映射
            Elem->Jacobian[0][0] = Elem->Vert_X[1] - Elem->Vert_X[0];
            Elem->Jacobian[0][1] = Elem->Vert_X[2] - Elem->Vert_X[0];
            Elem->Jacobian[0][2] = Elem->Vert_X[3] - Elem->Vert_X[0];
            Elem->Jacobian[1][0] = Elem->Vert_Y[1] - Elem->Vert_Y[0];
            Elem->Jacobian[1][1] = Elem->Vert_Y[2] - Elem->Vert_Y[0];
            Elem->Jacobian[1][2] = Elem->Vert_Y[3] - Elem->Vert_Y[0];
            Elem->Jacobian[2][0] = Elem->Vert_Z[1] - Elem->Vert_Z[0];
            Elem->Jacobian[2][1] = Elem->Vert_Z[2] - Elem->Vert_Z[0];
            Elem->Jacobian[2][2] = Elem->Vert_Z[3] - Elem->Vert_Z[0];
            A11 = Elem->Jacobian[1][1] * Elem->Jacobian[2][2] - Elem->Jacobian[1][2] * Elem->Jacobian[2][1];
            A12 = Elem->Jacobian[1][2] * Elem->Jacobian[2][0] - Elem->Jacobian[1][0] * Elem->Jacobian[2][2];
            A13 = Elem->Jacobian[1][0] * Elem->Jacobian[2][1] - Elem->Jacobian[1][1] * Elem->Jacobian[2][0];
            volumn = Elem->Jacobian[0][0] * A11 + Elem->Jacobian[0][1] * A12 + Elem->Jacobian[0][2] * A13;
            Elem->Volumn = 1.0 / 6.0 * fabs(volumn);
        }
        break;
    }
}
// 计算单元变换的Jacobian逆矩阵, 目前实现了Affine类型的变换的Jacobian逆矩阵
void ComputeElementInvJacobian(ELEMENT *Elem)
{
    INT i, j, k;
    INT NumVerts = Elem->NumVerts;
    double invdet;
    double A11, A12, A13, volumn;
    INT worlddim = Elem->worlddim;
    switch (worlddim)
    {
    case 1:
        if (NumVerts == 2)
        {
            if (Elem->Volumn < 0.0)
            {
                ComputeElementJacobian(Elem);
            }
            Elem->InvJacobian[0][0] = 1.0 / Elem->Jacobian[0][0];
        }
        break;
    case 2:
        if (NumVerts == 3)
        {
            if (Elem->Volumn < 0.0)
            {
                ComputeElementJacobian(Elem);
            }
            invdet = 0.5 / Elem->Volumn;
            Elem->InvJacobian[0][0] = invdet * Elem->Jacobian[1][1];
            Elem->InvJacobian[0][1] = -invdet * Elem->Jacobian[0][1];
            Elem->InvJacobian[1][0] = -invdet * Elem->Jacobian[1][0];
            Elem->InvJacobian[1][1] = invdet * Elem->Jacobian[0][0];
        }
        break;

    case 3:
        if (NumVerts == 4)
        {
            // 计算单元的Jacobi矩阵
            Elem->Jacobian[0][0] = Elem->Vert_X[1] - Elem->Vert_X[0];
            Elem->Jacobian[0][1] = Elem->Vert_X[2] - Elem->Vert_X[0];
            Elem->Jacobian[0][2] = Elem->Vert_X[3] - Elem->Vert_X[0];
            Elem->Jacobian[1][0] = Elem->Vert_Y[1] - Elem->Vert_Y[0];
            Elem->Jacobian[1][1] = Elem->Vert_Y[2] - Elem->Vert_Y[0];
            Elem->Jacobian[1][2] = Elem->Vert_Y[3] - Elem->Vert_Y[0];
            Elem->Jacobian[2][0] = Elem->Vert_Z[1] - Elem->Vert_Z[0];
            Elem->Jacobian[2][1] = Elem->Vert_Z[2] - Elem->Vert_Z[0];
            Elem->Jacobian[2][2] = Elem->Vert_Z[3] - Elem->Vert_Z[0];

            A11 = Elem->Jacobian[1][1] * Elem->Jacobian[2][2] - Elem->Jacobian[1][2] * Elem->Jacobian[2][1];
            A12 = Elem->Jacobian[1][2] * Elem->Jacobian[2][0] - Elem->Jacobian[1][0] * Elem->Jacobian[2][2];
            A13 = Elem->Jacobian[1][0] * Elem->Jacobian[2][1] - Elem->Jacobian[1][1] * Elem->Jacobian[2][0];
            volumn = Elem->Jacobian[0][0] * A11 + Elem->Jacobian[0][1] * A12 + Elem->Jacobian[0][2] * A13;
            // 计算单元的体积
            Elem->Volumn = 1.0 / 6.0 * fabs(volumn);
            // 计算逆Jacobi矩阵
            Elem->InvJacobian[0][0] = A11 / volumn;
            Elem->InvJacobian[0][1] = -(Elem->Jacobian[0][1] * Elem->Jacobian[2][2] - Elem->Jacobian[0][2] * Elem->Jacobian[2][1]) / volumn;
            Elem->InvJacobian[0][2] = (Elem->Jacobian[0][1] * Elem->Jacobian[1][2] - Elem->Jacobian[0][2] * Elem->Jacobian[1][1]) / volumn;
            Elem->InvJacobian[1][0] = A12 / volumn;
            Elem->InvJacobian[1][1] = (Elem->Jacobian[0][0] * Elem->Jacobian[2][2] - Elem->Jacobian[0][2] * Elem->Jacobian[2][0]) / volumn;
            Elem->InvJacobian[1][2] = -(Elem->Jacobian[0][0] * Elem->Jacobian[1][2] - Elem->Jacobian[0][2] * Elem->Jacobian[1][0]) / volumn;
            Elem->InvJacobian[2][0] = A13 / volumn;
            Elem->InvJacobian[2][1] = -(Elem->Jacobian[0][0] * Elem->Jacobian[2][1] - Elem->Jacobian[0][1] * Elem->Jacobian[2][0]) / volumn;
            Elem->InvJacobian[2][2] = (Elem->Jacobian[0][0] * Elem->Jacobian[1][1] - Elem->Jacobian[0][1] * Elem->Jacobian[1][0]) / volumn;
        }
        break;
    } // end switch (worlddim)
}

// lhc 23.01.28
// 提前获取该网格的所有单元的Jacobi的逆矩阵 并按照单元编号储存
void ComputeInvJacobiOfAllElem(MESH *mesh, BOOL is_curl)
{
    if (mesh->IsJacobiInfoAvailable == 1)
        return;
    INT num_elem;
    INT worlddim = mesh->worlddim;
    switch (worlddim)
    {
    case 1:
        num_elem = mesh->num_line;
        break;
    case 2:
        num_elem = mesh->num_face;
        break;
    case 3:
        num_elem = mesh->num_volu;
        break;
    }
    INT length_jacobi = worlddim * worlddim * num_elem;
    mesh->JacobiOfAllElem = malloc(length_jacobi * sizeof(DOUBLE));
    mesh->InvJacobiOfAllElem = malloc(length_jacobi * sizeof(DOUBLE));
    mesh->VolumnOfAllElem = malloc(num_elem * sizeof(DOUBLE));
    ELEMENT *Elem;
    Elem = ElementInitial(mesh);
    if(is_curl==1)
        Elem->is_curl = 1;

    // 单元循环
    INT idx_elem, i, j;
    for (idx_elem = 0; idx_elem < num_elem; idx_elem++)
    {
        ElementBuild(mesh, idx_elem, Elem);
        // 计算单元的体积和Jacobian矩阵的逆矩阵
        ComputeElementInvJacobian(Elem);
        // 这里 体积 Jacobi InvJacobi 是否需要都保存？
        mesh->VolumnOfAllElem[idx_elem] = Elem->Volumn;
        for (i = 0; i < worlddim; i++)
        {
            for (j = 0; j < worlddim; j++)
            {
                mesh->JacobiOfAllElem[(idx_elem * worlddim + i) * worlddim + j] = Elem->Jacobian[i][j];
                mesh->InvJacobiOfAllElem[(idx_elem * worlddim + i) * worlddim + j] = Elem->InvJacobian[i][j];
            }
        }
    }
    mesh->IsJacobiInfoAvailable = 1;
}

// 释放计算出的Jacobian信息
void DestroyInvJacobiOfAllElem(MESH *mesh)
{
    if (mesh->IsJacobiInfoAvailable == 1)
    {
        OpenPFEM_Free(mesh->JacobiOfAllElem);
        OpenPFEM_Free(mesh->InvJacobiOfAllElem);
        OpenPFEM_Free(mesh->VolumnOfAllElem);
        mesh->IsJacobiInfoAvailable = 0;
    }
}

void ElementDestroy(ELEMENT **Elem)
{
    OpenPFEM_Free((*Elem)->Vert_X);
    if ((*Elem)->Vert_Y != NULL)
    {
        OpenPFEM_Free((*Elem)->Vert_Y);
    }
    if ((*Elem)->Vert_Z != NULL)
    {
        OpenPFEM_Free((*Elem)->Vert_Z);
    }
    OpenPFEM_Free((*Elem));
}
void ElementPrint(ELEMENT *Elem)
{
    INT i, NumVerts = Elem->NumVerts;
    INT worlddim = Elem->worlddim;
    switch (worlddim)
    {
    case 1:
        for (i = 0; i < NumVerts; i++)
        {
            printf("The %d-th vert: %2.8f\n", i, Elem->Vert_X[i]);
        }
        break;
    case 2:
        for (i = 0; i < NumVerts; i++)
        {
            printf("The %d-th vert: (%2.8f, %2.8f)\n", i, Elem->Vert_X[i], Elem->Vert_Y[i]);
        }
        break;
    case 3:
        for (i = 0; i < NumVerts; i++)
        {
            printf("The %d-th vert: (%2.8f, %2.8f, %2.8f)\n", i, Elem->Vert_X[i], Elem->Vert_Y[i], Elem->Vert_Z[i]);
        }
        break;
    }
}
//==============================================================================================
// 下面是针对Element计算的一些几何量
// 对单元elem，计算单元每个边的外法向量，并将其按照边的012编号顺序排列至数组NormalVec中
// 设0号边外法向量为(n0x,n0y),1号边外法向量为(n1x,n1y),2号边外法向量为(n2x,n2y),
// NormalVec=[n0x,n0y,n1x,n1y,n2x,n2y]
// 三维的情况是计算每个面的外法向, 按照面的顺序来计算
void ComputeElementNormalVec(ELEMENT *Elem, DOUBLE *NormalVec)
{
    INT i, m, n, l;
    DOUBLE length;
    INT worlddim = Elem->worlddim;
    DOUBLE L[worlddim];
    DOUBLE R[worlddim];
    // 三维面的外法向量是计算边的方向向量的内积
    /*
    0号面 12\times13
    1号面 03\times02
    2号面 01\times03
    3号面 02\times01
    */
    INT ii1[4] = {1, 0, 0, 0};
    INT ii2[4] = {2, 3, 1, 2};
    INT ii3[4] = {1, 0, 0, 0};
    INT ii4[4] = {3, 2, 3, 1};
    DOUBLE nv[3];
    DOUBLE innerproduct;
    switch (worlddim)
    {
    case 2:
        // 对边进行循环，计算每条边对应的外法向量
        for (i = 0; i < 3; i++)
        {
            m = (i + 2) % 3;
            n = (i + 1) % 3;
            // 计算第i条边的方向向量
            L[0] = Elem->Vert_X[m] - Elem->Vert_X[n];
            L[1] = Elem->Vert_Y[m] - Elem->Vert_Y[n];
            length = sqrt(L[0] * L[0] + L[1] * L[1]);
            NormalVec[worlddim * i] = L[1] / length;
            NormalVec[worlddim * i + 1] = -L[0] / length;
            // L[0] = Elem->Vert_X[m] - Elem->Vert_X[i];
            // L[1] = Elem->Vert_Y[m] - Elem->Vert_Y[i];
            // innerproduct = L[0] * NormalVec[worlddim * i] + L[1] * NormalVec[worlddim * i + 1];
            // 这段代码用于检查生成的向量方向是否需要改变 运行结果是均不需要改变
            /* if (innerproduct < 0)
            {
                NormalVec[worlddim * i] = -NormalVec[worlddim * i];
                NormalVec[worlddim * i + 1] = -NormalVec[worlddim * i + 1];
                OpenPFEM_RankPrint(DEFAULT_COMM, "调整法向量方向\n");
            }else{
                OpenPFEM_RankPrint(DEFAULT_COMM, "未调整法向量方向\n");
            } */
            // printf("NormalVec:%f,%f\n",NormalVec[worlddim*i],NormalVec[worlddim*i+1]);
            // printf("%d\n",i);
        }
        break;
    case 3:
        // 对面进行循环，计算每个面对应的外法向量
        for (i = 0; i < Elem->NumVerts; i++)
        {
            L[0] = Elem->Vert_X[ii2[i]] - Elem->Vert_X[ii1[i]];
            L[1] = Elem->Vert_Y[ii2[i]] - Elem->Vert_Y[ii1[i]];
            L[2] = Elem->Vert_Z[ii2[i]] - Elem->Vert_Z[ii1[i]];
            R[0] = Elem->Vert_X[ii4[i]] - Elem->Vert_X[ii3[i]];
            R[1] = Elem->Vert_Y[ii4[i]] - Elem->Vert_Y[ii3[i]];
            R[2] = Elem->Vert_Z[ii4[i]] - Elem->Vert_Z[ii3[i]];
            nv[0] = L[1] * R[2] - L[2] * R[1];
            nv[1] = L[2] * R[0] - L[0] * R[2];
            nv[2] = L[0] * R[1] - L[1] * R[0];
            length = sqrt(nv[0] * nv[0] + nv[1] * nv[1] + nv[2] * nv[2]);
            NormalVec[worlddim * i] = nv[0] / length;
            NormalVec[worlddim * i + 1] = nv[1] / length;
            NormalVec[worlddim * i + 2] = nv[2] / length;
        }
        break;
    }
}
// 计算elem的边界长度h, 按局部编号顺序存储到 hEdge
// 计算elem的尺度h，存储到hElem
void ComputeElementInfo(ELEMENT *Elem, DOUBLE *hEdge, DOUBLE *SEdge, DOUBLE *hElem)
{
    DOUBLE p, length[6], a, b, c, dx, dy, dz;
    INT k, NumVerts = Elem->NumVerts;
    INT worlddim = Elem->worlddim;
    switch (worlddim)
    {
    case 2:
        hEdge[0] = sqrt((Elem->Vert_X[2] - Elem->Vert_X[1]) * (Elem->Vert_X[2] - Elem->Vert_X[1]) + (Elem->Vert_Y[2] - Elem->Vert_Y[1]) * (Elem->Vert_Y[2] - Elem->Vert_Y[1]));
        hEdge[1] = sqrt((Elem->Vert_X[0] - Elem->Vert_X[2]) * (Elem->Vert_X[0] - Elem->Vert_X[2]) + (Elem->Vert_Y[0] - Elem->Vert_Y[2]) * (Elem->Vert_Y[0] - Elem->Vert_Y[2]));
        hEdge[2] = sqrt((Elem->Vert_X[1] - Elem->Vert_X[0]) * (Elem->Vert_X[1] - Elem->Vert_X[0]) + (Elem->Vert_Y[1] - Elem->Vert_Y[0]) * (Elem->Vert_Y[1] - Elem->Vert_Y[0]));
        SEdge[0] = hEdge[0];
        SEdge[1] = hEdge[1];
        SEdge[2] = hEdge[2];
        p = (hEdge[0] + hEdge[1] + hEdge[2]) / 2.0;
        hElem[0] = hEdge[0] * hEdge[1] * hEdge[2] / 2.0 / sqrt(p * (p - hEdge[0]) * (p - hEdge[1]) * (p - hEdge[2]));
        break;
    case 3:
        // 计算每条边的长度
        length[0] = sqrt((Elem->Vert_X[1] - Elem->Vert_X[0]) * (Elem->Vert_X[1] - Elem->Vert_X[0]) + (Elem->Vert_Y[1] - Elem->Vert_Y[0]) * (Elem->Vert_Y[1] - Elem->Vert_Y[0]) + (Elem->Vert_Z[1] - Elem->Vert_Z[0]) * (Elem->Vert_Z[1] - Elem->Vert_Z[0]));
        length[1] = sqrt((Elem->Vert_X[2] - Elem->Vert_X[0]) * (Elem->Vert_X[2] - Elem->Vert_X[0]) + (Elem->Vert_Y[2] - Elem->Vert_Y[0]) * (Elem->Vert_Y[2] - Elem->Vert_Y[0]) + (Elem->Vert_Z[2] - Elem->Vert_Z[0]) * (Elem->Vert_Z[2] - Elem->Vert_Z[0]));
        length[2] = sqrt((Elem->Vert_X[3] - Elem->Vert_X[0]) * (Elem->Vert_X[3] - Elem->Vert_X[0]) + (Elem->Vert_Y[3] - Elem->Vert_Y[0]) * (Elem->Vert_Y[3] - Elem->Vert_Y[0]) + (Elem->Vert_Z[3] - Elem->Vert_Z[0]) * (Elem->Vert_Z[3] - Elem->Vert_Z[0]));
        length[3] = sqrt((Elem->Vert_X[2] - Elem->Vert_X[1]) * (Elem->Vert_X[2] - Elem->Vert_X[1]) + (Elem->Vert_Y[2] - Elem->Vert_Y[1]) * (Elem->Vert_Y[2] - Elem->Vert_Y[1]) + (Elem->Vert_Z[2] - Elem->Vert_Z[1]) * (Elem->Vert_Z[2] - Elem->Vert_Z[1]));
        length[4] = sqrt((Elem->Vert_X[3] - Elem->Vert_X[1]) * (Elem->Vert_X[3] - Elem->Vert_X[1]) + (Elem->Vert_Y[3] - Elem->Vert_Y[1]) * (Elem->Vert_Y[3] - Elem->Vert_Y[1]) + (Elem->Vert_Z[3] - Elem->Vert_Z[1]) * (Elem->Vert_Z[3] - Elem->Vert_Z[1]));
        length[5] = sqrt((Elem->Vert_X[3] - Elem->Vert_X[2]) * (Elem->Vert_X[3] - Elem->Vert_X[2]) + (Elem->Vert_Y[3] - Elem->Vert_Y[2]) * (Elem->Vert_Y[3] - Elem->Vert_Y[2]) + (Elem->Vert_Z[3] - Elem->Vert_Z[2]) * (Elem->Vert_Z[3] - Elem->Vert_Z[2]));
        // 计算每个面的面积和外接圆直径
        // 第0个面: 线: 3,5,4
        p = 0.5 * (length[3] + length[4] + length[5]);
        // 计算第0个面的面积
        SEdge[0] = sqrt(p * (p - length[3])) * sqrt((p - length[4]) * (p - length[5]));
        // 计算第0个面的外接圆半径
        hEdge[0] = 0.5 * length[3] * (length[4] * length[5] / SEdge[0]);
        // 第1个面: 边: 1,2,5
        p = 0.5 * (length[1] + length[2] + length[5]);
        SEdge[1] = sqrt(p * (p - length[1])) * sqrt((p - length[2]) * (p - length[5]));
        hEdge[1] = 0.5 * length[1] * (length[2] * length[5] / SEdge[1]);
        // 第2个面: 线 0,4,2
        p = 0.5 * (length[0] + length[2] + length[4]);
        SEdge[2] = sqrt(p * (p - length[0])) * sqrt((p - length[2]) * (p - length[4]));
        hEdge[2] = 0.5 * length[0] * (length[2] * length[4] / SEdge[2]);
        // 第3个面：线: 0,1,3
        p = 0.5 * (length[0] + length[1] + length[3]);
        SEdge[3] = sqrt(p * (p - length[0]) * (p - length[1]) * (p - length[3]));
        hEdge[3] = 0.5 * length[0] * (length[1] * length[3] / SEdge[3]);
        // 计算体的外接球直径
        a = length[0] * length[5];
        b = length[1] * length[4];
        c = length[2] * length[3];
        p = 0.5 * (a + b + c);
        // printf("p: %f, a: %f, b: %f, c: %f\n",p,a,b,c);
        hElem[0] = sqrt(p) * (sqrt((p - a) * (p - b) * (p - c)) / (3.0 * Elem->Volumn));
        // hElem[0] = hElem[0] / (3.0 * Elem->Volumn);
        break;
    }
}

// void ComputeElementLineDirection(MESH *mesh, INT Index, ELEMENT *Elem)
// {
//     INT worlddim = mesh->worlddim;
//     INT numlines, i;
//     FACE Face;
//     VOLU Volu;
//     switch (worlddim)
//     {
//     case 2:
//         // case2 未测试
//         Face = mesh->Faces[Index];
//         numlines = Face.NumLines;
//         INT start_face[3] = {0, 1, 2};
//         memset(Elem->if_line_positive, 0, numlines * sizeof(BOOL));
//         for (i = 0; i < numlines; i++)
//         {
//             if (mesh->Lines[Face.Line4Face[i]].Vert4Line[0] == Face.Vert4Face[start_face[i]])
//                 Elem->if_line_positive[i] = 1;
//         }
//         break;
//     case 3:
//         Volu = mesh->Volus[Index];
//         numlines = Volu.NumLines;
//         INT start_volu[6] = {0, 0, 0, 1, 1, 2};
//         memset(Elem->if_line_positive, 0, numlines * sizeof(BOOL));
//         for (i = 0; i < numlines; i++)
//         {
//             if (mesh->Lines[Volu.Line4Volu[i]].Vert4Line[0] == Volu.Vert4Volu[start_volu[i]])
//                 Elem->if_line_positive[i] = 1;
//         }
//         break;
//     }
// }

// a,b,c三边边长 p=(a+b+c)/2
// S = sqrt(p(p-a)(p-b)(p-c))
DOUBLE ComputeAreaByCoord(DOUBLE **Coord, INT worlddim)
{
    DOUBLE a, b, c, p, temp;
    INT i;
    temp = 0.0;
    for (i = 0; i < worlddim; i++)
        temp += (Coord[1][i] - Coord[2][i]) * (Coord[1][i] - Coord[2][i]);
    a = sqrt(temp);
    temp = 0.0;
    for (i = 0; i < worlddim; i++)
        temp += (Coord[0][i] - Coord[2][i]) * (Coord[0][i] - Coord[2][i]);
    b = sqrt(temp);
    temp = 0.0;
    for (i = 0; i < worlddim; i++)
        temp += (Coord[1][i] - Coord[0][i]) * (Coord[1][i] - Coord[0][i]);
    c = sqrt(temp);
    p = (a + b + c) / 2;
    temp = sqrt(p * (p - a) * (p - b) * (p - c));
    return temp;
}
