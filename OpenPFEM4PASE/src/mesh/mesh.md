# 并行网格的说明文档

##基本的数据结构的介绍



```c
#include "mesh.h"
#define PRINT_INFO 1
INT rank;
INT size;
void PTWCreate(PTW **ptw)
{
    PTW *p = (PTW *)malloc(sizeof(PTW));
    p->NumRows = -1;
    p->NumCols = -1;
    p->Ptw = NULL;
    p->Entries = NULL;
    p->AndEntries = NULL;
    *ptw = p;
}
void VertsCreate(VERT **verts, INT num)
{
    INT i;
    VERT *Verts = (VERT *)malloc(num * sizeof(VERT));
    for (i = 0; i < num; i++)
    {
        Verts[i].Index = i;
    }
    *verts = Verts;
}

void LinesCreate(LINE **lines, INT num)
{
    INT i;
    LINE *Lines = (LINE *)malloc(num * sizeof(LINE));
    for (i = 0; i < num; i++)
    {
        Lines[i].Index = i;
    }
    *lines = Lines;
}

void FacesCreate(FACE **faces, INT num)
{
    INT i;
    FACE *Faces = (FACE *)malloc(num * sizeof(FACE));
    for (i = 0; i < num; i++)
    {
        Faces[i].Index = i;
        Faces[i].NumVerts = MAXVERT4FACE;
        Faces[i].NumLines = MAXLINE4FACE;
    }
    *faces = Faces;
}

void VolusCreate(VOLU **volus, INT num)
{
    INT i;
    VOLU *Volus = (VOLU *)malloc(num * sizeof(VOLU));
    for (i = 0; i < num; i++)
    {
        Volus[i].Index = i;
        Volus[i].NumVerts = MAXVERT4VOLU;
        Volus[i].NumLines = MAXLINE4VOLU;
        Volus[i].NumFaces = MAXFACE4VOLU;
    }
    *volus = Volus;
}

void MeshCreate(MESH **mesh, MPI_Comm comm)
{
    if (*mesh != NULL)
    {
        OpenPFEM_Print("cannot create mesh with a non-null pointer MESH*mesh\n");
        return;
    }
    MESH *parmesh = (MESH *)malloc(sizeof(MESH));
#if MPI_USE
    parmesh->comm = comm;
#elif
    parmesh->comm = SQEMODE;
#endif
    parmesh->num_vert = -1;
    parmesh->num_line = -1;
    parmesh->num_face = -1;
    parmesh->num_volu = -1;
    parmesh->Verts = NULL;
    parmesh->Lines = NULL;
    parmesh->Faces = NULL;
    parmesh->Volus = NULL;
    parmesh->Fathers = NULL;
    parmesh->Ancestors = NULL;
    parmesh->SharedInfo = NULL;
    *mesh = parmesh;
    return;
}

void MeshDestory(MESH *mesh)
{
    OpenPFEM_Free(mesh->Verts);
    OpenPFEM_Free(mesh->Lines);
    OpenPFEM_Free(mesh->Faces);
    OpenPFEM_Free(mesh->Volus);
    OpenPFEM_Free(mesh->Fathers);
    OpenPFEM_Free(mesh->Ancestors);
    // sharedinfo再写一个free
    OpenPFEM_Free(mesh);
}

void VertsReCreate(VERT **verts, INT num, INT new_num)
{
    VERT *Verts = *verts;
    INT idx;
    Verts = (VERT *)realloc(Verts, new_num * sizeof(VERT));
    for (idx = num; idx < new_num; idx++)
    {
        Verts[idx].Index = idx;
    }
    *verts = Verts;
}

void LinesReCreate(LINE **lines, INT num, INT new_num)
{
    LINE *Lines = *lines;
    INT idx;
    Lines = (LINE *)realloc(Lines, new_num * sizeof(LINE));
    for (idx = num; idx < new_num; idx++)
    {
        Lines[idx].Index = idx;
    }
    *lines = Lines;
}

void FacesReCreate(FACE **faces, INT num, INT new_num)
{
    FACE *Faces = *faces;
    INT idx;
    Faces = (FACE *)realloc(Faces, new_num * sizeof(FACE));
    for (idx = num; idx < new_num; idx++)
    {
        Faces[idx].Index = idx;
        Faces[idx].NumVerts = MAXVERT4FACE;
        Faces[idx].NumLines = MAXLINE4FACE;
    }
    *faces = Faces;
}

void VolusReCreate(VOLU **volus, INT num, INT new_num)
{
    VOLU *Volus = *volus;
    INT idx;
    Volus = (VOLU *)realloc(Volus, new_num * sizeof(VOLU));
    for (idx = num; idx < new_num; idx++)
    {
        Volus[idx].Index = idx;
        Volus[idx].NumVerts = MAXVERT4VOLU;
        Volus[idx].NumLines = MAXLINE4VOLU;
        Volus[idx].NumFaces = MAXFACE4VOLU;
    }
    *volus = Volus;
}

static void Get_VertBDID_from_LineBDID(MESH *mesh)
{
    INT i;
    VERT *Verts = mesh->Verts, *vert;
    LINE *Lines = mesh->Lines, *line;
    /* 0 for interior */
    for (i = 0; i < mesh->num_vert; i++)
    {
        Verts[i].BD_ID = 0;
    }
    for (i = 0; i < mesh->num_line; i++)
    {
        line = &(Lines[i]);
        if (line->BD_ID != 0)
        {
            vert = &(Verts[line->Vert4Line[0]]);
            if (vert->BD_ID == 0)
            {
                vert->BD_ID = line->BD_ID;
            }
            vert = &(Verts[line->Vert4Line[1]]);
            if (vert->BD_ID == 0)
            {
                vert->BD_ID = line->BD_ID;
            }
        }
    }
    return;
}
```
下面这个函数是为了直接从每个面上的点的信息来产生每个面上的线的信息。当我们输入的网格只包含面和点的信息时，我们可以使用下面的功能函数来产生线的信息。
需要确认一下下面的程序中，是否已经有了边的信息？
``` c
//从线的节点信息和面的节点信息重建面的线信息
static void BuildLine4Face_fromVert4Face(MESH *mesh)
{
    INT idx_face, idx_line, j1, vert0ind, vert1ind, j2, j3, start, end, j;
    //产生Vert2Lines的信息
    PTW *Vert2Lines;
    PTWCreate(&Vert2Lines);
    GetVert2Lines(mesh, Vert2Lines);
    //printf("###########\n");
    INT *Ptw = Vert2Lines->Ptw, *Entries = Vert2Lines->Entries, *AndEntries = Vert2Lines->AndEntries;
    LINE *Lines = mesh->Lines;
    LINE *line;
    FACE *Faces = mesh->Faces;
    FACE *face;
    for (idx_face = 0; idx_face < mesh->num_face; idx_face++)
    {
        face = &(Faces[idx_face]);
        for (j1 = 0; j1 < face->NumLines; j1++)
        {
            j2 = (j1 + 1) % (face->NumLines); //第一个节点的局部编号
            j3 = (j1 + 2) % (face->NumLines); //第二个节点的局部编号
            vert0ind = face->Vert4Face[j2];
            vert1ind = face->Vert4Face[j3];
            //开始进行寻找线的编号
            start = Ptw[vert0ind];
            end = Ptw[vert0ind + 1];
            for (j = start; j < end; j++)
            {
                if (Entries[j] == vert1ind)
                { //找到了线的另一个节点, 则就可以得到相应的边的编号
                    mesh->Faces[idx_face].Line4Face[j1] = AndEntries[j];
                } // end if
            }     // end for j
        }         // end for j1
    }             // end for idx_face
    PTWDestroy(Vert2Lines);
} // end of this program
```

```c
//直接从文件中读入网格，网格是以Matlab格式存储的
static void Read_MatlabData_tri_root(MESH *mesh, const char *filename)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == ROOT_RANK)
    {
        INT status = 0, i = 0;
        FILE *file = fopen(filename, "r");
        assert(file);

        if (EOF == fscanf(file, "%d %d %d", &(mesh->num_vert), &(mesh->num_line), &(mesh->num_face)))
        {
            status = -1;
        }
        assert(status + 1);
#if PRINT_INFO
        OpenPFEM_Print("read %d vertices %d lines and %d faces for the mesh.\n", mesh->num_vert, mesh->num_line, mesh->num_face);
#endif

        VertsCreate(&(mesh->Verts), mesh->num_vert);
        LinesCreate(&(mesh->Lines), mesh->num_line);
        FacesCreate(&(mesh->Faces), mesh->num_face);
        /* vert */
        VERT *Verts = NULL;
        for (i = 0; i < mesh->num_vert; i++)
        {
            Verts = &(mesh->Verts[i]);
            Verts->Index = i;
            if (EOF == fscanf(file, "%lf %lf", &(Verts->Coord[0]), &(Verts->Coord[1])))
            {
                status = -1;
            }
        }
        assert(status + 1);
        /* line */
        LINE *Lines = NULL;
        for (i = 0; i < mesh->num_line; i++)
        {
            Lines = &(mesh->Lines[i]);
            Lines->Index = i;
            if (EOF == fscanf(file, "%d %d %d", &(Lines->Vert4Line[0]), &(Lines->Vert4Line[1]), &(Lines->BD_ID)))
            {
                status = -1;
            }
            Lines->Vert4Line[0]--;
            Lines->Vert4Line[1]--;
        }
        assert(status + 1);
        /* face */
        FACE *Faces = NULL;
        for (i = 0; i < mesh->num_face; i++)
        {
            Faces = &(mesh->Faces[i]);
            if (EOF == fscanf(file, "%d %d %d", &(Faces->Vert4Face[0]), &(Faces->Vert4Face[1]), &(Faces->Vert4Face[2])))
            {
                status = -1;
            }
            Faces->Vert4Face[0]--;
            Faces->Vert4Face[1]--;
            Faces->Vert4Face[2]--;
        }
        assert(status + 1);
        fclose(file);
        Get_VertBDID_from_LineBDID(mesh);
        BuildLine4Face_fromVert4Face(mesh);
    }
}
```
下面也是直接从文件读取网格的功能函数，具体的格式说明后续需要加上。
```c
//meshdatatype的格式介绍：
void ReadMeshRoot(MESH *mesh, const char *filename, MESHDATATYPE meshdttp, MESHTYPE meshtype)
{
    switch (meshdttp)
    {
    case MATLAB:
        switch (meshtype)
        {
        case TRIANGLE:
            Read_MatlabData_tri_root(mesh, filename);
            break;
        }
        break;
    }
}
```
下面是一致加密网格的操作，此函数中包括对二维网格和三维网格的操作。

``` c
void UniformRefineMesh(MESH *mesh, INT time)
{
    INT i;
    switch (WORLDDIM)
    {
    case 1:
        break;
    case 2:
        for (i = 0; i < time; i++)
        {
            UniformRefineMesh2D(mesh);
        }
#if PRINT_INFO
        OpenPFEM_Print("============================================\n");
        OpenPFEM_Print("UniformRefineMesh %d time(s) complete!\n", time);
        OpenPFEM_Print("============================================\n");
#endif
        break;
    case 3:
        break;
    }
}
```
下面是对二维网格的一致加密的程序, 未来需要加上并行加密的信息。
```c
//二维网格的一致加密程序，未来需要加上产生SharedInfo4GEO的信息。
void UniformRefineMesh2D(MESH *mesh)
{
    if (mesh->Verts == NULL || mesh->Lines == NULL || mesh->Faces == NULL)
    {
        return;
    }

    //开始加密
    INT i, idx_tmp;
    INT num_verts, num_lines, num_faces;
    INT New_verts, New_lines, New_faces;
    INT vert0, vert1, vert2;
    //获得局部的点、线、面的个数
    num_verts = mesh->num_vert;
    num_lines = mesh->num_line;
    num_faces = mesh->num_face;
    //计算出新的点、线、面的个数
    New_verts = num_verts + num_lines;
    New_lines = 2 * num_lines + 3 * num_faces;
    New_faces = 4 * num_faces;
    //为新的点、线、面建立相应的存储空间
    VertsReCreate(&(mesh->Verts), num_verts, New_verts);
    LinesReCreate(&(mesh->Lines), num_lines, New_lines);
    FacesReCreate(&(mesh->Faces), num_faces, New_faces);
    //产生父亲单元的信息
    if (!mesh->Fathers)
    {
        mesh->Fathers = malloc(New_faces * sizeof(int));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, New_faces * sizeof(int));
    }
    INT *Fathers = mesh->Fathers;

    VERT *Verts = mesh->Verts, *vert, *vert_s, *vert_e;
    LINE *Lines = mesh->Lines, *line, *line_old;
    FACE *Faces = mesh->Faces, *face, *face_old;
    // renew the datas of the mesh
    mesh->num_vert = New_verts;
    mesh->num_line = New_lines;
    mesh->num_face = New_faces;
    //产生新的点的数据
    for (i = 0; i < num_lines; i++)
    {
        vert = &(Verts[num_verts + i]);
        line = &(Lines[i]);
        vert_s = &(Verts[line->Vert4Line[0]]);
        vert_e = &(Verts[line->Vert4Line[1]]);
        //计算新节点的坐标
        vert->Coord[0] = 0.5 * (vert_s->Coord[0] + vert_e->Coord[0]);
        vert->Coord[1] = 0.5 * (vert_s->Coord[1] + vert_e->Coord[1]);
        //确定新点的边界信息
        vert->BD_ID = line->BD_ID;
        //产生新节点的编号
        vert->Index = num_verts + i;
    }

    //计算新产生的内部线的信息
    for (i = 0; i < num_faces; i++)
    {
        face = &(Faces[i]);
        //目前单元中新的线的编号
        //第一条线(与节点0对应的线)
        idx_tmp = 2 * num_lines + 3 * i;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[1] + num_verts;
        line->Vert4Line[1] = face->Line4Face[2] + num_verts;
        line->BD_ID = 0; // 0表示是区域内部
        line->Index = 2 * num_lines + 3 * face->Index;
        //第二条线
        idx_tmp = 2 * num_lines + 3 * i + 1;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[2] + num_verts;
        line->Vert4Line[1] = face->Line4Face[0] + num_verts;
        line->BD_ID = 0;
        line->Index = 2 * num_lines + 3 * face->Index + 1;
        //第三条线
        idx_tmp = 2 * num_lines + 3 * i + 2;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[0] + num_verts;
        line->Vert4Line[1] = face->Line4Face[1] + num_verts;
        line->BD_ID = 0;
        line->Index = 2 * num_lines + 3 * face->Index + 2;
    }

    //处理产生新的面，每个面生成4个面
    for (i = 0; i < num_faces; i++)
    {
        face_old = &(Faces[i]);
        // second
        idx_tmp = num_faces + 3 * i;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[2];
        face->Vert4Face[1] = face_old->Vert4Face[1];
        face->Vert4Face[2] = num_verts + face_old->Line4Face[0];
        line = &(Lines[face_old->Line4Face[0]]);
        if (face_old->Vert4Face[1] == line->Vert4Line[0])
        {
            face->Line4Face[0] = face_old->Line4Face[0];
        }
        else
        {
            face->Line4Face[0] = num_lines + face_old->Line4Face[0];
        }
        face->Line4Face[1] = 2 * num_lines + 3 * i + 1;
        line = &(Lines[face_old->Line4Face[2]]);
        if (face_old->Vert4Face[0] == line->Vert4Line[0])
        {
            face->Line4Face[2] = num_lines + face_old->Line4Face[2];
        }
        else
        {
            face->Line4Face[2] = face_old->Line4Face[2];
        }
        face->Index = num_faces + 3 * face_old->Index;

        // third
        idx_tmp = num_faces + 3 * i + 1;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[1];
        face->Vert4Face[1] = num_verts + face_old->Line4Face[0];
        face->Vert4Face[2] = face_old->Vert4Face[2];
        line = &(Lines[face_old->Line4Face[0]]);
        if (face_old->Vert4Face[1] == line->Vert4Line[0])
        {
            face->Line4Face[0] = num_lines + face_old->Line4Face[0];
        }
        else
        {
            face->Line4Face[0] = face_old->Line4Face[0];
        }
        line = &(Lines[face_old->Line4Face[1]]);
        if (face_old->Vert4Face[2] == line->Vert4Line[0])
        {
            face->Line4Face[1] = face_old->Line4Face[1];
        }
        else
        {
            face->Line4Face[1] = num_lines + face_old->Line4Face[1];
        }
        face->Line4Face[2] = 2 * num_lines + 3 * i + 2;
        face->Index = num_faces + 3 * face_old->Index + 1;

        // fourth
        idx_tmp = num_faces + 3 * i + 2;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[0];
        face->Vert4Face[1] = num_verts + face_old->Line4Face[1];
        face->Vert4Face[2] = num_verts + face_old->Line4Face[2];
        face->Line4Face[0] = 2 * num_lines + 3 * i;
        face->Line4Face[1] = 2 * num_lines + 3 * i + 1;
        face->Line4Face[2] = 2 * num_lines + 3 * i + 2;
        face->Index = num_faces + 3 * face_old->Index + 2;

        // first
        //得到父亲单元的编号
        Fathers[i] = i;
        vert0 = face_old->Vert4Face[0];
        vert1 = face_old->Vert4Face[1];
        vert2 = face_old->Vert4Face[2];
        face_old->Vert4Face[1] = num_verts + face_old->Line4Face[2];
        face_old->Vert4Face[2] = num_verts + face_old->Line4Face[1];
        face_old->Line4Face[0] = 2 * num_lines + 3 * i;
        line = &(Lines[face_old->Line4Face[1]]);
        if (vert2 == line->Vert4Line[0])
        {
            face_old->Line4Face[1] = num_lines + face_old->Line4Face[1];
        }
        else
        {
            face_old->Line4Face[1] = face_old->Line4Face[1];
        }
        line = &(Lines[face_old->Line4Face[2]]);
        if (vert0 == line->Vert4Line[0])
        {
            face_old->Line4Face[2] = face_old->Line4Face[2];
        }
        else
        {
            face_old->Line4Face[2] = num_lines + face_old->Line4Face[2];
        }
    }

    //最后处理原来的线(一分为二)
    for (i = 0; i < num_lines; i++)
    {
        line_old = &(Lines[i]);
        line = &(Lines[num_lines + i]);
        line->Vert4Line[0] = num_verts + i;
        line->Vert4Line[1] = line_old->Vert4Line[1];
        line->BD_ID = line_old->BD_ID;
        line->Index = num_lines + line_old->Index;
        line->Vert4Line[1] = num_verts + i;
    }

#if MPI_USE
    if (mesh->SharedInfo != NULL)
    {
        OpenPFEM_RankPrint(mesh->comm, "waiting to add uniform refinement for sharedinfo");
    }
#endif
}
```
下面的程序是对网格进行并行划分的过程，同时产生相应的并行信息和数据的发送和接收。
```c
void MeshPartition(MESH *mesh)
{
    extern int indicator_comm;
    if (MPI_USE == 0)
    {
        return;
    }
    //下面定义一些全局变量
    INT NumRanks = size, *ElemRanks, rankind, NumElems;
    PTW *Vert2Ranks, *Vert4Ranks;
    PTW *Vert4Elems = NULL;
    MPI_Comm_rank(mesh->comm, &rank);
    MPI_Comm_size(mesh->comm, &NumRanks);
        //===================================================================
    // 下面来处理线的信息，包括共享信息、点与线的局部编号信息
    PTW *Line2Ranks = NULL, *Line4Ranks = NULL, *Line4Elems = NULL;

    if (rank == ROOT_RANK)
    {
        //得到每个单元上的节点编号
        PTWCreate(&Vert4Elems);
        GetGEO4Elems(mesh, WORLDDIM, 0, Vert4Elems);
        NumElems = Vert4Elems->NumRows;
        //调用Metis得到单元的划分信息
        INT *ElemRanks = (INT *)malloc(NumElems * sizeof(INT));
        //返回值：ElemRanks存储了每个单元所属的进程号，可以称为Elem2Ranks
        MetisPartition(mesh, Vert4Elems, NumElems, ElemRanks);
        //先得到每个进程所分配的单元集合
        PTW *Elem4Ranks;
        PTWCreate(&Elem4Ranks);
        GetElem4Ranks(ElemRanks, NumElems, Elem4Ranks);
        //下面开始来处理每个进程所包含点的信息
        PTWCreate(&Vert2Ranks);
        PTWCreate(&Vert4Ranks);
        //得到与每个节点相联系的进程号
        printf("11111111111111\n");   
        printf("NumRanks: %d\n", NumRanks);
        GetGEO2Ranks(mesh, NumRanks, ElemRanks, WORLDDIM, 0, Vert2Ranks);
        printf("222222222222\n");
        OutPutPTW(Vert2Ranks);
        //得到每个进程号上的节点集合，Vert4Ranks中节点编号是全局编号
        PTWTranspose(Vert2Ranks, Vert4Ranks);
        OutPutPTW(Vert4Ranks);
        printf("3333333333333\n");
        //然后生成每个进程上共享点的信息
        SHAREDINFO4GEO **SharedInfo4Verts;
        SharedInfo4Verts = malloc(NumRanks * sizeof(SHAREDINFO4GEO*));
        // for(rankind=0;rankind<NumRanks;rankind++){
        // SharedInfo4Verts[rankind] = malloc(sizeof(SHAREDINFO4GEO));
        //}
        GetSharedINFO4GEO(Vert2Ranks, Vert4Ranks, SharedInfo4Verts);
        for (rankind = 0; rankind < NumRanks; rankind++)
        {
            printf("The shared inforamtion for the %d-th rank!\n", rankind);
            OutPutSharedInfo4GEO(SharedInfo4Verts[rankind]);
        }
        printf("444444444444\n");
        //======然后将SharedInfor4Verts数据结构体传送出去，每个进程一个这里的对象。
        // SharedInfo4Verts[rankind]


        //=============================================
        //下面开始来处理每个进程所包含线的信息
        //得到每个单元上线的编号,注意这里单元有可能是线，有可能是面，也有可能是体
        if (WORLDDIM >= 1)
        {
            //得到每个单元上线的信息
            PTWCreate(&Line4Elems);
            GetGEO4Elems(mesh, WORLDDIM, 1, Line4Elems);
            OutPutPTW(Line4Elems);
            //初始化具体的结构体对象
            PTWCreate(&Line2Ranks);
            PTWCreate(&Line4Ranks);
            //得到与每条线相联系的进程号
            printf("55555555555\n");
            for(rankind=0;rankind<NumRanks;rankind++)
            {
                printf("rankind: %d, NumRanks: %d\n",rankind, NumRanks);
                printf("ElemRanks[%d]=%d\n", rankind, ElemRanks[rankind]);
            }
            GetGEO2Ranks(mesh, NumRanks, ElemRanks, WORLDDIM, 1, Line2Ranks);
            //得到每个进程上线的编号集合
            PTWTranspose(Line2Ranks, Line4Ranks);
            //得到线的共享信息
            SHAREDINFO4GEO **SharedInfo4Lines;
            SharedInfo4Lines = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
            GetSharedINFO4GEO(Line2Ranks, Line4Ranks, SharedInfo4Lines);
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                printf("The shared inforamtion for the %d-th rank!\n", rankind);
                OutPutSharedInfo4GEO(SharedInfo4Lines[rankind]);
            }
            printf("666666666666\n");
            //下面来得到点与线的局部关系
            //得到每条线上点的编号
            PTW *Vert4Lines = NULL;
            if (WORLDDIM == 1)
            {
                //如果单元就是一维的，那么就不用再产生了
                Vert4Lines = Vert4Elems;
            }
            else
            {
                PTWCreate(&Vert4Lines);
                //得到每条线上的节点信息
                GetGEO4Elems(mesh, 1, 0, Vert4Lines);
            }
            PTW **LocalVert4Lines = malloc(NumRanks*sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalVert4Lines[rankind]);
            }
            GetLocalGEO4Elems(Vert4Lines, Line4Ranks, Vert2Ranks, LocalVert4Lines);
            for(rankind=0;rankind<NumRanks;rankind++)
            {
                printf("The vert4line inforamtion in %d-th rank",rankind);
                OutPutPTW(LocalVert4Lines[rankind]);
            }
            //将线的共享信息SharedInfo4Lines和LocalVert4Lines的信息发送出去
        } // end if(WORLDDIM==1)

        //===============下面来处理面的信息======
        PTW *Face2Ranks = NULL, *Face4Ranks = NULL;
        PTW *Face4Elems = NULL;
        if (WORLDDIM >= 2)
        { //只有至少二维的时候才需要处理面的信息
            //得到每个单元上面的编号                                 
            PTWCreate(&Face4Elems);
            //得到每个单元上面的信息
            printf("FFFFFFFFFFFFF\n");
            GetGEO4Elems(mesh, WORLDDIM, 2, Face4Elems);   
            //OutPutPTW(Face4Elems);
            PTWCreate(&Face2Ranks);
            PTWCreate(&Face4Ranks);
            //得到与每个面相联系的进程号
            GetGEO2Ranks(mesh, NumRanks, ElemRanks, WORLDDIM, 2, Face2Ranks);
            //得到每个进程上面的编号集合
            PTWTranspose(Face2Ranks, Face4Ranks);
            printf("KKKKKKKKK\n");
            //OutPutPTW(Face2Ranks);
            //OutPutPTW(Face4Ranks);
            SHAREDINFO4GEO **SharedInfo4Faces;
            SharedInfo4Faces = malloc(NumRanks*sizeof(SHAREDINFO4GEO*));
            GetSharedINFO4GEO(Face2Ranks, Face4Ranks, SharedInfo4Faces);
            //下面来得到点与面的局部关系
            //得到每个面上点的编号
            PTW *Vert4Faces = NULL;
            if (WORLDDIM == 2)
            {
                Vert4Faces = Vert4Elems;
            }
            else
            {
                PTWCreate(&Vert4Faces);
                //得到每个面上的节点信息
                GetGEO4Elems(mesh, 2, 0, Vert4Faces);
            }

            PTW **LocalVert4Faces = malloc(NumRanks*sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalVert4Faces[rankind]);
            }
            GetLocalGEO4Elems(Vert4Faces, Face4Ranks, Vert2Ranks, LocalVert4Faces);
            for(rankind=0;rankind<NumRanks;rankind++)
            {
                printf("The vert4face inforamtion in %d-th rank",rankind);
                OutPutPTW(LocalVert4Faces[rankind]);
            }
            printf("HHHHHHHHHHHH\n");
            //下面来得到线与面的局部关系
            //得到每个面上线的编号
            PTW *Line4Faces = NULL;
            if (WORLDDIM == 2)
            {
                Line4Faces = Line4Elems;
            }
            else
            {
                PTWCreate(&Line4Faces);
                //得到每条线上的节点信息
                GetGEO4Elems(mesh, 2, 1, Line4Faces);
            }
            PTW **LocalLine4Faces = malloc(NumRanks*sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalLine4Faces[rankind]);
            }
            GetLocalGEO4Elems(Line4Faces, Face4Ranks, Line2Ranks, LocalLine4Faces);
            for(rankind=0;rankind<NumRanks;rankind++)
            {
                printf("The line4face inforamtion in %d-th rank",rankind);
                OutPutPTW(LocalLine4Faces[rankind]);
            }
            printf("ZZZZZZZZZZ\n");
            //将线的共享信息SharedInfo4Fines, LocalVert4Faces和LocalLine4Faces的信息发送出去
        } // end for WORLDDIM>=2


        //下面来处理体的情况，目前来说网格最多是三维，所以前面对体的共享信息的处理可以省略
            PTW *Volu2Ranks, *Volu4Ranks;
        if (WORLDDIM >= 3)
        {
            //得到每个单元上体的编号
            PTW *Volu4Elems = NULL;
            PTWCreate(&Volu4Elems);
            //得到每个单元上体的信息
            GetGEO4Elems(mesh, WORLDDIM, 3, Volu4Elems);
            PTWCreate(&Volu2Ranks);
            PTWCreate(&Volu4Ranks);
            //得到与每个体相联系的进程号
            GetGEO2Ranks(mesh, NumRanks, ElemRanks, WORLDDIM, 3, Volu2Ranks);
            //得到每个进程上体的编号集合
            PTWTranspose(Volu2Ranks, Volu4Ranks);
            SHAREDINFO4GEO **SharedInfo4Volus;
            SharedInfo4Volus = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
            GetSharedINFO4GEO(Volu2Ranks, Volu4Ranks, SharedInfo4Volus);
            //下面来得到点与体的局部关系
            //得到每个体上点的编号
            PTW *Vert4Volus = NULL;
            if (WORLDDIM == 3)
            {
                Vert4Volus = Vert4Elems;
            }
            else
            {
                PTWCreate(&Vert4Volus);
                //得到每个体上的节点信息
                GetGEO4Elems(mesh, 3, 0, Vert4Volus);
            }
            PTW **LocalVert4Volus = malloc(NumRanks * sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalVert4Volus[rankind]);
            }
            GetLocalGEO4Elems(Vert4Volus, Volu4Ranks, Vert2Ranks, LocalVert4Volus);
            //下面来得到线与体的局部关系
            //得到每个体上线的编号
            PTW *Line4Volus = NULL;
            if (WORLDDIM == 3)
            {
                Line4Volus = Line4Elems;
            }
            else
            {
                PTWCreate(&Line4Volus);
                //得到每个体上的线的信息
                GetGEO4Elems(mesh, 3, 1, Line4Volus);
            }
            PTW **LocalLine4Volus = malloc(NumRanks * sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalLine4Volus[rankind]);
            }
            GetLocalGEO4Elems(Line4Volus, Volu4Ranks, Line2Ranks, LocalLine4Volus);
            //下面来得到面与体的局部关系
            //得到每个体上面的编号
            PTW *Face4Volus = NULL;
            if (WORLDDIM == 3)
            {
                Face4Volus = Face4Elems;
            }
            else
            {
                PTWCreate(&Face4Volus);
                //得到每个体上的线的信息
                GetGEO4Elems(mesh, 3, 2, Face4Volus);
            }
            PTW **LocalFace4Volus = malloc(NumRanks * sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalFace4Volus[rankind]);
            }
            GetLocalGEO4Elems(Face4Volus, Volu4Ranks, Face2Ranks, LocalLine4Volus);
            //将体的共享信息SharedInfo4Volus和LocalVert4Volus, LocalLine4Volus, LocalFace4Volus
            //的信息发送出去
        } // end if(WORLDDIM==3)

    } // end for ROOT_RANK
    //然后在每个进程上处理共享点的信息

     indicator_comm = 0;
    if (indicator_comm == 1)
    {
        return;
    }

} //至此完成了对网格的划分
```

```c
//得到每个进程上单元的几何
void GetElem4Ranks(INT *ElemRanks, INT NumElems, PTW *Elem4Ranks)
{
    INT NumRanks = size;
    INT *Ptw = Elem4Ranks->Ptw;
    INT *Entries = Elem4Ranks->Entries;
    Ptw = malloc((NumRanks + 1) * sizeof(INT));
    memset(Ptw, 0, (NumRanks + 1) * sizeof(INT));
    INT k, pos, rankind;
    for (k = 0; k < NumElems; k++)
    {
        rankind = ElemRanks[k]; //获得当前单元所属的进程号
        Ptw[rankind + 1]++;     //相应进程上增加一个单元
    }
    Ptw[0] = 0;
    for (k = 0; k < NumRanks; k++)
    {
        Ptw[k + 1] += Ptw[k];
    }
    Entries = malloc(Ptw[NumRanks] * sizeof(INT));
    INT *RankPos = malloc(NumRanks * sizeof(INT));
    memset(RankPos, 0, NumRanks * sizeof(INT));
    for (k = 0; k < NumElems; k++)
    {
        rankind = ElemRanks[k]; //获得当前单元所属的进程号
        pos = Ptw[rankind] + RankPos[rankind];
        Entries[pos] = k; //在相应的地方记录下单元的编号
        RankPos[rankind]++;
    }
    free(RankPos);
#if PRINT_INFO
    INT start, end, j;
    for (k = 0; k < NumRanks; k++)
    {
        start = Ptw[k];
        end = Ptw[k + 1];
        OpenPFEM_Print("第%d-个进程的单元: \n", k);
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,   ", Entries[j]);
        }
        OpenPFEM_Print("\n");
    }

#endif
} // end of this program
```
下面的程序对输入的单元与节点关系的信息进行划分，得到每个单元所属于的进程编号。
```c
//对输入的单元（由节点与单元的关系来决定单元相互之间的关系）进行进程划分
void MetisPartition(MESH *mesh, PTW *Vert4Elems, INT NumElems, INT *ElemRanks)
{
    idx_t i;
    idx_t elem_num = (idx_t)NumElems;
    idx_t vert_num = (idx_t)(Vert4Elems->NumCols);
    idx_t totalnum = (idx_t)(Vert4Elems->Ptw[NumElems]);
    idx_t *eptr = (idx_t *)malloc((elem_num + 1) * sizeof(idx_t));
    idx_t *eind = (idx_t *)malloc(totalnum * sizeof(idx_t));
    INT *PtwV4E = Vert4Elems->Ptw;
    INT *V4E = Vert4Elems->Entries;
    for (i = 0; i <= elem_num; i++)
    {
        eptr[i] = (idx_t)(PtwV4E[i]);
    }
    for (i = 0; i < totalnum; i++)
    {
        eind[i] = (idx_t)(V4E[i]);
    }
    idx_t *vwgt = NULL, *vsize = NULL, *options = NULL;
    idx_t ncommon = WORLDDIM;
    MPI_Comm_size(mesh->comm, &size);
    idx_t part_num = (idx_t)size;
    real_t *tpwgts = NULL;
    idx_t *objval = (idx_t *)malloc(sizeof(idx_t));
    idx_t *epart = (idx_t *)malloc(elem_num * sizeof(idx_t));
    idx_t *npart = (idx_t *)malloc(vert_num * sizeof(idx_t));
    METIS_PartMeshDual(&elem_num, &vert_num, eptr, eind, vwgt, vsize, &ncommon,
                       &part_num, tpwgts, options, objval, epart, npart);
    for (i = 0; i < NumElems; i++)
    {
        ElemRanks[i] = (int)(epart[i]);
#if PRINT_INFO
        OpenPFEM_Print("第%d个单元被分给了%d进程\n", i, ElemRanks[i]);
#endif
    }
    OpenPFEM_Free(eptr);
    OpenPFEM_Free(eind);
    OpenPFEM_Free(objval);
    OpenPFEM_Free(epart);
    OpenPFEM_Free(npart);
}
```
下面的这个程序是对二维的网格进行输出，用于程序调试
```c
void PrintMesh2D(MESH *mesh, INT printrank)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == printrank)
    {

        INT i, j;

        printf("\n================================================\n");
        printf("[RANK %d] 网格有%d个点 %d条线 %d个面: \n", rank, mesh->num_vert, mesh->num_line, mesh->num_face);
        printf("================================================\n");

        VERT *Verts = mesh->Verts;
        VERT *vert;
        printf("(1)点: \n");
        for (i = 0; i < mesh->num_vert; i++)
        {
            vert = &(Verts[i]);
            printf("点%d: 坐标(%g,%g) 边界信息%d 编号%d\n",
                   i, vert->Coord[0], vert->Coord[1], vert->BD_ID, vert->Index);
        }
        printf("-------------------------------------------------\n");

        LINE *Lines = mesh->Lines;
        LINE *line;
        printf("(2)线: \n");
        for (i = 0; i < mesh->num_line; i++)
        {
            line = &(Lines[i]);
            printf("线%d: 顶点(%d,%d) 边界信息%d 编号%d\n",
                   i, line->Vert4Line[0], line->Vert4Line[1], line->BD_ID, line->Index);
        }
        printf("-------------------------------------------------\n");

        FACE *Faces = mesh->Faces;
        FACE *face;
        printf("(3)面: \n");
        for (i = 0; i < mesh->num_face; i++)
        {
            face = &(Faces[i]);
            printf("面%d: 编号%d 顶点", i, face->Index);
            for (j = 0; j < face->NumVerts; j++)
            {
                printf("-%d", face->Vert4Face[j]);
            }
            printf(" 边");
            for (j = 0; j < face->NumLines; j++)
            {
                printf("-%d", face->Line4Face[j]);
            }
            printf("\n");
        }
        printf("================================================\n\n");
    }
    MPI_Barrier(mesh->comm);
    return;
}

void PTWTranspose(PTW *GEO4Elems, PTW *GEO2Elems)
{
    INT j, elemind, geoind, NumRows = GEO4Elems->NumCols, NumCols = GEO4Elems->NumRows;
    printf("NumRows=%d,  NumCols=%d\n", NumRows, NumCols);
    GEO2Elems->NumRows = NumRows;
    GEO2Elems->NumCols = NumCols;
    INT *Ptw4 = GEO4Elems->Ptw;
    INT *Entries4 = GEO4Elems->Entries;
    GEO2Elems->Ptw = malloc((NumRows + 1) * sizeof(INT));
    memset(GEO2Elems->Ptw, 0, (NumRows + 1) * sizeof(INT));
    INT *Ptw2 = GEO2Elems->Ptw;
    INT start, end;
    for (elemind = 0; elemind < NumCols; elemind++)
    {
        start = Ptw4[elemind];
        end = Ptw4[elemind + 1];
        for (j = start; j < end; j++)
        {
            Ptw2[Entries4[j] + 1]++;
            // printf("XXX Ptw2[%d]=%d\n", Entries4[j] + 1, Ptw2[Entries4[j] + 1]);
        }
    }
    Ptw2[0] = 0;
    for (geoind = 0; geoind < NumRows; geoind++)
    {
        Ptw2[geoind + 1] += Ptw2[geoind];
        // printf("YYY Ptw2[%d]=%d\n",geoind+1,Ptw2[geoind+1]);
    }
    //最后再来统计每个进程所包含的节点编号
    GEO2Elems->Entries = malloc(Ptw2[NumRows] * sizeof(INT));
    INT *Entries2 = GEO2Elems->Entries;
    INT *GeoPos = malloc(NumRows * sizeof(INT));
    memset(GeoPos, 0, NumRows * sizeof(INT));
    //进行赋值
    for (elemind = 0; elemind < NumCols; elemind++)
    {
        start = Ptw4[elemind];
        end = Ptw4[elemind + 1];
        for (j = start; j < end; j++)
        {
            // Elem2Ranks[j]: 目前节点所属的进程号，PtwVertRanks: 目前进程所在的起始位置
            geoind = Entries4[j];
            Entries2[Ptw2[geoind] + GeoPos[geoind]] = elemind;
            GeoPos[geoind]++;
        }
    }
    free(GeoPos);
    // OutPutPTW(GEO2Elems);
}

//得到每个单元上的几何元素
// ElemDim表示单元的维数,1表示是一维网格，2表示是二维网格，3表示是三维网格
// GeoDim表示GEO元素的维数,0表示点，1表示线，2表示面，3表示体
void GetGEO4Elems(MESH *mesh, INT ElemDim, INT GeoDim, PTW *GEO4Elems)
{
    //首先得到GEO4Elems
    INT k, j, pos, *Ptw = GEO4Elems->Ptw, *Entries = GEO4Elems->Entries;
    INT NumFaces, NumElems, NumVerts, NumLines;
    switch (GeoDim)
    {
    case 0:
        GEO4Elems->NumCols = mesh->num_vert;
        break;
    case 1:
        GEO4Elems->NumCols = mesh->num_line;
        break;
    case 2:
        GEO4Elems->NumCols = mesh->num_face;
        break;
    case 3:
        GEO4Elems->NumCols = mesh->num_volu;
        break;
    }
    LINE line, *Lines = mesh->Lines;
    FACE face, *Faces = mesh->Faces;
    VOLU volu, *Volus = mesh->Volus;
    switch (ElemDim)
    {
    case 1: //表示是一维的网格，那么geodim必须为0
        GEO4Elems->NumRows = mesh->num_line;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Entries = malloc(2 * NumElems * sizeof(INT));
        //对线进行循环得到相应的位置,并且GeoDim必须等于0
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            for (k = 0; k < NumElems; k++)
            {
                line = Lines[k];
                Ptw[k + 1] = 2 * (k + 1);
                Entries[2 * k] = line.Vert4Line[0];
                Entries[2 * k + 1] = line.Vert4Line[1];
            }
            break;
        case 1:
            Entries = malloc(NumElems*sizeof(INT));
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {
                Ptw[k + 1] = k+1;
                Entries[k] = k;
            } // end for k
            break;
        } // end switchx
        break;
    case 2: //二维网格的情况
        GEO4Elems->NumRows = mesh->num_face;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            GEO4Elems->NumCols = mesh->num_vert;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                Ptw[k + 1] = Ptw[k] + face.NumVerts;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                NumVerts = face.NumVerts;
                for (j = 0; j < NumVerts; j++)
                {
                    Entries[pos] = face.Vert4Face[j];
                    pos++;
                }
            }
            break;
        case 1:
            GEO4Elems->NumCols = mesh->num_line;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                Ptw[k + 1] = Ptw[k] + face.NumLines;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                NumLines = face.NumLines;
                for (j = 0; j < NumLines; j++)
                {
                    Entries[pos] = face.Line4Face[j];
                    pos++;
                }
            }
            break;
        case 2:
            Entries = malloc(NumElems*sizeof(INT));
            printf("LLLLLLLLL\n");
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {                
                Ptw[k + 1] = k+1;
                Entries[k] = k;
                printf("The %d-th element: %d\n", k, Entries[k]);
            }//end for k
            break;
        }//end switch
        break;
    case 3:
        GEO4Elems->NumRows = mesh->num_volu;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            GEO4Elems->NumCols = mesh->num_vert;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumVerts;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumVerts = volu.NumVerts;
                for (j = 0; j < NumVerts; j++)
                {
                    Entries[pos] = volu.Vert4Volu[j];
                    pos++;
                }
            }
            break;
        case 1:
            GEO4Elems->NumCols = mesh->num_line;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumLines;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumLines = volu.NumLines;
                for (j = 0; j < NumLines; j++)
                {
                    Entries[pos] = volu.Line4Volu[j];
                    pos++;
                }
            }
            break;
        case 2:
            GEO4Elems->NumCols = mesh->num_face;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumFaces;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumFaces = volu.NumFaces;
                for (j = 0; j < NumFaces; j++)
                {
                    Entries[pos] = volu.Face4Volu[j];
                    pos++;
                }
            }
            break;
        case 3:
            Entries = malloc(NumElems*sizeof(INT));
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {
                Ptw[k + 1] = k+1;
                Entries[k] = k;
            } // end for k
            break;
        } //end switch for GEODIM
        break;
    }
    GEO4Elems->Ptw = Ptw;
    GEO4Elems->Entries = Entries;
    //OutPutPTW(GEO4Elems);
}

//得到与几何元素相联系的进程信息
// GeoDim: 表示几何元素的维数
void GetGEO2Ranks(MESH *mesh, INT NumRanks, INT *ElemRanks, INT ElemDim, INT GeoDim, PTW *GEO2Ranks)
{
    INT i, j, k;
    INT start0, end0, start, end, length, flag, rankind, elemind, pos;
    PTW *GEO4Elems = NULL;
    PTWCreate(&GEO4Elems);
    GetGEO4Elems(mesh, ElemDim, GeoDim, GEO4Elems);
    PTW *GEO2Elems = NULL;
    PTWCreate(&GEO2Elems);
    //得到与几何元素相关的单元信息
    OutPutPTW(GEO4Elems);
    PTWTranspose(GEO4Elems, GEO2Elems);
    printf("AAAAAAAAAAAAAA\n");
    OutPutPTW(GEO2Elems);
    printf("BBBBBBBBBBBBBB\n");
    INT *Ptw0 = GEO2Elems->Ptw, *Entries0 = GEO2Elems->Entries;
    INT geoind, numgeo = GEO2Elems->NumRows;
    // GEO2Elems->Ptw = malloc((numgeo + 1) * sizeof(INT));
    printf("numgeo: %d, Ptw0[numgeo]: %d\n", numgeo, Ptw0[numgeo]);
    INT *CoarseGEO2Ranks = malloc(Ptw0[numgeo] * sizeof(INT));
    printf("CCCCCCCCC\n");
    memset(CoarseGEO2Ranks, -1, Ptw0[numgeo] * sizeof(INT));
    //线用Ptw表示CoarseGEO2Ranks中每行的当前位置
    printf("DDDDDDDDDD\n");
    // PTWCreate(&GEO2Ranks);
    GEO2Ranks->NumRows = numgeo;
    GEO2Ranks->NumCols = NumRanks;
    GEO2Ranks->Ptw = malloc((numgeo + 1) * sizeof(INT));
    memset(GEO2Ranks->Ptw, 0, (numgeo + 1) * sizeof(INT));
    printf("EEEEEEEEEE: numgeo: %d\n", numgeo);
    INT *Ptw = GEO2Ranks->Ptw;
    for (geoind = 0; geoind < numgeo; geoind++)
    {
        start0 = Ptw0[geoind];
        end0 = Ptw0[geoind + 1];
        pos = Ptw[geoind + 1]; //目前进程号的登记位置
        for (j = start0; j < end0; j++)
        {
            elemind = Entries0[j]; //得到目前位置的单元编号
            printf("elemind: %d\n", elemind);
            rankind = ElemRanks[elemind]; //取出当前单元所在的进程号
            printf("rankind: %d\n", rankind);
            flag = 0;
            for (i = 0; i < pos; i++)
            { //检查该进程号是否已经被记录
                if (CoarseGEO2Ranks[start0 + i] == rankind)
                {
                    flag = 1;
                    i = pos + 1;
                } // end if
            }     // end for i
            //如果flag==0即表示该进程号没有被记录
            if (flag == 0)
            {
                CoarseGEO2Ranks[start0 + pos] = rankind; //记录该进程号
                pos++;
            } // end if flag==0
        }     // end for j
        Ptw[geoind + 1] = pos;
    } // end for geoind
    //统计总共需要多长的数组来存储这些节点进程号的信息
    Ptw[0] = 0;
    for (k = 0; k < numgeo; k++)
    {
        Ptw[k + 1] += Ptw[k];
        // printf("Ptw[%d]=%d\n",k+1, Ptw[k+1]);
    }
    GEO2Ranks->Entries = malloc(Ptw[numgeo] * sizeof(INT));
    INT *Entries = GEO2Ranks->Entries;
    //然后再进行单元循环，把相应的对应关系完备
    for (geoind = 0; geoind < numgeo; geoind++)
    {
        //找到在CoarseVert2Ranks中的起始位置
        start0 = Ptw0[geoind];
        //找到在GEO2Ranks中的起始位置
        start = Ptw[geoind]; //这是起始位置
        length = Ptw[geoind + 1] - start;
        //处理该几何元素上的所有进程信息
        for (i = 0; i < length; i++)
        {
            Entries[start + i] = CoarseGEO2Ranks[start0 + i];
        } // end for i
    }     // end for geoind
    //释放内存空间
    free(CoarseGEO2Ranks);
    // printf("Ouput for checking!\n");
    // OutPutPTW(GEO2Ranks);
}

void GetGEO4Ranks(MESH *mesh, INT NumRanks, INT *ElemRanks, INT ElemDim, INT GeoDim, PTW GEO4Ranks)
{
}

//根据GEO2Ranks和GEO4Ranks的信息产生几何元素的共享信息
//输出信息：
//与GEO2Ranks中的AndEntries存储的是LocalGEOIndex2Ranks:记录几何元素在相应进程上的局部编号
// LocalGEOIndex2Ranks已经存储在GEO2Ranks的AndEntries数组中
// 2022-03-31: 加上Owners的信息
void GetSharedINFO4GEO(PTW *GEO2Ranks, PTW *GEO4Ranks, SHAREDINFO4GEO **SharedINFO4GEO)
{
    INT NumRanks = GEO4Ranks->NumRows; //获得进程数
    //SharedINFO4GEO = malloc(NumRanks*sizeof(SHAREDINFO4GEO*));//每个进程对应这里的一个元素
    INT rankind, geoind, i, j, k, start2, end2, start4, length, startrank;
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        printf("rankind: %d\n",rankind);
        SharedINFO4GEO[rankind] = malloc(sizeof(SHAREDINFO4GEO));
    }
    OutPutPTW(GEO2Ranks);
    INT NumGeos = GEO2Ranks->NumRows; //获得几何元素的个数
    INT *Ptw2 = GEO2Ranks->Ptw, *Ptw4 = GEO4Ranks->Ptw;
    INT *Entries2 = GEO2Ranks->Entries, *Entries4 = GEO4Ranks->Entries;
    //记录每个全局节点在每个进程上的局部编号,与GEO2Ranks结构相同
    GEO2Ranks->AndEntries = malloc(Ptw2[NumGeos] * sizeof(INT));
    INT *LocalGEOIndex2Ranks = GEO2Ranks->AndEntries;
    for (rankind = 0; rankind < NumRanks; rankind++)
    { //处理第rankind的进程
        start4 = Ptw4[rankind];
        length = Ptw4[rankind + 1] - start4; //该进程几何元素的个数
        for (j = 0; j < length; j++)
        {                                  //处理该进程上的第j个几何元素
            geoind = Entries4[start4 + j]; //得到第j个几何元素的全局编号
            //找到geoind在Entries2中的信息，将其相应的位置设置为该进程中的局部编号j
            start2 = Ptw2[geoind];
            end2 = Ptw2[geoind + 1];
            for (i = start2; i < end2; i++)
            { //在相应的几何元素行找到相应的位置
                if (Entries2[i] == rankind)
                {
                    LocalGEOIndex2Ranks[i] = j; //将相应的位置设置为GEO相应的局部编号
                    i = end2 + 1;               //如此就跳出循环
                }                               // end if
            }                                   // end for i
        }                                       // end for j
        SharedINFO4GEO[rankind]->SharedNum = 0;
    } // end for rankind 如下就得到了每个GEO元素所在的进程信息和相应的局部编号信息
    OutPutPTW2(GEO2Ranks);
    //接下来是为了产生几何元素的共享信息，可以从Ptw2中发现当GEO元素分布在
    //多于一个进程的情况下就是一个共享几何元素，并且相应的进程号和相应的局部编号都可以找到
    //首先统计每个进程需为共享元素申请的内存空间
    for (geoind = 0; geoind < NumGeos; geoind++)
    { //查看每个几何元素是否是共享元素
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        if (end2 - start2 > 1)
        { //表示该几何元素是共享的，需往相应的进程上加上相应的长度
            for (j = start2; j < end2; j++)
            {                                         //处理该几何元素在每个进程上的信息
                rankind = Entries2[j];                //得到几何元素所在的进程号
                SharedINFO4GEO[rankind]->SharedNum++; //相应进程的共享元素加1
            }                                         // end for j
        }                                             // end for if
    }                                                 // end for geoind, 由此得到了每个进程上所包含的共享几何元素的个数
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        printf("The %d-th rank: SharedNum: %d\n", rankind, SharedINFO4GEO[rankind]->SharedNum);
        SharedINFO4GEO[rankind]->Ptw =
            malloc((SharedINFO4GEO[rankind]->SharedNum + 1) * sizeof(INT));
        SharedINFO4GEO[rankind]->Owners =
            malloc(SharedINFO4GEO[rankind]->SharedNum * sizeof(INT));
    }                                              // end for rankind
    INT *RankPos = malloc(NumRanks * sizeof(INT)); //记录每个进程上共享元素的位置
    memset(RankPos, 0, NumRanks * sizeof(INT));    //表示每个进程上没有共享元素
    for (geoind = 0; geoind < NumGeos; geoind++)
    { //查看每个几何元素是否是共享元素
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        length = end2 - start2;
        if (length > 1)
        { //表示该几何元素是共享的，需往相应的进程上加上相应的长度
            for (j = start2; j < end2; j++)
            {                          //处理该几何元素在每个进程上的信息
                rankind = Entries2[j]; //得到几何元素所在的进程号
                //所在的进程应该增加该共享元素所需要的空间(每个共享元素应该有的长度)
                SharedINFO4GEO[rankind]->Ptw[RankPos[rankind] + 1] = length;
                //第一个进程为宿主
                SharedINFO4GEO[rankind]->Owners[RankPos[rankind]] = Entries2[start2];
                RankPos[rankind]++;
            } // end for j
        }     // end for if
    }         // end for geoind, 由此得到了每个进程上所包含的共享几何元素的个数
    //接下来给每个进程中申请记录共享元素的空间
    INT SharedNum;
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        SharedINFO4GEO[rankind]->Ptw[0] = 0;
        SharedNum = SharedINFO4GEO[rankind]->SharedNum;
        for (k = 0; k < SharedNum; k++)
        {
            SharedINFO4GEO[rankind]->Ptw[k + 1] += SharedINFO4GEO[rankind]->Ptw[k];
        } // end for k
        //申请空间
        SharedINFO4GEO[rankind]->SharedRanks = malloc(SharedINFO4GEO[rankind]->Ptw[SharedNum] * sizeof(INT));
        SharedINFO4GEO[rankind]->LocalIndex = malloc(SharedINFO4GEO[rankind]->Ptw[SharedNum] * sizeof(INT));
        printf("The length for the %d-th rank: %d\n", rankind, SharedINFO4GEO[rankind]->Ptw[SharedNum]);
    } // end for rankind
    //接下来对每个进程上的共享信息赋值
    memset(RankPos, 0, NumRanks * sizeof(INT));
    for (geoind = 0; geoind < NumGeos; geoind++)
    {
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        length = end2 - start2;
        if (length > 1)
        { //该共享元素的长度为length
            for (j = start2; j < end2; j++)
            {
                rankind = Entries2[j];        //获得进程号
                startrank = RankPos[rankind]; //获得当前进程的起始位置
                for (i = 0; i < length; i++)
                {
                    SharedINFO4GEO[rankind]->SharedRanks[startrank + i] = Entries2[start2 + i];           //记录共享元素所在的进程号
                    SharedINFO4GEO[rankind]->LocalIndex[startrank + i] = LocalGEOIndex2Ranks[start2 + i]; //记录共享元素在所在进程上的局部编号
                }                                                                                         // end for i
                RankPos[rankind] += length;
            } // end for j
        }     // end for if(length>1)
    }         // end for geoind 至此每个进程上的共享信息产生完备
    free(RankPos);
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        printf("The shared information for the %d-th rank\n", rankind);
        OutPutSharedInfo4GEO(SharedINFO4GEO[rankind]);
    }
} //至此得到了几何元素在各个进程中的共享信息

//功能: GEO4Elems和GEO2Ranks来得到LocalGEO4Elems
// GEO4Elems: 表示每个单元上几何元素的原始编号信息,Elem4Ranks表示每个单元所在的进程信息,GEO2Ranks表示每个几何元素所在的进程信息,
// LocalGEOIndex2Ranks表示每个几何元素的局部编号信息(与GEO2Ranks一样的位置结构),
// LocalGEO4Elems表示每个进程上每个单元几何元素的局部编号信息, 第一层指标是进程号, 即表示一个进程是一个PTW
//如何得到Elem4Ranks的信息？？？
void GetLocalGEO4Elems(PTW *GEO4Elems, PTW *Elem4Ranks, PTW *GEO2Ranks,
                       PTW **LocalGEO4Elems)
{
    INT NumRanks = Elem4Ranks->NumRows;
    INT NumELems = GEO4Elems->NumRows;
    INT NumGeos = GEO2Ranks->NumRows;
    //LocalGEO4Elems = malloc(NumRanks * sizeof(PTW *)); //每个进程对应一个PTW*的对象
    INT *LocalGEOIndex2Ranks = GEO2Ranks->AndEntries;  //取出每个几何元素在每个进程上的局部编号
    INT rankind, i, j, k, num_elem, start, end, start2, start3, start4, end4,
        elemind, geoind, length;
    //for (k = 0; k < NumRanks; k++)
    //    LocalGEO4Elems[k] = malloc(sizeof(PTW)); //每个进程申请空间    
    PTW *localgeo4elems;                         //用来临时指向每个进程的输出变量
    for (rankind = 0; rankind < NumRanks; rankind++)
    { //处理第rankind个进程
        start = Elem4Ranks->Ptw[rankind];
        end = Elem4Ranks->Ptw[rankind + 1];
        num_elem = end - start;             //该进程上的单元个数
        localgeo4elems = LocalGEO4Elems[rankind]; //指向目前进程中的对象
        localgeo4elems->NumRows = num_elem; //单元的个数
        localgeo4elems->Ptw = malloc((num_elem + 1) * sizeof(INT));
        memset(localgeo4elems->Ptw, 0, (num_elem + 1) * sizeof(INT));        
        for (k = 0; k < num_elem; k++)
        {
            elemind = Elem4Ranks->Entries[start + k]; //得到单元的全局编号
            localgeo4elems->Ptw[k + 1] = GEO4Elems->Ptw[elemind + 1] - GEO4Elems->Ptw[elemind];
        }
        localgeo4elems->Ptw[0] = 0;
        for (k = 0; k < num_elem; k++)
        {
            localgeo4elems->Ptw[k + 1] += localgeo4elems->Ptw[k];
        }
        //具体给局部编号
        localgeo4elems->Entries = malloc(localgeo4elems->Ptw[num_elem] * sizeof(INT));
        for (k = 0; k < num_elem; k++)
        {
            elemind = Elem4Ranks->Entries[start + k];     //得到单元的全局编号
            start2 = GEO4Elems->Ptw[elemind];             //得到该单元上几何元素的全局编号起始位置
            start3 = localgeo4elems->Ptw[k];              //得到目标数组的起始位置
            length = localgeo4elems->Ptw[k + 1] - start3; //目前单元上的几何元素的个数
            for (j = 0; j < length; j++)
            {                                            //目前单元的第j个几何元素
                geoind = GEO4Elems->Entries[start2 + j]; //得到几何元素的全局编号
                //去寻找相应的局部编号
                start4 = GEO2Ranks->Ptw[geoind];
                end4 = GEO2Ranks->Ptw[geoind + 1];
                for (i = start4; i < end4; i++)
                { //搜索相应的位置
                    if (GEO2Ranks->Entries[i] == rankind)
                    { //进行赋值
                        localgeo4elems->Entries[start3 + j] = LocalGEOIndex2Ranks[i];
                    } // end if
                }// end for i
            }// end for j
        }// end for k
    }// end for rankind
    //printf("ZZZZZZZZZZ\n");
} // end for this program

//根据得到的共享几何元素的信息来生成针对每个邻居进程的共享信息
// SharedInfor4GEO: 记录了本进程中针对每个共享几何元素的信息
//输出: SharedGEO4Ranks: 记录了针对每个邻居进程的共享元素信息
void ColletSharedGEO4Ranks(SHAREDINFO4GEO *SharedInfo4GEO, INT myrank, SHAREDGEO4RANK *SharedGEO4Ranks)
{
    INT SharedNumGeos = SharedInfo4GEO->SharedNum; //得到共享元素的个数
    INT *LocalIndex = SharedInfo4GEO->LocalIndex;
    INT *Ptw0 = SharedInfo4GEO->Ptw;
    INT *SharedRanks0 = SharedInfo4GEO->SharedRanks;
    INT *CoarseNeighRanks = malloc(Ptw0[SharedNumGeos] * sizeof(INT));
    memset(CoarseNeighRanks, -1, Ptw0[SharedNumGeos] * sizeof(INT));
    // INT *CoarsePtw4 = malloc((Ptw2[SharedNumGeos]+1)*sizeof(INT));
    // memset(CoarsePtw4,0,Ptw2[SharedNumGeos]*sizeof(INT));
    INT *GEOOwners = SharedInfo4GEO->Owners;
    INT *Owner4Ranks = SharedGEO4Ranks->Owners;
    INT posind, pos4, pos, localind, flag, i, j, k, rankind, start0, end0, owner, localindex;
    //首先得统计处相应的邻居进程的个数(有什么好方法吗？)
    INT SharedNumRanks = 0;
    for (k = 0; k < SharedNumGeos; k++)
    { //第k个几何元素
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1]; //得到起始和终点位置
        for (j = start0; j < end0; j++)
        {                              //处理第k个共享元素
            rankind = SharedRanks0[j]; //得到此处的进程号
            if (rankind != myrank)
            { //表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                {
                    flag = 0; //用来表示是否已经存储了目前的进程
                    if (CoarseNeighRanks[i] == rankind)
                    {
                        flag = 1;
                        i = SharedNumRanks + 1; //跳出循环
                    }                           // end if(CoarseNeighRanks[i]==rankind)
                }                               // end for i
                if (flag == 0)
                { //得到了一个新的进程号
                    CoarseNeighRanks[SharedNumRanks] = rankind;
                    SharedNumRanks++;
                } // end if flag==0
            }     // end if(rankind!=myrank)
        }         // end for j
    }             // end for k 得到了进程个数:SharedNumRanks
    SharedGEO4Ranks->SharedNumRanks = SharedNumRanks;
    INT *SharedRanks = SharedGEO4Ranks->SharedRanks;
    SharedRanks = malloc(SharedNumRanks * sizeof(INT));
    INT *Ptw = SharedGEO4Ranks->Ptw;
    Ptw = malloc((SharedNumRanks + 1) * sizeof(INT));
    memset(Ptw, 0, (SharedNumRanks + 1) * sizeof(INT));
    for (k = 0; k < SharedNumRanks; k++)
    {
        SharedRanks[k] = CoarseNeighRanks[k]; //得到邻居进程号
    }
    free(CoarseNeighRanks); // shif
    for (k = 0; k < SharedNumGeos; k++)
    { //第k个共享几何元素
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1]; //得到该几何元素的起始和终点位置
        for (j = start0; j < end0; j++)
        {                              //处理第k个共享元素
            rankind = SharedRanks0[j]; //得到此处的进程号
            if (rankind != myrank)
            { //表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                { //寻找该进程的位置
                    if (SharedRanks[i] == rankind)
                    {                           //找到了进程所在的位置
                        Ptw[i + 1]++;           //将相应的位置加1
                        i = SharedNumRanks + 1; //跳出循环
                    }                           // end if(CoarseNeighRanks[i]==rankind)
                }                               // end for i
            }                                   // end for if
        }                                       // end for j
    }                                           // end for k 得到了每个进程中共享元素的个数
    Ptw[0] = 0;
    for (k = 0; k < SharedNumRanks; k++)
        Ptw[k + 1] += Ptw[k];
    INT *SharedIndex = SharedGEO4Ranks->SharedIndex;
    INT *Index = SharedGEO4Ranks->Index;
    INT *Owners = SharedGEO4Ranks->Owners;
    SharedIndex = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    Index = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    Owners = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    INT *RankPos = malloc(SharedNumRanks * sizeof(INT));
    memset(RankPos, 0, SharedNumRanks * sizeof(INT));
    //下面来进行赋值
    for (k = 0; k < SharedNumGeos; k++)
    {
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1];   //得到该几何元素的起始和终点位置
        owner = GEOOwners[k]; //获得本元素的宿主进程
        for (j = start0; j < end0; j++)
        {
            rankind = SharedRanks0[j];
            if (rankind == myrank)
            {
                localindex = LocalIndex[j]; //找到了该元素在本进程的编号
            }                               // end if
        }                                   // end for j
        for (j = start0; j < end0; j++)
        {                              //处理第k个共享元素
            rankind = SharedRanks0[j]; //得到此处的进程号
            if (rankind != myrank)
            { //表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                { //寻找该进程的位置
                    if (SharedRanks[i] == rankind)
                    { //找到了进程所在的位置i
                        pos = Ptw[i] + RankPos[i];
                        SharedIndex[pos] = LocalIndex[j]; //记录邻居进程中的编号
                        Index[pos] = localindex;          //记录本进程中的编号
                        Owner4Ranks[pos] = owner;
                        RankPos[i]++; //该进程的位置加1
                    }                 // end if
                }                     // end for i
            }                         // end if(rankind!=myrank)
        }                             // end fof j
    }                                 // end for k
    free(RankPos);
} // end for this program

//打印出PTW中的内容
void OutPutPTW(PTW *ptw)
{
    INT k, NumRows = ptw->NumRows, j, start, end;
    OpenPFEM_Print("NumRows=%d,  NumCols=%d\n", ptw->NumRows, ptw->NumCols);
    for (k = 0; k < NumRows; k++)
    {
        OpenPFEM_Print("The %d-th row\n", k);
        start = ptw->Ptw[k];
        end = ptw->Ptw[k + 1];
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->Entries[j]);
        }
        OpenPFEM_Print("\n");
    }
}

void OutPutPTW2(PTW *ptw)
{
    INT k, NumRows = ptw->NumRows, j, start, end;
    OpenPFEM_Print("NumRows=%d,  NumCols=%d\n", ptw->NumRows, ptw->NumCols);
    OpenPFEM_Print("The [first etries, second entries]:\n");
    for (k = 0; k < NumRows; k++)
    {
        OpenPFEM_Print("The %d-th row\n", k);
        start = ptw->Ptw[k];
        end = ptw->Ptw[k + 1];
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->Entries[j]);
        }
        OpenPFEM_Print("\n");
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->AndEntries[j]);
        }
        OpenPFEM_Print("\n");
    }
    // OpenPFEM_Print("The second entries:\n");
    // for(k=0;k<NumRows;k++){
    //     OpenPFEM_Print("The %d-th row\n", k);
    //     start = ptw->Ptw[k]; end = ptw->Ptw[k+1];
    //     for(j=start;j<end;j++){
    //         OpenPFEM_Print("%d,  ", ptw->AndEntries[j]);
    //     }
    //     OpenPFEM_Print("\n");
    // }
}
//====
void OutPutSharedInfo4GEO(SHAREDINFO4GEO *SharedInfo4GEO)
{

    INT k, SharedNum = SharedInfo4GEO->SharedNum, start, end, j;
    OpenPFEM_Print("SharedNum: %d\n Owners: \n", SharedNum);
    for (k = 0; k < SharedNum; k++)
    {
        OpenPFEM_Print("%d,  ", SharedInfo4GEO->Owners[k]);
    }
    OpenPFEM_Print("\n");
    INT *Ptw = SharedInfo4GEO->Ptw, *SharedRanks = SharedInfo4GEO->SharedRanks, *LocalIndex = SharedInfo4GEO->LocalIndex;
    OpenPFEM_Print("The SharedRanks and Local index: \n");
    for (k = 0; k < SharedNum; k++)
    {
        start = Ptw[k];
        end = Ptw[k + 1];
        OpenPFEM_Print("The %d-th row\n", k);
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d, ", SharedRanks[j]);
        }
        OpenPFEM_Print("\n");
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d, ", LocalIndex[j]);
        }
        OpenPFEM_Print("\n");
    }
    // OpenPFEM_Print("The LocalIndex: \n");
    // for(k=0;k<SharedNum;k++){
    //     start = Ptw[k];   end = Ptw[k+1];
    //     OpenPFEM_Print("The %d-th row\n",k);
    //     for(j=start;j<end;j++){
    //         OpenPFEM_Print("%d, ", LocalIndex[j]);
    //     }
    //     OpenPFEM_Print("\n");
    // }
}
```