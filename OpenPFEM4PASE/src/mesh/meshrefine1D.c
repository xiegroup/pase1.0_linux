#include "mesh.h"
#include "constants.h"
#include "meshcomm.h"

#define PRINT_INFO 0
//INT time = 0;
static INT size, rank;
//下面是对一维网格的一致加密
void MeshUniformRefine1D(MESH *mesh)
{
    if (mesh->Verts == NULL || mesh->Lines == NULL)
    {
        return;
    }
    //开始加密
    INT i, num_verts, num_lines; 
    INT New_verts, New_lines; 
    //获得局部的点、线、面的个数
    num_verts = mesh->num_vert;
    num_lines = mesh->num_line;
    //计算出新的点、线的个数
    New_verts = num_verts + num_lines;
    New_lines = 2 * num_lines;
    //为新的点、线建立相应的存储空间
    VertsReCreate(&(mesh->Verts), num_verts, New_verts, mesh->worlddim);
    LinesReCreate(&(mesh->Lines), num_lines, New_lines);
    //产生父亲单元的信息
    if (!mesh->Fathers)
    {
        mesh->Fathers = malloc(New_lines * sizeof(int));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, New_lines * sizeof(int));
    }
    INT *Fathers = mesh->Fathers;
    VERT *Verts = mesh->Verts, *vert, *vert_s, *vert_e;
    LINE *Lines = mesh->Lines, *line, *line_old;
    // renew the datas of the mesh
    mesh->num_vert = New_verts;
    mesh->num_line = New_lines;

    //产生新的点的数据
    for (i = 0; i < num_lines; i++)
    {
        vert = &(Verts[num_verts + i]);  //加密得到新的节点的指针
        line = &(Lines[i]);
        vert_s = &(Verts[line->Vert4Line[0]]);
        vert_e = &(Verts[line->Vert4Line[1]]);
        //计算新节点的坐标
        vert->Coord[0] = 0.5 * (vert_s->Coord[0] + vert_e->Coord[0]);
        //加密得到的点都是内点
        vert->BD_ID = 0;
        //产生新节点的编号
        vert->Index = num_verts + i;
    }  
    //然后处理原来的线(一分为二)
    for (i = 0; i < num_lines; i++)
    {
        line_old = &(Lines[i]);
        line = &(Lines[num_lines + i]);
        line->Vert4Line[0] = num_verts + i;
        line->Vert4Line[1] = line_old->Vert4Line[1];
        //line->BD_ID = line_old->BD_ID;    //一维的时候不需要处理边界信息
        line->Index = num_lines + line_old->Index;
        line_old->Vert4Line[1] = num_verts + i;
    }
    //一维网格加密之后, 共享的点的信息不会改变，所以不需要进行共享信息的更新
} // end of UniformRefineMesh1D
//=========================================================================
// 下面是自适应加密的主程序
// 参数说明: mesh: 输入的网格, ElemErrors： 后验误差估计子，即每个单元上都有一个值来表示该单元上的误差
// theta: 自适应加密系数，与自适应迭代算法速度相关的参数
void MeshAdaptiveRefine1D(MESH *mesh, DOUBLE *ElemErrors, DOUBLE theta)
{
#if MPI_USE
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif
    INT num_line = mesh->num_line, k, lineind;
    //-1表示没有单元编号, 大于等于零表示需要加密的单元编号
    INT *RefineElems, NumRefines;
    // RefineElems 存储需要加密的单元编号
    RefineElems = malloc(num_line * sizeof(INT));
    memset(RefineElems, -1, num_line * sizeof(INT));
    //下面调用刘昊宸写的确定加密单元的程序来确定RefineElems集合
    FindRefineElem(mesh, ElemErrors, theta, &NumRefines, RefineElems);
    //一维的自适应加密只需要把需要加密的区间直接添加新的点即可
    // NumRefines: 表示需要加密边的个数
    INT endline = NumRefines; //表示需要加密线的个数
    INT newnum_line = mesh->num_line + endline;  //加密之后的线的个数
    INT newnum_vert = mesh->num_vert + endline;  //加密之后节点的个数
    VertsReCreate(&(mesh->Verts), mesh->num_vert, newnum_vert, mesh->worlddim);
    LinesReCreate(&(mesh->Lines), mesh->num_line, newnum_line);
    mesh->num_vert = newnum_vert;
    mesh->num_line = newnum_line;

    //定义保存父亲单元信息的对象    
    if (mesh->Fathers == NULL)
    {
        mesh->Fathers = malloc(newnum_line * sizeof(INT));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, newnum_line * sizeof(INT));
    }
    memset(mesh->Fathers, -1, newnum_line*sizeof(INT));
    for(k=0;k<num_line;k++)
    {
        mesh->Fathers[k] = k;
    }
    //保存祖先单元的信息, 主要用于扩展子空间算法的实现
    if (mesh->Ancestors == NULL)
    {
        mesh->Ancestors = malloc(newnum_line * sizeof(INT)); //首次建立祖先单元的信息
        for (k = 0; k < num_line; k++)
            mesh->Ancestors[k] = k;
    }
    else
    {    
        mesh->Ancestors = realloc(mesh->Ancestors, newnum_line * sizeof(INT));
    }
    INT newlineind, newvertind;
    //下面定义一些量来标记目前新的点、线的编号
    newlineind = mesh->num_line;
    newvertind = mesh->num_vert;
   //先处理所有需要加密的线, 进行线加密得到新的点，这个过程是确定的
    INT newindices[2];
    newindices[0] = newlineind; //记录目前最新线的编号
    newindices[1] = newvertind; //记录目前最新点的编号
    INT refineind;
    LINE *Lines = mesh->Lines, line;
    VERT *Verts = mesh->Verts, vert0, vert1;
    //先对现有网格上标记为需要加密的线进行加密，需要加密的线其实是已经都被加密了
    for (refineind = 0; refineind < endline; refineind++)
    {
        lineind = RefineElems[k];  //获得目前需要加密的线的编号
        line = Lines[lineind];  //需要加密的线
        //线得到目前线的两个节点
        vert0 = Verts[line.Vert4Line[0]];
        vert1 = Verts[line.Vert4Line[1]];
        //设置新的节点
        Verts[newvertind].Coord[0] = 0.5*(vert0.Coord[0] + vert1.Coord[0]);
        Verts[newvertind].BD_ID = 0; 
        Verts[newvertind].Index = newvertind;
        //生成新的线
        Lines[newlineind].Vert4Line[0] = newvertind;
        Lines[newlineind].Vert4Line[1] = line.Vert4Line[1];
        Lines[lineind].Vert4Line[1] = newvertind;
        newvertind ++ ;
        newlineind ++;
    }   
    //释放本程序中申请的内存
    free(RefineElems);
} // end 网格自适应加密完毕