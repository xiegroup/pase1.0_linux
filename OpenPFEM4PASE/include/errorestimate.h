#ifndef __ERRORESTIMATE__
#define __ERRORESTIMATE__
#include "constants.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "femfunction.h"
#include "quadrature.h"
#include "discreteform.h"
#include "assemble.h"
#include <math.h>
// 本文件包含各种有关误差估计的函数和操作
DOUBLE ErrorEstimate(FEMFUNCTION *femfun, INT NumMultiIndex, MULTIINDEX *MultiIndex, FUNCTIONVEC *fun, ERRORFORM *ErrorForm, QUADRATURE *Quadrature);
void FindEdgeCoord(MESH *mesh, INT ind, INT eind, DOUBLE *EdgeCoord);
// 后验误差估计
void PosterioriErrorEstimate(FEMFUNCTION *femfun, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
                             FUNCTIONVEC *rhs, ERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
                             QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError);
// 特征值问题的后验误差估计
void EigenPosterioriErrorEstimate(FEMFUNCTION *femfun, DOUBLE *eigenvalue, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
                                  EIGENERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
                                  QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError);
// void EigenPosterioriErrorEstimate_new(FEMFUNCTION *femfun, DOUBLE *eigenvalue, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
//                                   EIGENERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
//                                   QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError);
void ReorderFEMValuesForCurlElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *FEMValues);
void ReorderFEMValuesForDivElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *FEMValues);
#endif