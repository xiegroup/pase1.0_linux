/*
 * =====================================================================================
 *
 *       Filename:  D_T_P0_2D.h
 *
 *    Description:  define the base functions in 2D case
 *
 *        Version:  1.0
 *        Created:  2021/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   hhxie@lsec.cc.ac.cn
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __DTP02D__
#define __DTP02D__

#include <stdbool.h>
#include "enumerations.h"
#include "constants.h"
#include<stdio.h>
#include<stdlib.h>
// ***********************************************************************
// P1 element, discontinuouis, 2D
// ***********************************************************************
static int D_T_P0_2D_dof[3] = {0,0,1}; //所有的自由度都是在单元内部
static int D_T_P0_2D_Num_Bas = 1;
static int D_T_P0_2D_Value_Dim = 1;
static int D_T_P0_2D_Inter_Dim = 1;
static int D_T_P0_2D_Polydeg = 0;
static bool D_T_P0_2D_IsOnlyDependOnRefCoord = 1;
static int D_T_P0_2D_Accuracy = 1;
static MAPTYPE D_T_P0_2D_Maptype = Affine;

static void D_T_P0_2D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{ 
  values[0]=1.0;
}

// base function values
static void D_T_P0_2D_D00(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{ 
  values[0]=1.0;
}

// values of the derivatives in xi direction
static void D_T_P0_2D_D10(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]= 0.0;
}

// values of the derivatives in eta direction
static void D_T_P0_2D_D01(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]= 0.0;  
}
// values of the derivatives in xi-xi  direction
static void D_T_P0_2D_D20(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]=0.0;
}
// values of the derivatives in xi-eta direction
static void D_T_P0_2D_D11(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]=0.0;
}
// values of the derivatives in eta-eta direction
static void D_T_P0_2D_D02(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]=0.0;
}
// The Nodal functional definition for the interpolation
static void D_T_P0_2D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  double coord[2];
  int i;
  coord[0] = 0.0;
  coord[1] = 0.0;
  for(i=0;i<3;i++)
  {
      coord[0] += elem->Vert_X[i]; 
      coord[1] += elem->Vert_Y[i];
  }
  coord[0] *= 1.0/3.0;
  coord[1] *= 1.0/3.0;
  fun(coord, dim, values);
}
#endif