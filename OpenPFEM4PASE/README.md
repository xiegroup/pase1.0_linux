# OpenPFEM1.0

Open Parallel Finite Element Method Solver 1.0

---

`CMakeLists.txt` will create a shared library as `./shared/lib/libopenpfem.so` and copy all header files needed to `./shared/include`