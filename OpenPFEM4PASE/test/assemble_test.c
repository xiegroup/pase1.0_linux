#include "assemble.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void stiffmatrix_long(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
    // value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}

void stiffmatrix_new(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

// void stiffmatrix_aux(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
// {
//     value[0] = AuxFEMValues[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
// }

void rhsvec_long(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // value[0] = (3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
    value[0] = (1.0 + 3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
}

void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // value[0] = (3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
    value[0] = (1.0 + 3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]) - AuxFEMValues[0] * right[0];
}

void rhsvec_new(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * right[0];
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}

void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}

void ErrFun3DL2(DOUBLE *femvalue, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvalue[0] - Funvalue[0]) * (femvalue[0] - Funvalue[0]);
}

void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}

void PrintFathes(MESH *mesh)
{
    INT i;
    if (mesh->Fathers == NULL)
    {
        OpenPFEM_Print("网格没有fathers\n");
    }
    else
    {
        // 输出网格的fathers
        for (i = 0; i < mesh->num_volu; i++)
        {
            OpenPFEM_Print("网格单元%d的father为%d的\n", i, mesh->Fathers[i]);
        }
    }
}

void PrintAncestors(MESH *mesh)
{
    INT i;
    if (mesh->Fathers == NULL)
    {
        OpenPFEM_Print("网格没有ancestors\n");
    }
    else
    {
        // 输出网格的ancestors
        for (i = 0; i < mesh->num_volu; i++)
        {
            OpenPFEM_Print("网格单元%d的ancestor为%d的\n", i, mesh->Ancestors[i]);
        }
    }
}

void PrintFathersAndAncestors(MESH *mesh)
{
    INT i;
    // 输出网格的ancestors
    for (i = 0; i < mesh->num_volu; i++)
    {
        OpenPFEM_Print("网格单元%d的father为%d的\n", i, mesh->Fathers[i]);
        OpenPFEM_Print("网格单元%d的ancestor为%d的\n", i, mesh->Ancestors[i]);
        if (mesh->Fathers[i] != mesh->Ancestors[i])
            printf("not same!!!\n");
    }
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);
    // 将当前网格设置为祖先网格
    MeshSetAsAncestor(mesh);
    // 粗网格
    MESH *meshH = MeshDuplicate(mesh);
    // ComputeInvJacobiOfAllElem(meshH);
    // 细网格
    MeshUniformRefine(mesh, 1);
    /** 输出mesh的father和ancestor */
    // OpenPFEM_Print("==========加密二次后的网格==========\n");
    // // PrintFathes(mesh);
    // // PrintAncestors(mesh);
    // PrintFathersAndAncestors(mesh);
    OpenPFEM_Print("=========finish build mesh========\n");

    FEMSPACE *femspaceH = FEMSpaceBuild(meshH, C_T_P1_3D, BoundCond);
    // printf("%d\n", femspaceH->Base->IsOnlyDependOnRefCoord);
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P1_3D, BoundCond);
    // FEMSPACE *femspace = FEMSpaceBuild(meshH, C_T_P3_3D, BoundCond);
    
    /* TEST PROLONG MATRIX*/
    MATRIX *IhH_test = NULL;
    // VECTOR *Rhs = NULL;
    ProlongMatrixAssemble(&IhH_test, femspaceH, femspace, TYPE_PETSC);
    
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    MULTIINDEX stiffLmi[4] = {D000, D100, D010, D001}, stiffRmi[4] = {D000, D100, D010, D001};

    MULTIINDEX L2ErrorIndex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    // 在做校正步时 把解带入到右端时需要的信息
    INT NAuxFEMFunMultiIndex[1] = {1};
    MULTIINDEX auxfemfunmultiIndex[1] = {D000};

    DOUBLE starttime, endtime;
    DOUBLE H1Error = 0.0, L2Error = 0.0;

    // 粗网格计算
    OpenPFEM_Print("=========femspaceH========\n");
    DISCRETEFORM *StiffLinear_DF = DiscreteFormBuild(femspaceH, 4, stiffLmi, femspaceH, 4, stiffRmi,
                                                     stiffmatrix_long, rhsvec_long, BoundFun, Quadrature);
    BASE *base = StiffLinear_DF->LeftSpace->Base;
    starttime = MPI_Wtime();
    MATRIX *StiffMatrix = NULL;
    VECTOR *Rhs_long = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs_long, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);

    VECTOR *solution = NULL;
    VectorCreateByMatrix(&solution, StiffMatrix);

    OpenPFEM_Print("=========solve========\n");
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver, PETSC_KSPCG);
    LinearSolve(solver, StiffMatrix, Rhs_long, solution);
    FEMFUNCTION *FemSol = FEMFunctionBuild(femspaceH, 1);
    VectorGetFEMFunction(solution, FemSol, 0);
    L2Error = ErrorEstimate(FemSol, 1, &L2ErrorIndex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    H1Error = ErrorEstimate(FemSol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    MatrixDestroy(&StiffMatrix);
    // VectorDestroy(&Rhs_long);
    VectorDestroy(&solution);
    FEMFunctionDestroy(&FemSol);
    DiscreteFormDestroy(&StiffLinear_DF);
    LinearSolverDestroy(&solver);
    OpenPFEM_Print("=========end femspaceH========\n");
    // 细网格计算
    OpenPFEM_Print("=========femspaceh========\n");
    StiffLinear_DF = DiscreteFormBuild(femspace, 4, stiffLmi, femspace, 4, stiffRmi,
                                       stiffmatrix_long, rhsvec_long, BoundFun, Quadrature);
    starttime = MPI_Wtime();
    StiffMatrix = NULL;
    VECTOR *Rhs = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);

    VECTOR *solution_h = NULL;
    VectorCreateByMatrix(&solution_h, StiffMatrix);

    OpenPFEM_Print("=========solve========\n");
    solver = NULL;
    LinearSolverCreate(&solver, PETSC_KSPCG);
    LinearSolve(solver, StiffMatrix, Rhs, solution_h);
    FEMFUNCTION *FemSol_aux = FEMFunctionBuild(femspace, 1);
    VectorGetFEMFunction(solution_h, FemSol_aux, 0);
    L2Error = ErrorEstimate(FemSol_aux, 1, &L2ErrorIndex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    H1Error = ErrorEstimate(FemSol_aux, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    MatrixDestroy(&StiffMatrix);
    VectorDestroy(&Rhs);
    // VectorDestroy(&solution_h);
    // FEMFuntionDestroy(&FemSol_aux);
    DiscreteFormDestroy(&StiffLinear_DF);
    LinearSolverDestroy(&solver);
    OpenPFEM_Print("=========end femspace========\n");
    MPI_Barrier(MPI_COMM_WORLD);

    // 生成非线性项
    // FEMSPACE *femspaceH = BuildFEMSpace(meshH, C_T_P2_3D, BoundCond);
    AUXFEMFUNCTION *rhs_upper = AuxFEMFunctionBuild(1, &(FemSol_aux), &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
    StiffLinear_DF = DiscreteFormAuxFeFunBuild(femspaceH, 4, stiffLmi, femspaceH, 4, stiffRmi,
                                               rhs_upper, stiffmatrix, rhsvec, BoundFun, Quadrature);
    OpenPFEM_Print("=========femspaceAux========\n");
    starttime = MPI_Wtime();
    StiffMatrix = NULL;
    Rhs = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);

    solution = NULL;
    VectorCreateByMatrix(&solution, StiffMatrix);

    OpenPFEM_Print("=========solve========\n");
    solver = NULL;
    LinearSolverCreate(&solver, PETSC_KSPCG);
    LinearSolve(solver, StiffMatrix, Rhs, solution);
    FemSol = FEMFunctionBuild(femspaceH, 1);
    VectorGetFEMFunction(solution, FemSol, 0);
    L2Error = ErrorEstimate(FemSol, 1, &L2ErrorIndex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    H1Error = ErrorEstimate(FemSol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    MatrixDestroy(&StiffMatrix);
    VectorDestroy(&Rhs);
    VectorDestroy(&solution);
    FEMFunctionDestroy(&FemSol);
    DiscreteFormDestroy(&StiffLinear_DF);
    LinearSolverDestroy(&solver);
    OpenPFEM_Print("=========end femspace========\n");

    // test 用Aux的方式组装无Aux的问题 看看结果是否相同
    rhs_upper = AuxFEMFunctionBuild(1, &(FemSol_aux), &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
    StiffLinear_DF = DiscreteFormAuxFeFunBuild(femspaceH, 4, stiffLmi, femspaceH, 4, stiffRmi,
                                               rhs_upper, stiffmatrix_long, rhsvec_long, BoundFun, Quadrature);
    OpenPFEM_Print("=========femspace Aux format linear probs========\n");
    starttime = MPI_Wtime();
    StiffMatrix = NULL;
    Rhs = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);

    solution = NULL;
    VectorCreateByMatrix(&solution, StiffMatrix);

    OpenPFEM_Print("=========solve========\n");
    solver = NULL;
    LinearSolverCreate(&solver, PETSC_KSPCG);
    LinearSolve(solver, StiffMatrix, Rhs, solution);
    FemSol = FEMFunctionBuild(femspaceH, 1);
    VectorGetFEMFunction(solution, FemSol, 0);
    L2Error = ErrorEstimate(FemSol, 1, &L2ErrorIndex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    H1Error = ErrorEstimate(FemSol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    MatrixDestroy(&StiffMatrix);
    VectorDestroy(&Rhs);
    VectorDestroy(&solution);
    FEMFunctionDestroy(&FemSol);
    DiscreteFormDestroy(&StiffLinear_DF);
    LinearSolverDestroy(&solver);
    OpenPFEM_Print("=========end femspace========\n");

    // test  直接组装(u_h^{old},V_H)
    rhs_upper = AuxFEMFunctionBuild(1, &(FemSol_aux), &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
    StiffLinear_DF = DiscreteFormAuxFeFunBuild(femspaceH, 4, stiffLmi, femspaceH, 4, stiffRmi,
                                               rhs_upper, stiffmatrix_long, rhsvec_new, BoundFun, Quadrature);
    OpenPFEM_Print("=========femspace Rhs with Aux========\n");
    starttime = MPI_Wtime();
    StiffMatrix = NULL;
    VECTOR *Rhs_Aux = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs_Aux, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);

    // VectorPrint(Rhs_Aux, 0);

    DiscreteFormDestroy(&StiffLinear_DF);

    // 上述保留下的信息有
    // 1. Rhs_long 粗网格上计算出的 (f,v)
    // 2. Rhs 粗网格上计算出的 (f,v)-(u^{old},v) 其中u^{old}在细网格上
    // 3. solution_h u^{old}在细网格上的系数
    // 还需要生成的信息有
    // 4. 细网格上组装 (u,v)
    // 5. 插值矩阵 I_h^H
    StiffLinear_DF = DiscreteFormBuild(femspace, 4, stiffLmi, femspace, 4, stiffRmi,
                                       stiffmatrix_new, NULL, BoundFun, Quadrature);
    OpenPFEM_Print("=========test new========\n");

    starttime = MPI_Wtime();
    MATRIX *Ahh = NULL;
    // VECTOR *Rhs = NULL;
    MatrixAssemble(&Ahh, NULL, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix Ahh: %2.10f\n", endtime - starttime);

    starttime = MPI_Wtime();
    MATRIX *IhH = NULL;
    // VECTOR *Rhs = NULL;
    ProlongMatrixAssemble(&IhH, femspaceH, femspace, TYPE_PETSC);
    // MatrixAssemble(&Ahh, NULL, StiffLinear_DF, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix IhH: %2.10f\n", endtime - starttime);

    // VectorPrint(solution_h, 0);
    VECTOR *beta = NULL; // beta=Ahh*solution_h
    VectorCreateByMatrix(&beta, Ahh);
    // VectorPrint(beta, 0);
    MatrixVectorMult(Ahh, solution_h, beta);
    VECTOR *gamma = NULL; // gamma=IhH*beta
    VectorCreateByMatrixTranspose(&gamma, IhH);
    // VectorPrint(gamma, 0);
    // VectorCreateByMatrix(&gamma, IhH);
    MatrixTransposeVectorMult(IhH, beta, gamma);
    // MatrixVectorMult(IhH, beta, gamma);
    // VectorConvert(gamma, TYPE_OPENPFEM, 0);
    // VectorPrint(gamma, 0);
    // VectorConvert(Rhs_Aux, TYPE_OPENPFEM, 0);
    // VectorPrint(Rhs_Aux, 0);
    VectorAxpby(1.0, gamma, -1.0, Rhs_Aux);
    // VectorAxpby(1.0, Rhs, 1.0, Rhs_long);
    // VectorConvert(Rhs_Aux, TYPE_OPENPFEM, 0);
    // VectorPrint(Rhs_Aux, 0);
    // VectorOutput(Rhs_Aux, "b.dat");
    printf("Rhs_Aux存储信息 OpenPFEM %d petsc %d \n", Rhs_Aux->if_vector, Rhs_Aux->if_petsc);
    // if (Rhs_Aux->data != NULL)
    // {
    //     printf("test OpenPFEM %2.10f\n", Rhs_Aux->data[2]);
    // }
    // else
    // {
    //     printf("no data\n");
    // }
    // if (Rhs_Aux->data_petsc != NULL)
    // {
    //     printf("test petsc %2.10f\n", ((DOUBLE *)Rhs_Aux->data_petsc)[2]);
    // }
    // else
    // {
    //     printf("no data_petsc\n");
    // }
    // VectorPrint(Rhs_Aux, 0);
    DOUBLE norm;
    VectorNorm(Rhs_Aux, &norm);
    printf("norm %2.10f\n", norm);

    OpenPFEM_Finalize();
    return 0;
}
