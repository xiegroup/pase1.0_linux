#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"
#include "multilevelmesh.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE AddBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[2], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu2D(double X[2], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]);
}
void ExactGrad2D(double X[2], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]);
}
void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2];
}
void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0];
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0] + left[1] * right[1] + left[2] * right[2];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (2.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]);
    // value[0] = right[0];
}
void ErrFun2DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun2DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 2.0 * PI * PI * sin(PI * coord[0]) * sin(PI * coord[1]);
    // value[0] = 1.0;
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[1] + femvalue[2]) *
               (rhsvalue[0] + femvalue[1] + femvalue[2]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 2, DEFAULT_COMM);
    // BuildMesh(mesh, "../data/data_LShape.txt", MATLAB, TRIANGLE);
    MeshBuild(mesh, "../data/data_simple.txt", MATLAB, TRIANGLE);
    
    UniformRefineMesh(mesh, 1);
    printf("1111111\n");
    MPI_Barrier(MPI_COMM_WORLD);
    MeshPartition(mesh);
    printf("2222222\n");
    MPI_Barrier(MPI_COMM_WORLD);
    UniformRefineMesh(mesh, 0);
    printf("3333333\n");
    MPI_Barrier(MPI_COMM_WORLD);

    // 积分微分信息
    QUADRATURE *Quadrature = BuildQuadrature(QuadTriangle13);
    QUADRATURE *QuadratureEdge = BuildQuadrature(QuadLine4);
    MULTIINDEX stiffLmultiindex[3] = {D00, D10, D01};
    MULTIINDEX stiffRmultiindex[3] = {D00, D10, D01};
    MULTIINDEX auxfemfunmultiIndex[1] = {D00};
    MULTIINDEX L2multiindex[1] = {D00};
    MULTIINDEX H1ErrorIndex[2] = {D10, D01};
    MULTIINDEX ErrorIndex[1] = {D00};
    MULTIINDEX PosterioriErrorIndex[5] = {D00, D20, D02, D10, D01};
    INT NAuxFEMFunMultiIndex[1] = {1};

    // 加密
    INT ind_refine;
    INT max_refine = 5;
    INT ind_SCF;
    INT max_SCF = 10;
    INT NUMVOLU;
    INT conv_ind = 0;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *L2ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE Error = 0.0;
    DOUBLE Tol = 1e-12;
    DOUBLE TotalError;
    DOUBLE H1Error = 0.0, L2Error = 0.0;
    FEMSPACE *femspace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL;
    FEMSPACE *addfemspace = NULL;
    AUXFEMFUNCTION *outerauxfemfunc = NULL;
    MATRIX *StiffMatrix = NULL;
    FEMFUNCTION *solution = NULL;
    DOUBLE *PosteriorError = NULL;
    DOUBLE time1, time2;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        //计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_face), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;

        //针对该层网格生成有限元空间和线性部分的离散变分形式
        femspace = BuildFEMSpace(mesh, C_T_CR_2D, BoundCond);
        StiffDiscreteForm = BuildDiscreteForm(femspace, 3, &stiffLmultiindex[0], femspace, 3, &stiffRmultiindex[0],
                                              stiffmatrixlinear, rhsvec, BoundFun, Quadrature);
        
        time1 = GetTime();
    printf("3333333\n");
    MPI_Barrier(MPI_COMM_WORLD);
        StiffMatrix = AssembleMatrix(StiffDiscreteForm);
        
    printf("3333333\n");
    MPI_Barrier(MPI_COMM_WORLD);
    time2 = GetTime();
        OpenPFEM_Print("组装刚度矩阵的时间为%2.10f\n", time2 - time1);
        time1 = GetTime();
        LINEARSOLVER *solver = NULL;
        LinearSolverCreate(&solver);
        LinearSolverSetType(solver, PETSc);
        LinearSolve(StiffMatrix, solver, CG);
        FEMFUNCTION *solution = NULL;
        LinearSolverGetFEMVECSolution(solver, femspace, &solution);
        LinearSolverDestory(&solver);
        time2 = GetTime();
        OpenPFEM_Print("求解线性方程组的时间为%2.10f\n", time2 - time1);

        DiscreteFormDestroy(&StiffDiscreteForm);
        StiffDiscreteForm = NULL;

        H1Error = 0.0;
        L2Error = 0.0;
        L2ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 1, &L2multiindex[0], ExactSolu2D, ErrFun2DL2, Quadrature);
        H1ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 2, &H1ErrorIndex[0], ExactGrad2D, ErrFun2DH1, Quadrature);
        OpenPFEM_Print("L2误差为%2.14f  H1误差为%2.14f\n", L2ErrorOfBisection[ind_refine], H1ErrorOfBisection[ind_refine]);
        //后验误差估计
        PosteriorError = malloc(mesh->num_face * sizeof(DOUBLE));
        time1 = GetTime();
        PosterioriErrorEstimate(solution, 3, 2, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        time2 = GetTime();
        OpenPFEM_Print("后验误差计算的时间为%2.10f\n", time2 - time1);

        PostErrorOfBisection[ind_refine] = TotalError;
        // //网格加密
        // time1 = GetTime();
        // if (ind_refine < max_refine - 1)
        // {
        //     OpenPFEM_Print("Start Adaptive refining the mesh!\n");
        //     AdaptiveRefineMesh2D(mesh, PosteriorError, 0.6);
        //     OpenPFEM_Print("完成自适应加密\n");
        // }
        // time2 = GetTime();
        // OpenPFEM_Print("加密的时间为%2.10f\n", time2 - time1);

        // OpenPFEM_Free(PosteriorError);
        UniformRefineMesh(mesh, 1);
    }

    //输出每次自适应加密的结果

    OpenPFEM_Print("单元个数 N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("L2Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", L2ErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("H1Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", H1ErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");

    // INT myid;
    // MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    // char endtitle[20], number[12], name1[64], filename[128];
    // strcpy(filename, "Cube");
    // strcpy(endtitle, ".vtk");
    // sprintf(number, "%d", myid);
    // strcat(filename, number);
    // strcat(filename, endtitle);

    // OpenPFEM_Print("Out put the mesh!\n");
    // WriteMesh3DVTK(mesh, filename);

    OpenPFEM_Finalize();
    return 0;
}