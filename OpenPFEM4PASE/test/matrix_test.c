#include "assemble.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        // return DIRICHLET;
        return INNER;
    }
    else
    {
        return INNER;
    }
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}

void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (1.0 + 3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}

void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}

void ErrFun3DL2(DOUBLE *femvalue, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvalue[0] - Funvalue[0]) * (femvalue[0] - Funvalue[0]);
}

void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 2);

    // 单元和边界上的积分格式
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle13);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    // 有限元空间
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P2_3D, BoundCond);
    // 刚度矩阵
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 4, &stiffLmultiindex[0],
                                                        femspace, 4, &stiffRmultiindex[0],
                                                        stiffmatrix, rhsvec, BoundFun, Quadrature);
    DOUBLE starttime, endtime;
    starttime = MPI_Wtime();
    MATRIX *StiffMatrix = NULL;
    VECTOR *Rhs = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);
    OpenPFEM_Print("assemble matrix size: %d x %d\n", StiffMatrix->global_nrows,  StiffMatrix->global_ncols);
    VECTOR *Solution = NULL;
    VectorCreateByMatrix(&Solution, StiffMatrix);

    starttime = MPI_Wtime();
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver, PETSC_SUPERLU);
    LinearSolve(solver, StiffMatrix, Rhs, Solution);
    LinearSolverDestroy(&solver);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for solve: %2.10f\n", endtime - starttime);


    MatrixDestroy(&StiffMatrix);
    VectorDestroy(&Rhs);
    VectorDestroy(&Solution);
    QuadratureDestroy(&Quadrature);
    QuadratureDestroy(&QuadratureEdge);
    FEMSpaceDestroy(&femspace);
    DiscreteFormDestroy(&StiffDiscreteForm);
    MeshDestroy(&mesh);
    OpenPFEM_Finalize();
    return 0;
}
