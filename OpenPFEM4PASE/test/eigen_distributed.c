#include "OpenPFEM.h"
#include "eigensolver.h"
#include "errorestimate.h"
#include "meshrepartition.h"

// 定义边界类型
BOUNDARYTYPE BoundCond(INT bdid);
BOUNDARYTYPE MassBoundCond(INT bdid);
void stiffmatrix(DOUBLE* left, DOUBLE* right, DOUBLE* coord, DOUBLE* AuxFEMValues, DOUBLE* value);
void massmatrix(DOUBLE* left, DOUBLE* right, DOUBLE* coord, DOUBLE* AuxFEMValues, DOUBLE* value);
void BoundFun(double X[3], int dim, double* values);
void PostErrFunElem(DOUBLE lambda, DOUBLE* eigfun, DOUBLE* value);
void PostErrFunEdge(DOUBLE* femvalue, DOUBLE* rhsvalue, DOUBLE* nomalvec, DOUBLE* value);

// 首先对于进程数大于要求解的特征对个数num_eigen的情况进行求解
// 我们将所有进程分成num_eigen份 每份进程中的所有进程共同计算一个特征对
// 此例子中将通讯域的分解包装成了函数
int main(int argc, char* argv[]) {
    OpenPFEM_Init(&argc, &argv);
    // 要求解的特征对个数
    INT num_eigen = 2;
    // 将num_eigen对特征对的计算分别分配到不同的进程上
    // 获取本进程 进程号
    INT myid;
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    // 将MPI_COMM_WORLD分成num_eigen个互不重叠的子通讯域
    MPI_Comm* comm;
    INT my_block;
    SplitComm(num_eigen, &comm, &my_block);

    MESH* mesh = NULL;
    MeshCreate(&mesh, 3, comm[my_block]);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001};
    MULTIINDEX stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000};
    MULTIINDEX massRmultiindex[1] = {D000};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    QUADRATURE* Quadrature = QuadratureBuild(QuadTetrahedral56);
    QUADRATURE* QuadratureEdge = QuadratureBuild(QuadTriangle13);

    // 二分加密的次数
    INT BisectionTimes = 10;
    INT bisectionind;
    INT NUMVOLU;
    DOUBLE TotalError;
    // 存储每次二分加密后的单元个数和误差值
    INT* NumElemOfBisection = malloc(BisectionTimes * sizeof(INT));
    DOUBLE* PostErrorOfBisection = malloc(BisectionTimes * sizeof(DOUBLE));
    // 获取本进程在进程组中的编号
    INT myid_inblock;
    MPI_Comm_rank(comm[my_block], &myid_inblock);

    FEMSPACE* femspace = NULL;
    FEMSPACE* massfemspace = NULL;
    DISCRETEFORM* StiffDiscreteForm = NULL;
    DISCRETEFORM* MassDiscreteForm = NULL;
    MATRIX* StiffMatrix = NULL;
    MATRIX* MassMatrix = NULL;
    // 下面进行自适应加密
    OpenPFEM_Print("Start the bisection!\n");
    for (bisectionind = 0; bisectionind < BisectionTimes; bisectionind++) {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", bisectionind);
        // 计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[bisectionind] = NUMVOLU;
        femspace = FEMSpaceBuild(mesh, C_T_P1_3D, BoundCond);
        massfemspace = FEMSpaceBuild(mesh, C_T_P1_3D, MassBoundCond);
        // 组装刚度矩阵
        StiffDiscreteForm = DiscreteFormBuild(femspace, 3, &stiffLmultiindex[0], femspace, 3, &stiffRmultiindex[0],
                                              stiffmatrix, NULL, BoundFun, Quadrature);
        MassDiscreteForm = DiscreteFormBuild(massfemspace, 1, &massLmultiindex[0], massfemspace, 1, &massRmultiindex[0],
                                             massmatrix, NULL, BoundFun, Quadrature);
        // 组装有限元矩阵
        //  printf("fhosauhgohgio\n");
        MatrixAssemble(&StiffMatrix, NULL, StiffDiscreteForm, TYPE_PETSC);
        // printf("fhosauhgohgio\n");
        MatrixAssemble(&MassMatrix, NULL, MassDiscreteForm, TYPE_PETSC);

        // 求解
        // 这里发现如果矩阵的规模变化了 就需要重新生成solver 不然解处是有值的 这样没有办法生成新的向量结构
        EIGENSOLVER* solver = NULL;
        EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);

        VECTORS* evec = NULL;
        VectorsCreateByMatrix(&evec, StiffMatrix, num_eigen);
        DOUBLE* eval = (DOUBLE*)malloc(num_eigen * sizeof(DOUBLE));
        EigenSolverSolve(solver, StiffMatrix, MassMatrix, eval, evec, num_eigen);
        FEMFUNCTION* eigfunc0 = FEMFunctionBuild(femspace, 1);
        VectorsSetRange(evec, 0, 1);
        VectorsGetFEMFunction(evec, eigfunc0, 0);
        VectorsSetRange(evec, 0, num_eigen);

        //EigenSolverGetFEMVECEigenpair(solver, femspace, &eval, &eigvec, my_block);  // 取出第my_block个特征对
        if (myid_inblock == 0) {
            printf("第%d个特征值为%2.10f\n", my_block, eval[my_block]);
        }
        // 后验误差估计
        DOUBLE* PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        EigenPosterioriErrorEstimate(eigfunc0, eval, 4, 3, &PosterioriErrorIndex[0],
                                     PostErrFunElem, PostErrFunEdge, Quadrature,
                                     QuadratureEdge, PosteriorError, &TotalError);
        PostErrorOfBisection[bisectionind] = TotalError;  // sqrt(PostErrorOfBisection[bisectionind]);
        if (bisectionind < BisectionTimes - 1) {
            OpenPFEM_Print("Start Adaptive refining the mesh!\n");
            MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
            OpenPFEM_Print("完成自适应加密\n");
        }
        // 每次完成自适应加密后free后验误差
        OpenPFEM_Free(PosteriorError);
        // 每次完成自适应加密后free有限元函数
        // FreeFEMFuntion(eigvec);
        // 每次完成自适应加密后freesolver
        EigenSolverDestroy(&solver);
        // FreeFEMSpace(femspace);
        femspace = NULL;
        // FreeFEMSpace(massfemspace);
        massfemspace = NULL;
        // 每次完成自适应加密后free变分形式
        DiscreteFormDestroy(&StiffDiscreteForm);
        StiffDiscreteForm = NULL;
        DiscreteFormDestroy(&MassDiscreteForm);
        MassDiscreteForm = NULL;
        // 每次完成自适应加密后free矩阵
        // MatrixDestory(StiffMatrix);
        StiffMatrix = NULL;
        // MatrixDestory(MassMatrix);
        MassMatrix = NULL;
    }
    // 输出每次自适应加密的结果
    if (myid_inblock == 0) {
        printf("第%d个eigenpair N = [ ", my_block);
        for (bisectionind = 0; bisectionind < BisectionTimes; bisectionind++) {
            printf(" %d ", NumElemOfBisection[bisectionind]);
        }
        printf("];\n");
        printf("第%d个eigenpair PostErr = [ ", my_block);
        for (bisectionind = 0; bisectionind < BisectionTimes; bisectionind++) {
            printf(" %2.14f ", PostErrorOfBisection[bisectionind]);
        }
        printf("];\n");
    }
    char endtitle[20], number[12], name1[64], filename[128];
    strcpy(filename, "Cube");
    strcpy(endtitle, ".vtk");
    sprintf(number, "%d", myid);
    strcat(filename, number);
    strcat(filename, endtitle);

    OpenPFEM_Print("Out put the mesh!\n");
    Mesh3DVTKWrite(mesh, filename);
    OpenPFEM_Finalize();
    return 0;
}

BOUNDARYTYPE BoundCond(INT bdid) {
    if (bdid > 0) {
        return DIRICHLET;  // 对所有的边条件都定义为Dirichlet类型
        // return NEUMANN;
    } else {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid) {
    if (bdid > 0) {
        return MASSDIRICHLET;
    } else {
        return INNER;
    }
}
void stiffmatrix(DOUBLE* left, DOUBLE* right, DOUBLE* coord, DOUBLE* AuxFEMValues, DOUBLE* value) {
    // 针对特征值问题$（\nabla u, \nabla v)=\lambda (u, v)$构造的刚度矩阵形式
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE* left, DOUBLE* right, DOUBLE* coord, DOUBLE* AuxFEMValues, DOUBLE* value) {
    // 针对特征值问题$（\nabla u, \nabla v)=\lambda (u, v)$构造的质量矩阵形式
    value[0] = left[0] * right[0];
}

void BoundFun(double X[3], int dim, double* values) {
    values[0] = 0.0;
}

// MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
void PostErrFunElem(DOUBLE lambda, DOUBLE* eigfun, DOUBLE* value) {
    value[0] = (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3]) *
               (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3]);
}
// normalvec: 表示所在的edge上的外法向量
void PostErrFunEdge(DOUBLE* femvalue, DOUBLE* rhsvalue, DOUBLE* nomalvec, DOUBLE* value) {
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
