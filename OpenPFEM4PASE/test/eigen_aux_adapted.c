#include "OpenPFEM.h"
#include "eigensolver.h"
#include "errorestimate.h"
#include "linearsolver.h"
#include "multilevel.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2];
}

void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0];
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}
void PostErrFunElem(DOUBLE lambda, DOUBLE *eigfun, DOUBLE *value)
{
    value[0] = (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3] - eigfun[0] * eigfun[0] * eigfun[0]) *
               (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3] - eigfun[0] * eigfun[0] * eigfun[0]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}
int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);
    MeshSetAsAncestor(mesh);

    MESH *finermesh = NULL, *coarsemesh = NULL;

    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle13);

    MULTIINDEX stifflinearLmi[3] = {D100, D010, D001};
    MULTIINDEX stifflinearRmi[3] = {D100, D010, D001};
    MULTIINDEX stiffnonlinearLmi[1] = {D000};
    MULTIINDEX stiffnonlinearRmi[1] = {D000};

    MULTIINDEX massLmultiindex[1] = {D000};
    MULTIINDEX massRmultiindex[1] = {D000};
    // 在做校正步时 把解带入到右端时需要的信息
    INT NAuxFEMFunMultiIndex[1] = {1};
    MULTIINDEX auxfemfunmi[1] = {D000};
    // 计算误差 需要的积分信息
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    // 计算内积 需要的积分信息
    MULTIINDEX ErrorIndex[1] = {D000};

    INT eig_num = 3; // 求解时增加的特征对个数
    INT eig_aim = 0; // 自适应加密是针对的特征对是第几个

    INT SCF_ind, refine_ind, eig_ind;
    INT SCF_max = 10, refine_max = 5;
    DOUBLE TolValue = 1e-5;          // 判断特征值是否收敛
    DOUBLE TolVec = 1e-5;            // 判断特征向量是否收敛
    DOUBLE TolPosteriorError = 1e-8; // 判断自适应加密是否终止
    // 1. 在粗网格上利用自洽场迭代求解$-\Delta u + u^3 = \lambda u$ 求得特征对作为初值
    MULTILEVEL *multilevel;
    MultiLevelCreate(&multilevel, TYPE_PETSC);
    MultiLevelSetNev(multilevel, eig_num);

    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P1_3D, BoundCond);
    FEMSPACE *massfemspace = FEMSpaceBuild(mesh, C_T_P1_3D, MassBoundCond);
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 3, stifflinearLmi, femspace, 3, stifflinearRmi,
                                                        stiffmatrixlinear, NULL, BoundFun, Quadrature);
    DISCRETEFORM *MassDiscreteForm = DiscreteFormBuild(massfemspace, 1, massLmultiindex, massfemspace, 1, massRmultiindex,
                                                       massmatrix, NULL, BoundFun, Quadrature);
    MultiLevelAddLevel(multilevel, StiffDiscreteForm, MassDiscreteForm);
    MATRIX *StiffMatrix = multilevel->stiff_matrices[0];
    MATRIX *Mass = multilevel->mass_matrices[0];
    DOUBLE *Eval = NULL;
    VECTORS *Evec = NULL;
    MATRIX *Prolong;

    EIGENSOLVER *solver;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);

    // multilevel->eigvecs,eigvalues分配空间
    VectorsCreateByMatrix(&Evec, StiffMatrix, eig_num);
    Eval = (DOUBLE *)malloc(eig_num * sizeof(DOUBLE));

    EigenSolverSolve(solver, StiffMatrix, Mass, Eval, Evec, eig_num);
    FEMFUNCTION *eigfunc = FEMFunctionBuild(femspace, 1);
    FEMFUNCTION *eigfunc_tmp = FEMFunctionBuild(femspace, 1);
    VectorsSetRange(Evec, eig_aim, eig_aim + 1);
    VectorsGetFEMFunction(Evec, eigfunc);
    VectorsSetRange(Evec, 0, eig_num);
    DOUBLE eigval_temp = Eval[eig_aim];

    AUXFEMFUNCTION *auxfunc = AuxFEMFunctionBuild(1, &eigfunc, NAuxFEMFunMultiIndex, auxfemfunmi);
    DISCRETEFORM *StiffNonlinear = DiscreteFormAuxFeFunBuild(femspace, 1, stiffnonlinearLmi, massfemspace, 1, stiffnonlinearRmi,
                                                             auxfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);
    MATRIX *Stiff_NL = NULL;
    MatrixBeginAssemble(&Stiff_NL, NULL, StiffNonlinear, TYPE_PETSC);
    MatrixAxpby(1.0, StiffMatrix, 1.0, Stiff_NL, NONZERO_DIFF);
    EigenSolverSolve(solver, Stiff_NL, Mass, Eval, Evec, eig_num);
    eigval_temp -= Eval[eig_aim];
    eigval_temp = fabs(eigval_temp);
    VectorsSetRange(Evec, eig_aim, eig_aim + 1);
    VectorsGetFEMFunction(Evec, eigfunc_tmp);
    VectorsSetRange(Evec, 0, eig_num);
    FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc);
    DOUBLE Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    FEMFunctionAxpby(-2.0, eigfunc_tmp, -1.0, eigfunc);
    DOUBLE Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    OpenPFEM_Print("[0] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", Eval[0], eigval_temp, min(Error1, Error2));
    FEMFunctionCopy(eigfunc_tmp, eigfunc);
    eigval_temp = Eval[eig_aim];

    for (SCF_ind = 0; SCF_ind < SCF_max; SCF_ind++)
    {
        MatrixReAssemble(Stiff_NL, NULL);
        MatrixAxpby(1.0, StiffMatrix, 1.0, Stiff_NL, NONZERO_DIFF);
        EigenSolverSolve(solver, Stiff_NL, Mass, Eval, Evec, eig_num);
        eigval_temp -= Eval[eig_aim];
        eigval_temp = fabs(eigval_temp);
        VectorsSetRange(Evec, eig_aim, eig_aim + 1);
        VectorsGetFEMFunction(Evec, eigfunc_tmp);
        VectorsSetRange(Evec, 0, eig_num);
        FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc);
        Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
        FEMFunctionAxpby(-2.0, eigfunc_tmp, -1.0, eigfunc);
        Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
        OpenPFEM_Print("[%d] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", SCF_ind + 1, Eval[0], eigval_temp, min(Error1, Error2));
        if ((eigval_temp < TolValue) && (min(Error1, Error2) < TolVec))
        {
            SCF_ind = SCF_max;
            OpenPFEM_Print("End SCF!\n");
        }
        FEMFunctionCopy(eigfunc_tmp, eigfunc);
        eigval_temp = Eval[eig_aim];
    }
    FEMFunctionDestroy(&eigfunc);
    FEMFunctionDestroy(&eigfunc_tmp);
    eigval_temp = Eval[eig_aim];

    multilevel->eigvalues[0] = Eval;
    multilevel->eigvecs[0] = Evec;
    // Eval = NULL;
    // Evec = NULL;
    // 2. 自适应加密迭代

    DOUBLE TotalError = 0.0;
    INT *NumElemOfBisection = malloc((refine_max + 1) * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc((refine_max + 1) * sizeof(DOUBLE));
    eigfunc = FEMFunctionBuild(femspace, 1);
    coarsemesh = mesh;
    for (refine_ind = 0; refine_ind < refine_max; refine_ind++)
    {
        // 2.1 加密：根据已计算出的特征对计算后验误差 进行自适应加密
        finermesh = MeshDuplicate(coarsemesh);
        DOUBLE *PosteriorError = malloc(finermesh->num_volu * sizeof(DOUBLE));
        VectorsSetRange(multilevel->eigvecs[refine_ind], eig_aim, eig_aim + 1);
        VectorsGetFEMFunction(multilevel->eigvecs[refine_ind], eigfunc);
        VectorsSetRange(multilevel->eigvecs[refine_ind], 0, eig_num);
        EigenPosterioriErrorEstimate(eigfunc, &(eigval_temp), 4, 3,
                                     &PosterioriErrorIndex[0], PostErrFunElem, PostErrFunEdge, Quadrature,
                                     QuadratureEdge, PosteriorError, &TotalError);
        OpenPFEM_Print("[%d] Posterior Error is %2.14f.\n", refine_ind, TotalError);
        if (TotalError < TolPosteriorError)
        {
            refine_ind = refine_max;
        }
        if (0 == refine_ind)
        {
            NumElemOfBisection[refine_ind] = finermesh->num_volu;
            PostErrorOfBisection[refine_ind] = TotalError;
        }
        OpenPFEM_Print("Start Adaptive refining the mesh!\n");
        MeshAdaptiveRefine(finermesh, PosteriorError, 0.6);
        NumElemOfBisection[refine_ind + 1] = finermesh->num_volu;
        PostErrorOfBisection[refine_ind + 1] = TotalError;
        OpenPFEM_Print("单元个数 %d \n", finermesh->num_volu);
        OpenPFEM_Print("完成自适应加密\n");
        free(PosteriorError);
        // 2.2 插值：将特征向量插值到细空间
        femspace = FEMSpaceBuild(finermesh, C_T_P1_3D, BoundCond);
        massfemspace = FEMSpaceBuild(finermesh, C_T_P1_3D, MassBoundCond);
        StiffDiscreteForm = DiscreteFormBuild(femspace, 3, stifflinearLmi, femspace, 3, stifflinearRmi,
                                              stiffmatrixlinear, NULL, BoundFun, Quadrature);
        MassDiscreteForm = DiscreteFormBuild(massfemspace, 1, massLmultiindex, massfemspace, 1, massRmultiindex,
                                             massmatrix, NULL, BoundFun, Quadrature);
        MultiLevelAddLevel(multilevel, StiffDiscreteForm, MassDiscreteForm);

        // multilevel->eigvalues[refine_ind + 1] = (DOUBLE*)malloc(eig_num * sizeof(DOUBLE));
        Eval = (DOUBLE *)malloc(eig_num * sizeof(DOUBLE));
        StiffMatrix = multilevel->stiff_matrices[refine_ind + 1];
        Mass = multilevel->mass_matrices[refine_ind + 1];
        Prolong = multilevel->prolongs[refine_ind];
        // VectorsCreateByMatrixTranspose(&(multilevel->eigvecs[refine_ind + 1]), Prolong, eig_num);
        // VectorsCreateByMatrix(&multilevel->eigvecs[refine_ind + 1], StiffMatrix, eig_num);
        VectorsCreateByMatrix(&Evec, StiffMatrix, eig_num);
        // OpenPFEM_Print("--refine_ind:%d--refine_ind+1:%d--\n", multilevel->eigvecs[refine_ind]->global_length, multilevel->eigvecs[refine_ind + 1]->global_length);
        // OpenPFEM_Print("--row:%d--col:%d--\n", Prolong->global_nrows, Prolong->global_ncols);
        // MatrixTransposeVectorsMult(Prolong, multilevel->eigvecs[refine_ind], multilevel->eigvecs[refine_ind + 1]);
        MatrixVectorsMult(Prolong, multilevel->eigvecs[refine_ind], Evec);
        eigfunc = NULL;
        eigfunc_tmp = NULL;
        eigfunc = FEMFunctionBuild(femspace, 1);
        eigfunc_tmp = FEMFunctionBuild(femspace, 1);
        VectorsSetRange(Evec, eig_aim, eig_aim + 1);
        VectorsGetFEMFunction(Evec, eigfunc);
        VectorsSetRange(Evec, 0, eig_num);
        AUXFEMFUNCTION *auxfunc = AuxFEMFunctionBuild(1, &eigfunc, NAuxFEMFunMultiIndex, auxfemfunmi);
        DISCRETEFORM *StiffNonlinear = DiscreteFormAuxFeFunBuild(femspace, 1, stiffnonlinearLmi, massfemspace, 1, stiffnonlinearRmi,
                                                                 auxfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);
        MATRIX *Stiff_NL = NULL;
        MatrixBeginAssemble(&Stiff_NL, NULL, StiffNonlinear, TYPE_PETSC);
        for (SCF_ind = 0; SCF_ind < SCF_max; SCF_ind++)
        {
            MatrixReAssemble(Stiff_NL, NULL);
            MatrixAxpby(1.0, StiffMatrix, 1.0, Stiff_NL, NONZERO_DIFF);
            EigenSolverSolve(solver, Stiff_NL, Mass, Eval, Evec, eig_num);
            eigval_temp -= Eval[eig_aim];
            eigval_temp = fabs(eigval_temp);
            VectorsSetRange(Evec, eig_aim, eig_aim + 1);
            VectorsGetFEMFunction(Evec, eigfunc_tmp);
            VectorsSetRange(Evec, 0, eig_num);
            FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc);
            Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
            FEMFunctionAxpby(-2.0, eigfunc_tmp, -1.0, eigfunc);
            Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
            OpenPFEM_Print("[%d] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", SCF_ind + 1, Eval[eig_aim], eigval_temp, min(Error1, Error2));
            if ((eigval_temp < TolValue) && (min(Error1, Error2) < TolVec))
            {
                SCF_ind = SCF_max;
                OpenPFEM_Print("End SCF!\n");
            }
            FEMFunctionCopy(eigfunc_tmp, eigfunc);
            eigval_temp = Eval[eig_aim];
        }
        multilevel->eigvalues[refine_ind + 1] = Eval;
        OpenPFEM_Print("%d refine finished--\n", refine_ind);
        coarsemesh = finermesh;
        // coarsespace = finerspace;
    }
    OpenPFEM_Print("单元个数 N = [ ");
    for (refine_ind = 0; refine_ind <= refine_max; refine_ind++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[refine_ind]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (refine_ind = 0; refine_ind <= refine_max; refine_ind++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[refine_ind]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("eigvalue = [ ");
    for (refine_ind = 0; refine_ind <= refine_max; refine_ind++)
    {
        OpenPFEM_Print(" %2.14f ", multilevel->eigvalues[refine_ind][eig_aim]);
    }
    OpenPFEM_Print("];\n");

    OpenPFEM_Print("-----------------------------------\n");

    MPI_Finalize();
    // OpenPFEM_Finalize();
    return 0;

}