#include "errorestimate.h"
#include "meshrepartition.h"
#include "linearsolver.h"
// #include "multilevelsolver.h"

// Lshaped自适应加密+multilevel测试程序

BOUNDARYTYPE BoundCond(INT bdid);
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void BoundFun(double X[3], int dim, double *values);
void ErrFunL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value);
void ErrFunH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value);
void rhs(DOUBLE *coord, INT dim, DOUBLE *value);
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value);
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value);
int debugging=0;
INT main(INT argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    BuildMesh(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    UniformRefineMesh(mesh, 2);
    MeshPartition(mesh);
    UniformRefineMesh(mesh, 1);

    // 单元上的积分格式
    QUADRATURE *Quadrature = BuildQuadrature(QuadTetrahedral56);
    // 边界上的积分格式
    QUADRATURE *QuadratureEdge = BuildQuadrature(QuadTriangle13);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    // 误差
    DOUBLE H1Error, L2Error, TotalError;
    // 存储每次二分加密后的单元个数和误差值
    MULTIINDEX L2ErrorIndex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX PosterioriErrorIndex[6] = {D200, D020, D002, D100, D010, D001};

    FEMSPACE *femspace = BuildFEMSpace(mesh, C_T_P2_3D, BoundCond);
    DISCRETEFORM *StiffDiscreteForm = BuildDiscreteForm(femspace, 3, &stiffLmultiindex[0], femspace, 4, &stiffRmultiindex[0],
                                                        stiffmatrix, rhsvec, BoundFun, Quadrature);
    MESH *finermesh = NULL;
    FEMSPACE *finerfemspace = NULL;
    INT BisectionTimes = 4, idx_bisection;
    DOUBLE *PostErrorOfBisection = malloc(BisectionTimes * sizeof(DOUBLE));
    //(0) 加上第一层
    MULTILEVEL *multilevel = NULL;
    MultiLevelCreate(&multilevel);
    MultiLevelAddLevel(StiffDiscreteForm, NULL, multilevel);
    /* multilevel 求解器 */
    MULTILEVELSOLVER *MLsolver = NULL;
    MultiLevelSolverCreate(&MLsolver);
    MultiLevelSolverSetType(MLsolver, PETSc);
    MultiLevelSolverSetSmoothTimes(MLsolver, 3);
    MultiLevelSolverSetCorrectionTimes(MLsolver, 1);
    MultiLevelSolverSetIterTimes(MLsolver, 2);
    debugging = 1;
    for (idx_bisection = 0; idx_bisection < BisectionTimes; idx_bisection++)
    {
        OpenPFEM_Print("============= 第%d次加密 =============\n", idx_bisection);
        //(1) 求解线性方程组
        MultiLevelSolve(multilevel, MLsolver);
        FEMVEC *solution = NULL;
        MultiLevelSolverGetFEMSolution(MLsolver, multilevel, femspace, &solution);
        //(2) 后验误差估计
        DOUBLE *PosteriorError;
        if (finermesh == NULL)
            PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        else
            PosteriorError = malloc(finermesh->num_volu * sizeof(DOUBLE));
        // 进行后验误差估计的计算
        PosterioriErrorEstimate(solution, 3, 3, &PosterioriErrorIndex[0], rhs, PostErrFunElem, PostErrFunEdge,
                                Quadrature, QuadratureEdge, PosteriorError, &TotalError);
        // 记录目前网格上的总误差
        PostErrorOfBisection[idx_bisection] = TotalError;
        //(3) 加密
        finermesh = MeshDuplicate(mesh);
        MeshAdaptiveRefine(finermesh, PosteriorError, 0.6);
        //(4) 加一层
        femspace = BuildFEMSpace(finermesh, C_T_P2_3D, BoundCond);
        MultiLevelAddLevelBegin(StiffDiscreteForm, femspace, multilevel);
        //(5) 重分布
        BRIDGE *bridge = NULL;
        MeshRepartition(finermesh, &bridge);
        //(6) 完成对插值矩阵的组装
        femspace = BuildFEMSpace(finermesh, C_T_P2_3D, BoundCond);
        MultiLevelAddLevelEnd(StiffDiscreteForm, femspace, multilevel, bridge);
        // if (idx_bisection == 0)
            // PrintMatrix(multilevel->Matrices[1], 0);
        BridgeDestory(bridge);
        MeshDestory(mesh);
        debugging = 0;
        mesh = finermesh;
    }
    MultiLevelSolve(multilevel, MLsolver);
    MultiLevelSolverDestory(&MLsolver, multilevel);
    MultiLevelDestroy(&multilevel);

    FreeQuadrature(Quadrature);
    FreeQuadrature(QuadratureEdge);
    FreeFEMSpace(femspace);
    DiscreteFormDestroy(&StiffDiscreteForm);
    MeshDestory(finermesh);
    OpenPFEM_Finalize();
    return 0;
}

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[1] + left[1] * right[2] + left[2] * right[3];
}

void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = right[0];
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 1.0;
}

void ErrFunL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}

void ErrFunH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) +
               (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) +
               (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}

void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]) *
               (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]);
}

void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}