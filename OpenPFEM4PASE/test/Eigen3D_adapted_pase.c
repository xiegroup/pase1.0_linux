#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"


DOUBLE shift = 0.0;

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return STIFFDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // \frac{1}{2}(\nabla u, \nabla v) + \frac{1}{2}|x|^2(u,v)
    DOUBLE length = (coord[0] * coord[0] + coord[1] * coord[1] + coord[2] * coord[2]);
    value[0] = (left[1] * right[1] + left[2] * right[2] + left[3] * right[3] + left[0] * right[0] * length) / 2.0;
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

void PostErrFunElem(DOUBLE lambda, DOUBLE *eigfun, DOUBLE *coord, DOUBLE *value)
{
    // \lambda u +\frac{1}{2}\Delta u - \frac{1}{2}|x|^2u
    DOUBLE length = (coord[0] * coord[0] + coord[1] * coord[1] + coord[2] * coord[2]);
    value[0] = (lambda * eigfun[0] + (eigfun[1] + eigfun[2] + eigfun[3]) / 2.0 - eigfun[0] * length / 2.0) *
               (lambda * eigfun[0] + (eigfun[1] + eigfun[2] + eigfun[3]) / 2.0 - eigfun[0] * length / 2.0);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL, *finer_mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube6_4.txt", SIMPLEX, TETHEDRAL); //[-4,4]^3
    MeshUniformRefine(mesh, 4);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);

    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001}, stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral304), *QuadratureEdge = QuadratureBuild(QuadTriangle36);

    INT ind_refine, i, j, NUMVOLU;
    INT max_refine = 10;
    INT nev = 200;
    DOUBLE TotalError, TotalError_all;
    DOUBLE *eval = NULL;
    VECTORS *evec = NULL;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    FEMSPACE *femspace = NULL, *finer_femspace = NULL;
    FEMSPACE *massfemspace = NULL, *finer_massfemspace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL;
    DISCRETEFORM *MassDiscreteForm = NULL;
    MATRIX *StiffMatrix = NULL, *MassMatrix = NULL;
    DOUBLE *PosteriorError = NULL;
    DOUBLE *PosteriorError_all = NULL;
    FEMFUNCTION *eigfunc = NULL;

    // 设置精确解
    INT test_n, num, ii;
    DOUBLE *eval_real = malloc(nev * sizeof(DOUBLE));
    num = 0;
    for (test_n = 1; test_n <= nev; test_n++)
    {
        num += test_n * (test_n + 1) / 2;
        if (num >= nev)
        {
            for (ii = (num - test_n * (test_n + 1) / 2); ii < nev; ii++)
                eval_real[ii] = 1.5 + (DOUBLE)test_n - 1;
            test_n = nev + 5;
        }
        else
        {
            for (ii = (num - test_n * (test_n + 1) / 2); ii < num; ii++)
                eval_real[ii] = 1.5 + (DOUBLE)test_n - 1;
        }
    }
    DOUBLE *error_eval = malloc(nev * max_refine * sizeof(DOUBLE));
    memset(error_eval, 0.0, nev * max_refine * sizeof(DOUBLE));

    /* multilevel 结构体 */
    MULTILEVEL *multilevel = NULL;
    MultiLevelCreate(&multilevel, TYPE_OPENPFEM);
    MultiLevelSetNev(multilevel, nev);
    /* 第一层 */
    femspace = FEMSpaceBuild(mesh, C_T_P3_3D, BoundCond);
    massfemspace = FEMSpaceBuild(mesh, C_T_P3_3D, MassBoundCond);
    StiffDiscreteForm = DiscreteFormBuild(femspace, 4, stiffLmultiindex, femspace, 4, stiffRmultiindex,
                                          stiffmatrix, NULL, BoundFun, Quadrature);
    MassDiscreteForm = DiscreteFormBuild(massfemspace, 1, massLmultiindex, massfemspace, 1, massRmultiindex,
                                         massmatrix, NULL, BoundFun, Quadrature);
    MultiLevelAddLevel(multilevel, StiffDiscreteForm, MassDiscreteForm);
    MatrixDeleteDirichletBoundary(multilevel->stiff_matrices[0], StiffDiscreteForm);
    MatrixDeleteDirichletBoundary(multilevel->mass_matrices[0], MassDiscreteForm);
    MatrixConvert(multilevel->stiff_matrices[0], TYPE_PETSC, 0);
    MatrixConvert(multilevel->mass_matrices[0], TYPE_PETSC, 0);
    DiscreteFormDestroy(&StiffDiscreteForm);
    DiscreteFormDestroy(&MassDiscreteForm);
    VECTORS *init_evec = NULL;
    void *A_preconditioner = NULL;
    double s_time, e_time;
    double pase_solve_time = 0.0;
    int conv_nev = 0;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        // OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);

        // OpenPFEM_Print("(1) 求解特征值问题\n");
        s_time = MPI_Wtime();
        EIGENSOLVER *solver = NULL;
        EigenSolverCreate(&solver, MULTILEVEL_PASE);
        EigenSolverMultiLevelSetUp(solver, multilevel, nev);
        if (ind_refine > 0)
        {
            EigenSolverSetPASENev(solver, nev);
            EigenSolverSetPASEInitialSolution(solver, ind_refine - 1, init_evec);
            EigenSolverSetPASEPreconditioner(solver, 0, A_preconditioner, NULL);
        }
        if (ind_refine == 0)
        {
            EigenSolverPASESolve(solver, multilevel->stiff_matrices[ind_refine], multilevel->mass_matrices[ind_refine],
                                 &eval, &evec, &conv_nev);
        }
        else
        {
            VectorsCreateByMatrix(&evec, multilevel->stiff_matrices[ind_refine], conv_nev);
            EigenSolverMultiLevelSolve(solver, multilevel, eval, evec, conv_nev);
        }
        // OpenPFEM_Print("得到了%d个解\n", conv_nev);
        if (ind_refine > 0 && ind_refine < max_refine - 1)
        {
            EigenSolverGetPASEPreconditioner(solver, &A_preconditioner, NULL);
        }
        if (ind_refine > 0)
        {
            VectorsDestroy(&init_evec);
        }
        // OpenPFEM_Print("得到了%d个解2\n", conv_nev);
        VectorsCreateByMatrix(&init_evec, multilevel->stiff_matrices[ind_refine], conv_nev);
        VectorsSetRange(evec, 0, conv_nev);
        VectorsSetRange(init_evec, 0, conv_nev);
        // OpenPFEM_Print("得到了%d个解2.5\n", conv_nev);
        VectorsAxpby(1.0, evec, 0.0, init_evec);
        // OpenPFEM_Print("得到了%d个解3\n", conv_nev);
        EigenSolverDestroy(&solver);
        VectorsAddZeroDirichletBoundary(evec, NULL, femspace);
        e_time = MPI_Wtime();
        pase_solve_time += e_time - s_time;
        // OpenPFEM_Print("时间: %f\n", e_time - s_time);

        // OpenPFEM_Print("(2) 后验误差估计\n");
        s_time = MPI_Wtime();
        PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        PosteriorError_all = malloc(mesh->num_volu * sizeof(DOUBLE));
        memset(PosteriorError_all, 0.0, mesh->num_volu * sizeof(DOUBLE));
        TotalError_all = 0.0;
        eigfunc = NULL;
        eigfunc = FEMFunctionBuild(femspace, 1);
        for (i = 0; i < nev; i++)
        {
            VectorsSetRange(evec, i, i + 1);
            VectorsGetFEMFunction(evec, eigfunc);
            EigenPosterioriErrorEstimate(eigfunc, &(eval[i]), 4, 3, &PosterioriErrorIndex[0],
                                         PostErrFunElem, PostErrFunEdge, Quadrature,
                                         QuadratureEdge, PosteriorError, &TotalError);
            for (j = 0; j < mesh->num_volu; j++)
            {
                PosteriorError_all[j] += PosteriorError[j];
            }
            TotalError_all += TotalError * TotalError;
        }
        TotalError_all = sqrt(TotalError_all);
        PostErrorOfBisection[ind_refine] = TotalError_all;
        // 计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;
        VectorsDestroy(&evec);
        e_time = MPI_Wtime();
        // OpenPFEM_Print("时间: %f\n", e_time - s_time);

        // 网格加密
        // OpenPFEM_Print("(3) 自适应加密\n");
        s_time = MPI_Wtime();
        finer_mesh = MeshDuplicate(mesh);
        if (ind_refine < max_refine - 1)
        {
            MeshAdaptiveRefine(finer_mesh, PosteriorError_all, 0.6);
        }
        OpenPFEM_Free(PosteriorError);
        OpenPFEM_Free(PosteriorError_all);
        FEMFunctionDestroy(&eigfunc);
        e_time = MPI_Wtime();
        // OpenPFEM_Print("时间: %f\n", e_time - s_time);

        // OpenPFEM_Print("(4) 加一层\n");
        s_time = MPI_Wtime();
        finer_femspace = FEMSpaceBuild(finer_mesh, C_T_P3_3D, BoundCond);
        MultiLevelAddLevelSimple(multilevel, finer_femspace);
        // OpenPFEM_Print("重分布!\n");
        BRIDGE *bridge;
        MeshRepartition(finer_mesh, &bridge);
        ProlongMatrixReorder(&(multilevel->prolongs[ind_refine]), &finer_femspace, finer_mesh, bridge);
        multilevel->femspace = finer_femspace;
        finer_massfemspace = FEMSpaceBuild(finer_mesh, C_T_P3_3D, MassBoundCond);
        StiffDiscreteForm = DiscreteFormBuild(finer_femspace, 4, stiffLmultiindex, finer_femspace, 4, stiffRmultiindex,
                                              stiffmatrix, NULL, BoundFun, Quadrature);
        MassDiscreteForm = DiscreteFormBuild(finer_massfemspace, 1, massLmultiindex, finer_massfemspace, 1, massRmultiindex,
                                             massmatrix, NULL, BoundFun, Quadrature);
        MATRIX *StiffMatrix = NULL, *MassMatrix = NULL;
        MatrixAssemble(&StiffMatrix, NULL, StiffDiscreteForm, TYPE_OPENPFEM);
        MatrixAssemble(&MassMatrix, NULL, MassDiscreteForm, TYPE_OPENPFEM);
        MultiLevelSetMatrix(multilevel, StiffMatrix, MassMatrix, ind_refine + 1);

        MatrixDeleteDirichletBoundary(multilevel->stiff_matrices[ind_refine + 1], StiffDiscreteForm);
        MatrixDeleteDirichletBoundary(multilevel->mass_matrices[ind_refine + 1], MassDiscreteForm);
        MatrixConvert(multilevel->stiff_matrices[ind_refine + 1], TYPE_PETSC, 0);
        MatrixConvert(multilevel->mass_matrices[ind_refine + 1], TYPE_PETSC, 0);
        ProlongDeleteDirichletBoundary(multilevel->prolongs[ind_refine], femspace, finer_femspace);
        MatrixConvert(multilevel->prolongs[ind_refine], TYPE_PETSC, 0);
        DiscreteFormDestroy(&StiffDiscreteForm);
        DiscreteFormDestroy(&MassDiscreteForm);
        MeshDestroy(&mesh);
        FEMSpaceDestroy(&femspace);
        FEMSpaceDestroy(&massfemspace);
        mesh = finer_mesh;
        femspace = finer_femspace;
        massfemspace = finer_massfemspace;
        e_time = MPI_Wtime();
        // OpenPFEM_Print("时间: %f\n", e_time - s_time);
        // OpenPFEM_Free(eval);
    }

    OpenPFEM_Print("element N = [ ");
    for (i = 0; i < max_refine; i++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[i]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (i = 0; i < max_refine; i++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[i]);
    }
    OpenPFEM_Print("];\n");

    OpenPFEM_Print("The total time of PASE solving after %d times refinements is %f sec\n", max_refine, pase_solve_time);

    OpenPFEM_Free(eval);
    OpenPFEM_Free(NumElemOfBisection);
    OpenPFEM_Free(PostErrorOfBisection);
    MeshDestroy(&mesh);
    FEMSpaceDestroy(&femspace);
    FEMSpaceDestroy(&massfemspace);
    QuadratureDestroy(&Quadrature);
    QuadratureDestroy(&QuadratureEdge);

    OpenPFEM_Finalize();
    return 0;
}