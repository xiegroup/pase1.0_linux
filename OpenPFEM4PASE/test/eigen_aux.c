#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}

void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = pow(AuxFEMValues[0], 2) * left[0] * right[0];
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);

    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P3_3D, BoundCond);
    FEMSPACE *massfemspace = FEMSpaceBuild(mesh, C_T_P3_3D, MassBoundCond);
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    MULTIINDEX stiffLmi[4] = {D000, D100, D010, D001}, stiffRmi[4] = {D000, D100, D010, D001};
    MULTIINDEX massLmi[1] = {D000}, massRmi[1] = {D000};
    MULTIINDEX auxfemfunmi[1] = {D000};
    MULTIINDEX ErrorIndex[1] = {D000};
    INT NAuxFEMFunMultiIndex[1] = {1};

    DISCRETEFORM *StiffLinear_DF = DiscreteFormBuild(femspace, 4, stiffLmi, femspace, 4, stiffRmi,
                                                     stiffmatrixlinear, NULL, BoundFun, Quadrature);
    DISCRETEFORM *Mass_DF = DiscreteFormBuild(massfemspace, 1, massLmi, massfemspace, 1, massRmi,
                                              massmatrix, NULL, BoundFun, Quadrature);

    MATRIX *Stiff_L = NULL, *Mass = NULL;
    MatrixAssemble(&Stiff_L, NULL, StiffLinear_DF, TYPE_PETSC);
    MatrixAssemble(&Mass, NULL, Mass_DF, TYPE_PETSC);

    INT nev = 10;
    VECTORS *evec = NULL;
    VectorsCreateByMatrix(&evec, Stiff_L, nev);
    DOUBLE *eval = (DOUBLE *)malloc(nev * sizeof(DOUBLE));

    EIGENSOLVER *solver = NULL;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    EigenSolverSolve(solver, Stiff_L, Mass, eval, evec, nev);
    FEMFUNCTION *eigfunc0 = FEMFunctionBuild(femspace, 1);
    VectorsSetRange(evec, 0, 1);
    VectorsGetFEMFunction(evec, eigfunc0);
    VectorsSetRange(evec, 0, nev);
    DOUBLE Error = 0.0;
    Error = ErrorEstimate(eigfunc0, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    OpenPFEM_Print("EigenValue is %2.14f, Error is %2.14f.\n", eval[0], Error);
    AUXFEMFUNCTION *auxfunc = AuxFEMFunctionBuild(1, &eigfunc0, NAuxFEMFunMultiIndex, auxfemfunmi);
    DISCRETEFORM *StiffNonLinear_DF = DiscreteFormAuxFeFunBuild(massfemspace, 4, stiffLmi, massfemspace, 4, stiffRmi,
                                                                auxfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);
    INT ind_outer, max_outer = 10;
    MATRIX *Stiff_NL = NULL;
    MatrixBeginAssemble(&Stiff_NL, NULL, StiffNonLinear_DF, TYPE_PETSC);
    MatrixAxpby(1.0, Stiff_L, 1.0, Stiff_NL, NONZERO_DIFF);
    EigenSolverSolve(solver, Stiff_NL, Mass, eval, evec, nev);
    FEMFUNCTION *eigfunc_tmp = FEMFunctionBuild(femspace, 1);
    VectorsSetRange(evec, 0, 1);
    VectorsGetFEMFunction(evec, eigfunc_tmp);
    VectorsSetRange(evec, 0, nev);
    FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc0);
    Error = ErrorEstimate(eigfunc0, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    OpenPFEM_Print("[0] EigenValue is %2.14f, Error is %2.14f.\n", eval[0], Error);
    FEMFunctionCopy(eigfunc_tmp, eigfunc0);
    for (ind_outer = 0; ind_outer < max_outer; ind_outer++)
    {
        MatrixReAssemble(Stiff_NL, NULL);
        MatrixAxpby(1.0, Stiff_L, 1.0, Stiff_NL, NONZERO_DIFF);
        EigenSolverSolve(solver, Stiff_NL, Mass, eval, evec, nev);
        VectorsSetRange(evec, 0, 1);
        VectorsGetFEMFunction(evec, eigfunc_tmp);
        VectorsSetRange(evec, 0, nev);
        FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc0);
        Error = ErrorEstimate(eigfunc0, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
        OpenPFEM_Print("[%d] EigenValue is %2.5f, Error is %5e.\n", ind_outer + 1, eval[0], Error);
        FEMFunctionCopy(eigfunc_tmp, eigfunc0);
    }
    MatrixEndAssemble(Stiff_NL, NULL, StiffNonLinear_DF);
    QuadratureDestroy(&Quadrature);
    MeshDestroy(&mesh);
    FEMSpaceDestroy(&femspace);
    FEMSpaceDestroy(&massfemspace);
    DiscreteFormDestroy(&StiffLinear_DF);
    DiscreteFormDestroy(&Mass_DF);
    MatrixDestroy(&Stiff_L);
    MatrixDestroy(&Mass);
    OpenPFEM_Free(eval);
    VectorsDestroy(&evec);
    FEMFunctionDestroy(&eigfunc0);
    DiscreteFormDestroy(&StiffNonLinear_DF);
    MatrixDestroy(&Stiff_NL);
    FEMFunctionDestroy(&eigfunc_tmp);
    OpenPFEM_Finalize();
    return 0;
}