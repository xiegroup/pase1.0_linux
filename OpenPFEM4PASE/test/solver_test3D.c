#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (1.0 + 3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
}
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 3.0 * PI * PI * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]) *
               (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 2);
    // 单元和边界上的积分格式
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral127);
    //QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle13);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    // 有限元空间
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P2_3D, BoundCond);
    // 刚度矩阵
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 4, &stiffLmultiindex[0],
                                                        femspace, 4, &stiffRmultiindex[0],
                                                        stiffmatrix, rhsvec, BoundFun, Quadrature);
    DOUBLE starttime, endtime;
    starttime = MPI_Wtime();
    MATRIX *StiffMatrix = NULL;
    VECTOR *Rhs = NULL;
    MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);
    OpenPFEM_Print("assemble matrix size: %d x %d\n", StiffMatrix->global_nrows,  StiffMatrix->global_ncols);
    VECTOR *Solution = NULL;
    VectorCreateByMatrix(&Solution, StiffMatrix);

    starttime = MPI_Wtime();
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver, PETSC_KSPCG); // PETSC_SUPERLU);
    LinearSolve(solver, StiffMatrix, Rhs, Solution);
    LinearSolverDestroy(&solver);
    FEMFUNCTION *femsol = FEMFunctionBuild(femspace);
    VectorGetFEMFunction(Solution, femsol);
    endtime = MPI_Wtime();
    OpenPFEM_Print("The time for solve: %2.10f\n", endtime - starttime);

    MULTIINDEX massmultiindex[1] = {D000};
    DOUBLE H1Error = 0.0, L2Error = 0.0;
    L2Error = ErrorEstimate(femsol, 1, &massmultiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    H1Error = ErrorEstimate(femsol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);

    MatrixDestroy(&StiffMatrix);
    VectorDestroy(&Rhs);
    VectorDestroy(&Solution);
    QuadratureDestroy(&Quadrature);
    //QuadratureDestroy(&QuadratureEdge);
    FEMSpaceDestroy(&femspace);
    DiscreteFormDestroy(&StiffDiscreteForm);
    MeshDestroy(&mesh);
    OpenPFEM_Finalize();
    return 0;
}