#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return NEUMANN;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return NEUMANN; //MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
// function for the coefficient V(x)
DOUBLE funV(DOUBLE *X)
{
    DOUBLE r = sqrt(X[0]*X[0]+X[1]*X[1]);
    DOUBLE theta = atan2(X[1],X[0]);   //计算角度\in (-PI, PI)
    DOUBLE tmp1, tmp2, value;
    // define the function V(theta)
    if((theta>=-PI)&&(theta<-PI/3))
    {
        tmp1 = 3*theta/PI+1.0;
        tmp2 = tmp1*tmp1;
        value = (1.0-tmp2)*(1.0-tmp2);
    }
    else if((theta>=-PI/3)&&(theta<PI/3))
    {
        value = 0.2*(3.0-2.0*cos(3.0*theta));
    }
    else
    {
        tmp1 = 3*theta/PI-1.0;
        tmp2 = tmp1*tmp1;
        value = (1.0-tmp2)*(1.0-tmp2);
    }
    return value+2.0*(r-1.0)*(r-1.0)+5.8*exp(-5.0*r*r);
}//end for define funV


void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2])*funV(coord);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0]*funV(coord);
}


int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 2, DEFAULT_COMM);
    MeshBuild(mesh, "../data/data_simple.txt", MATLAB, TRIANGLE);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 2);

    MULTIINDEX stiffLmultiindex[2] = {D10, D01}, stiffRmultiindex[2] = {D10, D01};
    MULTIINDEX massLmultiindex[1] = {D00}, massRmultiindex[1] = {D00};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTriangle36); 
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P1_2D, BoundCond);
    FEMSPACE *massfemspace = FEMSpaceBuild(mesh, C_T_P1_3D, MassBoundCond);
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 2, stiffLmultiindex, femspace, 2, stiffRmultiindex,
                                                        stiffmatrix, NULL, BoundFun, Quadrature);
    DISCRETEFORM *MassDiscreteForm = DiscreteFormBuild(femspace, 1, massLmultiindex, femspace, 1, massRmultiindex,
                                                       massmatrix, NULL, BoundFun, Quadrature);

    MATRIX *StiffMatrix = NULL, *MassMatrix = NULL;
    MatrixAssemble(&StiffMatrix, NULL, StiffDiscreteForm, TYPE_SLEPC);
    MatrixAssemble(&MassMatrix, NULL, MassDiscreteForm, TYPE_SLEPC);

    INT nev = 4;
    VECTORS *evec = NULL;
    VectorsCreateByMatrix(&evec, StiffMatrix, nev);
    DOUBLE *eval = (DOUBLE *)malloc(nev * sizeof(DOUBLE));

    EIGENSOLVER *solver = NULL;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    int i;
    EigenSolverSolve(solver, StiffMatrix, MassMatrix, eval, evec, nev);
    for (i = 0; i < nev; i++)
    {
        OpenPFEM_Print("eval[%d]=%g\n", i, eval[i]);
    }

    EigenSolverDestroy(&solver);
    OpenPFEM_Free(eval);
    VectorsDestroy(&evec);
    MatrixDestroy(&StiffMatrix);
    MatrixDestroy(&MassMatrix);
    DiscreteFormDestroy(&StiffDiscreteForm);
    DiscreteFormDestroy(&MassDiscreteForm);
    FEMSpaceDestroy(&femspace);
    FEMSpaceDestroy(&massfemspace);
    QuadratureDestroy(&Quadrature);
    MeshDestroy(&mesh);
    OpenPFEM_Finalize();
    return 0;
}