#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[2] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[3] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[4] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[5] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[6] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
    values[7] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
    values[8] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 3; i < 12; i++)
        value[0] += left[i] * right[i];
    value[0] += (left[3] + left[7] + left[11]) * (right[3] + right[7] + right[11]);
}
void rhsvec(DOUBLE *right, DOUBLE *X, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = right[0] * (4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[0]) * cos(PI * X[2]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2])) +
               right[1] * (4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[1]) * cos(PI * X[2]) * sin(PI * X[0]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2])) +
               right[2] * (4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[1]) * cos(PI * X[2]) * sin(PI * X[0]) - PI * PI * cos(PI * X[0]) * cos(PI * X[2]) * sin(PI * X[1]));
}
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 3; i++)
    {
        // OpenPFEM_Print("fem: %2.10f, fun: %2.10f\n",femvale[i],Funvalue[i]);
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
    }
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 9; i++)
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
}
// void rhs(DOUBLE *X, INT dim, DOUBLE *value)
// {
//     value[0] = 4.0*PI*PI*sin(PI*X[0])*sin(PI*X[1])*sin(PI*X[2]) - PI*PI*cos(PI*X[0])*cos(PI*X[2])*sin(PI*X[1]) - PI*PI*cos(PI*X[0])*cos(PI*X[1])*sin(PI*X[2]);
//     value[1] = 4.0*PI*PI*sin(PI*X[0])*sin(PI*X[1])*sin(PI*X[2]) - PI*PI*cos(PI*X[0])*cos(PI*X[2])*sin(PI*X[1]) - PI*PI*cos(PI*X[0])*cos(PI*X[1])*sin(PI*X[2]);
//     value[2] = 4.0*PI*PI*sin(PI*X[0])*sin(PI*X[1])*sin(PI*X[2]) - PI*PI*cos(PI*X[0])*cos(PI*X[2])*sin(PI*X[1]) - PI*PI*cos(PI*X[0])*cos(PI*X[1])*sin(PI*X[2]);
// }
void rhs(DOUBLE *X, INT dim, DOUBLE *value)
{
    value[0] = 4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[0]) * cos(PI * X[2]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    value[1] = 4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[1]) * cos(PI * X[2]) * sin(PI * X[0]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    value[2] = 4.0 * PI * PI * sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]) - PI * PI * cos(PI * X[1]) * cos(PI * X[2]) * sin(PI * X[0]) - PI * PI * cos(PI * X[0]) * cos(PI * X[2]) * sin(PI * X[1]);
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    // value[0] = 0.0;
    DOUBLE u = rhsvalue[0] + 2.0 * femvalue[3] + femvalue[6] + femvalue[9] + femvalue[13] + femvalue[17];
    DOUBLE v = rhsvalue[1] + femvalue[4] + 2.0 * femvalue[7] + femvalue[10] + femvalue[12] + femvalue[20];
    DOUBLE w = rhsvalue[2] + femvalue[5] + femvalue[8] + 2.0 * femvalue[11] + femvalue[15] + femvalue[19];
    value[0] = u * u + v * v + w * w;
    // value[1] = v * v;
    // value[2] = w * w;
    // for (INT i = 0; i < 3; i++)
    // {
    //     value[i] = value[i] * value[i];
    // value[0] += (rhsvalue[0 + i] + femvalue[3 + i] + femvalue[6 + i] + femvalue[9 + i] - femvalue[0 + i])
    //           * (rhsvalue[0 + i] + femvalue[3 + i] + femvalue[6 + i] + femvalue[9 + i] - femvalue[0 + i]);
    //}
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    for (INT i = 0; i < 3; i++)
    {
        value[i] = 0.0;
        value[i] += (nomalvec[0] * femvalue[0 + i] + nomalvec[1] * femvalue[3 + i] + nomalvec[2] * femvalue[6 + i]);
    }
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);
    // 单元和边界上的积分格式
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral127);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle27);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    //                                0,1,2, 3,4,5, 6,7,8, 9,10,11
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    //                                0,1,2, 3,4,5, 6,7,8, 9,10,11
    INT ind_refine;
    INT max_refine = 4;
    INT numvolu;
    INT *NUMVOLU = malloc(max_refine * sizeof(INT));
    DOUBLE *L2Error = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1Error = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *PostError = malloc(max_refine * sizeof(DOUBLE));

    FEMSPACE *femspace;
    DISCRETEFORM *StiffDiscreteForm;
    MATRIX *StiffMatrix;
    VECTOR *Rhs;
    VECTOR *Solution;
    LINEARSOLVER *solver;
    FEMFUNCTION *femsol;
    MULTIINDEX massmultiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX PosterioriErrorIndex[10] = {D000, D200, D020, D002, D110, D101, D011, D100, D010, D001};
    //                                     0,1,2, 3,4,5, 6,7,8, 9,10,11, 12,13,14, 15,16,17, 18,19,20, 21,22,23, 24,25,26, 27,28,29
    DOUBLE *PosteriorError = NULL;
    DOUBLE TotalError;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("=======%d========\n", ind_refine);
        MPI_Allreduce(&(mesh->num_volu), &numvolu, 1, MPI_INT, MPI_SUM, mesh->comm);
        NUMVOLU[ind_refine] = numvolu;

        // 有限元空间
        // OpenPFEM_Print("Come to do the assembling!");
        femspace = FEMSpaceBuild(mesh, C_T_P5_3D_3D, BoundCond);
        // 刚度矩阵
        StiffDiscreteForm = DiscreteFormBuild(femspace, 4, &stiffLmultiindex[0],
                                              femspace, 4, &stiffRmultiindex[0],
                                              stiffmatrix, rhsvec, BoundFun, Quadrature);
        DOUBLE starttime, endtime;
        starttime = MPI_Wtime();
        StiffMatrix = NULL;
        Rhs = NULL;
        // OpenPFEM_Print("check the assemble!\n");
        MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);
        OpenPFEM_Print("assemble matrix size: %d x %d\n", StiffMatrix->global_nrows, StiffMatrix->global_ncols);
        Solution = NULL;
        VectorCreateByMatrix(&Solution, StiffMatrix);
        // return 0;
        starttime = MPI_Wtime();
        solver = NULL;
        LinearSolverCreate(&solver, PETSC_KSPCG); // PETSC_SUPERLU);
        LinearSolve(solver, StiffMatrix, Rhs, Solution);
        LinearSolverDestroy(&solver);
        // return 0;
        femsol = FEMFunctionBuild(femspace);
        VectorGetFEMFunction(Solution, femsol);
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for solve: %2.10f\n", endtime - starttime);
        // return 0;
        L2Error[ind_refine] = ErrorEstimate(femsol, 1, &massmultiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        // return 0;
        H1Error[ind_refine] = ErrorEstimate(femsol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error[ind_refine], H1Error[ind_refine]);
        // return 0;
        PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        // 残差的计算需要7个微分, 后面的面积分需要3个微分
        PosterioriErrorEstimate(femsol, 7, 3, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        PostError[ind_refine] = TotalError;

        MatrixDestroy(&StiffMatrix);
        VectorDestroy(&Rhs);
        VectorDestroy(&Solution);
        FEMSpaceDestroy(&femspace);
        DiscreteFormDestroy(&StiffDiscreteForm);
        if (ind_refine < max_refine - 1)
        {
            // 一致加密
            MeshUniformRefine(mesh, 1);
            // 自适应加密
            // MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
        }
        free(PosteriorError);
    }
    QuadratureDestroy(&Quadrature);
    MeshDestroy(&mesh);

    OpenPFEM_Print("N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NUMVOLU[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostError[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("L2Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", L2Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("H1Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", H1Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Finalize();
    return 0;
}