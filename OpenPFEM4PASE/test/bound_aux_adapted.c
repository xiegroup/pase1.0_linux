#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"
#include "multilevelmesh.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE AddBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}
void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0];
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]) + right[0] * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]));
    // value[0] = right[0];
}
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 3.0 * PI * PI * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]) + (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]));
    // value[0] = 1.0;
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[1] + femvalue[2] + femvalue[3] - femvalue[0] * femvalue[0] * femvalue[0]) *
               (rhsvalue[0] + femvalue[1] + femvalue[2] + femvalue[3] - femvalue[0] * femvalue[0] * femvalue[0]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}

//考虑方程$-\Delta u + u^3 = f$进行非线性迭代
//这个例子中 将刚度矩阵的线性部分和非线性部分分开计算
//线性部分只进行一次组装 非线性部分在每次迭代时组装
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    BuildMesh(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    // BuildMesh(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    UniformRefineMesh(mesh, 1);
    MeshPartition(mesh);
    UniformRefineMesh(mesh, 1);

    // 积分微分信息
    QUADRATURE *Quadrature = BuildQuadrature(QuadTetrahedral127);
    QUADRATURE *QuadratureEdge = BuildQuadrature(QuadTriangle13);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX auxfemfunmultiIndex[1] = {D000};
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX ErrorIndex[1] = {D000};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    INT NAuxFEMFunMultiIndex[1] = {1};

    // 创建ML结构体
    MULTILEVELMESH *multilevel = NULL;
    MultiLevelMeshCreate(&multilevel);
    MultiLevelMeshSetType(multilevel, PETSc);
    MultiLevelMeshSetLinearMethod(multilevel, CG);

    // 自适应加密
    INT ind_refine;
    INT max_refine = 20;
    INT ind_SCF;
    INT max_SCF = 10;
    INT NUMVOLU;
    INT conv_ind = 0;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *L2ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE Error = 0.0;
    DOUBLE Tol = 1e-12;
    DOUBLE TotalError;
    DOUBLE H1Error = 0.0, L2Error = 0.0;
    FEMSPACE *femspace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL;
    FEMSPACE *addfemspace = NULL;
    AUXFEMFUNCTION *outerauxfemfunc = NULL;
    MATRIX *StiffMatrix = NULL;
    FEMFUNCTION *solution = NULL;
    DOUBLE *PosteriorError = NULL;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        //计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;

        //针对该层网格生成有限元空间和线性部分的离散变分形式
        femspace = BuildFEMSpace(mesh, C_T_P2_3D, BoundCond);
        StiffDiscreteForm = BuildDiscreteForm(femspace, 4, &stiffLmultiindex[0], femspace, 4, &stiffRmultiindex[0],
                                              stiffmatrixlinear, rhsvec, BoundFun, Quadrature);

        //用线性的离散变分形式添加ML层数
        //添加层的过程完成：
        // 1. 给mesh femspace赋值
        // 2. 生成插值矩阵和限制矩阵
        // 3. 组装矩阵(线性部分)
        // 4. 生成solver solver中需要对解的规模进行生成 所以每改变一次有限元空间就需要free之前的solver 生成一次新solver
        // 5. 生成非线性迭代的初始值 第一层网格时为解线性部分的解 之后的网格层时为前一层的解进行插值得到的
        MultiLevelMeshAddLevel(StiffDiscreteForm, NULL, multilevel);
        DiscreteFormDestroy(&StiffDiscreteForm);
        StiffDiscreteForm = NULL;

        //建立非线性部分
        addfemspace = BuildFEMSpace(mesh, C_T_P2_3D, AddBoundCond);
        outerauxfemfunc = BuildAuxFEMFunction(1, (FEMFUNCTION **)&(multilevel->solution), &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
        StiffDiscreteForm = DiscreteFormAuxFeFunBuild(addfemspace, 4, &stiffLmultiindex[0], addfemspace, 4, &stiffLmultiindex[0],
                                                      outerauxfemfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);

        //自洽场迭代
        conv_ind = 0;
        for (ind_SCF = 0; ind_SCF < max_SCF; ind_SCF++)
        {
            //组装非线性部分刚度矩阵
            StiffMatrix = AssembleMatrix(StiffDiscreteForm);

            //求解
            //形式1
            MATRIXaXpbY(1.0, multilevel->Stiff_Matrices[ind_refine], 1.0, StiffMatrix);
            LinearSolve(StiffMatrix, multilevel->linear_solver, CG);
            //形式2
            // AuxLinearSolve(multilevel->Stiff_Matrices[ind_refine], 1, &StiffMatrix, multilevel->linear_solver, CG);
            MatrixDestory(StiffMatrix);
            StiffMatrix = NULL;

            //取出解并判断前后两步自洽场迭代的结果是否足够相近 相近退出自洽场迭代 否则继续
            LinearSolverGetFEMVECSolution(multilevel->linear_solver, multilevel->femspace, &solution);
            FEMVECaXpbY(1.0, solution, -1.0, multilevel->solution);
            Error = ErrorEstimate(multilevel->solution, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
            OpenPFEM_Print("difference of solution is %2.14f.\n", Error);
            
            FreeFEMFuntion(multilevel->solution);
            multilevel->solution = solution;
            solution = NULL;
            if (Error < Tol)
            {
                conv_ind = 1;
                OpenPFEM_Print("自洽场迭代次数为: %d\n", ind_SCF);
                ind_SCF = max_SCF + 1;
            }
        }
        if (conv_ind == 0)
        {
            OpenPFEM_Print("自适应迭代未收敛！\n");
            return 0;
        }

        DiscreteFormDestroy(&StiffDiscreteForm);
        FreeFEMSpace(addfemspace);

        H1Error = 0.0;
        L2Error = 0.0;
        L2ErrorOfBisection[ind_refine] = ErrorEstimate((FEMFUNCTION *)(multilevel->solution), 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        H1ErrorOfBisection[ind_refine] = ErrorEstimate((FEMFUNCTION *)(multilevel->solution), 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        OpenPFEM_Print("L2误差为%2.14f  H1误差为%2.14f\n", L2ErrorOfBisection[ind_refine], H1ErrorOfBisection[ind_refine]);
        //后验误差估计
        PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        PosterioriErrorEstimate((FEMFUNCTION *)(multilevel->solution), 4, 3, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        PostErrorOfBisection[ind_refine] = TotalError;
        //网格加密
        if (ind_refine < max_refine - 1)
        {
            OpenPFEM_Print("Start Adaptive refining the mesh!\n");
            MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
            OpenPFEM_Print("完成自适应加密\n");
        }
        OpenPFEM_Free(PosteriorError);
        // UniformRefineMesh(mesh, 1);
    }

    MultiLevelMeshDestory(&multilevel, multilevel->linear_solver->MVOps);

    //输出每次自适应加密的结果

    OpenPFEM_Print("单元个数 N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("L2Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", L2ErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("H1Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", H1ErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");

    OpenPFEM_Finalize();
    return 0;
}