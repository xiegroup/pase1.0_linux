#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

DOUBLE C = 1.0;
BOOL if_prolong = 0;

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 7)
    {
        return ROBIN; // DIRICHLET;
    }
    else if (bdid > 1)
    {
        return NEUMANN; // DIRICHLET;
    }
    else if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[2], int dim, double *values)
{
    values[0] = 0.0;
    values[1] = 0.0;
    values[0] = cos(PI * X[0]) * cos(PI * X[1]);
    values[1] = cos(PI * X[0]) * cos(PI * X[1]);
}
void NeumannBoundFun(double X[2], int dim, double *values)
{
    // printf("%2.10f\n",X[0]);
    if (X[0] < 1e-12)
    {
        values[0] = -PI * cos(PI * X[0]) * sin(PI * X[1]);
        values[1] = -PI * cos(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f< 1e-12 %2.10f value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[0] > (1 - 1e-8))
    {
        values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]);
        values[1] = PI * cos(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f> 1 - 1e-12 %2.10f value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[1] < 1e-12)
    {
        values[0] = -PI * sin(PI * X[0]) * cos(PI * X[1]);
        values[1] = -PI * sin(PI * X[0]) * cos(PI * X[1]);
        // printf("x %2.10f %2.10f< 1e-12 value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[1] > (1 - 1e-8))
    {
        values[0] = PI * sin(PI * X[0]) * cos(PI * X[1]);
        values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]);
        // printf("x %2.10f %2.10f> 1 - 1e-12 value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    values[0] = 0.0;
    values[1] = 0.0;
}
void RobinBoundFun(double X[2], int dim, double *values)
{
    // printf("%2.10f\n",X[0]);
    values[0] = C;
    if (X[0] < 1e-12)
    {
        values[1] = -PI * cos(PI * X[0]) * sin(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        values[2] = -PI * cos(PI * X[0]) * sin(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f< 1e-12 %2.10f value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[0] > (1 - 1e-8))
    {
        values[1] = PI * cos(PI * X[0]) * sin(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        values[2] = PI * cos(PI * X[0]) * sin(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f> 1 - 1e-12 %2.10f value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[1] < 1e-12)
    {
        values[1] = -PI * sin(PI * X[0]) * cos(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        values[2] = -PI * sin(PI * X[0]) * cos(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f %2.10f< 1e-12 value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    if (X[1] > (1 - 1e-8))
    {
        values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        values[2] = PI * sin(PI * X[0]) * cos(PI * X[1]) + C * sin(PI * X[0]) * sin(PI * X[1]);
        // printf("x %2.10f %2.10f> 1 - 1e-12 value %2.10f %2.10f\n", X[0], X[1], values[0], values[1]);
    }
    // values[0] = 0.0;
    // values[1] = 0.0;
}
void ExactSolu2D(double X[2], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]);
    values[1] = sin(PI * X[0]) * sin(PI * X[1]);
    values[0] = cos(PI * X[0]) * cos(PI * X[1]);
    values[1] = cos(PI * X[0]) * cos(PI * X[1]);
    // values[0] = X[0] * (1 - X[0]) * X[1] * (1 - X[1]);
    // values[1] = X[0] * (1 - X[0]) * X[1] * (1 - X[1]);
}
void ExactGrad2D(double X[2], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]); // D_xu_0
    values[1] = PI * cos(PI * X[0]) * sin(PI * X[1]); // D_xu_1
    values[2] = PI * sin(PI * X[0]) * cos(PI * X[1]); // D_yu_0
    values[3] = PI * sin(PI * X[0]) * cos(PI * X[1]); // D_yu_1
    values[0] = -PI * sin(PI * X[0]) * cos(PI * X[1]); // D_xu_0
    values[1] = -PI * sin(PI * X[0]) * cos(PI * X[1]); // D_xu_1
    values[2] = -PI * cos(PI * X[0]) * sin(PI * X[1]); // D_yu_0
    values[3] = -PI * cos(PI * X[0]) * sin(PI * X[1]); // D_yu_1
    // values[0] = (1 - 2 * X[0]) * X[1] * (1 - X[1]);
    // values[1] = (1 - 2 * X[0]) * X[1] * (1 - X[1]);
    // values[2] = X[0] * (1 - X[0]) * (1 - 2 * X[1]);
    // values[3] = X[0] * (1 - X[0]) * (1 - 2 * X[1]);
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = 0.0;
    INT i = 0;
    for (i = 0; i < 6; i++)
        value[0] += left[i] * right[i];
    // value[0] += (left[2] + left[5]) * (right[2] + right[5]);
}
void rhsvec(DOUBLE *right, DOUBLE *X, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // value[0] = right[0] * ((3.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1])) + right[1] * ((3.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]));
    value[0] = right[0] * ((2.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1])) + right[1] * ((2.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]));
    // value[0] = right[0] * ((3.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]) - PI * PI * sin(PI * X[0]) * sin(PI * X[1])) + right[1] * ((3.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]) - PI * PI * sin(PI * X[0]) * sin(PI * X[1]));
    value[0] = right[0] * ((2.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1])) + right[1] * ((2.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]));
    // value[0] = 2.0*PI * PI * (right[0] * sin(PI * X[0]) * sin(PI * X[1])
    //+ right[1] * sin(PI * X[0]) * sin(PI * X[1]));
    // value[0] = right[0] * (4 * X[1] * (1 - X[1]) + 2 * X[0] * (1 - X[0]) - (1 - 2 * X[0]) * (1 - 2 * X[1]))
    //+ right[1] * (2 * X[1] * (1 - X[1]) + 4 * X[0] * (1 - X[0]) - (1 - 2 * X[0]) * (1 - 2 * X[1]));
}
void ErrFun2DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 2; i++)
    {
        // OpenPFEM_Print("fem: %2.10f, fun: %2.10f\n",femvale[i],Funvalue[i]);
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
    }
}
void ErrFun2DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 4; i++)
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
}

void rhs(DOUBLE *X, INT dim, DOUBLE *value)
{
    // value[0] = (3.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]);
    // value[1] = (3.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]) - PI * PI * cos(PI * X[0]) * cos(PI * X[1]);
    // value[0] = (3.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]) - PI * PI * sin(PI * X[0]) * sin(PI * X[1]);
    // value[1] = (3.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]) - PI * PI * sin(PI * X[0]) * sin(PI * X[1]);
    value[0] = (2.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]);
    value[1] = (2.0 * PI * PI + 1) * sin(PI * X[0]) * sin(PI * X[1]);
    value[0] = (2.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]);
    value[1] = (2.0 * PI * PI + 1) * cos(PI * X[0]) * cos(PI * X[1]);
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    // value[0] = 0.0;
    // DOUBLE u = rhsvalue[0] + 2.0 * femvalue[2] + femvalue[4] + femvalue[7] - femvalue[0];
    // DOUBLE v = rhsvalue[1] + femvalue[3] + 2.0 * femvalue[5] + femvalue[6] - femvalue[1];
    DOUBLE u = rhsvalue[0] + femvalue[2] + femvalue[4] - femvalue[0];
    DOUBLE v = rhsvalue[1] + femvalue[3] + femvalue[5] - femvalue[1];
    value[0] = u * u + v * v;
    // value[1] = v*v;
    // value[2] = w*w;
    // for (INT i = 0; i < 3; i++)
    // {
    //     value[i] = value[i] * value[i];
    // value[0] += (rhsvalue[0 + i] + femvalue[3 + i] + femvalue[6 + i] + femvalue[9 + i] - femvalue[0 + i])
    //           * (rhsvalue[0 + i] + femvalue[3 + i] + femvalue[6 + i] + femvalue[9 + i] - femvalue[0 + i]);
    //}
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    for (INT i = 0; i < 2; i++)
    {
        value[i] = 0.0;
        value[i] += (nomalvec[0] * femvalue[0 + i] + nomalvec[1] * femvalue[2 + i]);
    }
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 2, DEFAULT_COMM);
    MeshBuild(mesh, "../data/data_simple.txt", MATLAB, TRIANGLE);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);
    // 单元和边界上的积分格式
    QUADRATURE *Quadrature = QuadratureBuild(QuadTriangle36);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadLine16);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[3] = {D00, D10, D01};
    MULTIINDEX stiffRmultiindex[3] = {D00, D10, D01};
    MULTIINDEX PosterioriErrorIndex[6] = {D00, D20, D02, D11, D10, D10};
    //                                   0 1   2 3  4 5  6 7  8 9  10 11
    INT ind_refine;
    INT max_refine = 4;
    INT numface;
    INT *NUMFACE = malloc(max_refine * sizeof(INT));
    DOUBLE *L2Error = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1Error = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *PostError = malloc(max_refine * sizeof(DOUBLE));
    FEMSPACE *femspace;
    DISCRETEFORM *StiffDiscreteForm;
    MATRIX *StiffMatrix;
    VECTOR *Rhs;
    VECTOR *Solution;
    LINEARSOLVER *solver;
    FEMFUNCTION *femsol;
    MULTIINDEX massmultiindex[1] = {D00};
    MULTIINDEX H1ErrorIndex[2] = {D10, D01};
    DOUBLE *PosteriorError = NULL;
    DOUBLE TotalError;
    FEMSPACE *new_femspace;
    MATRIX *ProlongMatrix;
    VECTOR *Solution_H;

    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("=======%d========\n", ind_refine);
        MPI_Allreduce(&(mesh->num_face), &numface, 1, MPI_INT, MPI_SUM, mesh->comm);
        NUMFACE[ind_refine] = numface;
        // 有限元空间
        OpenPFEM_Print("Come to do the assembling!");
        femspace = FEMSpaceBuild(mesh, C_T_P3_2D_2D, BoundCond);
        if (if_prolong == 1)
        {
            new_femspace = FEMSpaceBuild(mesh, C_T_P1_2D_2D, BoundCond);
            ProlongMatrixAssemble(&ProlongMatrix, new_femspace, femspace, TYPE_PETSC);
        }
        // 刚度矩阵
        StiffDiscreteForm = DiscreteFormBuild(femspace, 3, &stiffLmultiindex[0],
                                              femspace, 3, &stiffRmultiindex[0],
                                              stiffmatrix, rhsvec, BoundFun, Quadrature);
        // AddBoundFun(StiffDiscreteForm, NeumannBoundFun, NEUMANN, QuadratureEdge);
        // AddBoundFun(StiffDiscreteForm, RobinBoundFun, ROBIN, QuadratureEdge);
        DOUBLE starttime, endtime;
        starttime = MPI_Wtime();
        StiffMatrix = NULL;
        Rhs = NULL;
        OpenPFEM_Print("check the assemble!\n");
        MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
        // MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_OPENPFEM);//TYPE_PETSC);
        // MatrixPrint(StiffMatrix, 0);
        // MPI_Barrier(MPI_COMM_WORLD);
        // MatrixPrint(StiffMatrix, 1);
        // MPI_Barrier(MPI_COMM_WORLD);
        // return 0;
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);
        OpenPFEM_Print("assemble matrix size: %d x %d\n", StiffMatrix->global_nrows, StiffMatrix->global_ncols);
        Solution = NULL;
        VectorCreateByMatrix(&Solution, StiffMatrix);
        // return 0;
        starttime = MPI_Wtime();
        solver = NULL;
        LinearSolverCreate(&solver, PETSC_KSPCG); // PETSC_SUPERLU
        LinearSolve(solver, StiffMatrix, Rhs, Solution);
        LinearSolverDestroy(&solver);
        if (if_prolong == 1)
        {
            VectorCreateByMatrixTranspose(&Solution_H, ProlongMatrix);
            MatrixTransposeVectorMult(ProlongMatrix, Solution, Solution_H);
            femsol = FEMFunctionBuild(new_femspace);
            VectorGetFEMFunction(Solution_H, femsol);
        }
        else
        {
            // return 0;
            femsol = FEMFunctionBuild(femspace);
            VectorGetFEMFunction(Solution, femsol);
        }
        // FEMFunctionPrint(femsol);
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for solve: %2.10f\n", endtime - starttime);
        // return 0;
        L2Error[ind_refine] = ErrorEstimate(femsol, 1, &massmultiindex[0], ExactSolu2D, ErrFun2DL2, Quadrature);
        // return 0;
        H1Error[ind_refine] = ErrorEstimate(femsol, 2, &H1ErrorIndex[0], ExactGrad2D, ErrFun2DH1, Quadrature);
        OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error[ind_refine], H1Error[ind_refine]);
        PosteriorError = malloc(mesh->num_face * sizeof(DOUBLE));
        // 残差的计算需要4个微分, 后面的面积分需要2个微分
        PosterioriErrorEstimate(femsol, 4, 2, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        PostError[ind_refine] = TotalError;
        // return 0;
        MatrixDestroy(&StiffMatrix);
        VectorDestroy(&Rhs);
        VectorDestroy(&Solution);
        FEMSpaceDestroy(&femspace);
        DiscreteFormDestroy(&StiffDiscreteForm);
        if (ind_refine < max_refine - 1)
            MeshUniformRefine(mesh, 1);
    }
    QuadratureDestroy(&Quadrature);
    MeshDestroy(&mesh);

    OpenPFEM_Print("N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NUMFACE[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostError[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("L2Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", L2Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("H1Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", H1Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Finalize();
    return 0;
}