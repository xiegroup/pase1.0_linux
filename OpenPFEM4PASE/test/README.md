# 测试及用例说明

### `LShape_adapted.c`

在 3 维 L 型区域通过自适应加密, 求解

$$
-\Delta u = 1
$$

求解线性方程组的时候使用了多重网格的方法.

---



`matrix_test.c` 已修改，可以作为 **刚度矩阵组装** 和 **线性求解** 参考

`Eigen3D.c` 已修改，可以作为 **特征值求解** 参考

`eigen_aux.c` 已修改，可以作为 **重新组装刚度矩阵** ， **矩阵axpby** **有限元函数计算** 参考

`mlsolver_test.c` 已修改，可以作为 **multilevel结构体**， **multilevel线性解法器** 参考

`eigen_distributed.c` 已修改

`eigen_aux_adapted.c`已修改, 可以作为 **prolong矩阵插值** 参考, 使用 OpenPFEM_Finalize() 会不稳定

`solver_test.c` 作为测试例子随时改动中
  