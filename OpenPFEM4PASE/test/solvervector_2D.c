#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return NEUMANN;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[2], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu2D(double X[2], int dim, double *values)
{
    values[0] = cos(PI * X[0]) * cos(PI * X[1]);
    values[1] = cos(PI * X[0]) * cos(PI * X[1]);
}
void ExactGrad2D(double X[2], int dim, double *values)
{
    values[0] = -PI * cos(PI * X[1]) * sin(PI * X[0]);
    values[1] = -PI * cos(PI * X[1]) * sin(PI * X[0]);
    values[2] = -PI * cos(PI * X[0]) * sin(PI * X[1]);
    values[3] = -PI * cos(PI * X[0]) * sin(PI * X[1]);

}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    //-|delta u + u
    value[0] = 0.0;
    //INT i;
    //for (i = 0; i < 6; i++)
    //    value[0] += left[i] * right[i];
    //D00, D10, D01;   D10,  D01, D00
    //[0,1, 2,3, 4,5]; [0,1, 2,3, 4,5]
    value[0] = left[0]*right[4]+left[1]*right[5]+left[2]*right[0]+left[3]*right[1]+left[4]*right[2]+left[5]*right[3];
}
void rhsvec(DOUBLE *right, DOUBLE *X, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (1.0 + 2.0 * PI * PI) * right[4] * cos(PI * X[0]) * cos(PI * X[1])  +
                (1.0 + 2.0 * PI * PI) * right[5] * cos(PI * X[0]) * cos(PI * X[1]);
    //value[0] = PI * PI * (right[0] * (3.0 * cos(PI * X[0]) * cos(PI * X[1]) - sin(PI * X[0]) * sin(PI * X[1])) 
    //+ right[1] * (3.0 * cos(PI * X[0]) * cos(PI * X[1]) - sin(PI * X[0]) * sin(PI * X[1])));
}
void ErrFun2DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    INT i;
    for (i = 0; i < 2; i++)
    {
        //OpenPFEM_Print("fem: %2.10f, fun: %2.10f\n",femvale[i],Funvalue[i]);
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
    }
}
void ErrFun2DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 4; i++)
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 2.0 * PI * PI * sin(PI * coord[0]) * sin(PI * coord[1]);
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 2, DEFAULT_COMM);
    MeshBuild(mesh, "../data/data_simple.txt", MATLAB, TRIANGLE);
    MeshUniformRefine(mesh, 3);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);
    // 单元和边界上的积分格式
    QUADRATURE *Quadrature = QuadratureBuild(QuadTriangle36);
    // 左右有限元用到的导数信息
    MULTIINDEX stiffLmultiindex[3] = {D00, D10, D01};
    MULTIINDEX stiffRmultiindex[3] = {D10, D01, D00};
    INT ind_refine;
    INT max_refine = 3;
    INT numface;
    INT *NUMFACE = malloc(max_refine * sizeof(INT));
    DOUBLE *L2Error = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1Error = malloc(max_refine * sizeof(DOUBLE));
    FEMSPACE *femspace;
    DISCRETEFORM *StiffDiscreteForm;
    MATRIX *StiffMatrix;
    VECTOR *Rhs;
    VECTOR *Solution;
    LINEARSOLVER *solver;
    FEMFUNCTION *femsol;
    MULTIINDEX massmultiindex[1] = {D00};
    MULTIINDEX H1ErrorIndex[2] = {D10, D01};
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("=======%d========\n", ind_refine);
        MPI_Allreduce(&(mesh->num_face), &numface, 1, MPI_INT, MPI_SUM, mesh->comm);
        NUMFACE[ind_refine] = numface;

        // 有限元空间
        OpenPFEM_Print("Come to do the assembling!");
        femspace = FEMSpaceBuild(mesh, C_T_P2_2D_2D, BoundCond);
        // 刚度矩阵
        StiffDiscreteForm = DiscreteFormBuild(femspace, 3, &stiffLmultiindex[0],
                                              femspace, 3, &stiffRmultiindex[0],
                                              stiffmatrix, rhsvec, BoundFun, Quadrature);
        DOUBLE starttime, endtime;
        starttime = MPI_Wtime();
        StiffMatrix = NULL;
        Rhs = NULL;
        OpenPFEM_Print("check the assemble!\n");
        MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for assemble matrix: %2.10f\n", endtime - starttime);
        OpenPFEM_Print("assemble matrix size: %d x %d\n", StiffMatrix->global_nrows, StiffMatrix->global_ncols);
        Solution = NULL;
        VectorCreateByMatrix(&Solution, StiffMatrix);
        // return 0;

        starttime = MPI_Wtime();
        solver = NULL;
        LinearSolverCreate(&solver, PETSC_KSPCG); // PETSC_SUPERLU);
        LinearSolve(solver, StiffMatrix, Rhs, Solution);
        LinearSolverDestroy(&solver);
        // return 0;
        femsol = FEMFunctionBuild(femspace);
        VectorGetFEMFunction(Solution, femsol);
        endtime = MPI_Wtime();
        OpenPFEM_Print("The time for solve: %2.10f\n", endtime - starttime);
        // return 0;
        L2Error[ind_refine] = ErrorEstimate(femsol, 1, &massmultiindex[0], ExactSolu2D, ErrFun2DL2, Quadrature);
        // return 0;
        H1Error[ind_refine] = ErrorEstimate(femsol, 2, &H1ErrorIndex[0], ExactGrad2D, ErrFun2DH1, Quadrature);
        OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error[ind_refine], H1Error[ind_refine]);
        // return 0;
        MatrixDestroy(&StiffMatrix);
        VectorDestroy(&Rhs);
        VectorDestroy(&Solution);
        FEMSpaceDestroy(&femspace);
        DiscreteFormDestroy(&StiffDiscreteForm);
        if (ind_refine < max_refine - 1)
            MeshUniformRefine(mesh, 1);
    }
    QuadratureDestroy(&Quadrature);
    MeshDestroy(&mesh);

    OpenPFEM_Print("N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NUMFACE[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    // OpenPFEM_Print("PostErr = [ ");
    // for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    // {
    //     OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind_refine]);
    // }
    // OpenPFEM_Print("];\n");
    OpenPFEM_Print("L2Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", L2Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("H1Err = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", H1Error[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Finalize();
    return 0;
}